<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "Program";
$route['404_override'] = '';

$route['register'] = "program/register";
$route['login'] = "program/login";
$route['membership'] = "program/membership";
$route['tentang-kami'] = "program/aboutus";
$route['faq'] = "program/faq";
$route['cara-kerja'] = "program/cara_kerja";
$route['kebijakan-privasi']= "program/privacy_policy";
$route['syarat-dan-ketentuan']= "program/tnk";
$route['info-iklan']= "program/ads_program";
$route['individual-directory'] = "program/user_category";
$route['vendor-directory'] = "vendor/vendor";
$route['vendor-directory/(:num)'] = "vendor/vendor";
$route['vendor/view/(:num)/(:any)'] = "vendor/vendor/view/$1/$2";
$route['individual-directory'] = "expert/expert";
$route['individual-directory/(:num)'] = "expert/expert";
$route['expert/view/(:num)/(:any)'] = "expert/expert/view/$1/$2";
$route['myproject'] = "project/my_project";
$route['my-venue']="venue/my_venue";
$route['venue_directory']="venue/venue/venue_directory";
$route['venue/by-province/(:any)/(:num)']="venue/venue/by_province/$2";
$route['venue/view/(:num)/(:any)']="venue/venue/view/$1";
$route['venue/(:any)-(:any)/(:num)']="venue/venue/by_city/$3";
$route['artikel'] = "program/artikel";
$route['prnewswire']= "program/prnewswire";
$route['project/view/(:any)'] = "project/project/view_project/$1";
$route['profile/view/(:num)'] = "profile/profile/view_profile/$1";
$route['profile/([a-z-]+)/(\d+)'] = "profile/profile/view_profile/$2";
$route['project/category'] = "project/project/browse_category";
$route['project/featured'] = "project/project/featured_project";
$route['project/featured/(:num)'] = "project/project/featured_project";
$route['project/urgent'] = "project/project/urgent_project";
$route['project/urgent/(:num)'] = "project/project/urgent_project";
$route['project/my_bidding'] = "project/my_project/my_bidding";
$route['project/my_bidding/(:num)'] = "project/my_project/my_bidding";
$route['project/latest'] = "project/project/latest_project";
$route['project/latest/(:num)'] = "project/project/latest_project";
$route['project/percategory/(:num)'] = "project/project/percategory_project/$1";
$route['profile/notification'] = "profile/profile/notification_list";
$route['post/my_post'] = "post/my_post/index";
$route['event/view/(:num)/(:any)'] = "event/event/view_event/$1/$2";
$route['events']= "event/event/latest_event";
$route['events/(:num)']="event/event/latest_event";
$route['event/latest/(:num)'] = "event/event/latest_event";
$route['event/my_event'] = "event/my_event/my_event_list";
$route['event/my_event/(:num)'] = "event/my_event/my_event_list";
$route['event/featured'] = "event/event/featured_event";
$route['event/featured/(:num)'] = "post/post/featured_event";
$route['blog'] = "blog/blog/index";
$route['blog/(:num)'] = "blog/blog/index";
$route['blog/read/(:num)/(:any)'] = "blog/blog/read/$1/$2";
$route['jobs'] = "job/jobs";
$route['jobs/(:num)'] = "job/jobs/index/$1";
$route['term-and-condition'] = "program/term_and_condition";
$route['cari']= "program/search_main";
$route['sitemap.xml']= "sitemap/index";
/* End of file routes.php */
/* Location: ./application/config/routes.php */