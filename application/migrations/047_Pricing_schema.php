<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Pricing_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'pricing_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'pricing_info' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			),
			'pricing_price' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'pricing_words' => array(
				'type' => 'TEXT'
			),
			'pricing_for' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));

		$this->dbforge->add_key('pricing_id',TRUE);
		$this->dbforge->create_table('pricing');

	}

	public function down()
	{
		$this->dbforge->drop_table('pricing');
	}
}