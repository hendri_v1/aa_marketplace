<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event10_schema extends CI_Migration {
	
	public function up()
	{
		//table event_audience
		$fields2=array(
			'added_at' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'updated_at'=>array(
				'type' => 'INT',
				'constraint' => 11
			),
			'deleted_at' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('events',$fields2);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('events', 'added_at');
		$this->dbforge->drop_column('events', 'updated_at');
		$this->dbforge->drop_column('events',' deleted_at');
	}
}