<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue4_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'venue_owner' => array(
				'type' => 'INT'
				
			)
			
		);
		$this->dbforge->add_column('venues',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('venues', 'venue_owner');
	}
}