<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Post_owner_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'post_owner' => array(
				'type' => 'INT',
				'constraint' => 11
			)
			
		);
		$this->dbforge->add_column('posts',$fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('posts', 'post_owner');
	}
}