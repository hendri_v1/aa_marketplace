<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event9_schema extends CI_Migration {
	
	public function up()
	{
		//table event_audience
		$fields2=array(
			'is_draft' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('events',$fields2);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('events', 'is_draft');
	}
}