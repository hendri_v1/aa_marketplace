<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue7_schema extends CI_Migration {
	
	public function up()
	{
		//add new main venue table
		$this->dbforge->add_field(array(
			'venue_main_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'venue_main_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			),
			'venue_main_description' => array(
				'type' => 'TEXT'
			),
			'venue_main_address'=> array(
				'type' => 'TEXT'
			),
			'city_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_main_status' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('venue_main_id', TRUE);
		$this->dbforge->create_table('venue_main');

		//add billing history table, to record history of each payment
		$this->dbforge->add_field(array(
			'billing_history_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'billing_history_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'billing_history_info' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			)
		));
		$this->dbforge->add_key('billing_history_id',TRUE);
		$this->dbforge->create_table('billing_history');

		//add venue_main_id
		//table event_audience
		$fields2=array(
			'venue_main_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('venues',$fields2);

	}

	public function down()
	{
		$this->dbforge->drop_table('venue_main');
		$this->dbforge->drop_table('billing_history');
		$this->dbforge->drop_column('venues', 'venue_main_id');
	}
}