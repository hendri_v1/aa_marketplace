<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue8_schema extends CI_Migration {
	
	public function up()
	{
		$fields2=array(
			'venue_main_geo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'venue_main_image' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'venue_main_owner' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('venue_main',$fields2);

	}

	public function down()
	{
		$this->dbforge->drop_column('venue_main', 'venue_main_geo');
		$this->dbforge->drop_column('venue_main', 'venue_main_image');
		$this->dbforge->drop_column('venue_main', 'venue_main_owner');
	}
}