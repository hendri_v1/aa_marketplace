<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Project5_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'project_phase' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_visibility' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'is_pending' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'is_paid' => array(
				'type' => 'INT',
				'constraint' => 11
			)
			
		);
		$this->dbforge->add_column('projects',$fields);

		$this->dbforge->add_field(array(
			'billing_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'billing_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'billing_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'billing_for' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'billing_due' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'billing_status' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('billing_id', TRUE);
		$this->dbforge->create_table('billing');

		$this->dbforge->add_field(array(
			'billingdetail_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'billingdetail_info' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			),
			'billing_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'billingdetail_total' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('billingdetail_id', TRUE);
		$this->dbforge->create_table('billingdetail');

		$this->dbforge->add_field(array(
			'payment_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'payment_date' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			),
			'billing_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'payment_total' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'payment_method' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'payment_notes' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			)
		));
 		$this->dbforge->add_key('payment_id', TRUE);
		$this->dbforge->create_table('payment');

	}

	public function down()
	{
		$this->dbforge->drop_column('projects', 'project_phase');
		$this->dbforge->drop_column('projects', 'project_visibility');
		$this->dbforge->drop_column('projects', 'is_pending');
		$this->dbforge->drop_column('projects', 'is_paid');
		$this->dbforge->drop_table('billing');
		$this->dbforge->drop_table('billingdetail');
		$this->dbforge->drop_table('payment');
	}
}