<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event7_schema extends CI_Migration {
	
	public function up()
	{
		//table event_audience
		$fields2=array(
			'event_audience_status' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('event_audience',$fields2);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('event_audience', 'event_audience_status');
	}
}