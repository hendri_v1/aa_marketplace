<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Post_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'post_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'post_title' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'post_title_slug' => array(
				'type' => 'VARCHAR',
				'constraint' => 120
			),
			'post_type_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'post_content' => array(
				'type' => 'TEXT'
			),
			'post_addedat' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'post_updateat' => array(
				'type' => 'INT',
				'constraint' => 11
			)

		));
 		$this->dbforge->add_key('post_id', TRUE);
		$this->dbforge->create_table('posts');
		
		$this->dbforge->add_field(array(
			'post_type_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'post_type_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 50
			)
		));
 		$this->dbforge->add_key('post_type_id', TRUE);
		$this->dbforge->create_table('post_type');

		$this->dbforge->add_field(array(
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'event_title' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'event_title_slug' => array(
				'type' => 'VARCHAR',
				'constraint' => 120
			),
			'event_description' => array(
				'type' => 'TEXT'
			),
			'event_start_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_end_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_picture' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			),
			'event_owner' => array(
				'type' => 'INT',
				'constraint' => 11
			)

		));
 		$this->dbforge->add_key('event_id', TRUE);
		$this->dbforge->create_table('events');

	}

	public function down()
	{
		$this->dbforge->drop_table('posts');
		$this->dbforge->drop_table('post_type');
	}
}