<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue13_schema extends CI_Migration {
	
	public function up()
	{
		$fields2=array(
			'venue_width' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_height' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_length' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_u_shape' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_boardroom' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_classroom' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_reception' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_banquet' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_theatre' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_auditorium' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_circle' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('venues',$fields2);

	}

	public function down()
	{
		$this->dbforge->drop_column('venues', 'venue_width');
		$this->dbforge->drop_column('venues', 'venue_height');
		$this->dbforge->drop_column('venues', 'venue_length');
		$this->dbforge->drop_column('venues', 'venue_u_shape');
		$this->dbforge->drop_column('venues', 'venue_boardroom');
		$this->dbforge->drop_column('venues', 'venue_classroom');
		$this->dbforge->drop_column('venues', 'venue_reception');
		$this->dbforge->drop_column('venues', 'venue_banquet');
		$this->dbforge->drop_column('venues', 'venue_theatre');
	}
}