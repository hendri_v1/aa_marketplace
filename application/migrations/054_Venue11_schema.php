<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue11_schema extends CI_Migration {
	
	public function up()
	{
		$fields2=array(
			'venue_booking_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('venue_booking_detail',$fields2);

	}

	public function down()
	{
		$this->dbforge->drop_column('venue_booking_detail', 'venue_booking_id');
	}
}