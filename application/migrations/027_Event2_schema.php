<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event2_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'is_pending' => array(
				'type' => 'INT',
				'constraint' => 11
			)
			
		);
 		$this->dbforge->add_column('events',$fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('events', 'is_pending');
	}
}