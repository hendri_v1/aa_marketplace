<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_User2_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'userprofile_photo' => array(
				'type' => 'VARCHAR',
				'constraint' => 30,
			),
			'userprofile_viewed' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'userprofile_city' => array(
				'type' => 'VARCHAR',
				'constraint' => 30
			)
		);
		$this->dbforge->add_column('userprofile',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('userprofile', 'userprofile_photo');
		$this->dbforge->drop_column('userprofile', 'userprofile_viewed');
		$this->dbforge->drop_column('userprofile', 'userprofile_city');
	}
}