<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Location_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'province_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'province_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 50
			)
		));
 		$this->dbforge->add_key('province_id', TRUE);
		$this->dbforge->create_table('province');
		
		$this->dbforge->add_field(array(
			'city_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'city_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 50
			),
			'province_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('city_id', TRUE);
		$this->dbforge->create_table('city');

		$fields=array(
			'city_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
			
		);
		$this->dbforge->add_column('userprofile',$fields);
	}

	public function down()
	{
		$this->dbforge->drop_table('city');
		$this->dbforge->drop_table('province');
		$this->dbforge->drop_column('userprofile', 'city_id');
	}
}