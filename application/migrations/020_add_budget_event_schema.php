<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_budget_event_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'budget_event' => array(
				'type' => 'INT',
				'constraint' => 11
			)
			
		);
		$this->dbforge->add_column('events',$fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('events', 'budget_event');
	}
}