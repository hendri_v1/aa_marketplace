<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Category_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'category_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'category_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'category_for' => array(
				'type' => 'INT',
				'constraint' => 11
			)

		));


 		$this->dbforge->add_key('category_id', TRUE);
		$this->dbforge->create_table('category');

		$this->dbforge->add_field(array(
			'category_connector_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'category_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'category_connector_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)

		));

		
 		$this->dbforge->add_key('category_connector_id', TRUE);
		$this->dbforge->create_table('category_connector');
		
		

	}

	public function down()
	{
		$this->dbforge->drop_table('category');
		$this->dbforge->drop_table('category_connector');
	}
}