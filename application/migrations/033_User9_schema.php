<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_User9_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'userprofile_parentid' => array(
				'type' => 'INT',
				'constraint' => 11,
			)
			
		);
		$this->dbforge->add_column('userprofile',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('userprofile', 'userprofile_parentid');
	}
}