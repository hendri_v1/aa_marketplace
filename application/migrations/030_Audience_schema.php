<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Audience_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'audience_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('audience_id', TRUE);
		$this->dbforge->create_table('event_audience');

	}

	public function down()
	{
		$this->dbforge->drop_table('event_audience');
	}
}