<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue2_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'venue_gallery_hash' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
			)
			
		);
		$this->dbforge->add_column('venue_galleries',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('venue_galleries', 'venue_gallery_hash');
	}
}