<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Reset_password_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'reset_password' => array(
				'type' => 'VARCHAR',
				'constraint' => 30,
				 'null' => TRUE
			)
			
		);
		$this->dbforge->add_column('user',$fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('user', 'reset_password');
	}
}