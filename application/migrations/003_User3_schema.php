<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_User3_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'userprofile_address' => array(
				'type' => 'VARCHAR',
				'constraint' => 150,
			),
			'userprofile_aboutme' => array(
				'type' => 'TEXT'
			),
			'userprofile_video' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			)
		);
		$this->dbforge->add_column('userprofile',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('userprofile', 'userprofile_address');
		$this->dbforge->drop_column('userprofile', 'userprofile_aboutme');
		$this->dbforge->drop_column('userprofile', 'userprofile_video');
	}
}