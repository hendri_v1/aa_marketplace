<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event6_schema extends CI_Migration {
	
	public function up()
	{
		//table events
		$fields=array(
			'event_totalview' => array(
				'type' => 'INT',
				'constraint' => 11
				
			)
			
		);
		$this->dbforge->add_column('events',$fields);

		//table event_audience
		$fields2=array(
			'eventticket_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('event_audience',$fields2);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('events', 'event_totalview');
		$this->dbforge->drop_column('event_audience', 'eventticket_id');
	}
}