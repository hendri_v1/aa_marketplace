<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue12_schema extends CI_Migration {
	
	public function up()
	{
		$fields2=array(
			'venue_space' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('venues',$fields2);

	}

	public function down()
	{
		$this->dbforge->drop_column('venues', 'venue_space');
	}
}