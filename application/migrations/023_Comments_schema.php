<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Comments_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'comment_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'comment_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'comment_for' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'comment_text' => array(
				'type' => 'TEXT'
			),
			'comment_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'comment_status' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('comment_id', TRUE);
		$this->dbforge->create_table('comments');

	}

	public function down()
	{
		$this->dbforge->drop_table('comments');
	}
}