<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Rating_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'rating_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'rating_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rating_for' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rating_value' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rating_comment' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('rating_id', TRUE);
		$this->dbforge->create_table('rating');

	}

	public function down()
	{
		$this->dbforge->drop_table('rating');
	}
}