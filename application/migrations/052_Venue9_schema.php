<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue9_schema extends CI_Migration {
	
	public function up()
	{
		$fields2=array(
			'venue_main_contact_phone' => array(
				'type' => 'VARCHAR',
				'constraint' => 40
			),
			'venue_main_contact_mobile_phone' => array(
				'type' => 'VARCHAR',
				'constraint' => 40
			),
			
			'venue_main_contact_person' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			)
		);
		$this->dbforge->add_column('venue_main',$fields2);

	}

	public function down()
	{
		$this->dbforge->drop_column('venue_main', 'venue_main_contact_phone');
		$this->dbforge->drop_column('venue_main', 'venue_main_contact_person');
		$this->dbforge->drop_column('venue_main', 'venue_main_contact_mobile_phone');
	}
}