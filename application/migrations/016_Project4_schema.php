<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Project4_schema extends CI_Migration {
	
	public function up()
	{
		$fields = array(
            'project_location' => array(
            	'name' => 'city_id',
                'type' => 'INT',
                'constraint' => 11
            ),
		);

		$this->dbforge->modify_column('projects', $fields);

	}

	public function down()
	{
		$fields = array(
            'city_id' => array(
            	'name' => 'project_location',
                'type' => 'VARCHAR',
                'constraint' => 30
            ),
		);

		$this->dbforge->modify_column('projects', $fields);
	}
}