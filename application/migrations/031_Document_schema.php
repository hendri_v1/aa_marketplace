<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Document_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'document_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'document_title' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			),
			'document_url' => array(
				'type' => 'TEXT'
			)
		));
 		$this->dbforge->add_key('document_id', TRUE);
		$this->dbforge->create_table('documents');

	}

	public function down()
	{
		$this->dbforge->drop_table('documents');
	}
}