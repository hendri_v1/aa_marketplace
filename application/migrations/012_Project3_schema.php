<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Project3_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'project_winner' => array(
				'type' => 'INT',
				'constraint' => 11,
			)			
		);
		$this->dbforge->add_column('projects',$fields);
		
	}
	
	public function down()
	{
		$this->dbforge->drop_column('projects', 'project_winner');
	}
}