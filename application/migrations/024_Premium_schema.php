<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Premium_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'is_premium' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'premium_due' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'is_patnership' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'patnership_due' => array(
				'type' => 'INT',
				'constraint' => 11
			)
			
		);
		$this->dbforge->add_column('user',$fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('user', 'is_premium');
		$this->dbforge->drop_column('user', 'premium_due');
		$this->dbforge->drop_column('user', 'is_patnership');
		$this->dbforge->drop_column('user', 'patnership_due');
	}
}