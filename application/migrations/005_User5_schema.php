<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_User5_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'userextra_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'userprofile_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'userextra_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'userextra_title' => array(
				'type' => 'VARCHAR',
				'constraint' => '150'
			),
			'userextra_description' => array(
				'type' => 'TEXT'
			),
			'userextra_attachment' => array(
				'type' => 'VARCHAR',
				'constraint' => 80
			),
			'userextra_adddate' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'userextra_deleteat' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'userextra_updateat' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'userextra_status' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('userextra_id', TRUE);
		$this->dbforge->create_table('userextra');
	}
	
	public function down()
	{
		$$this->dbforge->drop_table('userextra');
	}
}