<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_User7_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'userprofile_function' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'userprofile_fb' => array(
				'type' => 'VARCHAR',
				'constraint' => 50
			),
			'userprofile_twitter' => array(
				'type' => 'VARCHAR',
				'constraint' => 50
			),
			'userprofile_linkedin' => array(
				'type' => 'VARCHAR',
				'constraint' => 50
			)
			
		);
		$this->dbforge->add_column('userprofile',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('userprofile', 'userprofile_function');
		$this->dbforge->drop_column('userprofile', 'userprofile_fb');
		$this->dbforge->drop_column('userprofile', 'userprofile_twitter');
		$this->dbforge->drop_column('userprofile', 'userprofile_linkedin');
	}
}