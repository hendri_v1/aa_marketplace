<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Project2_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'projectbid_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'project_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'projectbid_user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'projectbid_description' => array(
				'type' => 'TEXT'
			),
			'projectbid_attachment' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			),
			'projectbid_bid' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'projectbid_addedat' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('projectbid_id', TRUE);
		$this->dbforge->create_table('projectbid');
		
	}
	
	public function down()
	{
		$this->dbforge->drop_table('projectbid');
	}
}