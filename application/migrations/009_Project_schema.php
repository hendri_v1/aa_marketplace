<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Project_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'project_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'project_title' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			),
			'project_location' => array(
				'type' => 'VARCHAR',
				'constraint' => 80
			),
			'project_budget' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_end_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_can_bid' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_description' => array(
				'type' => 'TEXT'
			),
			'project_attachment' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'project_isfeatured' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_isurgent' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_ishelp' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_status' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'projectcat_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_totalview' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_addedat' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_updateat' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_deleteat' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('project_id', TRUE);
		$this->dbforge->create_table('projects');
		
		$this->dbforge->add_field(array(
			'projectcat_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'projectcat_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			)
		));
 		$this->dbforge->add_key('projectcat_id', TRUE);
		$this->dbforge->create_table('projectcat');
		
	}
	
	public function down()
	{
		$this->dbforge->drop_table('projects');
		$this->dbforge->drop_table('projectcat');
	}
}