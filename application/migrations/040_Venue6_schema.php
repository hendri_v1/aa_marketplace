<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue6_schema extends CI_Migration {
	
	public function up()
	{
	
		$this->dbforge->add_field(array(
			'venue_facility_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'venue_facility_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			),
			'venue_facility_type' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('venue_facility_id', TRUE);
		$this->dbforge->create_table('venue_facilities');

		$this->dbforge->add_field(array(
			'venue_facility_connector_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'venue_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_facility_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)

		));
 		$this->dbforge->add_key('venue_facility_connector_id', TRUE);
		$this->dbforge->create_table('venue_facility_connector');

	}

	public function down()
	{
		$this->dbforge->drop_table('venue_facilities');
		$this->dbforge->drop_table('venue_facility_connector');
	}
}