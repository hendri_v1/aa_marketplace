<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event4_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'eventspeaker_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'eventspeaker_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
			),
			'eventspeaker_description' => array(
				'type' =>'TEXT'
			),
			'eventspeaker_image' => array(
				'type' => 'VARCHAR',
				'constraint' => 30
			),
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'eventspeaker_hash' => array(
				'type' => 'VARCHAR',
				'constraint' => 40
			)
		));

		$this->dbforge->add_key('eventspeaker_id',TRUE);
		$this->dbforge->create_table('eventspeakers');

	}

	public function down()
	{
		$this->dbforge->drop_table('eventspeakers');
	}
}