<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Notifications2_schema extends CI_Migration {
	
	public function up()
	{
		$fields = array(
            'notification_project_id' => array(
                 'name' => 'notification_target',
                 'type' => 'INT',
            )
		);
		$this->dbforge->modify_column('notifications', $fields);
		$this->dbforge->drop_column('notifications','notification_message');

		//streams
		$this->dbforge->add_field(array(
			'stream_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'stream_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'stream_from' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'stream_target' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'stream_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'stream_status' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));

		$this->dbforge->add_key('stream_id',TRUE);
		$this->dbforge->create_table('streams');
	}
	
	public function down()
	{
		$fields = array(
            'notification_target' => array(
                 'name' => 'notification_project_id',
                 'type' => 'INT',
            )
		);
		$this->dbforge->modify_column('notifications', $fields);

		$fields = array(
                        'notification_message' => array('type' => 'VARCHAR','constraint'=>200)
		);
		$this->dbforge->add_column('notifications', $fields);

		$this->dbforge->drop_table('streams');
	}
}