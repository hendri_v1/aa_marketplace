<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_User8_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'userprofile_companyname' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
			),
			'userprofile_position' => array(
				'type' => 'VARCHAR',
				'constraint' => 40
			)
			
		);
		$this->dbforge->add_column('userprofile',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('userprofile', 'userprofile_companyname');
		$this->dbforge->drop_column('userprofile', 'userprofile_position');
	}
}