<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_User6_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'userextra_when' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'userextra_videourl' => array(
				'type' => 'TEXT',
				'constraint' => 200
			)
			
		);
		$this->dbforge->add_column('userextra',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('userextra', 'userextra_when');
		$this->dbforge->drop_column('userextra', 'userextra_videourl');
	}
}