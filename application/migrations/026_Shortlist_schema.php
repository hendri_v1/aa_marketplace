<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Shortlist_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'is_shortlisted' => array(
				'type' => 'INT',
				'constraint' => 11
			)
			
		);
 		$this->dbforge->add_column('projectbid',$fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('projectbid', 'is_shortlisted');
	}
}