<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Posisi_ads_schema extends CI_Migration {
	
	public function up()
	{
	
		$this->dbforge->add_field(array(
			'posisi_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'posisi_price' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('posisi_id', TRUE);
		$this->dbforge->create_table('posisi');

		$this->dbforge->add_field(array(
			'ads_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'posisi_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'ads_start' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'ads_stop' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('ads_id', TRUE);
		$this->dbforge->create_table('ads');

	}

	public function down()
	{
		$this->dbforge->drop_table('ads');
		$this->dbforge->drop_table('posisi');
	}
}