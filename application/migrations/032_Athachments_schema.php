<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Athachments_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'projectbid_attachment2' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			),
			'projectbid_attachment3' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			)
			
		);
		$this->dbforge->add_column('projectbid',$fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('projectbid', 'projectbid_attachment2');
		$this->dbforge->drop_column('projectbid', 'projectbid_attachment3');
	}
}