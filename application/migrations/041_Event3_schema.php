<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event3_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'event_address' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
				
			),
			'event_speaker' => array(
				'type' => 'VARCHAR',
				'constraint'=> 100
			),
			'event_geo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			)
			
		);
		$this->dbforge->add_column('events',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('events', 'event_address');
		$this->dbforge->drop_column('events', 'event_speaker');
		$this->dbforge->drop_column('events', 'event_geo');
	}
}