<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Jobs2_schema extends CI_Migration {

	public function up()
	{
		$fields2=array(
			'job_category_parent' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('job_categories',$fields2);

	}

	public function down()
	{
		$this->dbforge->drop_column('job_categories', 'job_category_parent');
	}
}
	