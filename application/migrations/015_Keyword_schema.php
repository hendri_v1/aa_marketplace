<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Keyword_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'keyword_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'keyword_text' => array(
				'type' => 'VARCHAR',
				'constraint' => 50
			)
		));
 		$this->dbforge->add_key('keyword_id', TRUE);
		$this->dbforge->create_table('keywords');
		
		$this->dbforge->add_field(array(
			'keyword_connector_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'keyword_connector_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'project_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'keyword_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('keyword_connector_id', TRUE);
		$this->dbforge->create_table('keyword_connector');

	}

	public function down()
	{
		$this->dbforge->drop_table('keywords');
		$this->dbforge->drop_table('keyword_connector');
	}
}