<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event11_schema extends CI_Migration {
	
	public function up()
	{
		//table event_audience
		$fields2=array(
			'is_private' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('events',$fields2);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('events', 'is_private');
	}
}