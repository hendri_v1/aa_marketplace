<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event5_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'eventticket_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'eventticket_seat' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'eventticket_earlybird_seat' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'eventticket_earlybird_price' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'eventticket_public' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));

		$this->dbforge->add_key('eventticket_id',TRUE);
		$this->dbforge->create_table('eventtickets');

	}

	public function down()
	{
		$this->dbforge->drop_table('eventtickets');
	}
}