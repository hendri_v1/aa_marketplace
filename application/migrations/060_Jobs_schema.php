<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Jobs_schema extends CI_Migration {
	
	public function up()
	{
	
		$this->dbforge->add_field(array(
			'job_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'job_title' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			),
			'job_description' => array(
				'type' => 'TEXT'
			),
			'job_qualification' => array(
				'type' => 'TEXT'
			),
			'job_poster'=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'city_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'job_status' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'job_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'job_position_level' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'job_category_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'job_salary_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'job_salary_start' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'job_salary_end' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'job_close_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'job_main_image' => array(
				'type' => 'VARCHAR',
				'constraint' => 80
			),
			'job_company_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'added_at' => array(
				'type' => 'INT',
				'constraint' => 11
			)

		));
 		$this->dbforge->add_key('job_id', TRUE);
		$this->dbforge->create_table('jobs');

		$this->dbforge->add_field(array(
			'job_category_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'job_category_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 80
			)
		));
		$this->dbforge->add_key('job_category_id', TRUE);
		$this->dbforge->create_table('job_categories');

	}

	public function down()
	{
		$this->dbforge->drop_table('jobs');
		$this->dbforge->drop_table('job_categories');
	}
}