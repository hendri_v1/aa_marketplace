<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue10_schema extends CI_Migration {
	
	public function up()
	{
		
		$this->dbforge->add_field(array(
			'venue_booking_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'venue_booking_start_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_booking_end_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_booking_added_date' =>array(
				'type' => 'INT',
				'constraint'=> 11
			),
			'venue_booking_limit' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_booking_status' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('venue_booking_id', TRUE);
		$this->dbforge->create_table('venue_booking');

		$this->dbforge->add_field(array(
			'venue_booking_detail_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_booking_detail_note' => array(
				'type' => 'TEXT'
			),
			'venue_booking_detail_status' => array(
				'type' => 'INT',
				'constraint' => 11
			)

		));
 		$this->dbforge->add_key('venue_booking_detail_id', TRUE);
		$this->dbforge->create_table('venue_booking_detail');
	}

	public function down()
	{
		$this->dbforge->drop_table('venue_booking');
		$this->dbforge->drop_table('venue_booking_detail');
	}
}

