<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Pages_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'pages_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'pages_title' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			),
			'pages_urltitle' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			),
			'pages_content' => array(
				'type' => 'TEXT'
			),
			'pages_addedat' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('pages_id', TRUE);
		$this->dbforge->create_table('pages');
		
	}
	
	public function down()
	{
		$this->dbforge->drop_table('pages');
	}
}