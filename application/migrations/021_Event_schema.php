<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Event_schema extends CI_Migration {
	
	public function up()
	{
		$fields=array(
			'is_featured' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'is_urgent' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'eventcategory_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_venue' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			)
			
		);
		$this->dbforge->add_column('events',$fields);

		$this->dbforge->add_field(array(
			'eventcategory_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'eventcategory_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			)
		));
 		$this->dbforge->add_key('eventcategory_id', TRUE);
		$this->dbforge->create_table('eventcategory');

		$this->dbforge->add_field(array(
			'eventconnector_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'keyword_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'event_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)

		));
 		$this->dbforge->add_key('eventconnector_id', TRUE);
		$this->dbforge->create_table('eventconnector');
	}

	public function down()
	{
		$this->dbforge->drop_column('events', 'is_featured');
		$this->dbforge->drop_column('events', 'is_urgent');
		$this->dbforge->drop_column('events', 'eventcategory_id');
		$this->dbforge->drop_column('events', 'event_venue');
		$this->dbforge->drop_table('eventcategory');
		$this->dbforge->drop_table('eventconnector');
	}
}