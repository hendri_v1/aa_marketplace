<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue_schema extends CI_Migration {
	
	public function up()
	{
	
		$this->dbforge->add_field(array(
			'venue_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'venue_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 150
			),
			'venue_description' => array(
				'type' => 'TEXT'
			),
			'venue_address'=> array(
				'type' => 'TEXT'
			),
			'venue_capacity' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_price' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'city_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_status' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'venue_type_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('venue_id', TRUE);
		$this->dbforge->create_table('venues');

		$this->dbforge->add_field(array(
			'venue_gallery_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'venue_gallery_file' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'venue_gallery_description' => array(
				'type' => 'TEXT'
			),
			'venue_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('venue_gallery_id', TRUE);
		$this->dbforge->create_table('venue_galleries');

		$this->dbforge->add_field(array(
			'venue_type_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'venue_type_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 40
			)
		));
 		$this->dbforge->add_key('venue_type_id', TRUE);
		$this->dbforge->create_table('venue_types');

	}

	public function down()
	{
		$this->dbforge->drop_table('venues');
		$this->dbforge->drop_table('venue_galleries');
		$this->dbforge->drop_table('venue_types');
	}
}