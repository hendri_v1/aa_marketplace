<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Notifications_schema extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field(array(
			'notification_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'notification_type' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'notification_message' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
			),
			'notification_from' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'notification_to' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'notification_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'notification_project_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'notification_status' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('notification_id', TRUE);
		$this->dbforge->create_table('notifications');
		
	}
	
	public function down()
	{
		$this->dbforge->drop_table('notifications');
	}
}