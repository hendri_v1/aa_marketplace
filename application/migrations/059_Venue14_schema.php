<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue14_schema extends CI_Migration {
	
	public function up()
	{
	
		$this->dbforge->add_field(array(
			'proposal_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'proposal_date' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'proposal_description' => array(
				'type' => 'TEXT'
			),
			'proposal_additional'=> array(
				'type' => 'TEXT'
			),
			'proposal_audience' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'proposal_status' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('proposal_id', TRUE);
		$this->dbforge->create_table('proposal');

		$fields2=array(
			'proposal_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		);
		$this->dbforge->add_column('venue_booking',$fields2);

	}

	public function down()
	{
		$this->dbforge->drop_table('proposal');
		$this->dbforge->drop_column('venue_booking','proposal_id');
	}
}