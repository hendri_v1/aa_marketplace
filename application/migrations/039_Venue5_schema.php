<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Venue5_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'venue_price_type' => array(
				'type' => 'INT'
				
			)
			
		);
		$this->dbforge->add_column('venues',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('venues', 'venue_price_type');
	}
}