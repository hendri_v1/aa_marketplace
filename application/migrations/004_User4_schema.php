<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_User4_schema extends CI_Migration {
	
	public function up()
	{
		//table userprofile
		$fields=array(
			'userprofile_aboutme_brief' => array(
				'type' => 'VARCHAR',
				'constraint' => 200,
			),
			'userprofile_phone' => array(
				'type' => 'TEXT',
				'constraint' => 30
			)
			
		);
		$this->dbforge->add_column('userprofile',$fields);
	}
	
	public function down()
	{
		$this->dbforge->drop_column('userprofile', 'userprofile_aboutme_brief');
		$this->dbforge->drop_column('userprofile', 'userprofile_phone');
	}
}