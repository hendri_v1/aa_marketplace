<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_User_schema extends CI_Migration {
	
	public function up()
	{
		//table user
		$this->dbforge->add_field(array(
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => '30'
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '50'
			),
			'last_login' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'user_status' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'user_email' => array(
				'type' => 'VARCHAR',
				'constraint' => 25
			),
			'user_register_date' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
 		$this->dbforge->add_key('user_id', TRUE);
		$this->dbforge->create_table('user');
		
		//table user_type
		$this->dbforge->add_field(array(
			'usertype_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'usertype_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '30'
			)
			
		));
 		$this->dbforge->add_key('usertype_id', TRUE);
		$this->dbforge->create_table('usertype');
		
		//table user_profile
		$this->dbforge->add_field(array(
			'userprofile_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'usertype_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'userprofile_firstname' => array(
				'type' => 'VARCHAR',
				'constraint' => '30'
			),
			'userprofile_lastname' => array(
				'type' => 'VARCHAR',
				'constraint' => '30'
			)
			
		));
		$this->dbforge->add_key('userprofile_id', TRUE);
		$this->dbforge->create_table('userprofile');
	}
	
	public function down()
	{
		$this->dbforge->drop_table('user');
	}
}