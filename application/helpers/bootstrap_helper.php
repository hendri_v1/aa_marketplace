<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$CI =& get_instance();
$CI->load->helper('form');
if (!function_exists('bt_text'))
{
    function bt_text($id='',$label='',$placeholder='',$value='',$required=false)
    {
        $rq = '';
        if ($required == true) {
            $rq = 'required="required"';
        }
        $return='<div class="form-group">';
		$return.='<label for="'.$id.'">'.$label.'</label>';
		$return.='<input type="text" class="form-control" name="'.$id.'" id="'.$id.'" placeholder="'.$placeholder.'" value="'.$value.'"'.$rq.'>';
        $return.='</div>';
		return $return;
    }
}

if (!function_exists('bt_file'))
{
    function bt_file($id='',$label='',$placeholder='',$value='')
    {
        $return='<div class="form-group">';
		$return.='<label for="'.$id.'">'.$label.'</label>';
		$return.='<input type="file" class="form-control" name="'.$id.'" id="'.$id.'" placeholder="'.$placeholder.'" value="'.$value.'" >';
        $return.='</div>';
		return $return;
    }
}

if (!function_exists('bt_select'))
{
	function bt_select($id='',$label='',$value=array(),$default='')
	{
		$return='<div class="form-group">';
		$return.='<label for="'.$id.'">'.$label.'</label>';
		$return.=form_dropdown($id,$value,$default,'class="form-control" id="'.$id.'"');
		$return.='</div>';
		return $return;
	}
}

if (!function_exists('bt_textarea'))
{
	function bt_textarea($id='',$label='',$value='',$height='50px')
	{
		$return='<div class="form-group">';
		$return.='<label for="'.$id.'">'.$label.'</label>';
		$return.='<textarea name="'.$id.'" id="'.$id.'" class="form-control" style="height:'.$height.';">'.$value.'</textarea>';
		$return.='</div>';
		return $return;
	}
}

if (!function_exists('bt_group_text'))
{
	function bt_group_text($id='',$label='',$value='',$icon='')
	{
		$return='<div class="form-group">';
		$return.='<div class="input-group">';
		$return.='<input type="text" name="'.$id.'" id="'.$id.'" class="form-control" placeholder="'.$label.'" value="'.$value.'">';
		$return.='<div class="input-group-addon"><i class="'.$icon.'"></i></div>';
		$return.='</div>';
		$return.='</div>';
		return $return;
	}
}

if (!function_exists('bt_group_select'))
{
	function bt_group_select($id='',$value=array(),$default='',$icon='')
	{
		$return='<div class="form-group">';
		$return.='<div class="input-group">';
		$return.=form_dropdown($id,$value,$default,'class="form-control" id="'.$id.'"');
		$return.='<div class="input-group-addon"><i class="'.$icon.'"></i></div>';
		$return.='</div>';
		$return.='</div>';
		return $return;
	}
}

