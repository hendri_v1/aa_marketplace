<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function paginate_bs3($base_url = '', $per_page = 0, $total_rows = 0, $segment, $suffix = FALSE, $hashtag="") {
    $CI = & get_instance();
    $CI->load->library('pagination');

    $config['full_tag_open'] = "<ul class='pager'>";
    $config['full_tag_close'] = "</ul>";
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "</a></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tagl_close'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tagl_close'] = "</li>";
    $config['first_tag_open'] = "<li>";
    $config['first_tagl_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tagl_close'] = "</li>";
    //$config['use_page_numbers'] = TRUE;
    $config['num_links'] = 5;
    $config['last_link'] = 'Akhir';
    $config['first_link'] = 'Awal';
    $config['uri_segment'] = $segment;

    $config['base_url'] = $base_url;
    $config['per_page'] = $per_page;
    $config['total_rows'] = $total_rows;
    if ($suffix == TRUE) {
        $config['suffix'] = '?' . http_build_query($_GET, '', "&").$hashtag;
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
    }

    return $CI->pagination->initialize($config);
}
