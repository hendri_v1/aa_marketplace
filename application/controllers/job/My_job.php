<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_job extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                if(!$this->session->userdata('logged'))
		{
			redirect('program');			
		}

		$this->load->model('job_model');
		
		
	}

	public function index()
	{
		$data['title'] = "Job Management";
		$data['nav_side'] = "jms";
		$data['query'] = $this->job_model->get_my_job();
		$this->load->view('job_include/my_job',$data);
	}

	public function add()
	{
		$data['title'] = "Tambah Job";
		$data['nav_side'] = "jms";
		$data['query'] = $this->job_model->get_categories();
		$data['province'] = $this->global_model->get_province();
		$this->load->view('job_include/add',$data);
	}

	public function save_job()
	{

		if($this->input->post())
		{
			if ($_FILES["job_main_image"]["name"]) 
			{
				$name = $_FILES["job_main_image"]["name"];
				$ext = end((explode(".", $name)));
				$filename = time().'.'.$ext;

				$config['upload_path'] = './uploads/project_images/';
				$config['file_name']  = $filename;
				$config['allowed_types']  = 'jpg|png|gif';

				$this->load->library('upload', $config);
				if ($this->upload->do_upload('job_main_image')) {
					$data = $this->upload->data();
					$uploaded=$data['file_name'];
				} else {
					$uploaded='';
					echo $this->upload->display_errors();
				}
			} 
			else 
			{
				$uploaded='';
			}

			$end = $this->input->post('job_close_date');
			$ymd2 = explode("/", $end);
		    $time2 = explode(" ", $ymd2[2]);
		    $tanggal = $time2[0].'-'.$ymd2[1].'-'.$ymd2[0].' '.$time2[1];
		    $enddate = strtotime($tanggal);

			$set_array=array(
				'job_title' => $this->input->post('job_title'),
				'job_description' => $this->input->post('job_description'),
				'job_qualification' => $this->input->post('job_qualification'),
				'job_poster' => $this->session->userdata('user_meta')->user_id,
				'city_id' => $this->input->post('city_id'),
				'job_status' => 1,
				'job_type' => $this->input->post('job_type'),
				'job_position_level' => $this->input->post('job_position_level'),
				'job_category_id' => $this->input->post('job_category_id'),
				'job_salary_type' => $this->input->post('job_salary_type'),
				'job_salary_start' => $this->input->post('job_salary_start'),
				'job_salary_end' => $this->input->post('job_salary_end'),
				'job_close_date' => $enddate,
				'job_main_image' => $uploaded,
				'job_company_name' => $this->input->post('job_company_name'),
				'added_at' => time()
			);
			$this->job_model->add_new($set_array);
		}
		redirect('job/my_job');
	}



}