<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
	}

	function index()
	{
		$data['title'] = "lowongan Kerja";
		$this->load->view('job_include/job_list',$data);
	}

}