<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_venue extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                if(!$this->session->userdata('logged'))
		{
			redirect('program');			
		}
		$this->load->model('user_role_model');
		$this->load->model('userpost_model');
		$this->load->model('userprofile_model');
		$this->load->model('venue_model');
	}

	function index()
	{
		$data['title']="My Venue";
		$data['query']=$this->venue_model->get_my_venue();
		$data['nav_side']="vms";
		$data['child_nav']="vms";
		$this->load->view('venue_include/my_venue',$data);
	}

	function add_new_main()
	{
		$data['map']=TRUE;
		$data['title']="Tambah Venue";
		$data['province'] = $this->global_model->get_province();
		$data['qvenue_types']=$this->venue_model->get_venue_types();
		$data['nav_side']="vms";
		$data['child_nav']="vms_new";
		$this->load->view('venue_include/add_new_venue_main',$data);
		
	}

	function save_main_venue()
	{
		$name = $_FILES["venue_main_image"]["name"];
		$ext = end((explode(".", $name)));
		$newname=time();
		$filename = $newname.'.'.$ext;

		$config['upload_path'] = './uploads/project_images/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['file_name']  = $filename;
		

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('venue_main_image')) {
			/*
			if (file_exists($path)) {
				//unlink($path);
			}
			*/
			$data = $this->upload->data();
			$this->load->library('image_lib');
			
			
			
			
			
			$config2['source_image'] = $data['full_path'];
			//$config2['create_thumb'] = TRUE;
			$config2['maintain_ratio'] = TRUE;
			$config2['width']= 200;
			$config2['height']	= 200;
			$config2['new_image']=$newname.'_thumb.'.$ext;
			$this->image_lib->clear();
			$this->image_lib->initialize($config2); 
			$this->image_lib->resize();

			$this->image_lib->clear();

			$config3['source_image']	= $data['full_path'];
			//$config2['create_thumb'] = FALSE;
			$config3['maintain_ratio'] = TRUE;
			$config3['width']= 1024;
			$config3['height']	= 768;
			$this->image_lib->clear();
			$this->image_lib->initialize($config3); 
			if ( ! $this->image_lib->resize())
			{
			    echo $this->image_lib->display_errors();
			    
			}

			
		} else {
			echo $this->upload->display_errors();
		}
		$set_array=array(
				'venue_main_name'=>$this->input->post('venue_main_name'),
				'venue_main_description'=>$this->input->post('venue_main_description'),
				'venue_main_address'=>$this->input->post('venue_main_address'),
				'city_id'=>$this->input->post('city_id'),
				'venue_main_status'=>0,
				'venue_main_geo'=>trim($this->input->post('venue_main_geo'), '()'),
				'venue_main_image'=>$data['file_name'],
				'venue_main_owner'=>$this->session->userdata('user_id'),
				'venue_main_contact_phone'=>$this->input->post('venue_main_contact_phone'),
				'venue_main_contact_mobile_phone'=>$this->input->post('venue_main_contact_mobile_phone'),
				'venue_main_contact_person'=>$this->input->post('venue_main_contact_person')
			);
		$venue_main_id=$this->venue_model->save_venue_main($set_array);
		$data['venue_meta']=$this->venue_model->get_venue_main_by_id($venue_main_id);
		$content = $this->load->view('mailing_view/mail_new_venue', $data, TRUE);
		$this->global_model->sendmailMandrill2($data['venue_meta']->user_email,'','Venue Anda Berhasil Didaftarkan',$content);
		redirect('my-venue');
	}
	function add_new_room($venue_main_id)
	{
	
		$data['title']="Tambah Room";
		
		$data['qvenue_types']=$this->venue_model->get_venue_types();
		$data['venue_gallery_hash']=md5(time());
		$data['row']=$this->venue_model->get_venue_main_by_id($venue_main_id);
		$data['nav_side']="vms";
		$data['child_nav']="vms_new_room";
		$this->load->view('venue_include/add_new_venue',$data);
	}

	function view_gallery_by_hash($venue_gallery_hash,$gallery_type=0)
	{
		$data['query']=$this->venue_model->get_gallery_by_hash($venue_gallery_hash,$gallery_type);
		$this->load->view('venue_include/venue_gallery_hash',$data);
	}

	function view_gallery_by_venue_id($venue_id,$gallery_type=0)
	{
		$data['query']=$this->venue_model->get_gallery_by_venue($venue_id,$gallery_type);
		$this->load->view('venue_include/venue_gallery_hash',$data);
	}

	function add_gallery()
	{
		$name = $_FILES["venue_image"]["name"];
		$ext = end((explode(".", $name)));
		$newname=time();
		$filename = $newname.'.'.$ext;

		$config['upload_path'] = './uploads/project_images/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['file_name']  = $filename;
		

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('venue_image')) {
			/*
			if (file_exists($path)) {
				//unlink($path);
			}
			*/
			$data = $this->upload->data();
			$this->load->library('image_lib');
			
			
			
			
			
			$config2['source_image'] = $data['full_path'];
			//$config2['create_thumb'] = TRUE;
			$config2['maintain_ratio'] = TRUE;
			$config2['width']= 200;
			$config2['height']	= 200;
			$config2['new_image']=$newname.'_thumb.'.$ext;
			$this->image_lib->clear();
			$this->image_lib->initialize($config2); 
			$this->image_lib->resize();

			$this->image_lib->clear();

			$config3['source_image']	= $data['full_path'];
			//$config2['create_thumb'] = FALSE;
			$config3['maintain_ratio'] = TRUE;
			$config3['width']= 1024;
			$config3['height']	= 768;
			$this->image_lib->clear();
			$this->image_lib->initialize($config3); 
			if ( ! $this->image_lib->resize())
			{
			    echo $this->image_lib->display_errors();
			    
			}

			
			
			
			$set_array=array(
				"venue_gallery_file"=>$data['file_name'],
				"venue_gallery_hash"=>$this->input->post('venue_gallery_hash'),
				"venue_gallery_thumb"=>$newname.'_thumb.'.$ext
			);
			$this->venue_model->add_venue_gallery($set_array);
			echo 'true';
		} else {
			echo $this->upload->display_errors();
		}

	}

	function add_room_config()
	{
		$name = $_FILES["room_config"]["name"];
		$ext = end((explode(".", $name)));
		$newname=time();
		$filename = $newname.'.'.$ext;

		$config['upload_path'] = './uploads/project_images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']  = $filename;
		

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('room_config')) {
			/*
			if (file_exists($path)) {
				//unlink($path);
			}
			*/
			$data = $this->upload->data();
			$this->load->library('image_lib');
			
			
			
			
			
			$config2['source_image'] = $data['full_path'];
			//$config2['create_thumb'] = TRUE;
			$config2['maintain_ratio'] = TRUE;
			$config2['width']= 200;
			$config2['height']	= 200;
			$config2['new_image']=$newname.'_thumb.'.$ext;
			$this->image_lib->clear();
			$this->image_lib->initialize($config2); 
			$this->image_lib->resize();

			$this->image_lib->clear();

			$config3['source_image']	= $data['full_path'];
			//$config2['create_thumb'] = FALSE;
			$config3['maintain_ratio'] = TRUE;
			$config3['width']= 1024;
			$config3['height']	= 768;
			$this->image_lib->clear();
			$this->image_lib->initialize($config3); 
			if ( ! $this->image_lib->resize())
			{
			    echo $this->image_lib->display_errors();
			    
			}

			
			
			
			$set_array=array(
				"venue_gallery_file"=>$data['file_name'],
				"venue_gallery_description"=>'room_config',
				"venue_gallery_hash"=>$this->input->post('venue_gallery_hash'),
				"venue_gallery_thumb"=>$newname.'_thumb.'.$ext
			);
			$this->venue_model->add_venue_gallery($set_array);
			echo 'true';
		} else {
			echo $this->upload->display_errors();
		}

	}

	function save_venue()
	{
		$set_array=array(
			'venue_name'=>$this->input->post('venue_name'),
			'venue_description'=>$this->input->post('venue_description'),
			
			'venue_capacity'=>$this->input->post('venue_capacity'),
			'venue_price'=>$this->input->post('venue_price'),
			'venue_price_type'=>$this->input->post('venue_price_type'),
			'venue_type_id'=>$this->input->post('venue_type_id'),
			'venue_owner'=>$this->session->userdata('user_id'),
			'venue_main_id'=>$this->input->post('venue_main_id'),
			'venue_space'=>$this->input->post('venue_space'),
			'venue_width'=>$this->input->post('venue_width'),
			'venue_height'=>$this->input->post('venue_height'),
			'venue_length'=>$this->input->post('venue_length'),
			'venue_u_shape'=>$this->input->post('venue_u_shape'),
			'venue_boardroom'=>$this->input->post('venue_boardroom'),
			'venue_classroom'=>$this->input->post('venue_classroom'),
			'venue_reception'=>$this->input->post('venue_reception'),
			'venue_banquet'=>$this->input->post('venue_banquet'),
			'venue_theatre'=>$this->input->post('venue_theatre'),
			'venue_auditorium'=>$this->input->post('venue_auditorium'),
			'venue_circle'=>$this->input->post('venue_circle')
		);
		$venue_id=$this->venue_model->save_venue($set_array);
		$this->venue_model->update_gallery_hash($this->input->post('venue_gallery_hash2'),$venue_id);
		
		$array_venue_facilities1=$this->input->post('venue_facilities');
		if(is_array($array_venue_facilities1))
		{
			foreach($array_venue_facilities1 as $key=>$value)
			{
				$this->venue_model->save_facility_connector($value,$venue_id);	
			}
		}
		$array_venue_facilities2=$this->input->post('venue_facilities1');
		if(is_array($array_venue_facilities2))
		{
			foreach($array_venue_facilities2 as $key=>$value)
			{
				$this->venue_model->save_facility_connector($value,$venue_id);	
			}
		}
		redirect('my-venue');
			
	}

	function availability($venue_main_id)
	{
		
		$data['title']="Atur Availability";
		$data['query']=$this->venue_model->get_room_by_venue_id($venue_main_id);
		$data['row']=$this->venue_model->get_venue_main_by_id($venue_main_id);
		$data['nav_side']="vms";
		$data['child_nav']="vms_avail";
		$this->load->view('venue_include/availability',$data);
	}

	function venue_calendar($venue_id=0,$year='',$month='')
	{
		if($year=='')
			$year=date('Y');
		if($month=='')
			$month=date('m');
		$data['year']=$year;
		$data['month']=$month;
		$prefs['show_next_prev']=TRUE;
		$prefs['next_prev_url']=site_url('venue/my_venue/venue_calendar/'.$venue_id);
		$prefs['template'] = '

		   {table_open}<table class="table">{/table_open}

		   {heading_row_start}<tr>{/heading_row_start}

		   {heading_previous_cell}<th><a href="javascript:void(0);" data-urlto="{previous_url}" class="prev_cal">&lt;&lt;</a></th>{/heading_previous_cell}
		   {heading_title_cell}<th colspan="{colspan}"><div class="text-center">{heading}</div></th>{/heading_title_cell}
		   {heading_next_cell}<th><a href="javascript:void(0);" data-urlto="{next_url}" class="next_cal">&gt;&gt;</a></th>{/heading_next_cell}

		   {heading_row_end}</tr>{/heading_row_end}

		   {week_row_start}<tr>{/week_row_start}
		   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr>{/cal_row_start}
		   {cal_cell_start}<td>{/cal_cell_start}

		   {cal_cell_content}<a href="javascript:void(0);" data-toremove="{content}" data-urlto="'.site_url('venue/my_venue/remove_book/'.$venue_id.'/'.$year.'/'.$month).'" class="remove_book"><span class="date_unavailable">{day}</span></a>{/cal_cell_content}
		   {cal_cell_content_today}<div class="highlight_unavailable"><a href="javascript:void(0);" data-toremove="{content}" data-urlto="'.site_url('venue/my_venue/remove_book/'.$venue_id.'/'.$year.'/'.$month).'" class="remove_book">{day}</a></div>{/cal_cell_content_today}

		   {cal_cell_no_content}<a href="javascript:void(0);" data-urlto="'.site_url('venue/my_venue/create_book/'.$venue_id.'/'.$year.'/'.$month).'" data-thedate="{day}" class="make_book"><span class="date_available">{day}</span></a>{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="highlight_available"><a href="javascript:void(0);" data-urlto="'.site_url('venue/my_venue/create_book/'.$venue_id.'/'.$year.'/'.$month).'" data-thedate="{day}" class="make_book">{day}</a></div>{/cal_cell_no_content_today}

		   {cal_cell_blank}&nbsp;{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}
		';
		$this->load->library('calendar',$prefs);
		
		$querybooked=$this->venue_model->get_booking_pm($year,$month,$venue_id);
		
		$booked_data=array();
		foreach($querybooked->result() as $rowsbooked)
		{
			$booked_data[mdate('%j',$rowsbooked->venue_booking_start_date)]=$rowsbooked->venue_booking_id;
		}

		$data['available_list']=$booked_data;
		$this->load->view('venue_include/venue_calendar_user',$data);
	}

	function create_book($venue_id=0,$year='',$month='')
	{
		$set_array=array(
			'venue_booking_start_date'=>human_to_unix($year.'-'.$month.'-'.$this->input->post('thedate').' 00:00:00'),
			'venue_booking_end_date'=>human_to_unix($year.'-'.$month.'-'.$this->input->post('thedate').' 23:59:59'),
			'venue_id'=>$venue_id,
			'venue_booking_added_date'=>time(),
			'venue_booking_limit'=>time()+86400,
			'venue_booking_status'=>1
		);
		$venue_booking_id=$this->venue_model->save_book_owner($set_array);
		$set_array2=array(
			'user_id'=>$this->session->userdata('user_id'),
			'venue_booking_detail_note'=>'Dibuat oleh Venue Owner',
			'venue_booking_detail_status'=>1,
			'venue_booking_id'=>$venue_booking_id
		);
		$this->venue_model->save_venue_booking_detail($set_array2);
		echo site_url('venue/my_venue/venue_calendar/'.$venue_id.'/'.$year.'/'.$month);
	}

	function remove_book($venue_id=0,$year,$month)
	{
		$this->venue_model->remove_booking($this->input->post('venue_booking_id'));
		echo site_url('venue/my_venue/venue_calendar/'.$venue_id.'/'.$year.'/'.$month);
	}

	function view_rooms($venue_main_id)
	{
		$data['query']=$this->venue_model->get_room_by_venue_id($venue_main_id);
		$data['row']=$this->venue_model->get_venue_main_by_id($venue_main_id);
		$data['nav_side']="vms";
		$data['title']="Kelola Room";
		$data['child_nav']="vms_room";
		$this->load->view('venue_include/my_rooms',$data);
	}

	function edit_room($venue_id)
	{
		$data['row']=$this->venue_model->get_room_by_room_id($venue_id)->row();
		$data['qvenue_types']=$this->venue_model->get_venue_types();
		$data['venue_gallery_hash']=md5(time());
		$data['nav_side']="vms";
		$data['child_nav']="vms_new";
		$data['title']="Edit Room";
		$this->load->view('venue_include/edit_room',$data);
	}

	function update_room()
	{
		$set_array=array(
			'venue_name'=>$this->input->post('venue_name'),
			'venue_description'=>$this->input->post('venue_description'),
			
			'venue_capacity'=>$this->input->post('venue_capacity'),
			'venue_price'=>$this->input->post('venue_price'),
			'venue_price_type'=>$this->input->post('venue_price_type'),
			'venue_type_id'=>$this->input->post('venue_type_id'),
			'venue_owner'=>$this->session->userdata('user_id'),
			'venue_main_id'=>$this->input->post('venue_main_id'),
			'venue_space'=>$this->input->post('venue_space'),
			'venue_width'=>$this->input->post('venue_width'),
			'venue_height'=>$this->input->post('venue_height'),
			'venue_length'=>$this->input->post('venue_length'),
			'venue_u_shape'=>$this->input->post('venue_u_shape'),
			'venue_boardroom'=>$this->input->post('venue_boardroom'),
			'venue_classroom'=>$this->input->post('venue_classroom'),
			'venue_reception'=>$this->input->post('venue_reception'),
			'venue_banquet'=>$this->input->post('venue_banquet'),
			'venue_theatre'=>$this->input->post('venue_theatre'),
			'venue_auditorium'=>$this->input->post('venue_auditorium'),
			'venue_circle'=>$this->input->post('venue_circle')
		);
		$venue_id=$this->input->post('venue_id');

		$this->venue_model->update_venue($set_array,$venue_id);
		$this->venue_model->update_gallery_hash($this->input->post('venue_gallery_hash2'),$venue_id);
		$this->venue_model->clear_facilities($venue_id);
		$array_venue_facilities1=$this->input->post('venue_facilities');
		if(is_array($array_venue_facilities1))
		{
			foreach($array_venue_facilities1 as $key=>$value)
			{
				$this->venue_model->save_facility_connector($value,$venue_id);	
			}
		}
		$array_venue_facilities2=$this->input->post('venue_facilities1');
		if(is_array($array_venue_facilities2))
		{
			foreach($array_venue_facilities2 as $key=>$value)
			{
				$this->venue_model->save_facility_connector($value,$venue_id);	
			}
		}
		redirect('venue/my_venue/view_rooms/'.$this->input->post('venue_main_id'));
	}
}