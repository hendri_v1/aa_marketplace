<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venue extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_role_model');
		$this->load->model('userpost_model');
		$this->load->model('userprofile_model');
		$this->load->model('venue_model');

	}

	function index()
	{

	}

	function view($venue_main_id)
	{
		
		$data['map']=TRUE;
		$data['jssor']=TRUE;
		$data['row']=$this->venue_model->get_venue_by_id($venue_main_id);
		$data['qgallery']=$this->venue_model->get_all_gallery($venue_main_id);
		$data['qroom']=$this->venue_model->get_room_by_venue_id($venue_main_id);
		$data['title']=$data['row']->venue_main_name;
		$data['meta_def'] = strip_tags(word_limiter($data['row']->venue_main_description, 50));
		$this->load->view('venue_include/view_venue',$data);
	}
	
	function venue_directory()
	{
		
		$data['title']="Direktori Venue";
		$data['province'] = $this->global_model->get_province();
		$filter_array=$this->input->get();
		$data['query'] = $this->venue_model->get_venue_front($filter_array);
		
		$data['qvenuetypes']=$this->venue_model->get_venue_types();
		$this->load->view('venue_include/venue_directory',$data);	
	}

	function by_province($province_id)
	{
		$data['row']=$this->global_model->get_province_by_id($province_id);

		$data['title']="Venue di ".$data['row']->province_name;
		$data['query']=$this->venue_model->get_venue_group_by_city($province_id);
		$this->load->view('venue_include/venue_by_province',$data);
	}

	function by_city($city_id)
	{
		$data['row']=$this->global_model->get_city_by_id($city_id);

		$data['title']="Venue di ".$data['row']->city_name;
		$data['query']=$this->venue_model->get_venue_by_city($city_id);
		$this->load->view('venue_include/venue_by_city',$data);	
	}

	function venue_calendar($venue_id=0,$year='',$month='')
	{
		$prefs['show_next_prev']=TRUE;
		$prefs['next_prev_url']=site_url('venue/venue/venue_calendar/'.$venue_id);
		$prefs['template'] = '

		   {table_open}<table class="table">{/table_open}

		   {heading_row_start}<tr>{/heading_row_start}

		   {heading_previous_cell}<th><a href="javascript:void(0);" data-urlto="{previous_url}" class="prev_cal">&lt;&lt;</a></th>{/heading_previous_cell}
		   {heading_title_cell}<th colspan="{colspan}"><div class="text-center">{heading}</div></th>{/heading_title_cell}
		   {heading_next_cell}<th><a href="javascript:void(0);" data-urlto="{next_url}" class="next_cal">&gt;&gt;</a></th>{/heading_next_cell}

		   {heading_row_end}</tr>{/heading_row_end}

		   {week_row_start}<tr>{/week_row_start}
		   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr>{/cal_row_start}
		   {cal_cell_start}<td>{/cal_cell_start}

		   {cal_cell_content}<a href="{content}"><span class="date_unavailable">{day}<span></a>{/cal_cell_content}
		   {cal_cell_content_today}<div class="highlight_unavailable"><a href="{content}">{day}</a></div>{/cal_cell_content_today}

		   {cal_cell_no_content}<span class="date_available">{day}</span>{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="highlight_available">{day}</div>{/cal_cell_no_content_today}

		   {cal_cell_blank}&nbsp;{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}
		';
		$this->load->library('calendar',$prefs);
		if($year=='')
			$year=date('Y');
		if($month=='')
			$month=date('m');
		$data['year']=$year;
		$data['month']=$month;
		$querybooked=$this->venue_model->get_booking_pm($year,$month,$venue_id);
		
		$booked_data=array();
		foreach($querybooked->result() as $rowsbooked)
		{
			$booked_data[mdate('%j',$rowsbooked->venue_booking_start_date)]='#';
		}

		$data['available_list']=$booked_data;
		$this->load->view('venue_include/venue_calendar',$data);
	}

	function venue_calendar_booking($venue_id=0,$year='',$month='')
	{
		if($year=='')
			$year=date('Y');
		if($month=='')
			$month=date('m');
		$data['year']=$year;
		$data['month']=$month;
		$prefs['show_next_prev']=TRUE;
		$prefs['next_prev_url']=site_url('venue/venue/venue_calendar_booking/'.$venue_id);
		$prefs['template'] = '

		   {table_open}<table class="table">{/table_open}

		   {heading_row_start}<tr>{/heading_row_start}

		   {heading_previous_cell}<th><a href="javascript:void(0);" data-urlto="{previous_url}" class="prev_cal">&lt;&lt;</a></th>{/heading_previous_cell}
		   {heading_title_cell}<th colspan="{colspan}"><div class="text-center">{heading}</div></th>{/heading_title_cell}
		   {heading_next_cell}<th><a href="javascript:void(0);" data-urlto="{next_url}" class="next_cal">&gt;&gt;</a></th>{/heading_next_cell}

		   {heading_row_end}</tr>{/heading_row_end}

		   {week_row_start}<tr>{/week_row_start}
		   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr>{/cal_row_start}
		   {cal_cell_start}<td>{/cal_cell_start}

		   {cal_cell_content}<a href="javascript:void(0);" data-toremove="{content}" data-urlto="'.site_url('venue/venue/remove_book/'.$venue_id.'/'.$year.'/'.$month).'" class="remove_book"><span class="date_unavailable">{day}</span></a>{/cal_cell_content}
		   {cal_cell_content_today}<div class="highlight_unavailable"><a href="javascript:void(0);" data-toremove="{content}" data-urlto="'.site_url('venue/venue/remove_book/'.$venue_id.'/'.$year.'/'.$month).'" class="remove_book">{day}</a></div>{/cal_cell_content_today}

		   {cal_cell_no_content}<a href="javascript:void(0);" data-urlto="'.site_url('venue/venue/create_book/'.$venue_id.'/'.$year.'/'.$month).'" data-thedate="{day}" class="make_book"><span class="date_available">{day}</span></a>{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="highlight_available"><a href="javascript:void(0);" data-urlto="'.site_url('venue/venue/create_book/'.$venue_id.'/'.$year.'/'.$month).'" data-thedate="{day}" class="make_book">{day}</a></div>{/cal_cell_no_content_today}

		   {cal_cell_blank}&nbsp;{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}
		';
		$this->load->library('calendar',$prefs);
		
		$querybooked=$this->venue_model->get_my_booking($year,$month,$venue_id);
		
		$booked_data=array();
		foreach($querybooked->result() as $rowsbooked)
		{
			if($rowsbooked->user_id==$this->session->userdata('user_id'))
				$booked_data[mdate('%j',$rowsbooked->venue_booking_start_date)]=$rowsbooked->venue_booking_id;
			else
				$booked_data[mdate('%j',$rowsbooked->venue_booking_start_date)]='#';
		}

		$data['available_list']=$booked_data;
		$this->load->view('venue_include/venue_calendar',$data);
	}

	function booking($venue_id)
	{
		$data['title']="Booking Venue";
		$data['row']=$this->venue_model->get_room_by_room_id($venue_id)->row();
		$this->load->view('venue_include/venue_booking',$data);
	}

	function create_book($venue_id=0,$year='',$month='')
	{
		$set_array=array(
			'venue_booking_start_date'=>human_to_unix($year.'-'.$month.'-'.$this->input->post('thedate').' 00:00:00'),
			'venue_booking_end_date'=>human_to_unix($year.'-'.$month.'-'.$this->input->post('thedate').' 23:59:59'),
			'venue_id'=>$venue_id,
			'venue_booking_added_date'=>time(),
			'venue_booking_limit'=>time()+86400,
			'venue_booking_status'=>0
		);
		$venue_booking_id=$this->venue_model->save_book_owner($set_array);
		$set_array2=array(
			'user_id'=>$this->session->userdata('user_id'),
			'venue_booking_detail_note'=>'temporary',
			'venue_booking_detail_status'=>0,
			'venue_booking_id'=>$venue_booking_id
		);
		$this->venue_model->save_venue_booking_detail($set_array2);
		echo site_url('venue/venue/venue_calendar_booking/'.$venue_id.'/'.$year.'/'.$month);
	}

	function remove_book($venue_id=0,$year,$month)
	{
		$this->venue_model->remove_booking($this->input->post('venue_booking_id'));
		echo site_url('venue/venue/venue_calendar_booking/'.$venue_id.'/'.$year.'/'.$month);
	}

	function my_booking_this_venue($venue_id)
	{
		$querybooked=$this->venue_model->get_my_unconfirmed_booking($venue_id);
		$data['query']=$querybooked;
		$this->load->view('venue_include/unconfirmed_booking',$data);
	}

	function save_booking()
	{
		//update user profile
		$set_array2=array(
			'userprofile_firstname'=>$this->input->post('userprofile_firstname'),
			'userprofile_lastname'=>$this->input->post('userprofile_lastname'),
			'userprofile_phone'=>$this->input->post('userprofile_phone')
		);
		$this->userprofile_model->update_profile($set_array2);

		//save the proposal
		$set_array=array(
			'proposal_date'=>time(),
			'proposal_description'=>$this->input->post('proposal_description'),
			'proposal_additional'=>$this->input->post('proposal_additional'),
			'proposal_audience'=>$this->input->post('proposal_audience'),
			'proposal_status'=>0,
			'user_id'=>$this->session->userdata('user_meta')->user_id
		);
		$proposal_id=$this->venue_model->save_proposal($set_array);

		$data['qbooking']=$this->venue_model->save_booking($this->input->post('venue_id'),$this->session->userdata('user_id'),$proposal_id);


		
		//send the email
		$data['room']=$this->venue_model->get_room_by_room_id($this->input->post('venue_id'))->row();
		$venue_main_id=$data['room']->venue_main_id;
		$data['venue_meta']=$this->venue_model->get_venue_main_by_id($venue_main_id);
		$content = $this->load->view('mailing_view/mail_new_proposal', $data, TRUE);
		$this->global_model->sendmailMandrill2($data['venue_meta']->user_email,'','Ada Proposal Request Untuk Venue Anda',$content);

		//send the email to requester
		$data['user_meta']=$this->session->userdata('user_meta');
		$content2 = $this->load->view('mailing_view/mail_user_proposal', $data, TRUE);
		
		$this->global_model->sendmailMandrill2($this->session->userdata('user_meta')->user_email,'','Request Proposal Anda Diterima',$content2);

		//send the email to HRPlasa
		$data['set_array']=$set_array;
		$content3 = $this->load->view('mailing_view/mail_proposal_hrplasa', $data, TRUE);
		
		$this->global_model->sendmailMandrill2('info@hrplasa.id','emma_yulini@yahoo.com','Ada Proposal Request Baru',$content3);

		redirect('venue/venue/booking/'.$this->input->post('venue_id'));
	}

}