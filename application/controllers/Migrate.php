<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* For detailed reference, please read https://ellislab.com/codeigniter/user-guide/libraries/migration.html */

class Migrate extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->library('migration');
		if($this->session->userdata('logged'))
		{
			if($this->session->userdata('user_meta')->usertype_id==99)
			{

			}
			else
			{
				redirect('program');
			}
		}
		else
		{
			redirect('program');
		}
	
	}

	function index()
	{
		//$migration_okay=$this->migration->current();  // set as defined in $config['migration_version'], in application/config/migration.php
		$migration_okay=$this->migration->latest();   // set to latest changes as applied in application/migrations
		//$migration_okay=$this->migration->version(2); // set to version N, as available in application/migrations
		if (! $migration_okay)
			show_error($this->migration->error_string());
		else
			echo 'Migrated to latest version';
	}

	function version()
	{
		$version=$this->uri->segment(3);
		$migration_okay=$this->migration->version($version);
		if (! $migration_okay)
			show_error($this->migration->error_string());
		else
			echo 'Migrated to version: '.$version;
	}

	function default_job_categ()
	{
		$job_categ=array(
			1 => 'Accounting',
			2 => 'General Business',
			3 => 'Admin & Clerical',
			4 => 'General Labor', 
			5 => 'Pharmaceutical',
			6 => 'Automotive',
			7 => 'Government',
			8 => 'Professional Services',
			9 => 'Banking',
			10 => 'Grocery',
			11 => 'Purchasing',
			12 => 'Procurement',
			13 => 'Biotech',
			14 => 'Health Care',
			15 => 'QA',
			16 => 'Quality Control',
			17 => 'Broadcast',
			18 => 'Journalism',
			19 => 'Hotel',
			20 => 'Hospitality',
			21 => 'Real Estate',
			22 => 'Business Development',
			23 => 'Human Resources',
			24 => 'Research',
			25 => 'Construction',
			26 => 'Information Technology',
			27 => 'Restaurant',
			28 => 'Food Service',
			29 => 'Consultant',
			30 => 'Installation',
			31 => 'Maint',
			32 => 'Repair',
			33 => 'Retail',
			34 => 'Customer Service',
			35 => 'Insurance',
			36 => 'Sales',
			37 => 'Design',
			38 => 'Inventory',
			39 => 'Science',
			40 => 'Distribution',
			41 => 'Shipping',
			42 => 'Legal',
			43 => 'Skilled Labor',
			44 => 'Trades',
			45 => 'Education',
			46 => 'Teaching',
			47 => 'Legal Admin',
			48 => 'Strategy',
			49 => 'Planning',
			50 => 'Engineering',
			51 => 'Management',
			52 => 'Supply Chain',
			53 => 'Entry Level',
			54 => 'New Grad',
			55 => 'Manufacturing',
			56 => 'Telecommunications',
			57 => 'Executive',
			58 => 'Marketing',
			59 => 'Training',
			60 => 'Facilities',
			61 => 'Media',
			62 => 'Journalism',
			63 => 'Newspaper',
			64 => 'Transportation',
			65 => 'Finance',
			66 => 'Nonprofit',
			67 => 'Social Services',
			68 => 'Warehouse'
		);

		foreach($job_categ as $values)
		{
			$this->db->set('job_category_name', $values);
			$this->db->insert('job_categories');
		}
	}

	function fix_added_at()
	{
		$this->db->where('billing_type',2);
		$this->db->group_by('billing_for');
		$query=$this->db->get('billing');
		foreach($query->result() as $rows)
		{
			$this->db->set('added_at',$rows->billing_date);
			$this->db->where('event_id',$rows->billing_for);
			$this->db->update('events');
		}
	}


}
