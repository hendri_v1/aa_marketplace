<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* For detailed reference, please read https://ellislab.com/codeigniter/user-guide/libraries/migration.html */

class Mail_test extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		
		$this->load->model('user_role_model');
		$this->load->model('userpost_model');
		$this->load->model('billing_model');
		$this->load->model('venue_model');
		$this->load->model('event_model');
	}
	
	function welcome_email($send_mail="yes",$to="h")
	{
		$data['user_meta']=$this->user_role_model->get_user_by_id(1);
		if($send_mail=="yes")
		{
			$content = $this->load->view('mailing_view/mail_welcome', $data, TRUE);
			if($to=="h")
				$to="hendriansah@live.com";
			else
				$to="eenx2007@gmail.com";
			$this->global_model->sendmailMandrill2($to,'','HRplasa Email Welcome Test',$content);	
		}
		else
		{
			$this->load->view('mailing_view/mail_welcome', $data);
		}
	}
	
	function new_event($send_mail="yes",$to="h")
	{
		
		$data['event_meta']=$this->event_model->get_event_by_id(1)->row();
		if($send_mail=="yes")
		{
			$content = $this->load->view('mailing_view/mailing_new_event', $data, TRUE);
			if($to=="h")
				$to="hendriansah@live.com";
			else
				$to="eenx2007@gmail.com";
			$this->global_model->sendmailMandrill2($to,'','HRplasa Email Welcome Test',$content);	
		}
		else
		{
			$this->load->view('mailing_view/mailing_new_event', $data);
		}
	}
	
	function event_approve($send_mail="yes",$to="h")
	{
		
		$data['event_meta']=$this->event_model->get_event_by_id(1)->row();
		if($send_mail=="yes")
		{
			$content = $this->load->view('mailing_view/mail_event_approval', $data, TRUE);
			if($to=="h")
				$to="hendriansah@live.com";
			else
				$to="eenx2007@gmail.com";
			$this->global_model->sendmailMandrill2($to,'','HRplasa Event Approval Test',$content);	
		}
		else
		{
			$this->load->view('mailing_view/mail_event_approval', $data);
		}
	}
	
	function send_billing($billing_id,$send_mail="yes",$to="h")
	{
		$row=$this->billing_model->get_billing_by_id($billing_id)->row();
		$data['bill_meta']=$row;
		$billing_type=$row->billing_type;
		$the_user='';
		$the_title='';
		if($billing_type==1)
		{
			$rowproject=$this->userproject_model->get_single_project($row->billing_for);
			$the_user=$rowproject->userprofile_firstname.' '.$rowproject->userprofile_lastname;
			$the_title="Project ".$rowproject->project_title;
		}
		elseif($billing_type==2)
		{
			$rowevent=$this->event_model->get_event_by_id($row->billing_for)->row();
			$the_user=$rowevent->userprofile_firstname.' '.$rowevent->userprofile_lastname;
			$the_title="Event ".$rowevent->event_title;	
		}
		elseif($billing_type==3)
		{
			$rowuser=$this->user_role_model->get_user_by_id($row->billing_for);
			$the_user=$rowuser->userprofile_firstname.' '.$rowuser->userprofile_lastname;
			$the_title="Upgrade Membership";	
		}
		$data['the_user']=$the_user;
		$data['the_title']=$the_title;
		if($send_mail=="yes")
		{
			$content = $this->load->view('mailing_view/mail_invoice', $data, TRUE);
			if($to=="h")
				$to="hendriansah@live.com";
			else
				$to="eenx2007@gmail.com";
			$this->global_model->sendmailMandrill2($to,'','HRplasa Send Invoice Test',$content);	
		}
		else
		{
			$this->load->view('mailing_view/mail_invoice', $data);
		}	
	}
	
	function venue_submitted($venue_id,$send_mail="yes",$to="h")
	{
		$data['venue_meta']=$this->venue_model->get_venue_by_id($venue_id);
		if($send_mail=="yes")
		{
			$content = $this->load->view('mailing_view/mail_new_venue', $data, TRUE);
			if($to=="h")
				$to="hendriansah@live.com";
			else
				$to="eenx2007@gmail.com";
			$this->global_model->sendmailMandrill2($to,'','HRplasa New Venue Test',$content);	
		}
		else
		{
			$this->load->view('mailing_view/mail_new_venue', $data);
		}
	}

	function send_mail_bulk($preview=false)
	{
		$query=$this->user_role_model->get_users();
		$to=array();
		foreach($query->result() as $rows)
		{	
			$to[]=array('email'=>$rows->user_email,'name' => $rows->userprofile_firstname);
		}
		$query=$this->event_model->get_next_week();
		$data['query']=$query;
		$data['total_event_next_week']=$query->num_rows();
		if($data['total_event_next_week']>0)
		{
			if($preview==false)
			{
				$content = $this->load->view('mailing_view/mail_campaign',$data,TRUE);
				echo $this->global_model->sendmailMandrillBulk($to,'','HRPlasa Test email '.rand(),$content);
			}
			else
			{
				$this->load->view('mailing_view/mail_campaign',$data);
			}
		}
	}
	
}
