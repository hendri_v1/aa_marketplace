<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('userpost_model');
		$this->load->model('user_role_model');

	}

	public function index()
	{
		$querycateg=$this->global_model->get_category();
		    foreach($querycateg->result() as $rowscateg)
		    {
		        $category_id[$rowscateg->category_id]=$rowscateg->category_name;
		    }
		$data['cate'] = $category_id;
		$data['title']="Post Tulisan Baru";
		$this->load->view('post_include/post',$data,false);
		
	}
	
	public function save_post()
	{
		/*slugnya mau di gimanain aku bingung :D*/
		$set_array=array(
			"post_title"=>$this->input->post('title'),
			"post_content"=>$this->input->post('content'),
			"post_type_id"=>$this->input->post('post_category_id'),
			"post_addedat"=>strtotime(date('Y-m-d')),
			"post_updateat"=>strtotime(date('Y-m-d')),
			"post_owner"=>$this->session->userdata('user_meta')->user_id
		);
		$post_idnya=$this->user_role_model->save_post($set_array);
		redirect('post/post/view_post/'.$post_idnya.'/'.url_title($set_array['post_title']));
	}

	function edit_post($post_id)
	{
		$querycateg=$this->global_model->get_category();
	    foreach($querycateg->result() as $rowscateg)
	    {
	        $category_id[$rowscateg->category_id]=$rowscateg->category_name;
	    }
		$data['cate'] = $category_id;
		$data['row']=$this->userpost_model->get_post_by_id($post_id)->row();
		$this->load->view('post_include/edit_post',$data);
	}

	function update_post()
	{
		$set_array=array(
			"post_title"=>$this->input->post('title'),
			"post_content"=>$this->input->post('content'),
			"post_type_id"=>$this->input->post('post_category_id'),
			"post_updateat"=>strtotime(date('Y-m-d')),
		);
		$this->userpost_model->update_post($set_array,$this->input->post('post_id'));
		redirect('post/post/view_post/'.$this->input->post('post_id').'/'.url_title($set_array['post_title']));
	}

	public function delete_post($post_id)
	{
		$this->user_role_model->delete_post($post_id);
		redirect('post/my_post');
	}



	public function view_post($id=0)
	{
		$data['query']=$this->userpost_model->get_post_by_id($id)->row_array();
		//echo $this->db->last_query();
		$data['title']=$data['query']['post_title'];
		$data['meta_def'] = strip_tags(word_limiter($data['query']['post_content'], 50));
		$this->load->view('post_include/view_post',$data,false);
	}

	public function read_blog($id=0)
	{
		$data['query']=$this->userpost_model->get_blog_by_id($id)->row();
		//echo $this->db->last_query();
		$data['title']=$data['query']->post_title;
		$data['meta_def'] = strip_tags(word_limiter($data['query']->post_content, 50));
		$this->load->view('post_include/read_blog',$data,false);
	}

	public function comment()
	{
		$this->load->view('post_include/add_comment');
	}

	public function add_comment()
	{
		$set_array=array(
			'user_id'=>$this->session->userdata('user_id'),
			'comment_type'=>2,
			'comment_for'=>$this->uri->segment(4),
			'comment_text'=>$this->input->post('comment_text'),
			'comment_date'=>strtotime(date('Y-m-d H:i:s')),
		);
		$this->userproject_model->save_comment($set_array);
		redirect('post/post/view_post/'.$this->uri->segment(4));
	}

	function vacancy()
	{
		$data['title']="Lowongan";
		$this->load->view('post_include/vacancy',$data);
	}

	function save_vacancy()
	{
		/*slugnya mau di gimanain aku bingung :D*/
		$content=$this->input->post('job_description').'<!---splitter--->'.$this->input->post('job_requirement');
		$set_array=array(
			"post_title"=>$this->input->post('title'),
			"post_content"=>$content,
			"post_type_id"=>$this->input->post('post_category_id'),
			"post_addedat"=>strtotime(date('Y-m-d')),
			"post_updateat"=>strtotime(date('Y-m-d')),
			"post_owner"=>$this->session->userdata('user_meta')->user_id
		);
		$post_idnya=$this->user_role_model->save_post($set_array);
		redirect('post/post/view_post/'.$post_idnya.'/'.url_title($set_array['post_title']));
	}

	

	public function delete_img_event()
	{
		$event_single=$this->event_model->get_event_by_id($this->uri->segment(4))->row_array();

		if ($this->uri->segment(4) == '') {
			show_404();
		}
		$config['upload_path'] = './uploads/event_images/';
		if (file_exists($config['upload_path'].$event_single['event_picture'])) {
    		unlink($config['upload_path'].$event_single['event_picture']);
    	}
		$this->userpost_model->dell_img_event($this->uri->segment(4));
		redirect('event/view/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
	}

}

/* End of file post.php */
/* Location: ./application/controllers/post/post.php */