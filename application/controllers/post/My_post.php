<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_post extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                if(!$this->session->userdata('logged'))
		{
			redirect('program');			
		}
		$this->load->model('userpost_model');
		$this->load->model('event_model');
		
	}

	function index()
	{
		$this->load->helper('pagination_style');
		$allData=$this->userpost_model->get_mypost(0,0);
		$query = $allData->result_array();
		$offset = $this->uri->segment(3);
		$segment = 3;
		$base_url = site_url('project/my_post/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);

		$data['title']="My Posts";
		$data['query']=$this->userpost_model->get_mypost($per_page,$offset);
		$data['child_nav']="bms";
		$this->load->view('post_include/my_post',$data,false);
	}

	

}

/* End of file my_post.php */
/* Location: ./application/controllers/post/my_post.php */