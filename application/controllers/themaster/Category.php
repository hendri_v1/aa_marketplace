<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged') OR $this->session->userdata('user_meta')->usertype_id != 99) {
            redirect('program');
        }
        $this->load->model('category_model');
    }

    public function index() {
        $data['title'] = 'Category Management';
        $this->load->view('admin_include/category', $data, FALSE);
    }

    function type($type = 1) {
        $data['categories'] = $this->category_model->getAll(0, 0, $type)->result();
        $this->load->view('admin_include/category_type', $data, false);
    }

    function edit($category_id = '') {
        if ($this->input->post('save')) {
            $category['category_name'] = $this->input->post('category_name');
            $category['category_for'] = $this->input->post('category_for');
            $this->category_model->save($category, $category_id);
        }
        $data['category'] = $this->category_model->find($category_id)->row();
        $this->load->view('admin_include/category_edit', $data, false);
    }

    function add() {
        $this->load->library('form_validation');
        if ($this->input->post('save')) {
            $this->form_validation->set_rules('category_name', 'Judul Category', 'required');
            $this->form_validation->set_rules('category_for', 'Category Untuk', 'required');
            if ($this->form_validation->run() == TRUE) {
                $category['category_name'] = $this->input->post('category_name');
                $category['category_for'] = $this->input->post('category_for');
                if ($this->category_model->add($category)) {
                    $this->session->set_flashdata('success', 'Category telah ditambahkan');
                    redirect(current_url());
                }
            }
        }
        $this->load->view('admin_include/category_add', '', false);
    }

    function delete() {
        $category_id = $this->input->post('category_id');
        $delete = $this->category_model->remove($category_id) ? 1 : 0;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => $delete)));
    }

}
