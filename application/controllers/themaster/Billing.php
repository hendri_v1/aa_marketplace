<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged') OR $this->session->userdata('user_meta')->usertype_id != 99) {
            redirect('program');
        }
        $this->load->model('billing_model');
        $this->load->model('userproject_model');
        $this->load->model('userpost_model');
        $this->load->model('user_role_model');
        $this->load->model('event_model');
    }

    function send_invoice($billing_id, $preview = "yes") {
        $row = $this->billing_model->get_billing_by_id($billing_id)->row();
        $data['bill_meta'] = $row;
        $billing_type = $row->billing_type;
        $the_user = '';
        $the_title = '';
        $email_to = '';
        if ($billing_type == 1) {
            $rowproject = $this->userproject_model->get_single_project($row->billing_for);
            $the_user = $rowproject->userprofile_firstname . ' ' . $rowproject->userprofile_lastname;
            $the_title = "Invoice : Project " . $rowproject->project_title;
            $email_to = $rowproject->user_email;
        } elseif ($billing_type == 2) {
            $rowevent = $this->event_model->get_event_by_id($row->billing_for)->row();
            $the_user = $rowevent->userprofile_firstname . ' ' . $rowevent->userprofile_lastname;
            $the_title = "Invoice : Event " . $rowevent->event_title;
            $email_to = $rowevent->user_email;
        } elseif ($billing_type == 3) {
            $rowuser = $this->user_role_model->get_user_by_id($row->billing_for);
            $the_user = $rowuser->userprofile_firstname . ' ' . $rowuser->userprofile_lastname;
            $the_title = "Invoice : Registration";
            $email_to = $rowuser->user_email;
        }
        $data['the_user'] = $the_user;
        $data['the_title'] = $the_title;
        if ($preview == "no") {
            $content = $this->load->view('mailing_view/mail_invoice', $data, TRUE);
            $this->global_model->sendmailMandrill2($email_to, '', $the_title, $content);
            echo "Invoice terkirim ke " . $email_to;
        } else {
            $this->load->view('mailing_view/mail_invoice', $data);
        }
    }

    function pending() {
        $data['query'] = $this->billing_model->get_all_billing_grouped();
        $this->load->view('admin_include/pending_billing', $data);
    }

    function paid(){
        $data['query'] = $this->billing_model->get_all_billing_grouped(TRUE);
        $this->load->view('admin_include/paid_billing',$data);
    }

}
