<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged') OR $this->session->userdata('user_meta')->usertype_id != 99) {
            redirect('program');
        }
        $this->load->model('userproject_model');
        $this->load->model('user_role_model');
    }

    function index() {
        $data['title'] = "Member Management";
        $data['nav_side'] ="mem_admin";
        $data['child_nav'] ="mem_admin";
        $this->load->view('admin_include/member', $data, false);
    }

    function pending_user() {
        $this->load->helper('pagination_style');
        $allData = $this->user_role_model->get_user_paginate(0, 0, false, false, true);
        $query = $allData->result_array();
        $offset = $this->uri->segment(4);
        $segment = 4;
        $base_url = site_url('themaster/member/pending_user');
        $per_page = 10;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryuser'] = $this->user_role_model->get_user_paginate($per_page, $offset, false, false, true);
        $this->load->view('admin_include/pending_member', $data, false);
    }

    function aktif_user() {
        $this->load->helper('pagination_style');
        $allData = $this->user_role_model->get_user_paginate(0, 0, false, false, false);
        $query = $allData->result_array();
        $offset = $this->uri->segment(4);
        $segment = 4;
        $base_url = site_url('themaster/member/aktif_user');
        $per_page = 10;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryuser'] = $this->user_role_model->get_user_paginate($per_page, $offset, false, false, false);
        $this->load->view('admin_include/aktif_member', $data, false);
    }

    function premium_user() {
        $this->load->helper('pagination_style');
        $allData = $this->user_role_model->get_user_paginate(0, 0, true, false, false);
        $query = $allData->result_array();
        $offset = $this->uri->segment(4);
        $segment = 4;
        $base_url = site_url('themaster/member/premium_user');
        $per_page = 10;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryuser'] = $this->user_role_model->get_user_paginate($per_page, $offset, true, false, false);
        $this->load->view('admin_include/premium_member', $data, false);
    }

    function search_user() {
        $this->load->helper('pagination_style');
        $allData = $this->user_role_model->get_user_paginate(0, 0, FALSE, false, false);
        $query = $allData->result_array();
        $offset = $this->uri->segment(4);
        $segment = 4;
        $base_url = site_url('themaster/member/search_user');
        $per_page = 10;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryuser'] = $this->user_role_model->get_user_paginate($per_page, $offset, FALSE, false, false);
        $this->load->view('admin_include/search_member', $data, false);
    }

    function search_user_results() {
        $this->load->helper('pagination_style');
        $search = $this->input->get('keywords');
        $allData = $this->user_role_model->get_user_paginate(0, 0, FALSE, false, false, $search);
        $query = $allData->result_array();
        $offset = $this->uri->segment(4);
        $segment = 4;
        $base_url = site_url('themaster/member/search_user_results');
        $per_page = 100;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryuser'] = $this->user_role_model->get_user_paginate($per_page, $offset, FALSE, false, false, $search);
        $this->load->view('admin_include/search_member_results', $data, false);
    }

    function view_user($user_id) {
        $data['row'] = $this->user_role_model->get_user_by_id($user_id);
        $data['querybilling'] = $this->userproject_model->get_single_billing(3, $user_id);
        $data['title']=$data['row']->username;
        $data['nav_side']="mem_admin";
        $data['child_nav']='mem_admin';
        $this->load->view('admin_include/view_member', $data, false);
    }

    function payment($billing_id, $user_id) {
        $data['billing_id'] = $billing_id;
        $data['user_id'] = $user_id;
        $this->load->view('admin_include/payment_user', $data);
    }

    function payment_save() {
        $save_array = array(
            'payment_date' => $this->input->post('payment_date'),
            'payment_total' => $this->input->post('payment_total'),
            'payment_method' => $this->input->post('payment_method'),
            'payment_notes' => $this->input->post('payment_notes'),
            'billing_id' => $this->input->post('billing_id')
        );
        $this->userproject_model->save_payment($save_array);
        $this->userproject_model->update_billing($this->input->post('billing_id'));
        redirect('themaster/member/view_user/' . $this->input->get('user_id'));
    }

    function upgrade_premium_now($user_id) {
        $this->db->set('is_premium', 1);
        $this->db->where('user_id', $user_id);
        $this->db->update('user');
        redirect('themaster/member/view_user/' . $user_id);
    }

    function upgrade_patnership($user_id) {
        $this->db->set('is_patnership', 1);
        $this->db->where('user_id', $user_id);
        $this->db->update('user');
        redirect('themaster/member');
    }

    function act_like_this($user_id) {
        $this->load->model('userprofile_model');
        $this->session->set_userdata('logged', true);
        $this->session->set_userdata('user_id', $user_id);
        $this->session->set_userdata('user_meta', $this->user_role_model->get_user_by_id($user_id, true));
        $categrow = $this->userprofile_model->get_user_category($user_id);
        $this->session->set_userdata('category_id', $categrow->category_id);
        //$this->user_role_model->update_last_login($user_id);
        redirect('program');
    }

}
