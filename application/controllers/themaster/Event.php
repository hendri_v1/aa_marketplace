<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged') OR $this->session->userdata('user_meta')->usertype_id != 99) {
            redirect('program');
        }
        $this->load->model('userproject_model');
        $this->load->model('userpost_model');
        $this->load->model('event_model');
        $this->load->model('billing_model');
    }

    function index() {
        $data['title'] = "Event Management";
        $data['nav_side'] = "em_admin";
        $this->load->view('admin_include/event', $data, false);
    }

    function pending_event() {
        $order_by = $this->input->get('order_by');
        $order_events_by = 0;
        switch ($order_by) {
            case 'start_date_asc':
                $order_events_by = 0;
                break;
            case 'start_date_desc':
                $order_events_by = 1;
                break;
            case 'title_asc':
                $order_events_by = 2;
                break;
            case 'title_desc':
                $order_events_by = 3;
                break;
            case 'event_organizer':
                $order_events_by = 4;
                break;
            default:
                break;
        }
        $data['query'] = $this->event_model->get_event_paginate(0, 0, false, true, FALSE, 0, 0, 0, '', $order_events_by);
        $this->load->view('admin_include/pending_event', $data, false);
    }

    function pending_featured() {
        $order_by = $this->input->get('order_by');
        $order_events_by = 0;
        switch ($order_by) {
            case 'start_date_asc':
                $order_events_by = 0;
                break;
            case 'start_date_desc':
                $order_events_by = 1;
                break;
            case 'title_asc':
                $order_events_by = 2;
                break;
            case 'title_desc':
                $order_events_by = 3;
                break;
            case 'event_organizer':
                $order_events_by = 4;
                break;
            default:
                break;
        }
        $data['query'] = $this->event_model->get_featured_list(0, 0, true, $order_events_by);
        $this->load->view('admin_include/pending_featured', $data, false);
    }

    function active_event() {
        $order_by = $this->input->get('order_by');
        $order_events_by = 0;
        switch ($order_by) {
            case 'start_date_asc':
                $order_events_by = 0;
                break;
            case 'start_date_desc':
                $order_events_by = 1;
                break;
            case 'title_asc':
                $order_events_by = 2;
                break;
            case 'title_desc':
                $order_events_by = 3;
                break;
            case 'event_organizer':
                $order_events_by = 4;
                break;
            default:
                break;
        }
        $data['query'] = $this->event_model->get_event_paginate(0, 0, true, false, FALSE, 0, 0, 0, '', $order_events_by);
        $this->load->view('admin_include/pending_event', $data, false);
    }

    function active_featured() {
        $order_by = $this->input->get('order_by');
        $order_events_by = 0;
        switch ($order_by) {
            case 'start_date_asc':
                $order_events_by = 0;
                break;
            case 'start_date_desc':
                $order_events_by = 1;
                break;
            case 'title_asc':
                $order_events_by = 2;
                break;
            case 'title_desc':
                $order_events_by = 3;
                break;
            case 'event_organizer':
                $order_events_by = 4;
                break;
            default:
                break;
        }
        $data['query'] = $this->event_model->get_featured_list(0, 0, FALSE, $order_events_by);
        $this->load->view('admin_include/active_featured', $data, false);
    }

    function view_event($event_id) {
        $data['query'] = $this->event_model->get_event_by_id($event_id)->row_array();
        $data['querybilling'] = $this->billing_model->get_single_billing(2, $event_id);
        $data['title']=$data['query']['event_title'];
        $data['nav_side']="em_admin";
        $this->load->view('admin_include/view_event', $data, false);
    }

    function payment($billing_id, $event_id) {
        $data['billing_id'] = $billing_id;
        $data['event_id'] = $event_id;
        $this->load->view('admin_include/payment_event', $data);
    }

    function payment_save() {
        $save_array = array(
            'payment_date' => $this->input->post('payment_date'),
            'payment_total' => $this->input->post('payment_total'),
            'payment_method' => $this->input->post('payment_method'),
            'payment_notes' => $this->input->post('payment_notes'),
            'billing_id' => $this->input->post('billing_id')
        );
        $this->userproject_model->save_payment($save_array);
        $this->userproject_model->update_billing($this->input->post('billing_id'));
        redirect('themaster/event/view_event/' . $this->input->get('event_id'));
    }

    function publish_event($event_id) {
        $this->event_model->publish_event($event_id);
        $data['event_meta'] = $this->event_model->get_event_by_id($event_id)->row();
        $content = $this->load->view('mailing_view/mail_event_approval', $data, TRUE);
        $this->global_model->sendmailMandrill2($data['event_meta']->user_email, '', 'Event Anda Diterbitkan', $content);
        redirect('themaster/event/view_event/' . $event_id);
    }

    function upublish_event($event_id) {
        $this->event_model->unpublish_event($event_id);
        redirect('themaster/event/view_event/' . $event_id);
    }

    function category_management() {
        $data['query'] = $this->event_model->get_eventcategory();
        $this->load->view('admin_include/eventcategory_management', $data);
    }

    function save_new_category() {
        $this->event_model->save_category($this->input->post('eventcategory_name'));
        redirect('themaster/event/category_management');
    }

    function make_featured($event_id) {
        $this->event_model->make_featured($event_id);
        redirect('themaster/event');
    }

    function stop_featured($event_id) {
        $this->event_model->stop_featured($event_id);
        redirect('themaster/event');
    }

}
