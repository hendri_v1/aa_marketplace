<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Venue extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged') OR $this->session->userdata('user_meta')->usertype_id != 99) {
            redirect('program');
        }
        $this->load->model('venue_model');
    }

    function index() {
        $data['title']="Venue Management";
        $data['nav_side']="vn_admin";
        $data['chlid_nav']="vn_admin";
        $this->load->view('admin_include/venue',$data);
    }

    function pending_venue() {
        $this->load->helper('pagination_style');
        $alldata = $this->venue_model->get_venue(0, 0, false, true);
        $query = $alldata->result_array();
        $offset = $this->uri->segment(4);
        $segment = 4;
        $base_url = site_url('themaster/venue/pending_venue');
        $per_page = 10;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryvenue'] = $this->venue_model->get_venue($per_page, $offset, false, true);
        $this->load->view('admin_include/pending_venue', $data);
    }
	
	function active_venue(){
		$this->load->helper('pagination_style');
			$alldata = $this->venue_model->get_venue(0, 0, true, false);
        $query = $alldata->result_array();
        $offset = $this->uri->segment(4);
        $segment = 4;
        $base_url = site_url('themaster/venue/pending_venue');
        $per_page = 10;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryvenue'] = $this->venue_model->get_venue($per_page, $offset, true, false);
        $this->load->view('admin_include/active_venue', $data);
	}

    function activate($venue_main_id) {
        $this->venue_model->activate_venue($venue_main_id);
		  $data['venue_meta']=$this->venue_model->get_venue_main_by_id($venue_main_id);
		  $content = $this->load->view('mailing_view/mail_active_venue', $data, TRUE);
		  $this->global_model->sendmailMandrill2($data['venue_meta']->user_email,'','Venue Anda Telah Diterbitkan',$content);
        redirect('themaster/venue');
    }

}