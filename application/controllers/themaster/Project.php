<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged') OR $this->session->userdata('user_meta')->usertype_id != 99) {
            redirect('program');
        }
        $this->load->model('userproject_model');
    }

    function index() {
        $data['title'] = "Project Management";
        $this->load->view('admin_include/project', $data, false);
    }

    function pending_project() {
        $this->load->helper('pagination_style');
        $allData = $this->userproject_model->get_projects_paginate(false, false, false, false, 0, 0, true);
        $query = $allData->result_array();
        $offset = $this->uri->segment(4);
        $segment = 4;
        $base_url = site_url('themaster/project/pending_project');
        $per_page = 10;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryproject'] = $this->userproject_model->get_projects_paginate(false, false, false, false, $per_page, $offset, true);
        $this->load->view('admin_include/pending_project_list', $data, false);
    }

    function active_project() {
        $this->load->helper('pagination_style');
        $allData = $this->userproject_model->get_projects_paginate(false, false, false, true, 0, 0, false);
        $query = $allData->result_array();
        $offset = $this->uri->segment(4);
        $segment = 4;
        $base_url = site_url('themaster/project/active_project');
        $per_page = 10;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryproject'] = $this->userproject_model->get_projects_paginate(false, false, false, true, $per_page, $offset, false);
        $this->load->view('admin_include/pending_project_list', $data, false);
    }

    public function view_project($project_id) {
        $data['row'] = $this->userproject_model->get_single_project($project_id);
        $data['queryprovince'] = $this->global_model->get_province();
        $querybid = $this->userproject_model->get_bidder($project_id);
        $data['totalbids'] = $querybid->num_rows();
        $data['querybid'] = $querybid;
        $data['title'] = $data['row']->project_title;
        $data['project_keywords'] = $this->global_model->get_keywords(2, $data['row']->project_id);
        $data['querybilling'] = $this->userproject_model->get_single_billing(1, $project_id);
        $this->load->view('admin_include/view_project', $data, false);
    }

    public function publish($project_id) {
        $this->userproject_model->publish($project_id);
        $holder = $this->userproject_model->get_single_project($project_id);
        //send mail user
        $to = $holder->user_email;
        $cc = '';
        $subject = 'Selamat Project ' . $holder->project_title . ' telash aktif';
        $data['arraybid'] = array(
            'project_name' => $holder->project_title,
            'name' => $holder->userprofile_firstname,
        );
        $content = $this->load->view('mailing_view/mail_project_published', $data, TRUE);
        //send mail owner
        $this->global_model->sendmailMandrill2($to, $cc, $subject, $content);
        redirect('themaster/project/view_project/' . $project_id);
    }

    public function reject($project_id) {
        $this->userproject_model->reject($project_id);
        $holder = $this->userproject_model->get_single_project($project_id);
        //send mail user
        $to = $holder->user_email;
        $cc = '';
        $subject = 'Mohon Maaf Project ' . $holder->project_title . ' kami tolak';
        $data['arraybid'] = array(
            'project_name' => $holder->project_title,
            'name' => $holder->userprofile_firstname,
        );
        $content = $this->load->view('mailing_view/mail_project_rejected', $data, TRUE);
        //send mail owner
        $this->global_model->sendmailMandrill2($to, $cc, $subject, $content);
        redirect('themaster/project/view_project/' . $project_id);
    }

    public function send_invoice($project_id) {
        $billing = $this->userproject_model->get_single_billing(1, $project_id);
        $billingId = $billing->result_array();
        $querydetbilling = $this->userproject_model->get_detail_billing($billingId[0]['billing_id']);
        $holder = $this->userproject_model->get_single_project($project_id);
        //send mail user
        $to = $holder->user_email;
        $cc = '';
        $subject = 'Invoice Project ' . $holder->project_title;
        $data['arraybid'] = array(
            'project_name' => $holder->project_title,
            'name' => $holder->userprofile_firstname,
            'querybilling' => $billing,
            'querydetbilling' => $querydetbilling,
        );
        $content = $this->load->view('mailing_view/mail_project_invoice', $data, true);
        //send mail owner
        $this->global_model->sendmailMandrill2($to, $cc, $subject, $content);
        redirect('themaster/project/view_project/' . $project_id);
    }

    function get_billing_detail($billing_id) {
        $data['querydetbilling'] = $this->userproject_model->get_detail_billing($billing_id);
        $this->load->view('admin_include/detail_billing', $data, false);
    }

    function payment($billing_id, $project_id) {
        $data['billing_id'] = $billing_id;
        $data['project_id'] = $project_id;
        $this->load->view('admin_include/payment', $data);
    }

    function payment_save() {
        $ymd = explode("/", $this->input->post('payment_date'));
        $time = explode(" ", $ymd[2]);
        $tanggal = $time[0] . '-' . $ymd[1] . '-' . $ymd[0] . ' ' . $time[1];
        $date = strtotime($tanggal);
        $save_array = array(
            'payment_date' => $date,
            'payment_total' => $this->input->post('payment_total'),
            'payment_method' => $this->input->post('payment_method'),
            'payment_notes' => $this->input->post('payment_notes'),
            'billing_id' => $this->input->post('billing_id')
        );
        $this->userproject_model->save_payment($save_array);
        $this->userproject_model->update_billing($this->input->post('billing_id'));
        redirect('themaster/project/view_project/' . $this->input->get('project_id'));
    }

    function payment_history($billing_id) {
        $data['query'] = $this->userproject_model->get_payment_by_id($billing_id);
        $this->load->view('admin_include/payment_history', $data, false);
    }

}
