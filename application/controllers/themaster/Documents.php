<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged') OR $this->session->userdata('user_meta')->usertype_id != 99) {
            redirect('program');
        }
        $this->load->model('userpost_model');
    }

    function index() {
        $data['query'] = $this->userpost_model->get_documents();
        $this->load->view('admin_include/documents', $data, false);
    }

    function add() {
        $set_array = array(
            'document_title' => $this->input->post('document_title'),
            'document_url' => $this->input->post('document_url')
        );
        $this->userpost_model->save_documents($set_array);
        redirect('themaster/documents');
    }

}
