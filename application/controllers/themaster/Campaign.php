<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Campaign extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged') OR $this->session->userdata('user_meta')->usertype_id != 99) {
            redirect('program');
        }
        $this->load->model('event_model');
        $this->load->model('user_role_model');
        $this->load->model('userpost_model');
    }

    function index() {
        $data['nav_side']="marketingnav";
        $data['title']="Campaign Manager";
        $this->load->view('admin_include/campaign_manager',$data);
    }

    function add_new() {
        $data['all_active_event'] = $this->event_model->get_event_paginate(0, 0, TRUE);
        $data['all_blog'] = $this->userpost_model->get_blog();
        $data['title']="New Campaign";
        $data['nav_side']="marketingnav";
        $this->load->view('admin_include/new_campaign', $data);
    }

    function preview_campaign() {
        $data['row_event'] = $this->event_model->get_event_by_id($this->input->post('event_id'))->row();
        $data['row_blog'] = $this->userpost_model->get_post_by_id($this->input->post('post_id'))->row();
        $data['campaign_title'] = $this->input->post('campaign_title');
        $data['campaign_word'] = $this->input->post('campaign_word');
        $content = $this->load->view('mailing_view/mail_marketing_campaign', $data, TRUE);
        echo $content;
    }

    function send_campaign($preview = false) {
        $data['row_event'] = $this->event_model->get_event_by_id($this->input->post('event_id'))->row();
        $data['row_blog'] = $this->userpost_model->get_post_by_id($this->input->post('post_id'))->row();
        $data['campaign_title'] = $this->input->post('campaign_title');
        $data['campaign_word'] = $this->input->post('campaign_word');
        $query = $this->user_role_model->get_users();
        $additional_recipents = $this->input->post('additional_recipents');
        $the_additional = explode(';', $additional_recipents);
        $to = array();
        if ($this->input->post('recipents') == 1) {
            foreach ($query->result() as $rows) {
                if ($rows->user_email <> '')
                    $to[] = array('email' => $rows->user_email, 'name' => $rows->userprofile_firstname);
            }
        }
        elseif ($this->input->post('recipents') == 2) {
            foreach ($the_additional as $value) {
                $to[] = array('email' => $value, 'name' => $value);
            }
        } elseif ($this->input->post('recipents') == 3) {
            foreach ($query->result() as $rows) {
                if ($rows->user_email <> '')
                    $to[] = array('email' => $rows->user_email, 'name' => $rows->userprofile_firstname);
            }
            foreach ($the_additional as $value) {
                $to[] = array('email' => $value, 'name' => $value);
            }
        }
        $data['query'] = $query;
        if ($preview == false) {
            $content = $this->load->view('mailing_view/mail_marketing_campaign', $data, TRUE);
            $this->global_model->sendmailMandrillBulk2($to, '', $this->input->post('campaign_title'), $content);
            redirect('themaster/campaign');
        } else {
            $this->load->view('mailing_view/mail_marketing_campaign', $data);
        }
    }

}
