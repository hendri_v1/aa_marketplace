<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('userpost_model');
		
	}

	public function index()
	{
		$this->load->helper('pagination_style');
		$allData=$this->userpost_model->get_blog(0,0);
		$query = $allData->result_array();
		$offset = $this->uri->segment(2);
		$segment = 2;
		$base_url = site_url('blog/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);

		$data['title']="Blog";
		$data['query']=$this->userpost_model->get_blog($per_page,$offset);

		$this->load->view('blog_include/blog',$data,false);		
	}

	function jobs()
	{
		$this->load->helper('pagination_style');
		$allData=$this->userpost_model->get_job(0,0);
		$query = $allData->result_array();
		$offset = $this->uri->segment(2);
		$segment = 2;
		$base_url = site_url('jobs/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);

		$data['title']="Daftar Lowongan";
		$data['query']=$this->userpost_model->get_job($per_page,$offset);

		$this->load->view('jobs',$data,false);	
	}

	public function read($id=0)
	{
		$data['row']=$this->userpost_model->get_blog_by_id($id)->row();
		//echo $this->db->last_query();
		$data['title']=$data['row']->post_title;
		$data['meta_def'] = strip_tags(word_limiter($data['row']->post_content, 50));
		$texthtml = $data['row']->post_content;
	    ob_start();
	    ob_end_clean();
	    $html= $texthtml;

	    $doc = new DOMDocument();
	    @$doc->loadHTML($html);
	    $img = $doc->getElementsByTagName('img');
	    $data['featured_image']='';
	    if ($img->item(0) == null)
	    {
	    	$data['featured_image']='';
	    }
	    else
		    $data['featured_image']=$img->item(0)->getAttribute('src');
		$this->load->view('blog_include/read_blog',$data,false);
	}



}

/* End of file blog.php */
/* Location: ./application/controllers/blog/blog.php */