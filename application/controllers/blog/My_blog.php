<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                if(!$this->session->userdata('logged'))
		{
			redirect('program');			
		}
		$this->load->model('userpost_model');
		$this->load->model('event_model');
		$this->load->model('user_role_model');
		
	}

	function index()
	{
		$this->load->helper('pagination_style');
		$allData=$this->userpost_model->get_mypost(0,0);
		$query = $allData->result_array();
		$offset = $this->uri->segment(4);
		$segment = 4;
		$base_url = site_url('blog/my_blog/index/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);

		$data['title']="My Blog Post";
		$data['query']=$this->userpost_model->get_mypost($per_page,$offset);
		$data['child_nav']="bms_side";
		$data['nav_side']="bms";
		$this->load->view('blog_include/my_blog',$data,false);
	}

	function add_new()
	{
		$querycateg=$this->global_model->get_category();
	    foreach($querycateg->result() as $rowscateg)
	    {
	        $category_id[$rowscateg->category_id]=$rowscateg->category_name;
	    }
		$data['cate'] = $category_id;
		$data['title']="Post Tulisan Baru";
		$data['nav_side']="bms";
		$data['child_nav']="bms_new";
		$this->load->view('blog_include/new_post',$data,false);
	}

	public function save_post()
	{
		/*slugnya mau di gimanain aku bingung :D*/
		$set_array=array(
			"post_title"=>$this->input->post('title'),
			"post_content"=>$this->input->post('content'),
			"post_type_id"=>$this->input->post('post_category_id'),
			"post_addedat"=>strtotime(date('Y-m-d')),
			"post_updateat"=>strtotime(date('Y-m-d')),
			"post_owner"=>$this->session->userdata('user_meta')->user_id
		);
		$post_idnya=$this->user_role_model->save_post($set_array);
		redirect('blog/my_blog');
	}

	public function delete_post($post_id)
	{
		$this->user_role_model->delete_post($post_id);
		redirect('blog/my_blog');
	}

	function edit_post($post_id)
	{
		$querycateg=$this->global_model->get_category();
	    foreach($querycateg->result() as $rowscateg)
	    {
	        $category_id[$rowscateg->category_id]=$rowscateg->category_name;
	    }
		$data['cate'] = $category_id;
		$data['row']=$this->userpost_model->get_post_by_id($post_id)->row();
		$data['nav_side']="bms";
		$data['child_nav']="bms_new";
		$data['title']="Edit Post";
		$this->load->view('blog_include/edit_post',$data);
	}

	function update_post()
	{
		$set_array=array(
			"post_title"=>$this->input->post('title'),
			"post_content"=>$this->input->post('content'),
			"post_type_id"=>$this->input->post('post_category_id'),
			"post_updateat"=>strtotime(date('Y-m-d')),
		);
		$this->userpost_model->update_post($set_array,$this->input->post('post_id'));
		redirect('blog/my_blog');
	}
	

}

/* End of file my_post.php */
/* Location: ./application/controllers/post/my_post.php */