<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('event_model');
        $this->load->model('billing_model');
        $this->load->model('userprofile_model');
    }

    public function index() {
        
    }

    public function view_event($id = 0) {
        $registered = $this->event_model->get_registered_event($id)->row_array();
        if (!empty($registered)) {
            $data['registered'] = true;
        } else {
            $data['registered'] = false;
        }
        $data['province'] = $this->global_model->get_province();
        $event = $this->event_model->get_event_by_id($id)->row_array();
        if (isset($event['is_pending']) && $event['is_pending'] == 0) {
            if($event['is_private']==0)
            {
                if (!$this->session->userdata('logged') OR ( $this->session->userdata('user_id') != $event['event_owner'] )) {
                    redirect(site_url());
                }
            }
            
        }
        if($event['deleted_at']>0)
        {
            redirect(site_url());
        }
        /*
        if (isset($event['event_speaker']) && empty($event['event_speaker'])) {
            redirect(site_url());
        }
         * 
         */
        $data['query'] = $event;
        $result_related=$this->event_model->get_related_event($id);
        if(empty($result_related))
            $data['related']=array();
        else
            $data['related'] = $result_related->result();

        $data['comments'] = $this->event_model->get_comments(array('comment_for' => $id, 'comment_type' => 5))->result_array();
        $data['city'] = $this->global_model->get_city($data['query']['province_id'])->result_array();
        $data['audience'] = $this->event_model->get_audience($id)->result();
        $data['title'] = $data['query']['event_title'];
        $data['navactive'] = "browseallnav";
        $data['qeventspeakers'] = $this->event_model->get_speaker_by_event_id($id);
        $data['qeventtickets'] = $this->event_model->get_eventticket($id);
        $data['meta_def'] = strip_tags(word_limiter($data['query']['event_description'], 50));
        //update the totalview
        if ($this->session->userdata('user_id')) {
            if ($this->session->userdata('user_id') <> $data['query']['event_owner'])
                $this->event_model->update_total_view($id);
        } else {
            $this->event_model->update_total_view($id);
        }
        $data['map'] = TRUE;
        $this->load->view('event_include/view_event', $data, false);
    }

    function latest_event() {
        $this->load->helper('pagination_style');
        $data['title'] = "Direktori Event";
        $data['navactive'] = "browseallnav";
        //collecting the GET data
        $show_events_by = TRUE;
        if ($this->input->get('show_events_by') == 1)
            $show_events_by = FALSE;
        $province_id = $this->input->get('province_id');
        $city_id = $this->input->get('city_id');
        $eventcategory_id = $this->input->get('eventcategory_id');
        $event_keyword = $this->input->get('event_keyword');
        $order_events_by = $this->input->get('order_events_by');
        $min_price = str_replace('.', '', $this->input->get('min_price'));
        $max_price = str_replace('.', '', $this->input->get('max_price'));
        //endofcollecting GET data
        $allData = $this->event_model->get_event_paginate(0, 0, $show_events_by, FALSE, FALSE, $province_id, $city_id, $eventcategory_id, $event_keyword, $order_events_by, $min_price, $max_price);
        $query = $allData->result_array();
        $offset = $this->uri->segment(2);
        $segment = 2;
        $base_url = site_url('events');
        $per_page = 14;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment, TRUE,'#list-events');
        $data['highest_price'] = $this->event_model->get_max_price_ticket();
        $data['total_rows'] = $total_rows;
        $data['province'] = $this->global_model->get_province();
        $data['queryevents'] = $this->event_model->get_event_paginate($per_page, $offset, $show_events_by, FALSE, FALSE, $province_id, $city_id, $eventcategory_id, $event_keyword, $order_events_by, $min_price, $max_price);
        $data['queryeventcategory'] = $this->event_model->get_eventcategory()->result();

        $data['queryfeatured'] = $this->event_model->get_event_paginate(3, $offset / 5, $show_events_by, FALSE, TRUE, $province_id, $city_id, $eventcategory_id, $event_keyword, $order_events_by, $min_price, $max_price);

        $data['meta_def'] = "Direktori Event-event terbaru di HRplasa";
        if ($this->input->get('dashboard'))
            $this->load->view('event_include/latest_ajax', $data, false);
        else
            $this->load->view('event_include/latest_event', $data, false);
    }

    function featured_event() {
        $this->load->helper('pagination_style');
        $data['title'] = "Latest Event";
        $data['navactive'] = "myprojectnav";
        $allData = $this->event_model->get_event_paginate(0, 0, true, false, true);
        $query = $allData->result_array();
        $offset = $this->uri->segment(2);
        $segment = 2;
        $base_url = site_url('events');
        $per_page = 10;
        $total_rows = count($query);
        paginate_bs3($base_url, $per_page, $total_rows, $segment);
        $data['queryevents'] = $this->event_model->get_event_paginate($per_page, $offset, true, false, true);
        if ($this->input->get('dashboard'))
            $this->load->view('post_include/featured_ajax', $data, false);
        else
            $this->load->view('post_include/featured_event', $data, false);
    }

    public function register_event() {
        if($this->session->userdata('user_id'))
        {
            $event_id = $this->uri->segment(4);
            $data['row'] = $this->event_model->get_event_by_id($event_id)->row();
            $data['reventtickets'] = $this->event_model->get_eventticket($event_id)->row();
            $data['title']="Register Event ".$data['row']->event_title;
            $this->load->view('event_include/register_event', $data);
        }
        else
            redirect('login?redirect_to=event/event/register_event/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
    }

    function save_registration() {
        $where_array=array(
            'user_id'=>$this->input->post('user_id'),
            'event_id'=>$this->input->post('event_id')
        );
        $check_registered=$this->event_model->check_registered($where_array);
        if($check_registered==0)
        {
            //update phone number
            $set_array_user=array(
                'userprofile_phone'=>$this->input->post('userprofile_phone')
            );
            $this->userprofile_model->update_profile2($set_array_user,$this->input->post('user_id'));

            $set_array = array(
                'user_id' => $this->input->post('user_id'),
                'event_id' => $this->input->post('event_id'),
                'eventticket_id' => $this->input->post('eventticket_id'),
                'event_audience_status' => 0
            );
            $this->event_model->save_audience($set_array);
            if ($this->input->post('is_earlybird') == 1) {
                $this->event_model->min_total_ticket($this->input->post('eventticket_id'));
                $this->event_model->min_early_bird($this->input->post('eventticket_id'));
            } else {
                $this->event_model->min_total_ticket($this->input->post('eventticket_id'));
            }
            $setBilling = array(
                'billing_date' => strtotime(date('Y-m-d')),
                'billing_type' => 3,
                'billing_for' => $this->session->userdata('user_id'),
                'billing_due' => strtotime("+3 days")
            );
            $biling_id = $this->userproject_model->save_billing($setBilling);
            $setBillingDetailMembership = array(
                'billingdetail_info' => 'Biaya Registrasi Event ' . $this->input->post('event_title'),
                'billing_id' => $biling_id,
                'billingdetail_total' => $this->input->post('ticket_price')
            );
            $this->userproject_model->save_billing_details($setBillingDetailMembership);
            
            //send email to registrar
            $to1 = $this->session->userdata('user_meta')->user_email;
            $cc1 = '';
            $data['name'] = $this->session->userdata('user_meta')->userprofile_firstname . ' ' . $this->session->userdata('user_meta')->userprofile_lastname;
            $data['event_title'] = $this->input->post('event_title');
            $data['event_url'] = $this->input->post('event_url');
            $content = $this->load->view('mailing_view/mail_register_event', $data, TRUE);
            $this->global_model->sendmailMandrill2($to1, $cc1, 'Pendaftaran Pada Event di HRPlasa', $content);
            
            //send email to event_owner
            $revent = $this->event_model->get_event_by_id($this->input->post('event_id'))->row();
            $to2 = $revent->user_email;
            $cc2 = '';
            $data2['name'] = $revent->userprofile_firstname . ' ' . $revent->userprofile_lastname;
            $data2['row'] = $revent;
            $data2['registrar_name'] = $data['name'];
            $content2 = $this->load->view('mailing_view/mail_info_register_event', $data2, TRUE);
            $this->global_model->sendmailMandrill2($to2, $cc2, 'Ada Pendaftaran Pada Event Anda', $content2);
            //set_the_notification
            $this->notifications_model->add_notification(1, $this->session->userdata('user_id'), $revent->event_owner, $revent->event_id);
        }
        //back to event
        redirect('event/view/' . $this->input->post('event_id') . '/' . url_title($this->input->post('event_title')));
    }

    public function edit_event() {
        $event_single = $this->event_model->get_event_by_id($this->uri->segment(4))->row_array();
        $start = $this->input->post('startdate');
        $end = $this->input->post('enddate');
        $ymd = explode("/", $start);
        $time = explode(" ", $ymd[2]);
        $tanggal = $time[0] . '-' . $ymd[1] . '-' . $ymd[0] . ' ' . $time[1];
        $startdate = strtotime($tanggal);
        $ymd2 = explode("/", $end);
        $time2 = explode(" ", $ymd2[2]);
        $tanggal = $time2[0] . '-' . $ymd2[1] . '-' . $ymd2[0] . ' ' . $time2[1];
        $enddate = strtotime($tanggal);
        if ($_FILES["image"]["name"]) {
            $name = $_FILES["image"]["name"];
            $ext = end((explode(".", $name)));
            $filename = time() . '.' . $ext;
            $config['upload_path'] = './uploads/event_images/';
            $config['file_name'] = $filename;
            $config['allowed_types'] = 'jpg|png|gif';
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $data = $this->upload->data();
                $uploaded = $data['file_name'];
            } else {
                $uploaded = $event_single['event_picture'];
                echo $this->upload->display_errors();
            }
            if (file_exists($config['upload_path'] . $event_single['event_picture'])) {
                unlink($config['upload_path'] . $event_single['event_picture']);
            }
        } else {
            $uploaded = $event_single['event_picture'];
        }
        $description = $this->input->post('overview') . '  <!---split--->  ' . $this->input->post('outline') . ' <!---split---> ' . $this->input->post('outcome') . ' <!---split---> ' . $this->input->post('sertifikat');
        $set_array = array(
            "event_title" => $this->input->post('title'),
            "event_title_slug" => url_title($this->input->post('title')),
            "event_description" => $description,
            "city_id" => $this->input->post('city_id'),
            "event_venue" => $this->input->post('venue'),
            "event_start_date" => $startdate,
            "event_end_date" => $enddate,
            "event_picture" => $uploaded,
            "budget_event" => $this->input->post('event_budget'),
        );
        $this->event_model->del_key_event($this->uri->segment(4));
        $this->event_model->update_event($this->uri->segment(4), $set_array);
        $keywords_split = explode(',', $this->input->post('keywords'));
        foreach ($keywords_split as $keyword_text) {
            //echo $keyword_text;
            $this->event_model->add_keywords($keyword_text, $this->uri->segment(4));
        }
        redirect('event/view/' . $this->uri->segment(4) . '/' . url_title($this->input->post('title')));
    }

    public function comment() {
        $this->load->view('event_include/add_comment');
    }

    public function add_comment($id = '', $title = '') {
        $set_array = array(
            'user_id' => $this->session->userdata('user_id'),
            'comment_type' => 5,
            'comment_for' => $this->uri->segment(4),
            'comment_text' => $this->input->post('comment_text'),
            'comment_date' => strtotime(date('Y-m-d H:i:s')),
        );
        if ($this->event_model->save_comment($set_array)) {
            $this->load->model('user_role_model');
            $data2['user'] = $this->user_role_model->get_user_by_event($this->uri->segment(4));
            $data2['comment'] = $set_array;
            $data2['event'] = $this->event_model->get_event_by_id($this->uri->segment(4))->row();
            $content2 = $this->load->view('mailing_view/mail_new_comment_event', $data2, TRUE);
            $this->global_model->sendmailMandrill2($data2['user']['user_email'], '', 'Ada Komentar Baru Pada Event Anda', $content2);
        }
        redirect('event/view/' . $this->uri->segment(4) . '/' . $this->uri->segment(5));
    }

}
