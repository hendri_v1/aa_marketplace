<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_event extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                if(!$this->session->userdata('logged'))
		{
			redirect('program');			
		}
		$this->load->model('event_model');
		$this->load->model('billing_model');
	}

	public function index()
	{
		
	}

	public function my_event_list($offset = '')
	{            
		$this->load->helper('pagination_style');
        $event_status = $this->input->get('event_status');
        $order_by=$this->input->get('order_by');
        $active_only = FALSE;
        $pending_only = FALSE;
        $out_of_date = FALSE;
        $draft_only = FALSE;
        $is_private = FALSE;
        switch ($event_status) {
            case 1:
                $active_only = TRUE;
                break;
            case 2:
                $pending_only = TRUE;
                break;
            case 3:
                $out_of_date = TRUE;
                break;
            case 4:
                $draft_only = TRUE;
                break;
            case 5:
            	$is_private = TRUE;
            default:
                break;
        }
        $per_page=$this->input->get('per_page');
		if($per_page==0)
			$per_page=10;

		$allData=$this->event_model->get_event(0,0,$active_only,$pending_only,$out_of_date,$draft_only,$order_by,$is_private);
		$query = $allData->result_array();
		$offset = $this->uri->segment(3);
		$segment = 3;
		$base_url = site_url('event/my_event/');
		
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment, TRUE);

        $data['query']=$this->event_model->get_event($per_page,$offset,$active_only,$pending_only,$out_of_date,$draft_only,$order_by,$is_private);
		
		$data['title']="Event Management System";
		$data['nav_side']="ems";
		$data['child_nav']="ems_side";
		
                $data['base_filter'] = site_url('event/my_event/' . $offset);
		$this->load->view('event_include/my_event',$data,false);
	}

	public function add_new()
	{
		$data['map']=TRUE;
		$data['title']="Post New Event";
		$data['eventcategory'] = $this->event_model->get_eventcategory()->result_array();
		$data['province'] = $this->global_model->get_province();
		$data['eventspeaker_hash']=md5(time());
		$data['qpricing']=$this->billing_model->get_pricing_by_type(1);
		$data['nav_side']="ems";
		$data['child_nav']="ems_new";
		$this->load->view('event_include/new_event',$data,false);
	}

	public function add_private()
	{
		$data['map']=TRUE;
		$data['title']="Post New Event";
		$data['eventcategory'] = $this->event_model->get_eventcategory()->result_array();
		$data['province'] = $this->global_model->get_province();
		$data['eventspeaker_hash']=md5(time());
		$data['nav_side']="ems";
		$data['child_nav']="ems_new_private";
		$this->load->view('event_include/new_private_event',$data,false);
	}

	public function save_event()
	{
		//validation start
		$is_error=0;
		$error_array=array();
		$province_id=$this->input->post('province_id');
		$city_id=$this->input->post('city_id');
		$venue=$this->input->post('venue');
		$event_address=$this->input->post('event_address');
		$event_title=$this->input->post('title');
		$startdate=$this->input->post('startdate');
		$enddate=$this->input->post('enddate');
		$keywords=$this->input->post('keywords');
		if($province_id==0)
		{
			$error_array[]="Provinsi Harus Dipilih";
			$is_error++;
		}
		if($city_id==0)
		{
			$error_array[]="Kota Harus Dipilih";
			$is_error++;
		}
		if($venue=='')
		{
			$error_array[]="Venue Harus Diisi";
			$is_error++;
		}
		if($event_address=='')
		{
			$error_array[]="Alamat Harus Diisi";
			$is_error++;
		}
		if($event_title=='')
		{
			$error_array[]="Judul Event Harus Diisi";
			$is_error++;
		}
		if($startdate=='')
		{
			$error_array[]="Tanggal Mulai Harus Diisi";
			$is_error++;
		}
		if($enddate=='')
		{
			$error_array[]="Tanggal Selesai Harus Diisi";
			$is_error++;
		}
		if($keywords=='')
		{
			$error_array[]="Harap Mengisi Keyword";
			$is_error++;
		}


		//validation end
		if($is_error==0)
		{
			$start = $this->input->post('startdate');
			$end = $this->input->post('enddate');

		    $ymd = explode("/", $start);
		    $time = explode(" ", $ymd[2]);
		    $tanggal = $time[0].'-'.$ymd[1].'-'.$ymd[0].' '.$time[1];
		    $startdate = strtotime($tanggal);

		    $ymd2 = explode("/", $end);
		    $time2 = explode(" ", $ymd2[2]);
		    $tanggal = $time2[0].'-'.$ymd2[1].'-'.$ymd2[0].' '.$time2[1];
		    $enddate = strtotime($tanggal);

		    if ($_FILES["image"]["name"]) {
				$name = $_FILES["image"]["name"];
				$ext = end((explode(".", $name)));
				$filename = time().'.'.$ext;

				$config['upload_path'] = './uploads/event_images/';
				$config['file_name']  = $filename;
				$config['allowed_types']  = 'jpg|png|gif';

				$this->load->library('upload', $config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$uploaded=$data['file_name'];
				} else {
					$uploaded='';
					echo $this->upload->display_errors();
				}
			} else {
				$uploaded='';
			}

			$description = $this->input->post('overview').'  <!---split--->  '.$this->input->post('outline').' <!---split---> '.$this->input->post('outcome').' <!---split---> '.$this->input->post('sertifikat');

			$set_array=array(
				"event_title"=>$this->input->post('title'),
				"event_title_slug"=>url_title($this->input->post('title')),
				"event_description"=>$description,
				"city_id"=>$this->input->post('city_id'),
				"event_venue"=>$this->input->post('venue'),
				"event_start_date"=>$startdate,
				"event_end_date"=>$enddate,
				"event_picture"=>$uploaded,
				"event_owner"=>$this->session->userdata('user_meta')->user_id,
				"budget_event"=>$this->input->post('event_budget'),
				"eventcategory_id"=>$this->input->post('eventcategory'),
				"event_address"=>$this->input->post('event_address'),
				"event_speaker"=>$this->input->post('event_speaker'),
				"event_geo"=>trim($this->input->post('event_geo'), '()'),
				"added_at"=>time(),
				"is_private"=>$this->input->post('is_private')
			);
			$event_idnya=$this->event_model->save_event($set_array);

			//update event speaker ID
			$this->db->set('event_id',$event_idnya);
			$this->db->where('eventspeaker_hash',$this->input->post('eventspeaker_hash'));
			$this->db->update('eventspeakers');
			
			//save the ticket data
			$set_array_ticket=array(
				'eventticket_seat'=>$this->input->post('eventticket_seat'),
				'eventticket_earlybird_seat'=>$this->input->post('eventticket_earlybird_seat'),
				'eventticket_earlybird_price'=>$this->input->post('eventticket_earlybird_price'),
				'eventticket_public'=>$this->input->post('eventticket_public'),
				'event_id'=>$event_idnya
			);
			$this->event_model->add_eventticket($set_array_ticket);

			$keywords_split=explode(',', $this->input->post('keywords'));
			foreach($keywords_split as $keyword_text)
			{
				//echo $keyword_text;
				$this->event_model->add_keywords($keyword_text,$event_idnya);
			}
			if($this->input->post('is_private')==0)
			{
				//billing starts here
				$setBilling = array(
					'billing_date'=>strtotime(date('Y-m-d')),
					'billing_type'=>2,
					'billing_for'=>$event_idnya,
					'billing_due'=>strtotime("+7 days")
				);
				$biling_id = $this->billing_model->save_billing($setBilling);
				foreach($this->input->post('pricing') as $pricing_id)
				{
					$row=$this->billing_model->get_pricing_by_id($pricing_id);
					$setarrayBilling = array(
						'billingdetail_info'=>$row->pricing_info,
						'billing_id'=>$biling_id,
						'billingdetail_total'=>$row->pricing_price
					);

					$this->billing_model->save_billing_details($setarrayBilling);
				}
				
				//end of billing
			}

			//sending the email
			if($this->input->post('is_private')==0)
			{
				$data['event_meta']=$this->event_model->get_event_by_id($event_idnya)->row();
				$content = $this->load->view('mailing_view/mailing_new_event', $data, TRUE);
				$this->global_model->sendmailMandrill2($data['event_meta']->user_email,'','Event Berhasil Didaftarkan',$content);
			}
			else
			{
				$data['event_meta']=$this->event_model->get_event_by_id($event_idnya)->row();
				$content = $this->load->view('mailing_view/mailing_new_private_event', $data, TRUE);
				$this->global_model->sendmailMandrill2($data['event_meta']->user_email,'','Event Private Anda Berhasil Ditambahkan',$content);
			}
			redirect('event/my_event');
		}
		else
		{
			$this->session->set_flashdata('error_array', $error_array);
			$this->session->set_flashdata('collected_fields',$this->input->post());
			if($this->input->post('is_private')==1)
				redirect('event/my_event/add_private');
			else
				redirect('event/my_event/add_new');
		}
	}

	function add_speaker()
	{
		$name = $_FILES["speaker_image"]["name"];
		$ext = end((explode(".", $name)));
		$newname=time();
		$filename = $newname.'.'.$ext;

		$config['upload_path'] = './uploads/event_images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']  = $filename;
		

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('speaker_image')) {
			/*
			if (file_exists($path)) {
				//unlink($path);
			}
			*/
			$data = $this->upload->data();
			$config2['source_image']	= './uploads/event_images/'.$data['file_name'];
			
			$config2['maintain_ratio'] = TRUE;
			$config2['width']= 200;
			$config2['height']	= 200;
			$this->load->library('image_lib',$config2);
			$this->image_lib->resize();
			

			$set_array=array(
				"eventspeaker_name"=>$this->input->post('event_speaker'),
				"eventspeaker_description"=>$this->input->post('eventspeaker_info'),
				"eventspeaker_image"=>$data['file_name'],
				"eventspeaker_hash"=>$this->input->post("eventspeaker_hash")
			);
			$this->event_model->add_speaker($set_array);
			/*
			$set_array=array(
				"venue_gallery_file"=>$data['file_name'],
				"venue_gallery_hash"=>$this->input->post('venue_gallery_hash'),
				"venue_gallery_thumb"=>$newname.'_thumb.'.$ext
			);
			$this->venue_model->add_venue_gallery($set_array);*/
			echo 'true';
		} else {
			echo $this->upload->display_errors();
		}
	}

	function view_speaker_by_hash($eventspeaker_hash)
	{
		$data['query']=$this->event_model->get_speaker_by_hash($eventspeaker_hash);
		$this->load->view('event_include/speaker_by_hash',$data);
	}

	function choose_speaker()
	{
		$data['query']=$this->event_model->get_my_speaker();
		$this->load->view('event_include/choose_speaker',$data);
	}

	function get_the_speaker()
	{
		$row=$this->event_model->get_speaker_by_id($this->input->post('eventspeaker_id'))->row();
		$set_array=array(
			'eventspeaker_name'=>$row->eventspeaker_name,
			'eventspeaker_image'=>$row->eventspeaker_image,
			'eventspeaker_hash'=>$this->input->post('eventspeaker_hash'),
			'eventspeaker_description'=>$row->eventspeaker_description,

		);
		$this->event_model->add_speaker($set_array);
	}

	function manage_tickets($event_id)
	{
		$data['event_id']=$event_id;
		$data['row']=$this->event_model->get_event_by_id($event_id)->row();
		$data['qticket']=$this->event_model->get_eventticket($event_id);
		if($data['qticket']->num_rows()>0)
			$data['rowticket']=$data['qticket']->row();

		$this->load->view('event_include/manage_tickets',$data);
	}

	function activate_ticketing()
	{
		//save the ticket data
		$set_array_ticket=array(
			'eventticket_seat'=>$this->input->post('eventticket_seat'),
			'eventticket_earlybird_seat'=>$this->input->post('eventticket_earlybird_seat'),
			'eventticket_earlybird_price'=>$this->input->post('eventticket_earlybird_price'),
			'eventticket_public'=>$this->input->post('eventticket_public'),
			'event_id'=>$this->input->post('event_id')
		);
		$this->event_model->add_eventticket($set_array_ticket);
		redirect('event/my_event');
	}

	function edit_ticketing()
	{
		$set_array_ticket=array(
			'eventticket_seat'=>$this->input->post('eventticket_seat'),
			'eventticket_earlybird_seat'=>$this->input->post('eventticket_earlybird_seat'),
			'eventticket_earlybird_price'=>$this->input->post('eventticket_earlybird_price'),
			'eventticket_public'=>$this->input->post('eventticket_public'),
			
		);
		$eventticket_id=$this->input->post('eventticket_id');
		$this->event_model->edit_eventticket($eventticket_id,$set_array_ticket);
		redirect('event/my_event');
	}

	function view_audience($event_id)
	{
		$data['query']=$this->event_model->get_audience($event_id);
		$data['row']=$this->event_model->get_event_by_id($event_id)->row();
		$data['title']="Daftar Audience ".$data['row']->event_title;
		$data['nav_side']="ems";
		$data['child_nav']="ems_aulist";
		$this->load->view('event_include/event_audience_list',$data);
	}

	function edit_event($event_id)
	{
		$data['map']=TRUE;
		$data['title']="Post New Event";
		$data['eventcategory'] = $this->event_model->get_eventcategory()->result_array();
		$data['province'] = $this->global_model->get_province();
		$data['eventspeaker_hash']=md5(time());
		$data['row']=$this->event_model->get_event_by_id($event_id)->row();
		$data['nav_side']="ems";
		$data['child_nav']="ems_new";
		$this->load->view('event_include/edit_event',$data);
	}

	function update_event($event_id)
	{
		$event_single=$this->event_model->get_event_by_id($event_id)->row_array();
		//validation start
		$is_error=0;
		$error_array=array();
		$province_id=$this->input->post('province_id');
		$city_id=$this->input->post('city_id');
		$venue=$this->input->post('venue');
		$event_address=$this->input->post('event_address');
		$event_title=$this->input->post('title');
		$startdate=$this->input->post('startdate');
		$enddate=$this->input->post('enddate');
		$keywords=$this->input->post('keywords');
		$is_draft=$this->input->post('is_draft');
		if($province_id==0)
		{
			$error_array[]="Provinsi Harus Dipilih";
			$is_error++;
		}
		if($city_id==0)
		{
			$error_array[]="Kota Harus Dipilih";
			$is_error++;
		}
		if($venue=='')
		{
			$error_array[]="Venue Harus Diisi";
			$is_error++;
		}
		if($event_address=='')
		{
			$error_array[]="Alamat Harus Diisi";
			$is_error++;
		}
		if($event_title=='')
		{
			$error_array[]="Judul Event Harus Diisi";
			$is_error++;
		}
		if($startdate=='')
		{
			$error_array[]="Tanggal Mulai Harus Diisi";
			$is_error++;
		}
		if($enddate=='')
		{
			$error_array[]="Tanggal Selesai Harus Diisi";
			$is_error++;
		}
		

		//validation end
		if($is_error==0)
		{
			$event_single=$this->event_model->get_event_by_id($event_id)->row_array();

			$start = $this->input->post('startdate');
			$end = $this->input->post('enddate');

		    $ymd = explode("/", $start);
		    $time = explode(" ", $ymd[2]);
		    $tanggal = $time[0].'-'.$ymd[1].'-'.$ymd[0].' '.$time[1];
		    $startdate = strtotime($tanggal);

		    $ymd2 = explode("/", $end);
		    $time2 = explode(" ", $ymd2[2]);
		    $tanggal = $time2[0].'-'.$ymd2[1].'-'.$ymd2[0].' '.$time2[1];
		    $enddate = strtotime($tanggal);

		    if ($_FILES["image"]["name"]) {

				$name = $_FILES["image"]["name"];
				$ext = end((explode(".", $name)));
				$filename = time().'.'.$ext;

				$config['upload_path'] = './uploads/event_images/';
				$config['file_name']  = $filename;
				$config['allowed_types']  = 'jpg|png|gif';


				$this->load->library('upload', $config);
				if ($this->upload->do_upload('image')) {
					$data = $this->upload->data();
					$uploaded=$data['file_name'];
				} else {
					$uploaded=$event_single['event_picture'];
					echo $this->upload->display_errors();
				}
				if (file_exists($config['upload_path'].$event_single['event_picture'])) {
		    		unlink($config['upload_path'].$event_single['event_picture']);
		    	}
			} else {
				$uploaded=$event_single['event_picture'];
			}
			$description = $this->input->post('overview').'  <!---split--->  '.$this->input->post('outline').' <!---split---> '.$this->input->post('outcome').' <!---split---> '.$this->input->post('sertifikat');
			$set_array=array(
				"event_title"=>$this->input->post('title'),
				"event_title_slug"=>url_title($this->input->post('title')),
				"event_description"=>$description,
				"city_id"=>$this->input->post('city_id'),
				"event_venue"=>$this->input->post('venue'),
				"event_start_date"=>$startdate,
				"event_end_date"=>$enddate,
				"event_picture"=>$uploaded,
				"budget_event"=>$this->input->post('event_budget'),
				"eventcategory_id"=>$this->input->post('eventcategory'),
				"event_address"=>$this->input->post('event_address'),
				"event_speaker"=>$this->input->post('event_speaker'),
				"event_geo"=>trim($this->input->post('event_geo'), '()'),
                "is_draft" => $is_draft,
                "updated_at"=>time()
			);
			$this->event_model->update_event($event_id,$set_array);
			redirect('event/my_event/edit_event/'.$event_id);
		}
		else
		{
			$this->session->set_flashdata('error_array', $error_array);
			
			$this->session->set_flashdata('collected_fields',$this->input->post());
			redirect('event/my_event/edit_event/'.$event_id);
		}

	}

	function delete_dialog($event_id=0)
	{
		$data['row']=$this->event_model->get_event_by_id($event_id)->row();
		$this->load->view('event_include/delete_dialog',$data);
	}

	function delete_event()
	{
		$this->event_model->delete_event($this->input->post('event_id'));
		redirect('event/my_event');
	}

}