<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                if(!$this->session->userdata('logged'))
		{
			redirect('program');			
		}
		$this->load->model('user_role_model');
		$this->load->model('userprofile_model');
	}
	
	public function index()
	{
		$this->load->model('userpost_model');
		$this->load->model('event_model');
		$user_id=$this->session->userdata('user_id');
		$where=array(
			'user.user_id'=>$this->session->userdata('user_id')
		);
		
		$data['row']=$this->user_role_model->get_user_by_id($user_id);
		
		$data['navactive']="myprofilenav";
		$queryrate=$this->userprofile_model->get_rating(3,$user_id);
		$therate=0;
		$therater=0;
		foreach($queryrate->result() as $rowsrate)
		{
			$therate=$therate+$rowsrate->rating_value;
			$therater++;
		}
		if($therater<>0)
		{
			$avgrate=$therate/$therater;
		}
		else
			$avgrate=0;

		$userextra_type=0;
		// if($this->session->userdata('user_meta')->usertype_id==1)
		// 	$userextra_type=1;
		// else
		// 	$userextra_type=2;

		$data['my_project']=$this->userproject_model->get_list_project($user_id);
		$data['my_bid']=$this->userproject_model->bidding_list_paginate($user_id,0,0);
		$data['my_post']=$this->userpost_model->get_list_post($user_id);
		$data['my_event']=$this->event_model->get_event_by_user(0,0,$user_id);

		$data['province'] = $this->global_model->get_province();
		$data['city'] = $this->global_model->get_city($data['row']->province_id)->result_array();

		$data['case']=$this->userprofile_model->get_extra(1,$data['row']->userprofile_id);	
		$data['certificate']=$this->userprofile_model->get_extra(2,$data['row']->userprofile_id);	
		$data['avgrate']=$avgrate;
		$data['therater']=$therater;
		$name_to_show='';
		if($data['row']->usertype_id==1)
		{
			$name_to_show=$data['row']->userprofile_firstname.' '.$data['row']->userprofile_lastname;
		}
		else
		{
			$name_to_show=$data['row']->userprofile_companyname;
		}
        $data['title']=$name_to_show;           
        $data['nav_side']="pms";
        $data['child_nav']="pms_side";                
		$data['meta_def']="Profil dari ".$name_to_show.", Silahkan Mengunjungi HR Plasa Untuk melihat lengkapnya";
		$this->load->view('profile_include/my_profile',$data,false);	
	}
	
	public function edit_profile($edittype='')
	{
		$where=array(
			'user.user_id'=>$this->session->userdata('user_id')
		);
		
		$data['row']=$this->user_role_model->get_user_by_id($this->session->userdata('user_id'));
		$data['title']=$data['row']->userprofile_firstname;
		$data['edittype']=$edittype;
		$this->load->view('member_include/edit_profile',$data,false);		
	}
	
	function update_profile()
	{
		$set_array=array();
		$edittype=$this->input->post('edittype');
		
		if($this->session->userdata('user_meta')->usertype_id==1 or $this->session->userdata('user_meta')->usertype_id==3)
		{
			$set_array=array(
				"userprofile_firstname"=>$this->input->post('userprofile_firstname'),
				"userprofile_lastname"=>$this->input->post('userprofile_lastname'),
				"userprofile_address"=>$this->input->post('userprofile_address'),
				"userprofile_function"=>$this->input->post('userprofile_function'),
				"userprofile_city"=>$this->input->post('userprofile_city'),
				"userprofile_phone"=>$this->input->post('userprofile_phone'),
				"userprofile_fb"=>$this->input->post('userprofile_fb'),
				"userprofile_twitter"=>$this->input->post('userprofile_twitter'),
				"userprofile_linkedin"=>$this->input->post('userprofile_linkedin'),
				"userprofile_function"=>$this->input->post('userprofile_function'),
				"userprofile_aboutme"=>$this->input->post('userprofile_aboutme')
			);	
		}
		elseif($this->session->userdata('user_meta')->usertype_id==2)
		{
			$set_array=array(
				"userprofile_companyname"=>$this->input->post('userprofile_companyname'),
				"userprofile_position"=>$this->input->post('userprofile_position'),
				"userprofile_firstname"=>$this->input->post('userprofile_firstname'),
				"userprofile_lastname"=>$this->input->post('userprofile_lastname'),
				"userprofile_address"=>$this->input->post('userprofile_address'),
				"userprofile_function"=>$this->input->post('userprofile_function'),
				"userprofile_phone"=>$this->input->post('userprofile_phone'),
				"userprofile_fb"=>$this->input->post('userprofile_fb'),
				"userprofile_twitter"=>$this->input->post('userprofile_twitter'),
				"userprofile_linkedin"=>$this->input->post('userprofile_linkedin'),
				"userprofile_aboutme"=>$this->input->post('userprofile_aboutme')
				
			);
		}
		
		
		//echo $this->input->post('edittype');
		$this->userprofile_model->update_profile($set_array);
		redirect('profile/my_profile');	
	}

	function get_image()
	{
		$name = $_FILES["img_profile"]["name"];
		$ext = end((explode(".", $name)));
		$filename = time().'.'.$ext;

		$config['upload_path'] = './uploads/user_images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']  = '2000';
		$config['max_width']  = '1000';
		$config['max_height']  = '1000';
		$config['file_name']  = $filename;

		$this->load->library('upload', $config);
		if ($this->upload->do_upload('img_profile')) {
			$profile=$this->user_role_model->get_user_by_id($this->session->userdata('user_id'));
			$path = './uploads/user_images/'.$profile->userprofile_photo;
			if (file_exists($path)) {
				//unlink($path);
			}
			$data = $this->upload->data();
			$set_array=array(
				"userprofile_photo"=>$data['file_name']
			);
			$this->userprofile_model->update_profile($set_array);
			echo 'true';
		} else {
			echo $this->upload->display_errors();
		}

	}
	
	function manage_extra($edittype)
	{
		$data['edittype']=$edittype;
		$this->load->view('member_include/manage_extra',$data,false);
	}
	
	function add_extra()
	{
		if ($_FILES["userextra_attachment"]["name"]) {
			$name = $_FILES["userextra_attachment"]["name"];
			$ext = end((explode(".", $name)));
			$filename = time().'.'.$ext;

			$config['upload_path'] = './uploads/userextra/';
			$config['file_name']  = $filename;
			$config['allowed_types']  = '*';

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('userextra_attachment')) {
				$data = $this->upload->data();
				$uploaded=$data['file_name'];
			} else {
				echo $this->upload->display_errors();
			}
		} else {
			$uploaded='';
		}
		$set_array=array(
			"userprofile_id"=>$this->session->userdata('user_meta')->userprofile_id,
			"userextra_type"=>$this->input->post('userextra_type'),
			"userextra_title"=>$this->input->post('userextra_title'),
			"userextra_description"=>$this->input->post('userextra_description'),
			"userextra_attachment"=>$uploaded,
			"userextra_adddate"=>strtotime(date('Y-m-d')),
			"userextra_updateat"=>strtotime(date('Y-m-d')),
			"userextra_status"=>1,
			"userextra_when"=>strtotime($this->input->post('userextra_when')),
		);
		$this->userprofile_model->add_extra($set_array);
		redirect('profile/my_profile');	
	}

	function edit_extra($edittype,$editid)
	{
		$data['extras']=$this->userprofile_model->get_extra_single($editid)->row();
		$data['edittype']=$edittype;
		$this->load->view('member_include/edit_extra',$data,false);
	}

	function update_extra($editid)
	{
		$extra=$this->userprofile_model->get_extra_single($editid)->row();
		if ($_FILES["userextra_attachment"]["name"]) {
			$name = $_FILES["userextra_attachment"]["name"];
			$ext = end((explode(".", $name)));
			$filename = time().'.'.$ext;

			$config['upload_path'] = './uploads/userextra/';
			$config['file_name']  = $filename;
			$config['allowed_types']  = '*';

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('userextra_attachment')) {
				$path = './uploads/userextra/'.$extra->userextra_attachment;
				if (file_exists($path)) {
					unlink($path);
				}
				$data = $this->upload->data();
				$uploaded=$data['file_name'];
			} else {
				echo $this->upload->display_errors();
			}
		} else {
			$uploaded=$extra->userextra_attachment;
		}
		$set_array=array(
			"userextra_type"=>$this->input->post('userextra_type'),
			"userextra_title"=>$this->input->post('userextra_title'),
			"userextra_description"=>$this->input->post('userextra_description'),
			"userextra_attachment"=>$uploaded,
			"userextra_updateat"=>strtotime(date('Y-m-d')),
			"userextra_when"=>strtotime($this->input->post('userextra_when')),
		);
		$this->userprofile_model->update_extra($set_array,$editid);
		redirect('profile/my_profile');	
	}

	public function download($file = '',$name = '')
	{
		$ext = end((explode(".", $file)));

		$this->load->helper('download');
		$path = 'uploads/userextra/'.$file;
		$data = file_get_contents($path); // Read the file's contents
		force_download($name.'.'.$ext, $data); 
	}

	public function manage_keywords($edit_type)
	{
		$data['row']=$this->user_role_model->get_user_by_id($this->session->userdata('user_id'));
		$qkeyword =$this->global_model->get_keywords(1,$this->session->userdata('user_id'));
		$flag = '';
		if ($qkeyword->num_rows() != 0) {
			foreach ($qkeyword->result() as $rkeyword) {
				if ($flag == '') {
					$flag = $rkeyword->keyword_text;
				} else {
					$flag = $flag.','.$rkeyword->keyword_text;
				}
			}
			$data['keys'] = $flag;
		} else {
			$data['keys'] = $flag;
		}
		$data['edit_type']=$edit_type;
		$this->load->view('member_include/manage_keywords',$data,false);
	}

	public function add_keywords()
	{
		$qkeyword =$this->global_model->get_keywords(1,$this->session->userdata('user_id'))->result_array();
		if (!empty($qkeyword)) {
			$this->global_model->dell_keywords($this->input->post('keyword_connector_type'),$this->input->post('user_id'));
		}
		$this->global_model->add_keywords($this->input->post('keyword_text'),$this->input->post('keyword_connector_type'),$this->input->post('user_id'));
		redirect('profile/my_profile');
	}

	public function upgrade()
	{
		$type = $this->uri->segment(4);
		if ($type == 'submit') {
			$setBilling = array(
				'billing_date'=>strtotime(date('Y-m-d')),
				'billing_type'=>3,
				'billing_for'=>$this->session->userdata('user_id'),
				'billing_due'=>strtotime("+7 days")
			);
			$biling_id = $this->userproject_model->save_billing($setBilling);

			$user_type = $this->session->userdata('user_meta')->usertype_id;

			if ($user_type == 1) {
				$price = 1500000;
			} else {
				$price = 3000000;
			}

			$setBillingDetailMembership = array(
				'billingdetail_info'=>'Request Upgrade to Premium',
				'billing_id'=>$biling_id,
				'billingdetail_total'=>$price
			);
			$this->userproject_model->save_billing_details($setBillingDetailMembership);
			
			$set_array = array(
				'user_status' => 1
			);
			$this->user_role_model->update_user($set_array,$this->session->userdata('user_id'));

			$getUser = $this->user_role_model->get_user_by_id($this->session->userdata('user_id'));

			//send mail upgrader
			$to2 = $this->session->userdata('user_meta')->user_email;
			$cc2 = '';
			$subject2 = 'Anda Telah Upgrade';

			$data['name'] = '';
			$content2 = $this->load->view('mailing_view/mail_upgrade_request', $data, TRUE);

			//send mail upgrader
			$this->global_model->sendmailMandrill2($to2,$cc2,$subject2,$content2);

			redirect('profile/my_profile/');
		} else{
			$this->load->view('member_include/upgrade_premium');
		}
	}
}