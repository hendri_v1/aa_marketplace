<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged'))
		{
			echo "You are not authorized to open this page";	
			exit();
			
		}
	}
	
	public function index()
	{
		$data['title']="Thank you for Registering";
		$this->load->view('member_include/welcome',$data,false);	
	}
}