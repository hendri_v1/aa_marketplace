<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Billing extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('billing_model');
        $this->load->model('user_role_model');
	}

	function index()
	{
		$data['title']="Tagihan Saya";
		$data['nav_side']="billing";
		$data['child_nave']="billing";
		$data['query']=$this->billing_model->get_my_billing_grouped();
		$this->load->view('member_include/my_billing',$data);
	}

}