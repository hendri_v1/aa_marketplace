<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                
		$this->load->model('user_role_model');
		$this->load->model('userprofile_model');
		$this->load->model('userpost_model');
	}

	function view_profile($user_id)
	{
		$this->load->model('event_model');
		$where=array(
			'user.user_id'=>$user_id
		);
		
		$data['row']=$this->user_role_model->get_user_by_id($user_id);
		
		$data['navactive']="myprofilenav";
		$queryrate=$this->userprofile_model->get_rating(3,$user_id);
		$therate=0;
		$therater=0;
		foreach($queryrate->result() as $rowsrate)
		{
			$therate=$therate+$rowsrate->rating_value;
			$therater++;
		}
		if($therater<>0)
		{
			$avgrate=$therate/$therater;
		}
		else
			$avgrate=0;

		$userextra_type=0;
		// if($this->session->userdata('user_meta')->usertype_id==1)
		// 	$userextra_type=1;
		// else
		// 	$userextra_type=2;

		$data['my_project']=$this->userproject_model->get_list_project($user_id);
		$data['my_bid']=$this->userproject_model->bidding_list_paginate($user_id,0,0);
		$data['my_post']=$this->userpost_model->get_list_post($user_id);
		$data['my_event']=$this->event_model->get_event_by_user(0,0,$user_id);


		$data['case']=$this->userprofile_model->get_extra(1,$data['row']->userprofile_id);	
		$data['certificate']=$this->userprofile_model->get_extra(2,$data['row']->userprofile_id);	
		$data['avgrate']=$avgrate;
		$data['therater']=$therater;
		$name_to_show='';
		if($data['row']->usertype_id==1)
		{
			$name_to_show=$data['row']->userprofile_firstname.' '.$data['row']->userprofile_lastname;
		}
		else
		{
			$name_to_show=$data['row']->userprofile_companyname;
		}
        $data['title']=$name_to_show;                           
		$data['meta_def']="Profil dari ".$name_to_show.", Silahkan Mengunjungi HR Plasa Untuk melihat lengkapnya";
		$this->load->view('member_include/view_profile',$data,false);	
	}

	function notification_list()
	{
		if(!$this->session->userdata('logged'))
		{
			redirect('program');			
		}
		$this->load->model('event_model');
		$data['title']="Notification Center";
		$data['query']=$this->notifications_model->get_my_notification(false,true,$this->session->userdata('user_id'));
		$data['nav_side']="notif";
		$data['nav_side']="notif";
		$this->load->view('member_include/notification_list',$data,false);
	}

	
}