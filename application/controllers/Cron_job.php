<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* For detailed reference, please read https://ellislab.com/codeigniter/user-guide/libraries/migration.html */

class Cron_job extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		
		$this->load->model('user_role_model');
		$this->load->model('userpost_model');
		$this->load->model('billing_model');
		$this->load->model('venue_model');
		$this->load->model('event_model');
	}

	function send_mail_bulk($preview=false)
	{
		$query=$this->user_role_model->get_users();
		$to=array();
		foreach($query->result() as $rows)
		{	
			if($rows->user_email<>'')
				$to[]=array('email'=>$rows->user_email,'name' => $rows->userprofile_firstname);
		}
		$query=$this->event_model->get_next_week();
		$data['query']=$query;
		$data['total_event_next_week']=$query->num_rows();
		if($data['total_event_next_week']>0)
		{
			if($preview==false)
			{
				$content = $this->load->view('mailing_view/mail_campaign',$data,TRUE);
				echo $this->global_model->sendmailMandrillBulk($to,'','HRPlasa Weekly Update '.date('M Y'),$content);
			}
			else
			{
				$this->load->view('mailing_view/mail_campaign',$data);
			}
		}
	}
}