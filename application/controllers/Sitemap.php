<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $page[] = 'event';
        $page[] = 'vendor';
        $page[] = 'expert';
        $page[] = 'venue';
        $page[] = 'lowongan';
        $page[] = 'page';
        $page[] = 'post';

        $extract_page = array();
        foreach ($page as $value) {
            $extract_page[] = site_url('sitemap/extract/' . $value);
        }

        $data['sitemaps'] = $extract_page;
        $this->output->set_header("HTTP/1.0 200 OK");
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("X-Robots-Tag: noindex");
        $this->output->set_content_type('application/xml');
        $this->load->view('sitemap/sitemap', $data);
    }

    public function extract($type = '') {

        $sitemaps = array();
        switch ($type) {
            case 'event':
                $this->load->model('event_model');
                $sitemaps = $this->event_model->get_event_paginate(0, 0, TRUE, FALSE, FALSE)->result();
                foreach ($sitemaps as $value) {
                    $value->loc = site_url('event/view/' . $value->event_id . '/' . $value->event_title_slug);
                    $value->lastmod = site_url('event/view/' . $value->event_start_date);
                }
                break;
            case 'vendor':
                $this->load->model('global_model');
                $sitemaps = $this->global_model->get_category(2)->result();
                foreach ($sitemaps as $value) {
                    $value->loc = site_url('program/vendor_list/' . $value->category_id . '/' . url_title($value->category_name));
                    $value->lastmod = date('Y-m-d H:i:s');
                }
                break;
            case 'expert':
                $this->load->model('global_model');
                $sitemaps = $this->global_model->get_category(1)->result();
                foreach ($sitemaps as $value) {
                    $value->loc = site_url('program/individual_list/' . $value->category_id . '/' . url_title($value->category_name));
                    $value->lastmod = date('Y-m-d H:i:s');
                }
                break;
            case 'venue':
                $this->load->model('venue_model');
                $sitemaps = $this->venue_model->get_venue(0, 0, true)->result();
                foreach ($sitemaps as $value) {
                    $value->loc = site_url('venue/' . $value->venue_id);
                    $value->lastmod = date('Y-m-d H:i:s');
                }
                break;
            case 'lowongan':
                $this->load->model('userpost_model');
                $sitemaps = $this->userpost_model->get_job(0, 0)->result();
                foreach ($sitemaps as $value) {
                    $value->loc = site_url('post/view_post/' . $value->post_id . '/' . $value->post_title_slug);
                    $value->lastmod = $value->post_updateat;
                }
                break;
            case 'page':
                $lastmod = date('Y-m-d H:i:s');
                $sitemaps[] = array('loc' => '', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'login', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'register', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'events', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'individual-directory', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'vendor-directory', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'venue_directory', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'jobs', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'our-focus', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'cara-kerja', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'membership', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'faq', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'blog', 'lastmod' => $lastmod);
                $sitemaps[] = array('loc' => 'prnewswire', 'lastmod' => $lastmod);
                $temp = array();
                foreach ($sitemaps as $value) {
                    $object = new stdClass();
                    $object->loc = site_url($value['loc']);
                    $object->lastmod = $value['lastmod'];
                    $temp[] = $object;
                }
                $sitemaps = $temp;
                break;
            case 'post':
                $this->load->model('userpost_model');
                $sitemaps = $this->userpost_model->get_blog(0, 0)->result();
                foreach ($sitemaps as $value) {
                    $value->loc = site_url('post/view_post/' . $value->post_id . '/' . $value->post_title_slug);
                    $value->lastmod = $value->post_updateat;
                }
                break;
            default:
                $sitemaps = array();
                break;
        }

        $data['sitemaps'] = $sitemaps;
        $this->output->set_header("HTTP/1.0 200 OK");
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        $this->output->set_header("X-Robots-Tag: noindex");
        $this->output->set_content_type('application/xml');
        $this->load->view('sitemap/sitemap_extract', $data);
    }

}
