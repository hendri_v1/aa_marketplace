<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class project extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('userproject_model');
		$this->load->model('user_role_model');
		
	}
	
	public function index()
	{
		$data['utype'] = array(1=>'All',2=>'Assosiate Trainer Only',3=>'Asessor Only',4=>'HR consultant only',5=>'Vendor Only'); 
		$category = $this->userproject_model->get_category()->result_array();
		$data['title']="Post a Project";
		$data['province'] = $this->global_model->get_province();
		$this->load->view('project_include/post_project',$data,false);	
	}

	public function add_project()
	{
		if ($_FILES["project_attachment"]["name"]) {
			$name = $_FILES["project_attachment"]["name"];
			$ext = end((explode(".", $name)));
			$filename = time().'.'.$ext;

			$config['upload_path'] = './uploads/project_images/';
			$config['file_name']  = $filename;
			$config['allowed_types']  = '*';

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('project_attachment')) {
				$data = $this->upload->data();
				$uploaded=$data['file_name'];
				echo "test";
			} else {
				echo $this->upload->display_errors();
			}
		} else {
			$uploaded='';
		}
		if ($this->input->post('is_featured') == 'on') {
			$is_featured = 1;
		} else {
			$is_featured = 0;
		}
		if ($this->input->post('is_urgent') == 'on') {
			$is_urgent = 1;
		} else {
			$is_urgent = 0;
		}
		if ($this->input->post('is_help') == 'on') {
			$is_help = 1;
		} else {
			$is_help = 0;
		}
		$project_end_date=human_to_unix($this->input->post('project_end_date').' 00:00:00');
		$set_array=array(
			"projectcat_id"=>$this->input->post('project_category_id'),
			"project_title"=>$this->input->post('project_title'),
			"city_id"=>$this->input->post('city_id'),
			"project_budget"=>$this->input->post('project_budget'),
			"project_end_date"=>$project_end_date,
			"project_can_bid"=>$this->input->post('project_can_bid'),
			"project_description"=>$this->input->post('project_description'),
			"project_attachment"=>$uploaded,
			"project_isfeatured"=>$is_featured,
			"project_isurgent"=>$is_urgent,
			"project_ishelp"=>$is_help,
			"user_id"=>$this->session->userdata('user_meta')->user_id,
			"project_status"=>1,
			"project_addedat"=>strtotime(date('Y-m-d')),
			"project_updateat"=>strtotime(date('Y-m-d')),
			"project_phase"=>$this->input->post('project_phase'),
			"project_visibility"=>$this->input->post('project_visibility')
		);
		$project_idnya=$this->userproject_model->add_project($set_array);
		$keywords_split=explode(',', $this->input->post('keyword_text'));

		foreach($keywords_split as $keyword_text)
		{
			//echo $keyword_text;
			$this->global_model->add_keywords($keyword_text,2,$project_idnya);
		}
		$this->global_model->add_category($this->input->post('project_category_id'),2,$project_idnya);

		$setBilling = array(
			'billing_date'=>strtotime(date('Y-m-d')),
			'billing_type'=>1,
			'billing_for'=>$project_idnya,
			'billing_due'=>strtotime("+7 days")
		);
		$biling_id = $this->userproject_model->save_billing($setBilling);

		$setBillingDetailComision = array(
			'billingdetail_info'=>'Comision Fee',
			'billing_id'=>$biling_id,
			'billingdetail_total'=>0.02*$set_array['project_budget']
		);
		$this->userproject_model->save_billing_details($setBillingDetailComision);
		
		if ($is_featured != 0) {

			// $days = ceil(($project_end_date - strtotime(date('Y-m-d'))) / 86400);
			
			$setBillingDetailfeauterd = array(
				'billingdetail_info'=>'Feauterd Fee',
				'billing_id'=>$biling_id,
				'billingdetail_total'=>100000
			);

			$this->userproject_model->save_billing_details($setBillingDetailfeauterd);
		}

		if ($is_urgent != 0) {
			$setBillingDetailUrgent = array(
				'billingdetail_info'=>'Urgent Fee',
				'billing_id'=>$biling_id,
				'billingdetail_total'=>300000
			);
			
			$this->userproject_model->save_billing_details($setBillingDetailUrgent);
		}

		if ($is_help != 0) {
			$setBillingDetailHelp = array(
				'billingdetail_info'=>'Urgent Help',
				'billing_id'=>$biling_id,
				'billingdetail_total'=>250000
			);
			
			$this->userproject_model->save_billing_details($setBillingDetailHelp);
		}

		redirect('project/view/'.$project_idnya);
	}

	public function view_project($project_id=0)
	{
		if ($project_id == 0) {
			show_404();
		}
		
		$checkproject = $this->userproject_model->get_single_project($project_id);	
		
		if (empty($checkproject)) {
			show_404();
		}

		$data['row']=$this->userproject_model->get_single_project($project_id);
		$data['num_shortlisted']=$this->userproject_model->count_shortlisted($project_id);
		$data['comments']=$this->userproject_model->get_comments(array('comment_for'=>$project_id,'comment_type'=>1))->result_array();
		$data['queryprovince']=$this->global_model->get_province();
		$querybid=$this->userproject_model->get_bidder($project_id);
		$data['totalbids']=$querybid->num_rows();
		$data['querybid']=$querybid;
		$data['title']=$data['row']->project_title;
		$data['project_keywords'] = $this->global_model->get_keywords(2,$data['row']->project_id);
		$data['have_bidded']=$this->userproject_model->have_bidded($project_id);
		$this->load->view('project_include/view_project',$data,false);
	}

	public function bid_project($project_id=0)
	{
		if ($project_id == 0) {
			show_404();
		}
		$checkbid = $this->userproject_model->get_single_bidder_id($project_id);
		if (!empty($checkbid)) {
			redirect('project/view/'.$project_id);
		}
		$checkbider = $this->userproject_model->get_bidder($project_id)->row_array();
		if (!empty($checkbider) || $this->input->post('projectbid_bid') == '') {
			show_404();
		}
		if ($_FILES["projectbid_attachment"]["name"]) {
			$name = $_FILES["projectbid_attachment"]["name"];
			$ext = end((explode(".", $name)));
			$filename = time().'.'.$ext;

			$config['upload_path'] = './uploads/project_images/';
			$config['file_name']  = $filename;
			$config['allowed_types']  = '*';

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('projectbid_attachment')) {
				$data = $this->upload->data();
				$uploaded=$data['file_name'];
			} else {
				echo $this->upload->display_errors();
			}
		} else {
			$uploaded='';
		}
		if ($_FILES["projectbid_attachment2"]["name"]) {
			$name = $_FILES["projectbid_attachment2"]["name"];
			$ext = end((explode(".", $name)));
			$filename = time().'22'.'.'.$ext;

			$config['upload_path'] = './uploads/project_images/';
			$config['file_name']  = $filename;
			$config['allowed_types']  = '*';

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('projectbid_attachment2')) {
				$data = $this->upload->data();
				$uploaded2=$data['file_name'];
			} else {
				echo $this->upload->display_errors();
			}
		} else {
			$uploaded2='';
		}
		if ($_FILES["projectbid_attachment3"]["name"]) {
			$name = $_FILES["projectbid_attachment3"]["name"];
			$ext = end((explode(".", $name)));
			$filename = time().'33'.'.'.$ext;

			$config['upload_path'] = './uploads/project_images/';
			$config['file_name']  = $filename;
			$config['allowed_types']  = '*';

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('projectbid_attachment3')) {
				$data = $this->upload->data();
				$uploaded2=$data['file_name'];
			} else {
				echo $this->upload->display_errors();
			}
		} else {
			$uploaded3='';
		}
		$set_array=array(
			'project_id'=>$project_id,
			'projectbid_user_id'=>$this->session->userdata('user_id'),
			'projectbid_description'=>$this->input->post('projectbid_description'),
			'projectbid_attachment'=>$uploaded,
			'projectbid_attachment2'=>$uploaded2,
			'projectbid_attachment3'=>$uploaded3,
			'projectbid_bid'=>$this->input->post('projectbid_bid'),
			'projectbid_addedat'=>time()
		);
		$this->userproject_model->add_bid($set_array);
		// turn off for a while $this->notifications_model->bid_project_notification($this->input->post('project_owner'),$project_id);


		$getBider = $this->session->userdata('user_meta');
		$getOwner = $this->user_role_model->get_user_by_id($this->input->post('project_owner'));
		$getProject = $this->userproject_model->get_single_project($project_id);
		$this->load->model('global_model');
		
		//send mail user
		$to = $getBider->user_email;
		$cc = '';
		$subject = 'Anda melakukan Bid sebuah Project';
		$data['arraybid']=array(
			'project_name'=>$getProject->project_title,
			'bid_total'=>$this->input->post('projectbid_bid'),
			'owner_data' =>$getOwner,
			'thebidder'=>$getBider->userprofile_firstname
		);
		$content = $this->load->view('mailing_view/mail_bidder', $data, TRUE);

		//send mail owner
		$this->global_model->sendmailMandrill2($to,$cc,$subject,$content);

		//send mail owner
		$to2 = $getOwner->user_email;
		$cc2 = '';
		$subject2 = 'Bid Untuk Project '.$getProject->project_title;
		$data2['arraybid']=array(
			'project_name'=>$getProject->project_title,
			'bid_total'=>$this->input->post('projectbid_bid'),
			'bidder_data' =>$getBider,
			'theowner'=>$getOwner->userprofile_firstname
		);
		$content2 = $this->load->view('mailing_view/mail_project_holder', $data2, TRUE);
		
		//send mail owner
		$this->global_model->sendmailMandrill2($to2,$cc2,$subject2,$content2);


		redirect('project/view/'.$project_id);
	}

	function featured_project()
	{
		$this->load->helper('pagination_style');
		$data['title']="My Project";
		$data['navactive']="myprojectnav";
		$data['queryprovince']=$this->global_model->get_province();
		$allData=$this->userproject_model->get_projects_paginate(true,false,false,true,0,0);

		$query = $allData->result_array();
		$offset = $this->uri->segment(3);
		$segment = 3;
		$base_url = site_url('project/featured/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);

		$data['query']=$this->userproject_model->get_projects_paginate(true,false,false,true,$per_page,$offset);
		$data['navactive']='browseprojectnav';
		$data['title']='Featured Project';
		//$this->load->view('project_include/all_featured',$data,false);
		if($this->input->get('dashboard'))
			$this->load->view('project_include/featured_ajax',$data,false);
		else
			$this->load->view('project_include/all_featured',$data,false);
	}

	function percategory_project($projectcat_id)
	{
		$this->load->helper('pagination_style');
		$data['title']="Project List";
		$data['navactive']="myprojectnav";
		$data['queryprovince']=$this->global_model->get_province();
		$allData=$this->userproject_model->get_projects_paginate(false,false,false,true,0,0,false,$projectcat_id);

		$query = $allData->result_array();
		$offset = $this->uri->segment(4);
		$segment = 3;
		$base_url = site_url('project/featured/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);

		$data['query']=$this->userproject_model->get_projects_paginate(false,false,false,true,$per_page,$offset,false,$projectcat_id);
		$data['navactive']='browseprojectnav';
		$data['title']='Featured Project';
		//$this->load->view('project_include/all_featured',$data,false);
		if($this->input->get('dashboard'))
			$this->load->view('project_include/latest_ajax',$data,false);
		else
			$this->load->view('project_include/all_latest',$data,false);
	}

	function browse_category()
	{
		$data['title']="Category";
		$data['querycvendor']=$this->global_model->get_category(0);
		$this->load->view('global_include/user_category',$data,false);
	}

	function urgent_project()
	{
		$this->load->helper('pagination_style');
		$data['title']="My Project";
		$data['navactive']="myprojectnav";
		$data['queryprovince']=$this->global_model->get_province();
		$allData=$this->userproject_model->get_projects_paginate(false,true,false,true,0,0);

		$query = $allData->result_array();
		$offset = $this->uri->segment(3);
		$segment = 3;
		$base_url = site_url('project/featured/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);

		$data['query']=$this->userproject_model->get_projects_paginate(false,true,false,true,$per_page,$offset);
		$data['navactive']='browseprojectnav';
		$data['title']='Featured Project';
		//$this->load->view('project_include/all_featured',$data,false);
		if($this->input->get('dashboard'))
			$this->load->view('project_include/urgent_ajax',$data,false);
		else
			$this->load->view('project_include/all_urgent',$data,false);
	}

	function latest_project()
	{
		$this->load->helper('pagination_style');
		$data['title']="My Project";
		$data['navactive']="myprojectnav";
		$data['queryprovince']=$this->global_model->get_province();
		$allData=$this->userproject_model->get_projects_paginate(false,false,false,true,0,0);

		$query = $allData->result_array();
		$offset = $this->uri->segment(3);
		$segment = 3;
		$base_url = site_url('project/latest/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);

		$data['query']=$this->userproject_model->get_projects_paginate(false,false,false,true,$per_page,$offset);
		$data['navactive']="browseprojectnav";
		$data['title']="Latest Project";
		//$this->load->view('project_include/all_latest',$data,false);
		if($this->input->get('dashboard'))
			$this->load->view('project_include/latest_ajax',$data,false);
		else
			$this->load->view('project_include/all_latest',$data,false);
	}

	public function search_project()
	{
		$key = $this->input->post('key');
		$location = $this->input->post('location');
		$category = $this->input->post('category');
		$budget = $this->input->post('budget');
		$resultproject = $this->user_role_model->main_search_project($key,$location,$budget,$category)->result_array();
		$resultuser = $this->user_role_model->main_search_user($key,$location,$category)->result_array();
		$resultevent = $this->user_role_model->main_search_event($key,$location,$category)->result_array();

		
		$data['queryproject']=$resultproject;
		$data['queryuser']=$resultuser;
		$data['queryevent']=$resultevent;

		$data['key']=$key;
		$data['location']=$location;
		$data['budget']=$budget;
		$data['category']=$category;
		// $data['navactive']="browseprojectnav";
		$data['queryprovince']=$this->global_model->get_province();
		$data['title']="Search Result";
		$this->load->view('project_include/search_project_user',$data,false);
	}

	public function comment()
	{
		$this->load->view('project_include/add_comment');	
	}

	public function add_comment()
	{
		$set_array=array(
			'user_id'=>$this->session->userdata('user_id'),
			'comment_type'=>1,
			'comment_for'=>$this->uri->segment(4),
			'comment_text'=>$this->input->post('comment_text'),
			'comment_date'=>strtotime(date('Y-m-d H:i:s')),
		);
		$this->userproject_model->save_comment($set_array);
		redirect('project/view/'.$this->uri->segment(4));
	}

	public function rating($project,$user)
	{
		$data['project']=$project;
		$data['user']=$user;
		$this->load->view('project_include/add_rating',$data,false);	
	}
	
	public function add_rating($project,$user)
	{
		if ($this->input->post('score') != '') {
			$set_array=array(
				'user_id'=>$this->session->userdata('user_id'),
				'comment_type'=>4,
				'comment_for'=>$user,
				'comment_text'=>$this->input->post('comment_text'),
				'comment_date'=>strtotime(date('Y-m-d H:i:s')),
			);
			$comment = $this->userproject_model->save_comment($set_array);
		} else {
			$comment = 0;
		}

		$set_array2=array(
			'rating_type'=>3,
			'rating_for'=>$user,
			'rating_value'=>$this->input->post('score'),
			'rating_comment'=>$comment,
		);
		$this->userproject_model->save_rating($set_array2);
		redirect('project/view/'.$project);
	}
}