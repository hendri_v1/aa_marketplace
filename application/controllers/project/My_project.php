<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class my_project extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
                if(!$this->session->userdata('logged'))
		{
			redirect('program');			
		}
		$this->load->model('userproject_model');
		
	}

	function index()
	{
		$this->load->helper('pagination_style');
		$data['title']="My Project";
		$data['navactive']="myprojectnav";
		$allData=$this->userproject_model->get_projects_paginate(false,false,true,false,0,0);

		$query = $allData->result_array();
		$offset = $this->uri->segment(4);
		$segment = 4;
		$base_url = site_url('project/my_project/index/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);
		$data['query']=$this->userproject_model->get_projects_paginate(false,false,true,false,$per_page,$offset);

		$this->load->view('project_include/my_project',$data,false);
	}

	function view_bidding($projectbid_id,$shortlisted = 'false')
	{
		$data['shortlisted'] = $shortlisted; 
		$data['row'] = $this->userproject_model->get_single_bidder($projectbid_id);
		$this->load->view('project_include/view_bidder',$data);
	}

	function add_shortlist($projectbid_id,$project_id)
	{
		$this->userproject_model->update_shortlisted($projectbid_id);
		redirect('project/view/'.$project_id);
	}

	function update_winner($projectbid_id,$project_id)
	{
		$this->userproject_model->update_winner($project_id,$projectbid_id);
		// turn off for a while $this->notifications_model->winner_notification($this->input->get('bidder_id'),$project_id);

		$this->load->model('global_model');
		$this->load->model('user_role_model');

		$getOwner = $this->session->userdata('user_meta');
		$getpemenang = $this->user_role_model->get_user_by_id($this->input->get('bidder_id'));
		$getProject = $this->userproject_model->get_single_project($project_id);

		
		//send mail user
		$to = $getpemenang->user_email;
		$cc = '';
		$subject = 'Anda Terpilih Sebagai Pemenang';
		$data1['array_email'] = array(
			'project_name' => $getProject->project_title,
			'thebidder' => $getpemenang->userprofile_firstname
		);
		$content = $this->load->view('mailing_view/mail_winner', $data1, TRUE);

		//send mail owner
		$this->global_model->sendmailMandrill2($to,$cc,$subject,$content);

		//send mail owner
		$to2 = $getOwner->user_email;
		$cc2 = '';
		$subject2 = 'Anda telah memilih pemenang';
		$data2['array_email'] = array(
			'project_name' => $getProject->project_title,
			'thebidder' => $getpemenang->userprofile_firstname.' '.$getpemenang->userprofile_lastname
		);
		$content2 = $this->load->view('mailing_view/mail_winner_owner', $data2, TRUE);
		
		//send mail owner
		$this->global_model->sendmailMandrill2($to2,$cc2,$subject2,$content2);

		$biders = $this->userproject_model->get_bidder($project_id)->result_array();

		foreach ($biders as $notwin) {
			if ($notwin['projectbid_user_id'] != $getpemenang->user_id) {
				//send mail not win
				$to3 = $notwin['user_email'];
				$cc3 = '';
				$subject3 = 'Anda tidak terpilih sebagai pemenang';
				$data3['example'] = '';
				$content3 = $this->load->view('mailing_view/mail_notwin', $data2, TRUE);
				
				//send mail not win
				$this->global_model->sendmailMandrill2($to3,$cc3,$subject3,$content3);
			}
		}

		redirect('project/view/'.$project_id);
	}

	function my_bidding()
	{
		$this->load->helper('pagination_style');
		$allData=$this->userproject_model->bidding_list_paginate($this->session->userdata('user_id'),0,0);
		$query = $allData->result_array();
		$offset = $this->uri->segment(3);
		$segment = 3;
		$base_url = site_url('project/my_bidding/');
		$per_page = 10;
		$total_rows = count($query);
		paginate_bs3($base_url, $per_page, $total_rows,$segment);

		$data['title']="My Bidding";
		$data['navactive']="mybiddingnav";
		$data['query']=$this->userproject_model->bidding_list_paginate($this->session->userdata('user_id'),$per_page,$offset);
		$this->load->view('project_include/my_bidding',$data,false);
	}
}