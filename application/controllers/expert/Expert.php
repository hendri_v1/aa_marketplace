<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expert extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_role_model');
		$this->load->model('userprofile_model');
		$this->load->model('userpost_model');
	}

	function index()
	{
		$this->load->helper('pagination_style');
		$data['title']="HR Expert Directory";
		$data['queryusercomplete']=$this->user_role_model->get_user_complete(true);
		$data['province']=$this->global_model->get_province();
		$data['querycvendor']=$this->global_model->get_category(2);
		$filter_array=$this->input->get();
		$alldata=$this->user_role_model->get_user_paginate(0,0,false,false,false,'',1,true,$filter_array);
		$query=$alldata->result_array();
		$base_url=site_url('individual-directory');
		$total_rows=count($query);
		$limit=15;
		$segment = 2;
		$per_page=15;
		$offset=$this->uri->segment(2);
		paginate_bs3($base_url, $per_page, $total_rows, $segment,TRUE);
		$data['querylist']=$this->user_role_model->get_user_paginate($limit,$offset,false,false,false,'',1,true,$filter_array);
		$this->load->view('expert_include/expert',$data,false);
	}

	function view($user_id=0)
	{
		$this->load->model('event_model');
		$where=array(
			'user.user_id'=>$user_id
		);
		
		$data['row']=$this->user_role_model->get_user_by_id($user_id);
		
		$data['navactive']="myprofilenav";
		$queryrate=$this->userprofile_model->get_rating(3,$user_id);
		$therate=0;
		$therater=0;
		foreach($queryrate->result() as $rowsrate)
		{
			$therate=$therate+$rowsrate->rating_value;
			$therater++;
		}
		if($therater<>0)
		{
			$avgrate=$therate/$therater;
		}
		else
			$avgrate=0;

		$userextra_type=0;
		// if($this->session->userdata('user_meta')->usertype_id==1)
		// 	$userextra_type=1;
		// else
		// 	$userextra_type=2;

		$data['my_project']=$this->userproject_model->get_list_project($user_id);
		$data['my_bid']=$this->userproject_model->bidding_list_paginate($user_id,0,0);
		$data['my_post']=$this->userpost_model->get_list_post($user_id);
		$data['my_event']=$this->event_model->get_event_by_user(0,0,$user_id);


		$data['case']=$this->userprofile_model->get_extra(1,$data['row']->userprofile_id);	
		$data['certificate']=$this->userprofile_model->get_extra(2,$data['row']->userprofile_id);	
		$data['avgrate']=$avgrate;
		$data['therater']=$therater;
		$name_to_show='';
		if($data['row']->usertype_id==1)
		{
			$name_to_show=$data['row']->userprofile_firstname.' '.$data['row']->userprofile_lastname;
		}
		else
		{
			$name_to_show=$data['row']->userprofile_companyname;
		}
        $data['title']=$name_to_show;                           
		$data['meta_def']="Profil dari ".$name_to_show.", Silahkan Mengunjungi HR Plasa Untuk melihat lengkapnya";
		$this->load->view('expert_include/view_expert',$data,false);	
	}

}