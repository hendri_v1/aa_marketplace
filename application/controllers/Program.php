<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Program extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_role_model');
		$this->load->model('userpost_model');
		$this->load->model('userprofile_model');
		$this->load->model('event_model');
		$this->load->model('venue_model');
	}

	public function index()
	{
		$data['title'] = 'Pasar Online SDM Pertama di Indonesia';
		$data['navactive']="homenav";
		$data['queryprovince']=$this->global_model->get_province();
		$data['total_all_events']=$this->global_model->get_all_event();
		$data['total_active_events']=$this->global_model->get_all_event(TRUE);
		if($this->session->userdata('logged'))
		{
		
			if($this->session->userdata('user_meta')->usertype_id==99)
			{
				
				$data['nav_side']="dashboard";
				$data['title']="Admin Dashboard";
				
				$this->load->view('admin_include/dashboard',$data,false);
			}
			else
			{
				
				//$data['queryprovince']=$this->global_model->get_province();
				$data['queryusers']=$this->user_role_model->get_user_paginate(0,0,true);
				$data['queryusercomplete']=$this->user_role_model->get_user_complete(true);
				$data['queryfeaturedpremium']=$this->event_model->get_event_paginate(6,0,TRUE,FALSE,TRUE,0,0,0,'',0,0,0,true);
				$data['queryfeaturedevents']=$this->event_model->get_event_paginate(6,0,TRUE,FALSE,TRUE);
				$data['querypopularevents']=$this->event_model->get_popular_events(10);
				$data['queryvendor']=$this->user_role_model->get_user_paginate(6,0,false,false,false,'',2,true);
				$data['queryblog']=$this->userpost_model->get_blog(5,0);
				$data['my_event']=$this->event_model->get_event(0,0);
				
				$this->load->view('program',$data,false);
				
			}
		}
		else
		{
			
			//$data['queryprovince']=$this->global_model->get_province();
			$data['queryusers']=$this->user_role_model->get_user_paginate(0,0,true);
			$data['queryusercomplete']=$this->user_role_model->get_user_complete(true);
			$data['queryfeaturedpremium']=$this->event_model->get_event_paginate(6,0,TRUE,FALSE,TRUE,0,0,0,'',0,0,0,true);
			$data['queryfeaturedevents']=$this->event_model->get_event_paginate(6,0,TRUE,FALSE,TRUE);
			$data['querypopularevents']=$this->event_model->get_popular_events(10);
			$data['queryvendor']=$this->user_role_model->get_user_paginate(6,0,false,false,false,'',2,true);
			$data['queryblog']=$this->userpost_model->get_blog(5,0);
			$this->load->view('program.php',$data,false);
		}
	}

	public function dashboard()
	{
		$this->load->model('billing_model');
		if($this->session->userdata('logged'))
		{
			$data['title']="Dashboard";
			$data['nav_side']="dashboard";
			$data['top_side']="dashboard";
			$data['my_event']=$this->event_model->get_event(0,0);
			$data['my_event_popular']=$this->event_model->get_event(4,0,TRUE,FALSE,FALSE,FALSE,3);
			$data['latest_event']=$this->event_model->get_event_paginate(5,0,TRUE);
			$data['my_post']=$this->userpost_model->get_mypost();
			$data['qbilling']=$this->billing_model->get_my_billing_grouped();
			$this->load->view('member_include/dashboard',$data);
		}
		else
		{
			redirect('program');
		}
	}

	public function login()
	{
		if ($this->session->userdata('user_id') != '') {
			redirect('program');
		}
		$data['title'] = 'Login Form';
		$this->load->view('global_include/login.php',$data,false);
	}
	public function get_login()
	{
		if($this->global_model->valid_email($this->input->post('email'))==TRUE)
		{
			$where = array(
				'user_email' => $this->input->post('email'), 
				'password' => hash('sha1', $this->input->post('password')),
			);
		}
		else
		{
			$where = array(
				'username' => $this->input->post('email'),
				'password' => hash('sha1',$this->input->post('password'))
			);
		}
		$checkmail = $this->user_role_model->check_mail($where)->row_array();
		//echo $checkmail['user_id'];
		if (empty($checkmail)) {
			$this->session->set_flashdata('notif', 'Login Failed');
			redirect('program/login');
		} else{
			//$this->session->set_flashdata('success', 'Login Success');
                        if(!$this->input->post('remember')){
                            $this->session->sess_expiration = 7200;
                            $this->session->sess_expire_on_close = FALSE;
                        }
			$this->session->set_userdata('logged',true);
			$this->session->set_userdata('user_id',$checkmail['user_id']);
			$this->session->set_userdata('user_meta',$this->user_role_model->get_user_by_id($checkmail['user_id'],true));
			$categrow=$this->userprofile_model->get_user_category($checkmail['user_id']);
			$this->session->set_userdata('category_id',$categrow->category_id);
			$this->user_role_model->update_last_login($checkmail['user_id']);
			if ($this->input->get('redirect_to')) {
				redirect($this->input->get('redirect_to'));
			} else {
				redirect('program');
			}

		}
	}
	public function register()
	{
		$data['province'] = $this->global_model->get_province();
		$data['utype'] = array(1=>'HR Expert',2=>'HR Vendor',3=>'Professional - Non HR');
		$data['title']='Register';
		$this->load->view('global_include/register.php',$data,false);
	}

	public function save_register()
	{
		//do the validation
		//print_r($this->input->post());
		$validation_not_passed=0;
		$error_message=array();
		if($this->input->post('username')=='')
		{
			$validation_not_passed++;
			$error_message[]="Mohon Masukkan Username";
		}
		if($this->input->post('password')=='')
		{
			$validation_not_passed++;
			$error_message[]="Mohon Masukkan Password";
		}
		if($this->input->post('email')=='')
		{
			$validation_not_passed++;
			$error_message[]="Mohon Masukkan Email";
		}
		if($this->input->post('registered')==1)
		{
			if($this->input->post('userprofile_function')==98)
			{
				$validation_not_passed++;
				$error_message[]="Mohon pilih kategori";	
			}
			
		}
		elseif($this->input->post('registered')==2)
		{
			if($this->input->post('userprofile_function2')==99)
			{
				$validation_not_passed++;
				$error_message[]="Mohon pilih kategori";	
			}
			
			if($this->input->post('cname')=='')
			{
				$validation_not_passed++;
				$error_message[]="Nama Perusahaan Harus Diisi";	
			}
			if($this->input->post('position')=='')
			{
				$validation_not_passed++;
				$error_message[]="Jabatan Harus diisi";	
			}
		}
		if($this->input->post('province_id')==0)
		{
			$validation_not_passed++;
			$error_message[]="Mohon pilih provinsi";	
		}
		if($this->input->post('city_id')==0)
		{
			$validation_not_passed++;
			$error_message[]="Mohon pilih kota";	
		}
		if($validation_not_passed>0)
		{
			$error_return='<ul>';
			foreach($error_message as $key=>$value)
			{
				$error_return=$error_return.'<li>'.$value.'</li>';	
			}
			$error_return=$error_return.'</ul>';
			$this->session->set_flashdata('error_return',$error_return);
			redirect('register');
		}
		else
		{
			
			$postuser = array(
				'username' => $this->input->post('username'), 
				'user_email' => $this->input->post('email'), 
				'password' => hash('sha1', $this->input->post('password')), 
				'user_register_date' => strtotime(date('Y-m-d H:i:s')), 
			);
			$user_id=$this->user_role_model->save_user($postuser);
			$cateogry_id=0;
			if($this->input->post('registered')==1)
				$category_id=$this->input->post('userprofile_function');
			else
				$category_id=$this->input->post('userprofile_function2');
			
			$postprofile = array( 
				'user_id' => $user_id, 
				'usertype_id' => $this->input->post('registered'), 
				'userprofile_firstname' => $this->input->post('fname'), 
				'userprofile_lastname' => $this->input->post('lname'),
				'userprofile_companyname' => $this->input->post('cname'),
				'userprofile_position' => $this->input->post('position'),
				'userprofile_function' => $category_id,
				'city_id' => $this->input->post('city_id')
			);
			
			$this->user_role_model->save_userprofile($postprofile);
			$this->global_model->add_category($category_id,1,$user_id);
	
			$where = array(
				'user_email' => $this->input->post('email'), 
				'password' => hash('sha1', $this->input->post('password')),
			);
			$checkmail = $this->user_role_model->check_mail($where)->row_array();
			$this->session->set_userdata('logged',true);
			$this->session->set_userdata('user_id',$checkmail['user_id']);
			$this->session->set_userdata('user_meta',$this->user_role_model->get_user_by_id($checkmail['user_id']));
			$categrow=$this->userprofile_model->get_user_category($checkmail['user_id']);
			$this->session->set_userdata('category_id',$categrow->category_id);
			$this->load->model('global_model');
			
			$to = $this->input->post('email');
			$cc = '';
			$subject = 'Terima Kasih telah mendaftar di hrplasa.id';
	
			$data['user_meta']=$this->user_role_model->get_user_by_id($checkmail['user_id']);
			$content = $this->load->view('mailing_view/mail_welcome', $data, TRUE);
	
			$this->global_model->sendmailMandrill2($to,$cc,$subject,$content);
			
			if ($this->input->get('redirect_to')) {
				redirect($this->input->get('redirect_to'));
			} else {
				redirect('profile/my_profile');
			}
			
			
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('program');
	}

	public function getProvince()
	{
		$province_id = $this->input->post('province');
		$city = $this->global_model->get_city($province_id)->result();
		$option = '';
		$option .='<option value="0">Semua Kota</option>';
		foreach ($city as $listCity) {
			$option .= '<option value="'.$listCity->city_id.'">'.$listCity->city_name.'</option>';
		}
		echo $option;
	}

	function getProvinceforMap()
	{
		$province_id = $this->input->post('province');
		$rowprovince=$this->global_model->get_province_by_id($province_id);
		$city = $this->global_model->get_city($province_id)->result();
		$option = '<option value="0">Pilih Kota</option>';
		foreach ($city as $listCity) {
			$option .= '<option value="'.$listCity->city_id.'">'.$listCity->city_name.'</option>';
		}
		$kembali=array();
		$kembali['option']=$option;
		$kembali['province_name']=$rowprovince->province_name;

		echo json_encode($kembali);
	}

	function getCityName()
	{
		$city_id=$this->input->post('city_id');
		$rowcity=$this->global_model->get_city_by_id($city_id);
		echo $rowcity->city_name.', '.$rowcity->province_name;
	}

	public function search_keywords()
	{
		if (isset($_GET["term"])) {
			$dataSearch = $this->user_role_model->get_keywords($_GET["term"]);
			echo json_encode($dataSearch);
		} else {
			redirect('program');
		}
	}

	public function search_main()
	{
		$this->load->model('venue_model');
		$directory=$this->input->get('directory');
		$location=$this->input->get('location');
		$keyword=$this->input->get('key');
		$search_method=$this->input->get('search_method');
		if($directory==0)
		{
			//save the search query
			$data['title']="Hasil Pencarian";
			$data['directory']=$directory;
			$data['keyword']=$keyword;
			$data['search_method']=$search_method;
			$data['location']=$location;
			$data['queryprovince']=$this->global_model->get_province();
			//get the events
			$data['queryevent']=$this->event_model->search_by_keyword($keyword,$search_method,$location);
			//get the venue
			$data['queryvenue']=$this->venue_model->search_by_keyword($keyword);
			//get the member individual
			$data['queryindividual']=$this->user_role_model->search_by_keyword($keyword,1);
			//get the member vendor
			$data['queryvendor']=$this->user_role_model->search_by_keyword($keyword,2);
			$this->load->view('global_include/global_search_result',$data);
		}
		elseif($directory==1)
		{
			redirect('events/?province_id='.$location.'&event_keyword='.$keyword);
		}
		
		
	}

	function vendor_category()
	{
		$this->load->helper('pagination_style');
		$data['title']="HR Vendor Directory";
		$data['queryusercomplete']=$this->user_role_model->get_user_complete(true);
		$data['province']=$this->global_model->get_province();
		$data['querycvendor']=$this->global_model->get_category(2);
		$alldata=$this->user_role_model->get_user_paginate(0,0,false,false,false,'',2,true);
		$query=$alldata->result_array();
		$base_url=site_url('vendor-directory');
		$total_rows=count($query);
		$limit=16;
		$segment = 2;
		$per_page=16;
		$offset=$this->uri->segment(2);
		paginate_bs3($base_url, $per_page, $total_rows, $segment);
		$data['querylist']=$this->user_role_model->get_user_paginate($limit,$offset,false,false,false,'',2,true);
		$this->load->view('global_include/vendor_category',$data,false);
	}

	function user_category()
	{
		$data['title']="HR Expert Directory";
		$data['queryusercomplete']=$this->user_role_model->get_user_complete(true);
		$data['province']=$this->global_model->get_province();
		$data['querycuser']=$this->global_model->get_category(1);
		$this->load->view('global_include/user_category',$data,false);
	}

	function individual_list($category_id, $category_name)
	{
		$this->load->model('userprofile_model');
		$data['title']=urldecode($category_name);
		$data['category_name']=urldecode($category_name);
		$data['querylist']=$this->userprofile_model->list_per_category($category_id,1);
		$this->load->view('global_include/user_category_list',$data,false);
	}

	function vendor_list($category_id,$category_name)
	{
		$this->load->model('userprofile_model');
		$data['title']=urldecode($category_name);
		$data['category_name']=urldecode($category_name);
		$data['querylist']=$this->userprofile_model->list_per_category($category_id,1);
		$this->load->view('global_include/vendor_category_list',$data,false);
	}

	function membership()
	{
		$data['title']="Membership";
		$data['navactive']="membershipnav";
		$this->load->view('membership',$data,false);
	}

	function aboutus()
	{
		$data['title']="About Us";
		$this->load->view('global_include/aboutus',$data,false);
	}

	function term_and_condition()
	{
		$data['title']="Term and Condition";
		$this->load->view('termandcondition',$data,false);
	}

	function faq()
	{
		$data['title']="FAQ";
		$this->load->view('global_include/faq',$data);
	}

	function cara_kerja()
	{
		$data['title']="Cara Kerja";
		$this->load->view('carakerja',$data);
	}
	
	function privacy_policy()
	{
		$data['title']="Kebijakan Privasi";
		$this->load->view('privacy_policy',$data);	
	}
	
	function tnk()
	{
		$data['title']="Syarat dan Ketentuan";
		$this->load->view('tnk',$data);	
	}
	
	function ads_program()
	{
		$data['title']="Info Iklan";
		$this->load->view('ads_program',$data);	
	}

	function artikel()
	{
		$data['title']="Kumpulan Artikel";
		$data['query']=$this->userpost_model->get_documents();
		$this->load->view('member_include/artikel',$data,false);
	}

	function prnewswire()
	{
		$data['title']="Barita dari PR Newswire";
		$this->load->view('prnewswire');
	}

	function check_exist()
	{
		if($this->input->post('check_what')=="username")
		{
			$array_where=array(
				"username"=>$this->input->post('tocheck')
			);
		}
		elseif($this->input->post('check_what')=="email")
		{
			$array_where=array(
				"user_email"=>$this->input->post('tocheck')
			);
		}
		$checkresult=$this->user_role_model->check_exist($array_where);
		$kembali=array();
		if($checkresult==0)
		{
			$kembali['error']=false;
			$kembali['message']=$this->input->post('check_what').' '.$this->input->post('tocheck').' tersedia, Anda dapat menggunakan ini';
		}
		else
		{
			$kembali['error']=true;
			$kembali['message']=$this->input->post('check_what').' '.$this->input->post('tocheck').' sudah digunakan, gunakan '.$this->input->post('check_what').' yang lain';
		}
		echo json_encode($kembali);
	}

	function send_mail_test()
	{
		
		echo $this->global_model->sendmailMandrill('hendriansah@live.com','',rand(),rand());	
	}

	function send_mail_test2()
	{
		echo $this->global_model->sendmailserver('hendriansah@live.com','',rand(),rand());	
	}

	function send_mail_test3()
	{
		echo $this->global_model->sendmailMandrill2('hendriansah@live.com','',rand(),rand());	
	}

	

	function reset($kode='')
	{
		if ($kode == '') {
			$data['title']="Forgot Password";
			$this->load->view('forgot',$data,false);
		} else  {
			$getUser = $this->user_role_model->get_user_by_kode($kode);
			if (!empty($getUser)) {
				$data['title']="Reset Password";
				$this->load->view('reset',$data,false);
			} else{
				$this->session->set_flashdata('notif', 'Data Not found');
				redirect('program/reset/');
			}
		}
	}

	function save_password($kode='')
	{
		$getUser = $this->user_role_model->get_user_by_kode($kode);
		if (!empty($getUser)) {
			$password = hash('sha1', $this->input->post('password'));
			$this->user_role_model->update_user(array('password'=>$password,'reset_password'=>NULL),$getUser[0]['user_id']);
			redirect('program/login/');
		} else{
			$this->session->set_flashdata('notif', 'Data Not found');
			redirect('program/reset/');
		}
		
	}
	
	function find_email()
	{
		$getMail = $this->user_role_model->get_user_by_email($this->input->post('email'));
		if (!empty($getMail)) {
			$kode = $this->global_model->randomizeMailCode(30);
			//send mail upgrader
			$to = $getMail[0]['user_email'];
			$subject = 'Reset Pasword HRPlasa';
			$data['kode'] = $kode;
			$data['name'] = $getMail[0]['userprofile_firstname'];
			$content = $this->load->view('mailing_view/mail_reset', $data, TRUE);

			//send mail upgrader
			$this->global_model->sendmailMandrill2($to,'',$subject,$content);
			$this->user_role_model->update_user(array('reset_password'=>$data['kode']),$getMail[0]['user_id']);
			$this->session->set_flashdata('success', 'Kode reset password telah di kirim ke email anda');
		} else {
			$this->session->set_flashdata('notif', 'Data Not found');
		}
		redirect('program/reset/');
	}

	function phpinfo_test()
	{
		phpinfo();
	}
}

/* End of file program.php */
/* Location: ./application/controllers/program.php */