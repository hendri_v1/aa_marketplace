<div class="mt-80">
    <div class="hero-image">
        <div class="hero-image-inner" style="background-image: url('assets/img/main_bg.jpeg');">
            <div class="hero-image-content">
                <div class="container">
                    <h1>Mulailah Dari Sekarang</h1>

                    <p>Bergabung bersama HRPlasa.id sekarang juga <br> dan manfaatkan fasilitas yang kami sediakan.</p>

                    <a href="<?php echo site_url('login');?>" class="btn btn-primary btn-lg">Login</a>
                    <a href="<?php echo site_url('register');?>" class="btn btn-secondary btn-lg">Sign Up For FREE</a>
                </div><!-- /.container -->
            </div><!-- /.hero-image-content -->

            <div class="hero-image-form-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-8 col-lg-4 col-lg-offset-7">
                            <form method="get" action="?">
                                <h2>Universal Search</h2>

                                <div class="hero-image-keyword form-group">
                                    <input type="text" class="form-control" placeholder="Keyword">
                                </div><!-- /.form-group -->

                                <div class="hero-image-location form-group">
                                    <select class="form-control" title="Pilih Provinsi">
                                        <?php foreach($queryprovince as $rowsprovince): ?>
                                            <option <?php if(isset($location)){ if($location == $rowsprovince->province_id){ echo 'selected="selected"'; }}?> value="<?php echo $rowsprovince->province_id;?>"><?php echo $rowsprovince->province_name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div><!-- /.form-group -->

                               

                                <div class="hero-image-category form-group">
                                    <select class="form-control" title="Direktori">
                                        <option value="1">Event</option>
                                        <option value="2">HR Vendor</option>
                                        <option value="3">HR Expert</option>
                                        <option value="4">Venue</option>
                                        <option value="5">Project</option>
                                        <option value="6">Lowongan</option>
                                    </select>
                                </div><!-- /.form-group -->

                                

                                <button type="submit" class="btn btn-primary btn-block">Search</button>
                            </form>
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.hero-image-form-wrapper -->
        </div><!-- /.hero-image-inner -->
    </div><!-- /.hero-image -->
 </div>