<?php $this->load->view('member_include/html_head');?>

	<div class="row">
		<div class="col-sm-12">
			
				<form action="<?php echo site_url('job/my_job/save_job');?>" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-6">
							<div class="background-white p20">
								<?php echo bt_text('job_title','Judul Lowongan','masukkan judul');?>

								<?php echo bt_textarea('job_description','Deskripsi');?>

								<?php echo bt_textarea('job_qualification', 'Kualifikasi');?>

								<?php echo bt_file('job_main_image','Gambar Utama');?>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="background-white p20">
								<?php
                                    $array_province=array();
                                    $array_province[0]="Pilih Provinsi";
                                    foreach($province as $rowsprovince)
                                    {
                                        $array_province[$rowsprovince->province_id]=$rowsprovince->province_name;
                                    }
                                    echo bt_select('province_id','Pilih Provinsi',$array_province);

                                ?>

                                <?php
                                    $array_city=array();
                                    $array_city[0]="Pilih Kota";
                                    echo bt_select('city_id','Pilih Kota',$array_city);
                                ?>

                                <?php 
									$job_salary_type = array(
										1 => 'Bulanan',
										2 => 'Tahunan',
										3 => 'Mingguan',
										4 => 'Per Jam'
									);
									echo bt_select('job_salary_type', 'Tipe Gaji',$job_salary_type);
								?>

								<?php echo bt_text('job_company_name','Nama Perusahaan','Diisi bila tidak sama dengan akun ini');?>

								<?php
									$job_category_id=array();
									foreach($query->result() as $rows)
									{
										$job_category_id[$rows->job_category_id] = $rows->job_category_name;
									}
									echo bt_select('job_category_id', 'Kategori', $job_category_id);
								?>

								

                          	</div>
                        </div>
                        <div class="col-sm-3">
                        	<div class="background-white p20">
								<?php
									$job_position_level=array(
										1 => 'Fresh Grad',
										2 => 'Entry Level',
										3 => 'Staff (non-managemet)',
										4 => 'Supervisor / Coordinator',
										5 => 'Manager / Ass Manager',
										6 => 'CEO / GM / Director / Senior Manager'

									);
									echo bt_select('job_position_level','Level Pekerjaan', $job_position_level);
								?>

								
								<?php
									$job_type = array(
										1 => 'Tetap',
										2 => 'Kontrak',
										3 => 'Freelance',
										4 => 'Interns / Magang'
									);
									echo bt_select('job_type', 'Tipe', $job_type);
								?>

								<?php echo bt_text('job_salary_start','Gaji Mulai Dari');?>

								<?php echo bt_text('job_salary_end','Gaji Hingga');?>

								<?php echo bt_text('job_close_date','Job Closing','',date('d/m/Y h.m'));?>

								

								<input type="submit" value="Simpan" class="btn btn-primary" />
							</div>

						</div>
					</div>
					
				</form>
			
		</div>
	</div>

<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/libraries/bootstrap-datetimepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/libraries/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var config = {
            toolbar:
            [
                ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Undo', 'Redo', '-', 'SelectAll'],
                ['UIColor']
            ]
        };

        $('#job_qualification,#job_description').ckeditor(config);


        $('#province_id').change(function(event) {
            var prov = $("#province_id").val();

             $.post('<?php echo site_url('program/getProvinceforMap');?>',
             {
                province:prov
             },
             function(data)
             {
                
                $('#city_id').html(data.option);
             },'json'
             );
        });

        $('#job_close_date').datetimepicker({
          language: 'id',
          useCurrent: false 
        });
	})


</script>