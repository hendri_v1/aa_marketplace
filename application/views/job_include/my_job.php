<?php $this->load->view('member_include/html_head');?>

<div class="row">
	<div class="col-sm-12">
		<a href="<?php echo site_url('job/my_job/add');?>" class="btn btn-primary">Tambah Baru</a>
		<hr />
		<div class="background-white p20">
			<table class="table">
				<thead>
					<tr><th>#ID</th><th>Judul</th><th>Tanggal Dibuat</th><th>Tanggal Tutup</th><th>Status</th><th>Applicant</th><th>Action</th></tr>
				</thead>
				<tbody>
					<?php foreach($query->result() as $rows): ?>
						<tr>
							<td><?php echo $rows->job_id;?></td>
							<td><?php echo $rows->job_title;?></td>
							<td><?php echo mdate('%d %M %Y',$rows->added_at);?></td>
							<td><?php echo mdate('%d %M %Y',$rows->job_close_date);?></td>
							<td>
								<?php if($rows->job_status==1): ?>
									<span class="label label-primary">Aktif</span>
								<?php endif;?>
							</td>
							<td></td>
							<td>
								<div class="btn-group" role="group">
									<a href="#" class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
									<a href="#" class="btn btn-sm btn-danger"><i class="fa fa-close"></i></a>
								</div>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php $this->load->view('member_include/html_footer');?>