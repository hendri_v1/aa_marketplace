<?php $this->load->view('member_include/html_head');?>
<form action="<?php echo site_url('event/my_event/save_event/')?>" class="form" id="myform" method="POST" enctype="multipart/form-data">

	<div class="row">
		<div class="col-sm-12">
			<?php 
                $collected_fields=array();
                $collected_fields['province_id']=0;
                $collected_fields['city_id']=0;
                $collected_fields['venue']='';
                $collected_fields['event_address']='';
                $collected_fields['startdate']='';
                $collected_fields['enddate']='';
                $collected_fields['keywords']='';
                $collected_fields['title']='';
            ?>
            <?php if($this->session->flashdata('error_array')): ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-icon alert-dismissible alert-warning" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <strong>Perhatian</strong>
                            <ul>
                                <?php foreach($this->session->flashdata('error_array') as $key => $value): ?>
                                    <li><?php echo $value;?></li>
                                <?php endforeach;?>
                            <?php $collected_fields=$this->session->flashdata('collected_fields');?>
                            </ul>
                        </div>
                        <div class="boxes text-warning">
                            
                            
                        </div>
                    </div>
                </div>
            <?php endif;?>
            <div class="background-white p20">
            	<ul class="nav nav-tabs" role="tablist">
                  <li class="active"><a href="#locationevent" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i> Lokasi</a></li>
                  <li><a href="#event_type" role="tab" data-toggle="tab"><i class="fa fa-calendar"></i> Info Event</a></li>
                  <li><a href="#thespeaker" role"tab" data-toggle="tab"><i class="fa fa-user"></i> Speaker</a></li>
                  <li><a href="#detailevent" role="tab" data-toggle="tab"><i class="fa fa-list"></i> Detail Event</a></li>
                  <li><a href="#eventticket" role="tab" data-toggle="tab"><i class="fa fa-ticket"></i> Tiket Online</a></li>
                  <li><a href="#postnow" role="tab" data-toggle="tab"><i class="fa fa-floppy-o"></i> Pilihan Posting</a></li>
                </ul>
                <div class="tab-content">
                    
                    <div id="locationevent" class="tab-pane fade in active">
                        <div class="p20">
                            <div class="row" >
                                <div class="col-sm-6">
                                    <?php
                                        $array_province=array();
                                        $array_province[0]="Pilih Provinsi";
                                        foreach($province as $rowsprovince)
                                        {
                                            $array_province[$rowsprovince->province_id]=$rowsprovince->province_name;
                                        }
                                        echo bt_select('province_id','Pilih Provinsi',$array_province,$collected_fields['province_id']);

                                    ?>
                                    <?php
                                        $array_city=array();
                                        $array_city[0]="Pilih Kota";
                                        echo bt_select('city_id','Pilih Kota',$array_city,$collected_fields['city_id']);
                                    ?>
                                    <?php echo bt_text('event_address','Alamat Lengkap','Contoh : Jalan Jendral Sudirman No. 30',$collected_fields['event_address']);?>
                                    <?php echo bt_text('venue','Venue','Masukkan Venue',$collected_fields['venue']);?>

                                    
                                    <?php echo bt_text('event_geo','Alamat Geo Location','Akan terisi otomatis saat mengunci peta');?>
                                    Jika Penanda yang dihasilkan secara otomatis tidak tepat, Geser penanda untuk mengatur lokasi yang tepat
                                </div>
                                <div class="col-sm-6">
                                    <label>Geser Penanda Untuk Mengatur Lokasi Yang Tepat</label>
                                    <div id="map_canvas" style="height:400px;">

                                    </div>
                                    <span id="the_address"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="event_type" class="tab-pane fade">
                        <div class="p20">
                            <div class="row" >
                                <div class="col-sm-12">
                                    <?php echo bt_text('title','Judul Event','Masukkan Judul Event',$collected_fields['title']);?>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="project_category_id">Pilih Kategori Event Anda</label>
                                        <select name="eventcategory" class="form-control" id="project_category_id">
                                            <?php if (!empty($eventcategory)): ?>
                                                <?php foreach ($eventcategory as $list): ?>
                                                    <option value="<?php echo $list['eventcategory_id'] ?>"><?php echo $list['eventcategory_name'] ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                    <div id="datetimepicker9" class="input-append date">
                                            <div class="form-group">
                                                <label>Tanggal dan Jam Mulai</label>
                                                <div class='input-group date col-md-12' id='datetimepicker9'>
                                                    <input type='text' placeholder="Tanggal dan Jam Mulai" name="startdate" class="form-control" value="<?php echo $collected_fields['startdate'];?>" />
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                    </div>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <?php echo bt_text('event_budget','Biaya','Masukkan 0 Apabila Gratis');?>
                                    <div id="datetimepicker10" class="input-append date">
                                        <div class="form-group">
                                            <label>Tanggal dan Jam Selesai</label>
                                            <div class='input-group date col-md-12' id='datetimepicker10'>
                                                <input type='text' placeholder="Tanggal dan Jam Selesai" name="enddate" class="form-control" value="<?php echo $collected_fields['enddate'];?>" />
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class='form-group'>
                                        <label>Keywords</label>
                                            <input type='text' placeholder="Keywords pisahkan dengan koma" name="keywords" class="form-control" value="<?php echo $collected_fields['keywords'];?>" />
                                    </div>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Gambar Utama</label>
                                        <input type="file" name="image" class="form-control">
                                    </div> 
                                </div>
                            </div> 
                            
                        </div>
                    </div>
                    <div id="thespeaker" class="tab-pane fade">
                        <div class="p20">
                            <div class="row">
                                <div class="col-sm-12">
                                     <a href="#" class="btn btn-info" data-toggle="modal" data-target="#myModal" id="choose_speaker"><i class="fa fa-user"></i> Pilih Speaker</a> <a href="#" class="btn btn-primary" id="show_speaker_form">Masukkan Speaker</a>
                                     <hr />
                                     <div class="row" id="add_speaker_form">
                                        <div class="col-sm-4">
                                            
                                            <?php echo bt_text('event_speaker','Nama Speaker atau Pembicara');?>
                                            
                                            <div class="form-group">
                                                <label>Foto Speaker / Pembicara</label>
                                                <input type="file" name="speaker_image" class="form-control">
                                            </div>
                                            
                                            
                                            <input type="hidden" name="eventspeaker_hash" id="eventspeaker_hash" value="<?php echo $eventspeaker_hash;?>">
                                            <a href="javascript:void(0);" id="add_speakers" class="btn btn-primary">Tambahkan</a> 
                                        </div>
                                        <div class="col-sm-8">
                                            <?php echo bt_textarea('eventspeaker_info','Deskripsi');?>
                                        </div>
                                     </div>
                                </div>
                                
                                
                                <div class="col-sm-12" id="eventspeaker_load">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="detailevent" class="tab-pane fade">
                        <div class="p20">
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php echo bt_textarea('overview','Overview');?>
                                </div>
                                <div class="col-sm-6">
                                    <?php echo bt_textarea('outline','Outline');?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php echo bt_textarea('outcome','Outcome');?>
                                    
                                    
                                    
                                </div>
                                <div class="col-sm-6">
                                   <?php echo bt_textarea('sertifikat','Sertifikat');?>
                                </div>
                            </div>
                            
                            <div>
                                <div class="form-group">
                                    
                                </div>
                            </div>
                            
                            
                            
                            
                            
                        </div>
                    </div>

                    <div id="eventticket" class="tab-pane fade">
                        <div class="p20">
                            <div class="row">
                                <div class="col-sm-3">
                                    <?php echo bt_text('eventticket_seat','Total Online Register Seat','Masukkan 0 Jika tidak ada',0);?>
                                </div>
                                <div class="col-sm-3">
                                    <?php echo bt_text('eventticket_earlybird_seat','Total Online Early Bird Seat','Masukkan 0 Jika tidak ada',0);?>
                                </div>
                                <div class="col-sm-3">
                                    <?php echo bt_text('eventticket_earlybird_price','Harga Early Bird','Masukkan 0 Jika Gratis',0);?>
                                </div>
                                <div class="col-sm-3">
                                    <?php 
                                        $eventicket_public=array(0=>'Tampilkan Peserta Terdaftar',1=>'Hanya Tampilkan Total Terdaftar',2=>'Sembunyikan Data Pendaftaran');
                                        echo bt_select('eventticket_public','Informasi Peserta',$eventicket_public);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="postnow" class="tab-pane fade">
                        <div class="p20">
                             <?php foreach($qpricing->result() as $rpricing): ?>
                            <div class="row">                            
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            
                                                <input type="checkbox" name="pricing[]" id="pricing_<?php echo $rpricing->pricing_id;?>" value="<?php echo $rpricing->pricing_id;?>"> 
                                            <label for="pricing_<?php echo $rpricing->pricing_id;?>">
                                                <?php echo $rpricing->pricing_words;?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3" align="right">
                                    <span class="label label-tag"><i class="glyphicon glyphicon-star"></i> <?php echo $rpricing->pricing_info;?></span><br />
                                    <span class="lead text-warning"><?php echo number_format($rpricing->pricing_price,0,',','.');?></span> <br />per Event
                                </div> 
                            </div>
                            <hr />
                            <?php endforeach;?>
                            <input type="hidden" name="is_private" id="is_private" value="0">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</form>

<?php $this->load->view('member_include/html_footer');?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Pilih Speaker</h4>
            </div>
            <div class="modal-body">
                ...
            </div>

        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/libraries/bootstrap-datetimepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/libraries/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
    long_address="";
    $(document).ready(function(e) {
        var config = {
            toolbar:
            [
                ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Undo', 'Redo', '-', 'SelectAll'],
                ['UIColor']
            ]
        };

        $('#add_speaker_form').hide();
        $('#show_speaker_form').click(function(e){
            e.preventDefault();
            $('#add_speaker_form').slideDown();
        })
        $('#choose_speaker').click(function(){

            $('#add_speaker_form').slideUp();
            $('.modal-body').html(loader);
            $('.modal-body').load('<?php echo site_url('event/my_event/choose_speaker');?>');
        })

        $('#event_budget').blur(function(){
            if($(this).val()=='' || $(this).val()==0)
                alert('Anda memberi nilai 0, sistem akan menayangkan ini sebagai Gratis');
        })
        $('#eventspeaker_info').css('height','100px');
        $('#overview,#outline,#outcome,#sertifikat').ckeditor(config);
        $('#province_id').change(function(event) {
            var prov = $("#province_id").val();

             $.post('<?php echo site_url('program/getProvinceforMap');?>',
             {
                province:prov
             },
             function(data)
             {
                long_address='';
                $('#city_id').html(data.option);
                GMaps.geocode({
                  address: data.province_name,
                  callback: function(results, status) {
                    if (status == 'OK') {
                      var latlng = results[0].geometry.location;
                      map.setCenter(latlng.lat(), latlng.lng());
                      map.setZoom(9);
                    }
                  }
                });
             },'json'
             );
        });

        $('#city_id').change(function(){
            var city=$(this).val();
            $.post('<?php echo site_url('program/getCityName');?>',
            {
                city_id:city
            },
            function(data)
            {
               
                long_address=data;
                $('#the_address').html(long_address);
            });

        });

        $('#event_address').blur(function(event) {
            new_address=$(this).val()+', '+long_address;
            long_address=new_address;
               GMaps.geocode({
                  address: long_address,
                  callback: function(results, status) {
                    if (status == 'OK') {
                      var latlng = results[0].geometry.location;

                      map.setCenter(latlng.lat(), latlng.lng());
                      map.setZoom(15);
                      map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng(),
                        draggable: true,
                        dragend:function(e){
                           $('#event_geo').val(this.getPosition());
                        }
                      });
                      $('#event_geo').val(latlng.lat()+','+latlng.lng());
                    }
                  }
                });
                $('#the_address').html(long_address);
        });

        $('#refresh_map').click(function(e){
            e.preventDefault();
           
            GMaps.geocode({
              address: long_address,
              callback: function(results, status) {
                if (status == 'OK') {
                  var latlng = results[0].geometry.location;

                  map.setCenter(latlng.lat(), latlng.lng());
                  map.setZoom(15);
                  map.addMarker({
                    lat: latlng.lat(),
                    lng: latlng.lng(),
                    draggable: true,
                    dragend:function(e){
                       $('#event_geo').val(this.getPosition());
                    }
                  });
                  $('#event_geo').val(latlng.lat()+','+latlng.lng());
                }
              }
            });
            $('#the_address').html(long_address);
        })

        $('#datetimepicker9,#datetimepicker10').datetimepicker({
          language: 'id',
          useCurrent: false 
        });
        $("#datetimepicker9").on("dp.change",function (e) {
          $('#datetimepicker10').data("DateTimePicker").setMinDate(e.date);
        });
        $("#datetimepicker10").on("dp.change",function (e) {
          $('#datetimepicker9').data("DateTimePicker").setMaxDate(e.date);
        });

        map=new GMaps({
          div: '#map_canvas',
          lat: -12.043333,
          lng: -77.028333,
          zoom:9
        });
        GMaps.geocode({
          address: 'Jakarta',
          callback: function(results, status) {
            if (status == 'OK') {
              var latlng = results[0].geometry.location;
              map.setCenter(latlng.lat(), latlng.lng());
              
            }
          }
        });
        <?php if($collected_fields['province_id']>0): ?>
            var prov = $("#province_id").val();

             $.post('<?php echo site_url('program/getProvinceforMap');?>',
             {
                province:prov
             },
             function(data)
             {
                long_address='';
                $('#city_id').html(data.option);
                <?php if($collected_fields['city_id']>0): ?>
                    $('#city_id').val('<?php echo $collected_fields['city_id'];?>');
                <?php endif;?>
                GMaps.geocode({
                  address: data.province_name,
                  callback: function(results, status) {
                    if (status == 'OK') {
                      var latlng = results[0].geometry.location;
                      map.setCenter(latlng.lat(), latlng.lng());
                      map.setZoom(9);
                    }
                  }
                });
             },'json'
             );

                
        <?php endif;?>
        $('#add_speakers').click(function() {
            var formData = new FormData($('#myform')[0]);
            $('#eventspeaker_load').html(loader);
            $.ajax({
                url:"<?php echo site_url('event/my_event/add_speaker') ?>",
                type: 'POST',
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                success: function (data) {
                    if (data == 'true') {
                       $('#eventspeaker_load').load('<?php echo site_url('event/my_event/view_speaker_by_hash');?>/<?php echo $eventspeaker_hash;?>');
                       $('#event_speaker').val('');
                       $('#speaker_image').val('');
                       $('#eventspeaker_info').val('');
                    } else {
                        //$('#error_upload').html(data);
                        $('#error_alert').hide();
                        $('#error_alert').html('<div class="alert text-center alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + data + '</div>');
                        $('#error_alert').fadeIn('slow',function(){
                            $(this).delay(5000).fadeOut('slow');
                        });
                    }
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
         

        });
    });

    
</script>