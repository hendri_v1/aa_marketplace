<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<div class="background-white p20">
				<table class="table table-striped table-hover">
					<thead>
						<tr class="default">
							<th>#ID Pendaftaran</th><th>Nama</th><th>Lokasi</th><th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($query->result() as $rows): ?>
							<tr>
								<td><?php echo $rows->audience_id;?></td>
								<td><?php echo $rows->userprofile_firstname;?> <?php echo $rows->userprofile_lastname;?></td>
								<td><?php echo $rows->userprofile_address;?>, <?php echo strtolower($rows->city_name);?></td>
								
								<td><?php
										if($rows->event_audience_status==0)
											echo "Menunggu Pembayaran";
										else
											echo "Lunas";
									?>
								</td>
								
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php $this->load->view('member_include/html_footer');?>