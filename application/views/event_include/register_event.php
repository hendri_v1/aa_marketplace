<?php $this->load->view('html_head');?>
	<div class="main">
        <div class="main-inner">
            <div class="content">
            	<div class="container">
					<div class="row">
						<div class="col-sm-7">
							<h3 class="page-title">Rincian Pendaftaran</h3>
							<div class="background-white p20 mb30">
								<table class="table">
									<tr>
										<td>Judul Event</td>
										<td><?php echo $row->event_title;?></td>
									</tr>
									<tr>
										<td>Waktu Pelaksanaan</td>
										<td><?php echo mdate('%d/%m/%Y %h:%i:%s',$row->event_start_date).' - '.mdate('%d/%m/%Y %h:%i:%s',$row->event_end_date);?></td>
									</tr>
									<tr>
										<td>Tempat Pelaksanaan</td>
										<td><?php echo $row->event_venue.', '.$row->event_address.' '.$row->city_name;?></td>
									</tr>
									<tr>
										<td>Organizer</td>
										<td><?php if($row->usertype_id==1)
												echo $row->userprofile_firstname.' '.$row->userprofile_lastname;
											elseif($row->usertype_id==2)
												echo $row->userprofile_companyname;?></td>

									</tr>
									<tr>
										<td>Biaya</td>
										<?php 
											$is_earlybird=0;
											$the_price=0;
											if($reventtickets->eventticket_earlybird_seat>0):
												$is_earlybird=1;
												$the_price=$reventtickets->eventticket_earlybird_price; ?>
											<td><s>Rp. <?php echo number_format($row->budget_event,0,',','.');?></s> Rp. <?php echo number_format($reventtickets->eventticket_earlybird_price,0,',','.');?></td>
										<?php else:
												$the_price=$row->budget_event;
											 ?>
											<td>Rp. <?php echo number_format($row->budget_event,0,',','.');?></td>
										<?php endif;?>
									</tr>
								</table>
							</div>
						</div><!-- col -->
						<div class="col-sm-5">
							<h3 class="page-title">Konfirmasi Pendaftaran</h3>
							<div class="background-white p20 mb30">
								<form action="<?php echo site_url('event/event/save_registration');?>" method="post" id="form_register">
									<input type="hidden" name="eventticket_id" value="<?php echo $reventtickets->eventticket_id;?>">
									<input type="hidden" name="event_id" value="<?php echo $row->event_id;?>">
									<input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id');?>">
									<input type="hidden" name="is_earlybird" value="<?php echo $is_earlybird;?>">
									<input type="hidden" name="ticket_price" value="<?php echo $the_price;?>">
									<input type="hidden" name="event_title" value="<?php echo $row->event_title;?>">
									<input type="hidden" name="event_url" value="<?php echo site_url('event/view/'.$row->event_id.'/'.$row->event_title_slug);?>">
									<div class="form-group">
										<label for="userprofile_phone">Nomor Telepon</label>
										<input type="text" name="userprofile_phone" class="form-control" placeholder="Nomor Telepon" value="<?php echo $this->session->userdata('user_meta')->userprofile_phone;?>">
									</div>
									<input type="submit" value="Daftar Sekarang" id="submitnow" class="btn btn-primary btn-block">
								</form>
								<br />
								<a href="<?php echo site_url('event/view/'.$row->event_id.'/'.$row->event_title_slug);?>" class="btn btn-warning btn-block">Batal</a>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>



<?php $this->load->view('html_footer');?>