<div class="row">
	<div class="col-sm-12">
		Apakah Anda yakin akan menghapus event dengan judul "<?php echo $row->event_title;?>"?<br />
		Event yang telah dihapus tidak dapat dikembalikan lagi.
	</div>
	<div class="col-sm-12">
		<form action="<?php echo site_url('event/my_event/delete_event');?>" method="post">
			<input type="hidden" name="event_id" value="<?php echo $row->event_id;?>">
			<hr />
			<button class="btn btn-danger">Hapus</button>
		</form>
	</div>
</div>