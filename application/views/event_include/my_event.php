<?php $this->load->view('member_include/html_head');?>

<div class="row">
	<div class="col-sm-12">
		<div class="row">
            <div class="col-sm-6">
                <a href="<?php echo site_url('event/my_event/add_new') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Buat Event Publik</a>
                <a href="<?php echo site_url('event/my_event/add_private') ?>" class="btn btn-danger btn-sm"><i class="fa fa-key"></i> Buat Event Private</a>
                <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-user"></i> Kelola Speaker <span class="label">[Segera]</span></a>
            	
            </div>
            
        
            <div class="col-sm-6 text-right">
                <div class="form-inline">
                    <div class="form-group">
                        <?php
                            $availability = array('' => 'Semua Status', 1 => 'Aktif', 2 => 'Pending', 3 => 'Sudah Lewat', 4 => 'Ditarik', 5 => 'Private / Internal');
                            echo form_dropdown('event_status_form', $availability, $this->input->get('event_status'), 'class="form-control input-sm form-filter" id="event_status_form"');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            $order_by=array(0=>'Urutkan',1=>'Urut Berdasarkan Judul A-Z',2=>'Urut Berdasarkan Judul Z-A',3=>'Urut Berdasarkan Total Dilihat',4=>'Urutkan Dari Event Terdekat', 5=>'Urutkan Dari Event Terjauh');
                            echo form_dropdown('order_by_form',$order_by,$this->input->get('order_by'),'class="form-control input-sm form-filter" id="event_order_form"');
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            $per_page=array(10=>'10 per halaman',20=>'20 per halaman',50=>'50 per halaman',100=>'100 per halaman');
                            echo form_dropdown('per_page_form',$per_page,$this->input->get('per_page'),'class="form-control input-sm form-filter" id="per_page_form"');
                        ?>
                    </div>
                </div>
                

            </div>
            
        </div>
        <div class="row">
        	<hr />
        	<div class="col-sm-12 background-white p20">
				<table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Judul</th>
                            <th>Waktu</th>
                            <th class="text-right">Biaya</th>
                            <th style="min-width:95px;">Tiket</th>
                            <th class="text-center">Pendaftar</th>
                            
                            <th class="text-center" style="min-width:100px;">Dilihat <a href="#"><span class="caret caret-dropup"></span></a></th>
                            <th>Status</th>
                            <th style="min-width:150px;">Action</th>
                            <th style="min-width:180px;">Promote</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($query->result() as $rows):
                            ?>
                            <tr>
                                <td><?php echo $rows->event_id; ?></td>
                                <td>
                                    <?php echo $rows->event_title; ?>
                                    
                                </td>
                                <td style="min-width:100px;">
                                    <span class="small"><?php echo date('d/m/Y H:i:s', $rows->event_start_date); ?></span><br />
                                    <span class="small"><?php echo date('d/m/Y H:i:s', $rows->event_end_date); ?></span>
                                </td>
                                <td class="text-right"><?php
                                    if ($rows->budget_event == 0)
                                        echo "Free Entry";
                                    else
                                        echo "Rp." . number_format($rows->budget_event, 0, ',', '.');
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $qeventtickets = $this->event_model->get_eventticket($rows->event_id);
                                    $totalq = $qeventtickets->num_rows();
                                    if ($totalq == 0):
                                        ?>
                                        <span class="small">Belum Diaktifkan</span><br /> 
                                        
                                    <?php else: ?>
                                        <?php $reventticket = $qeventtickets->row(); ?>
                                        <?php echo $reventticket->eventticket_seat; ?> Seat
                                    <?php endif; ?>
                                    <br />
                                    <a href="#" class="text-danger btn-kelola-tiket small" data-toggle="modal" data-target="#myModal" data-event_id="<?php echo $rows->event_id; ?>">Pengaturan</a>
                                </td>
                                <td class="text-center">
                                    <a href="<?php echo site_url('event/my_event/view_audience/' . $rows->event_id); ?>" title="Lihat Pendaftar"><?php
                                        $totalaudience = $this->event_model->get_audience($rows->event_id)->num_rows();
                                        echo $totalaudience;
                                        ?></a>
                                </td>
                                <td title="Kunjungan oleh Anda tidak dihitung dalam counter ini" class="text-center">
                                    <span ><?php echo $rows->event_totalview; ?></span>
                                </td>
                                <td>
                                    <br />
                                    <?php if ($rows->event_start_date < time()): ?>
                                        <span class="label label-default">Sudah Lewat</span>
                                    <?php else: ?>
                                        <?php
                                        if ($rows->is_draft == 1) {
                                             echo '<span class="label label-warning">Draft<span>';
                                        } else {
                                            if ($rows->is_pending == 0) {
                                                if($rows->is_private==1)
                                                    echo '<span class="label label-danger">Private</span>';
                                                else
                                                    echo '<span class="label label-warning">Pending<span>';
                                            } else {
                                                echo '<span class="label label-success">Aktif</span>';
                                                if ($rows->is_featured == 2)
                                                    echo '<span class="label label-primary">Featured</span>';
                                            }
                                        }
                                        ?>
                                    <?php endif; ?>
                                </td>
                                <td class="inline-wrap">
                                    <div class="btn-group" role="group">
                                        <a href="<?php echo site_url('event/view/' . $rows->event_id . '/' . url_title($rows->event_title)) ?>" target="_blank" title="Lihat Event" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                        <a href="<?php echo site_url('event/my_event/edit_event/' . $rows->event_id); ?>" title="Edit Event" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                        <a href="#" class="btn btn-danger btn-sm trash-event" data-toggle="modal" data-target="#myModal" data-event_id="<?php echo $rows->event_id; ?>" title="Hapus"><i class="fa fa-trash-o"></i></a>
                                        
                                    </div>
                                </td>
                                <td>
                                    <?php if($rows->is_pending==1): ?>
                                        <?php if($rows->is_private==0): ?>
                                            <div class="btn-group" role="group" id="share_group">
                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo site_url('event/view/' . $rows->event_id . '/' . url_title($rows->event_title)) ?>" class="btn btn-info btn-sm"><i class="fa fa-facebook"></i></a>
                                                <a href="http://twitter.com/home?status=<?php echo site_url('event/view/' . $rows->event_id . '/' . url_title($rows->event_title)) ?>" class="btn btn-info btn-sm"><i class="fa fa-twitter"></i></a>
                                                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo site_url('event/view/' . $rows->event_id . '/' . url_title($rows->event_title)) ?>" class="btn btn-info btn-sm"><i class="fa fa-linkedin"></i></a>
                                                <a href="https://plus.google.com/share?url=<?php echo site_url('event/view/' . $rows->event_id . '/' . url_title($rows->event_title)) ?>" class="btn btn-info btn-sm"><i class="fa fa-google-plus"></i></a>
                                            </div>
                                        <?php endif;?>
                                    <?php else: ?>
                                        <div class="btn-group" role="group" id="share_disabled">
                                            <a href="javascript:void(0);" class="btn btn-default btn-sm"><i class="fa fa-facebook"></i></a>
                                            <a href="javascript:void(0);" class="btn btn-default btn-sm"><i class="fa fa-twitter"></i></a>
                                            <a href="javascript:void(0);" class="btn btn-default btn-sm"><i class="fa fa-linkedin"></i></a>
                                            <a href="javascript:void(0);" class="btn btn-default btn-sm"><i class="fa fa-google-plus"></i></a>
                                        </div>
                                    <?php endif;?>

                                </td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
        	<div class="col-sm-12">
        		<?php echo $this->pagination->create_links(); ?>
        	</div>
        </div>
	</div>
</div>
                

<?php $this->load->view('member_include/html_footer');?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.btn-kelola-tiket').click(function (e) {
            e.preventDefault;
            event_id = $(this).data('event_id');
            $('.modal-title').html('Kelola Tiket');
            $('.modal-body').html(loader);
            $('.modal-body').load('<?php echo site_url('event/my_event/manage_tickets'); ?>/' + event_id);
        });

        $('.trash-event').click(function(e){
            e.preventDefault();
            event_id = $(this).data('event_id');
            $('.modal-title').html('Hapus Event');
            $('.modal-body').html(loader);
            $('.modal-body').load('<?php echo site_url('event/my_event/delete_dialog');?>/'+event_id);
        });
    });

    function change_filter(filter) {
        
        window.location = '<?php echo $base_filter; ?>?' + filter;
    }

    $('.form-filter').change(function(){
        event_status_form=$('#event_status_form').val();
        event_order_form=$('#event_order_form').val();
        per_page_form=$('#per_page_form').val();
        var filter='event_status='+event_status_form+'&order_by='+event_order_form+'&per_page='+per_page_form;
        change_filter(filter);

    })
    function PopupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }
    $('#share_group a').click(function(e){
        e.preventDefault();
        the_link=$(this).attr('href');
        PopupCenter(the_link, "", 500,400);

    });

    $('#share_group a').each(function(){
        $(this).addClass('btn-info')
    })

    $('#share_disabled a').each(function(){
        $(this).addClass('btn-default');
        
    })
</script>