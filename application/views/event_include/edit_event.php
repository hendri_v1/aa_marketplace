<?php $this->load->view('member_include/html_head');?>
<form action="<?php echo site_url('event/my_event/update_event/' . $row->event_id) ?>" class="form" id="myform" method="POST" enctype="multipart/form-data">
	<div class="row">
		<div class="col-sm-12">
			<?php
            $collected_fields = array();
            $collected_fields['province_id'] = $row->province_id;
            $collected_fields['city_id'] = $row->city_id;
            $collected_fields['venue'] = $row->event_venue;
            $collected_fields['event_address'] = $row->event_address;
            $collected_fields['startdate'] = mdate('%d/%m/%Y %H:%i', $row->event_start_date);
            $collected_fields['enddate'] = mdate('%d/%m/%Y %H:%i', $row->event_end_date);
            $collected_fields['keywords'] = '';
            $collected_fields['title'] = $row->event_title;
            ?>
            <?php if ($this->session->flashdata('error_array')): ?>
            	<div class="row">
	                <div class="col-sm-12">
	                    <div class="boxes text-warning">
	                        <span class="lead">Data Tidak Dapat Disimpan</span>
	                        <ul>
	                            <?php foreach ($this->session->flashdata('error_array') as $key => $value): ?>
	                                <li><?php echo $value; ?></li>
	                            <?php endforeach; ?>
	                            <?php //$collected_fields=$this->session->flashdata('collected_fields');?>
	                        </ul>
	                    </div>
	                </div>
	            </div>
            <?php endif; ?>
            <div class="background-white p20">
	            <ul class="nav nav-tabs" role="tablist">
	                <li class="active"><a href="#locationevent" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i> Lokasi</a></li>
	                <li><a href="#event_type" role="tab" data-toggle="tab"><i class="fa fa-calendar"></i> Info Event</a></li>
	                <li><a href="#detailevent" role="tab" data-toggle="tab"><i class="fa fa-list"></i> Detail Event</a></li>
	                <li><a href="#postnow" role="tab" data-toggle="tab"><i class="fa fa-floppy-o"></i> Pilihan Posting</a></li>
	            </ul>
	            <div class="tab-content">
	                <div id="locationevent" class="tab-pane fade in active">
	                    <div class="p20">
	                        <div class="row" >
	                            <div class="col-sm-6">
	                                <?php
	                                $array_province = array();
	                                $array_province[0] = "Select Province";
	                                foreach ($province as $rowsprovince) {
	                                    $array_province[$rowsprovince->province_id] = $rowsprovince->province_name;
	                                }
	                                echo bt_select('province_id', 'Pilih Provinsi', $array_province, $collected_fields['province_id']);
	                                ?>
	                                <?php
	                                $array_city = array();
	                                $array_city[0] = "Pilih Kota";
	                                echo bt_select('city_id', 'Pilih Kota', $array_city, $collected_fields['city_id']);
	                                ?>
	                                <?php echo bt_text('event_address', 'Alamat Lengkap', 'Contoh : Jalan Jendral Sudirman No. 30', $collected_fields['event_address']); ?>
	                                <?php echo bt_text('venue', 'Venue', 'Masukkan Venue', $collected_fields['venue']); ?>
	                                
	                                <?php echo bt_text('event_geo', 'Alamat Geo Location', 'Akan terisi otomatis saat mengunci peta', $row->event_geo); ?>
	                            </div>
	                            <div class="col-sm-6">
	                                <label>Geser Penanda Untuk Mengatur Lokasi Yang Tepat</label>
	                                <div id="map_canvas" style="height:400px;">
	                                </div>
	                                <span id="the_address"></span>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div id="event_type" class="tab-pane fade">
	                    <div class="p20">
	                        <div class="row" >
	                            <div class="col-sm-12">
	                                <?php echo bt_text('title', 'Judul Event', 'Masukkan Judul Event', $collected_fields['title']); ?>
	                            </div>
	                            <div class="col-sm-6">
	                                <div class="form-group">
	                                    <label for="project_category_id">Pilih Kategori Event Anda</label>
	                                    <select name="eventcategory" class="form-control" id="project_category_id">
	                                        <?php if (!empty($eventcategory)): ?>
	                                            <?php foreach ($eventcategory as $list): ?>
	                                                <option value="<?php echo $list['eventcategory_id'] ?>" <?php if ($row->eventcategory_id == $list['eventcategory_id']) echo "selected"; ?>><?php echo $list['eventcategory_name'] ?></option>
	                                            <?php endforeach ?>
	                                        <?php endif ?>
	                                    </select>
	                                </div>
	                                <div id="datetimepicker9" class="input-append date">
	                                    <div class="form-group">
	                                        <label>Tanggal dan Jam Mulai</label>
	                                        <div class='input-group date col-md-12' id='datetimepicker9'>
	                                            <input type='text' placeholder="Tanggal dan Jam Mulai" name="startdate" class="form-control" value="<?php echo $collected_fields['startdate']; ?>" />
	                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
	                                            </span>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-sm-6">
	                                <?php echo bt_text('event_budget', 'Biaya', 'Masukkan 0 Apabila Gratis', $row->budget_event); ?>
	                                <div id="datetimepicker10" class="input-append date">
	                                    <div class="form-group">
	                                        <label>Tanggal dan Jam Selesai</label>
	                                        <div class='input-group date col-md-12' id='datetimepicker10'>
	                                            <input type='text' placeholder="Tanggal dan Jam Selesai" name="enddate" class="form-control" value="<?php echo $collected_fields['enddate']; ?>" />
	                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
	                                            </span>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="col-sm-12">
	                                <div class='form-group'>
	                                    <label>Keywords</label>
	                                    <input type='text' placeholder="Keywords pisahkan dengan koma" name="keywords" class="form-control" value="<?php echo $collected_fields['keywords']; ?>" />
	                                </div>
	                            </div>
	                            <div class="col-sm-6">
	                                <div class="form-group">
	                                    <label>Gambar Utama | Abaikan apabila tidak ingin mengganti</label>
	                                    <input type="file" name="image" class="form-control">
	                                </div> 
	                            </div>
	                        </div> 
	                    </div>
	                </div>
	                <div id="detailevent" class="tab-pane fade">
	                    <div class="p20">
	                        <?php $event_description = explode('<!---split--->', $row->event_description); ?>
	                        <div class="row">
	                            <div class="col-sm-6">
	                                <?php echo bt_textarea('overview', 'Overview', $event_description[0]); ?>
	                            </div>
	                            <div class="col-sm-6">
	                                <?php echo bt_textarea('outline', 'Outline', $event_description[1]); ?>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-sm-6">
	                                <?php echo bt_textarea('outcome', 'Outcome', $event_description[2]); ?>
	                            </div>
	                            <div class="col-sm-6">
	                                <?php echo bt_textarea('sertifikat', 'Sertifikat', $event_description[3]); ?>
	                            </div>
	                        </div>
	                        <div>
	                            <div class="form-group">
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div id="postnow" class="tab-pane fade">
	                    <div class="p20">
	                        <div class="row">
	                            <div class="col-sm-3">
	                                <?php
	                                $availability = array(0 => 'Aktif', 1 => 'Back to Draft');
	                                echo form_dropdown('is_draft', $availability, $row->is_draft, 'class="form-control input-sm"');
	                                ?>
	                            </div>
	                            <div class="col-sm-8">
	                                <button type="submit" class="btn btn-primary">Update</button>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
</form>

<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/bootstrap-datetimepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/libraries/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
    long_address = "";
    $(document).ready(function (e) {
        var config = {
            toolbar:
                    [
                        ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Undo', 'Redo', '-', 'SelectAll'],
                        ['UIColor']
                    ]
        };
        $('#event_budget').blur(function () {
            if ($(this).val() == '' || $(this).val() == 0)
                alert('Anda memberi nilai 0, sistem akan menayangkan ini sebagai Gratis');
        })
        $('#overview,#outline,#outcome,#sertifikat').ckeditor(config);
        $('#province_id').change(function (event) {
            var prov = $("#province_id").val();
            $.post('<?php echo site_url('program/getProvinceforMap'); ?>',
                    {
                        province: prov
                    },
            function (data)
            {
                long_address = '';
                $('#city_id').html(data.option);
                GMaps.geocode({
                    address: data.province_name,
                    callback: function (results, status) {
                        if (status == 'OK') {
                            var latlng = results[0].geometry.location;
                            map.setCenter(latlng.lat(), latlng.lng());
                            map.setZoom(9);
                        }
                    }
                });
            }, 'json'
                    );
        });
        $('#city_id').change(function () {
            var city = $(this).val();
            $.post('<?php echo site_url('program/getCityName'); ?>',
                    {
                        city_id: city
                    },
            function (data)
            {
                long_address = data;
                $('#the_address').html(long_address);
            });
        });
        $('#event_address').blur(function(event) {
            new_address=$(this).val()+', '+long_address;
            long_address=new_address;
               GMaps.geocode({
                  address: long_address,
                  callback: function(results, status) {
                    if (status == 'OK') {
                      var latlng = results[0].geometry.location;

                      map.setCenter(latlng.lat(), latlng.lng());
                      map.setZoom(15);
                      map.addMarker({
                        lat: latlng.lat(),
                        lng: latlng.lng(),
                        draggable: true,
                        dragend:function(e){
                           $('#event_geo').val(this.getPosition());
                        }
                      });
                      $('#event_geo').val(latlng.lat()+','+latlng.lng());
                    }
                  }
                });
                $('#the_address').html(long_address);
        });
        
        $('#datetimepicker9,#datetimepicker10').datetimepicker({
            language: 'id',
            useCurrent: false
        });
        $("#datetimepicker9").on("dp.change", function (e) {
            $('#datetimepicker10').data("DateTimePicker").setMinDate(e.date);
        });
        $("#datetimepicker10").on("dp.change", function (e) {
            $('#datetimepicker9').data("DateTimePicker").setMaxDate(e.date);
        });
        map = new GMaps({
            div: '#map_canvas',
            lat: -12.043333,
            lng: -77.028333,
            zoom: 9
        });
        

    
        <?php if ($collected_fields['province_id'] > 0): ?>
            var prov = $("#province_id").val();
            $.post('<?php echo site_url('program/getProvinceforMap'); ?>',
                {
                    province: prov
                },
                function (data)
                {
                    long_address = '';
                    $('#city_id').html(data.option);
                    <?php if ($collected_fields['city_id'] > 0): ?>
                        $('#city_id').val('<?php echo $collected_fields['city_id']; ?>');
                        var city = $('#city_id').val();
                        $.post('<?php echo site_url('program/getCityName'); ?>',
                        {
                            city_id: city
                        },
                        function (data)
                        {
                            long_address = data;
                            $('#the_address').html(long_address);
                        });
                        <?php if($collected_fields['event_address']<>'' and $row->event_geo==''): ?>
                            new_address=$('#event_address').val()+', '+long_address;
                            long_address=new_address;
                               GMaps.geocode({
                                  address: long_address,
                                  callback: function(results, status) {
                                    if (status == 'OK') {
                                      var latlng = results[0].geometry.location;

                                      map.setCenter(latlng.lat(), latlng.lng());
                                      map.setZoom(15);
                                      map.addMarker({
                                        lat: latlng.lat(),
                                        lng: latlng.lng(),
                                        draggable: true,
                                        dragend:function(e){
                                           $('#event_geo').val(this.getPosition());
                                        }
                                      });
                                      $('#event_geo').val(latlng.lat()+','+latlng.lng());
                                    }
                                  }
                                });
                                $('#the_address').html(long_address);
                        <?php endif;?>
                    <?php endif; ?>
                    <?php if($row->event_geo<>''): ?>
                        <?php $split_lat_lang=explode(',',$row->event_geo);?>
                        map.setCenter(<?php echo $row->event_geo;?>);
                        map.setZoom(15);
                        map.addMarker({
                            lat: <?php echo $split_lat_lang[0];?>,
                            lng: <?php echo $split_lat_lang[1];?>,
                            draggable: true,
                            dragend:function(e){
                               $('#event_geo').val(this.getPosition());
                            }
                        });
                    <?php else: ?>
                        GMaps.geocode({
                            address: data.province_name,
                            callback: function (results, status) {
                                if (status == 'OK') {
                                    var latlng = results[0].geometry.location;
                                    map.setCenter(latlng.lat(), latlng.lng());
                                    map.setZoom(9);
                                }
                            }
                        });
                    <?php endif;?>
                }, 'json'
            );
        <?php else: ?>
            GMaps.geocode({
                address: 'Jakarta',
                callback: function (results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                    }
                }
            });
        <?php endif; ?>
        
    });
</script>