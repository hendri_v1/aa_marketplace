<div class="row" id="choose_speaker_box">
	<?php foreach($query->result() as $rows): ?>
		<div class="col-sm-4">
			<div class="background-white p20 mb30 boxes text-center" thei="0" style="cursor:pointer;">
				<img src="<?php echo base_url();?>uploads/event_images/<?php echo $rows->eventspeaker_image;?>" height="80">
				<hr class="space" />
				<?php echo $rows->eventspeaker_name;?>
				<input type="hidden" type="text" class="eventspeaker_id" value="<?php echo $rows->eventspeaker_id;?>" activated="0">
			</div>
		</div>
	<?php endforeach;?>
	<div class="col-sm-12 text-right">
		<hr />
		<a href="#" class="btn btn-primary" id="add_to_event">Tambahkan Ke Event</a>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		
		$('#choose_speaker_box .boxes').click(function(){
			i=$(this).attr('thei');
			if(i=="0")
			{
				the_id=$(this).children('.eventspeaker_id').val();
				$(this).css('background-color','#75caeb');
				$(this).css('color','#FFF');
				$(this).attr('thei',"1");
			}
			else
			{
				$(this).css('background-color','#FFF');
				$(this).css('color','#555555');
				$(this).attr('thei',"0");
			}
		});

		$('#add_to_event').click(function(){
			e_hash=$('#eventspeaker_hash').val();
			$('.eventspeaker_id').each(function(){
				if($(this).parent().attr('thei')==1)
				{
					e_speaker_id=$(this).val();
					
					$.post('<?php echo site_url('event/my_event/get_the_speaker');?>',
					{
						eventspeaker_id:e_speaker_id,
						eventspeaker_hash:e_hash
					},
					function(data)
					{

					});
				}
			});
			$('#eventspeaker_load').load('<?php echo site_url('event/my_event/view_speaker_by_hash');?>/'+e_hash);
			$('#myModal').modal('hide');
		});
	});
</script>