<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner">
			<div class="container">
				<div class="row">

					<div class="document-title" style="background-image:url('<?php echo base_url();?>assets/img/event.jpeg');background-size:cover;">
				        <h1>Direktori</h1>

				        <ul class="breadcrumb">
				            <li><a href="<?php echo base_url();?>">HRplsa</a></li>
				            <li><a href="#">Direktori Event</a></li>
				        </ul>
				    </div>
				    <div class="col-sm-8 col-lg-9">
						<div class="content">
							<h2 class="page-title">
								<?php echo $total_rows;?> Event
								<form method="get" action="<?php echo site_url('events'); ?>" id="filter-sort" class="filter-sort">
							        <div class="form-group">
						                
						                <?php 
					                        $order_events_by = array(0 => "Tanggal Terdekat", 1 => "Tanggal Terjauh", 2 => "A-Z", 3 => "Z-A", 4 => "Kategori");
					                        echo bt_group_select('order_events_by', $order_events_by, $this->input->get('order_events_by'), 'glyphicon glyphicon-sort-by-attributes');
					                      ?>
						            </div><!-- /.form-group -->
						            <div class="form-group">
					                
						                <?php 
					                        $show_events_by = array(0 => "Akan Datang", 1 => "Semua Event");
					                        echo bt_group_select('show_events_by', $show_events_by, $this->input->get('show_events_by'), 'fa fa-calendar');
					                        ?>
						            </div><!-- /.form-group -->
						            <input type="hidden" name="event_keyword" value="<?php echo $this->input->get('event_keyword');?>">
						            <input type="hidden" name="eventcategory_id" value="<?php echo $this->input->get('eventcategory_id');?>">
						            <input type="hidden" name="province_id" value="<?php echo $this->input->get('province_id');?>">
						            <input type="hidden" name="city_id" value="<?php echo $this->input->get('city_id');?>">
						            <input type="hidden" name="min_price" value="<?php echo $this->input->get('min_price');?>">

							    </form>
							    
							</h2>
							<div class="cards-row">
								<?php $i=0; foreach ($queryevents->result() as $rowsevents): $i++; ?>
									<div class="card-row" style="cursor:pointer;">
							            <div class="card-row-inner">
							            	<?php $mini_image='';
							            		if($rowsevents->event_picture=='')
							            			$mini_image=upload_path.'uploads/user_images/'.$this->global_model->get_user_photo($rowsevents->userprofile_photo);
							            		else
							            			$mini_image=upload_path.'uploads/event_images/'.$rowsevents->event_picture;
							            	?>
							                <div class="card-row-image" data-background-image="<?php echo $mini_image; ?>" style="background-image: url(&quot;<?php echo upload_path ?>uploads/user_images/<?php echo $this->global_model->get_user_photo($rowsevents->userprofile_photo); ?>&quot;);">
							                    <?php if($rowsevents->is_featured==2): ?>
							                    	<div class="card-row-label">
														Featured
													</div><!-- /.card-row-label -->
							                    <?php endif;?>
							                        <div class="card-row-price">
							                        	<?php
					                                    if ($rowsevents->usertype_id == 1)
					                                        echo $rowsevents->userprofile_firstname . ' ' . $rowsevents->userprofile_lastname;
					                                    else
					                                        echo $rowsevents->userprofile_companyname;
					                                    ?>
					                                </div><!-- -->
							                    
							                </div><!-- /.card-row-image -->

							                <div class="card-row-body" style="padding:15px;">
							                    
							                    <div class="card-row-content">
							                    	<p>
							                    	<a href="<?php echo site_url('event/view/' . $rowsevents->event_id . '/' . url_title(strtolower(word_limiter($rowsevents->event_title,10)))); ?>"><?php echo $rowsevents->event_title; ?></a><br />
							                    	<?php $sameday=$rowsevents->event_end_date-$rowsevents->event_start_date;?>
		                                            <?php if($sameday>86400): ?>
		                                              	<i class="fa fa-clock-o"></i> <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_start_date);?> - <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_end_date);?> 
		                                          	<?php else: ?>
		                                            	<i class="fa fa-clock-o"></i> <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_start_date);?> - <?php echo mdate('%H:%i:%s',$rowsevents->event_end_date);?> 
		                                          	<?php endif;?>
                                          			</p>
                                          		</div><!-- /.card-row-content -->
							                </div><!-- /.card-row-body -->

							                <div class="card-row-properties" style="padding:15px;">
							                    <dl>
							                        	
							                            <dd>Harga</dd><dt>
							                            	<?php if ($rowsevents->budget_event == 0): ?>
				                                                  Free Entry
				                                              <?php else: ?>
				                                                  <?php echo "Rp " . number_format($rowsevents->budget_event, 0, ',', '.'); ?>
				                                              <?php endif; ?>
							                            </dt>
							                        

							                        
							                            <dd>Kategori</dd><dt><?php echo $rowsevents->eventcategory_name;?></dt>
							                        

							                        
							                            <dd>Location</dd><dt><?php echo ucfirst(strtolower($rowsevents->city_name)); ?></dt>
							                        

							                        <dd>Venue</dd>
							                        <dt>
							                            <?php echo $rowsevents->event_venue; ?>
							                        </dt>
							                    </dl>
							                </div><!-- /.card-row-properties -->
							            </div><!-- /.card-row-inner -->
							        
						        	</div>
						        <?php endforeach;?>
							</div>
							<?php echo $this->pagination->create_links(); ?>
						</div>
				    </div><!-- /.col-->
				    <div class="col-sm-4 col-lg-3">
				    	<h2 class="page-title">Filter</h2>
						<div class="sidebar">
							<div class="widget">
								
								<div class="background-white p20">
							        <form method="get" action="<?php echo site_url('events'); ?>">
							            <div class="form-group">
							                <label for="">Keyword</label>
							                <?php echo bt_group_text('event_keyword', 'Keyword', $this->input->get('event_keyword'), 'fa fa-key'); ?>
							            </div><!-- /.form-group -->

							            <div class="form-group">
							                <label for="">Category</label>

							                <?php 
						                        $array_eventcategory = array();
						                        $array_eventcategory[0] = "Semua Kategori";
						                        foreach ($queryeventcategory as $rowseventcategory) {
						                            $array_eventcategory[$rowseventcategory->eventcategory_id] = $rowseventcategory->eventcategory_name;
						                        }
						                        echo bt_group_select('eventcategory_id', $array_eventcategory, $this->input->get('eventcategory_id'), 'fa fa-tags');
						                      ?>
							            </div><!-- /.form-group -->

							            <div class="form-group">
							                <label for="">Provinsi</label>
							                <?php
						                      $array_province = array();
						                      $array_province[0] = "Semua Provinsi";
						                      foreach ($province as $rowsprovince) {
						                          $array_province[$rowsprovince->province_id] = $rowsprovince->province_name;
						                      }
						                      echo bt_group_select('province_id', $array_province, $this->input->get('province_id'), 'fa fa-bullseye');
						                      ?>
							            </div><!-- /.form-group -->
										<div class="form-group">
							                <label for="">Kota</label>
							                <?php 

						                        $array_city = array();
						                        $array_city[0] = "Semua Kota";
						                        echo bt_group_select('city_id', $array_city, '', 'fa fa-location-arrow');
						                      ?>
							            </div><!-- /.form-group -->
							            
							            
							            <div class="form-group">
							                <label>Filter Harga</label>
				                            <div class="slider-price">
				                                <div id="price_slide"></div>
				                            </div>
							                
							            </div><!-- /.form-group -->
							           
										<input type="hidden" name="order_events_by" value="<?php echo $this->input->get('order_events_by');?>">
										<input type="hidden" name="show_events_by" value="<?php echo $this->input->get('show_events_by');?>">
							            <button class="btn btn-primary btn-block" style="margin-top:25px;" type="submit">Search</button>
							        </form>
							    </div>
							</div><!-- /.widget -->
						</div><!-- ./sidebar -->
				    </div><!-- /.col-->
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('html_footer');?>
<script type="text/javascript">
    lebar = 0;
    $(document).ready(function (e) {
        $(window).bind("load resize", function () {
            lebar = $(this).width();
        });
        /** slider price */
        var min_price_event_default = 0;
        var max_price_event_default = <?php echo ($highest_price < 1000000) ? 1000000 : $highest_price; ?>;
        var min_price_event = <?php echo $this->input->get('min_price') ? (int) str_replace('.', '', $this->input->get('min_price')) : 0; ?>;
        var max_price_event = <?php echo $this->input->get('max_price') ? (int) str_replace('.', '', $this->input->get('max_price')) : $highest_price; ?>;
        $('#price_slide').noUiSlider({
            start: [min_price_event, max_price_event],
            connect: true,
            step: 500,
            range: {
                'min': min_price_event_default,
                'max': max_price_event_default
            }
        });
        $('#price_slide').Link('lower').to('-inline-<div class="tooltip-slider"></div>', function (value, test) {
            $(this).html('<strong>Min: </strong><span> Rp. ' + value + '</span><input type="hidden" name="min_price" value="' + value + '"/>');
        }, wNumb({
            thousand: '.',
            decimals: 0
        }));
        $('#price_slide').Link('upper').to('-inline-<div class="tooltip-slider"></div>', function (value) {
            $(this).html('<strong>Max: </strong><span> Rp. ' + value + '</span><input type="hidden" name="max_price" value="' + value + '"/>');
        }, wNumb({
            thousand: '.',
            decimals: 0
        }));

// A select element can't show any decimals

        $(".card-row").click(function () {
            window.location = $(this).find("a").attr("href");
            return false;
        });
		<?php if ($this->input->get('province_id') <> 0): ?>
            var prov = <?php echo $this->input->get('province_id'); ?>;
            $.post('<?php echo site_url('program/getProvince'); ?>',
                    {
                        province: prov
                    },
            function (data)
            {
                $('#city_id').html(data);
   		 <?php if ($this->input->get('city_id') <> 0): ?>
                    $('#city_id').val('<?php echo $this->input->get('city_id'); ?>');
    	<?php endif; ?>
            }
            );
		<?php endif; ?>
        $('#province_id').change(function (event) {
            var prov = $("#province_id").val();
            $.post('<?php echo site_url('program/getProvince'); ?>',
                    {
                        province: prov
                    },
            function (data)
            {
                $('#city_id').html(data);
                $('.bootstrap-select').selectpicker('refresh');
            }
            );
        });

        $('#order_events_by').change(function(){
        	$('#filter-sort').submit();
        });
        $('#show_events_by').change(function(){
        	$('#filter-sort').submit();
        })
<?php /* $('#events .form-control').addClass('input-sm'); */ ?>
<?php /* if($total_rows>0): ?>
  lebar=$(this).width();
  if(lebar>768)
  {
  $(window).scroll(function(e){
  $el = $('#events_search');
  width=$('#events_search').width();
  if ($(this).scrollTop() > 200 && $el.css('position') != 'fixed'){
  $('#events_search').css({'position': 'fixed', 'top': '10px','width':width,'z-index':1029});
  }
  if ($(this).scrollTop() < 200 && $el.css('position') == 'fixed')
  {
  $('#events_search').css({'position': 'static', 'top': '0px'});
  }
  });
  }
  <?php endif; */ ?>
    });
</script>