
<hr />
	<div class="row">
		<?php foreach($query->result() as $rows): ?>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-4">
						<img src="<?php echo base_url();?>uploads/event_images/<?php echo $rows->eventspeaker_image;?>" class="img-thumbnail">
					</div>
					<div class="col-sm-8">
						<strong><?php echo $rows->eventspeaker_name;?></strong><br />
						<?php echo $rows->eventspeaker_description;?>
					</div>
				</div>
				<hr />
			</div>
			
		<?php endforeach;?>
	</div>
