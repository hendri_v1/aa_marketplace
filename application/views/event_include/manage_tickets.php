<?php if($qticket->num_rows()==0): ?>
    <form method="post" action="<?php echo site_url('event/my_event/activate_ticketing');?>">
        <div class="row">
            <div class="col-sm-6">
                <?php echo bt_text('eventticket_seat','Total Online Register Seat','Masukkan 0 Jika tidak ada');?>
            </div>
            <div class="col-sm-6">
                <?php echo bt_text('eventticket_earlybird_seat','Total Online Early Bird Seat','Masukkan 0 Jika tidak ada');?>
            </div>
            <div class="col-sm-6">
                <?php echo bt_text('eventticket_earlybird_price','Harga Early Bird','Masukkan 0 Jika Gratis',$row->budget_event);?>
            </div>
            <div class="col-sm-6">
                <?php 
                    $eventicket_public=array(0=>'Tampilkan Peserta Terdaftar',1=>'Hanya Tampilkan Total Terdaftar',2=>'Sembunyikan Data Pendaftaran');
                    echo bt_select('eventticket_public','Informasi Peserta',$eventicket_public);
                ?>
            </div>
            <input type="hidden" name="event_id" value="<?php echo $event_id;?>">
            <div class="col-sm-12">
                <input type="submit" class="btn btn-warning" value="Simpan Pengaturan Tiket">
            </div>
        </div>
    </form>
<?php else: ?>
    <form method="post" action="<?php echo site_url('event/my_event/edit_ticketing');?>">
        <div class="row">
            <div class="col-sm-6">
                <?php echo bt_text('eventticket_seat','Total Online Register Seat','Masukkan 0 Jika tidak ada',$rowticket->eventticket_seat);?>
            </div>
            <div class="col-sm-6">
                <?php echo bt_text('eventticket_earlybird_seat','Total Online Early Bird Seat','Masukkan 0 Jika tidak ada',$rowticket->eventticket_earlybird_seat);?>
            </div>
            <div class="col-sm-6">
                <?php echo bt_text('eventticket_earlybird_price','Harga Early Bird','Masukkan 0 Jika Gratis',$rowticket->eventticket_earlybird_price);?>
            </div>
            <div class="col-sm-6">
                <?php 
                    $eventicket_public=array(0=>'Tampilkan Peserta Terdaftar',1=>'Hanya Tampilkan Total Terdaftar',2=>'Sembunyikan Data Pendaftaran');
                    echo bt_select('eventticket_public','Informasi Peserta',$eventicket_public,$rowticket->eventticket_public);
                ?>
            </div>
            <input type="hidden" name="eventticket_id" value="<?php echo $rowticket->eventticket_id;?>">
            <div class="col-sm-12">
                <input type="submit" class="btn btn-warning" value="Simpan Pengaturan Tiket">
            </div>
        </div>
    </form>

<?php endif;?>