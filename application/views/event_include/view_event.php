<?php $this->load->view('html_head');?>
<?php $ticketing = $qeventtickets->num_rows(); ?>
<?php if ($ticketing > 0): $rticketing = $qeventtickets->row(); ?>
<?php endif;?>
	<div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="mt-80 mb80">
                    <?php 
                        $the_image='';
                        if($query['event_picture']=='')
                            $the_image=base_url().'assets/img/main_bg.jpeg';
                        else
                            $the_image=upload_path.'uploads/event_images/'.$query['event_picture'];
                    ?>
                    <div class="detail-banner" style="background-image: url(<?php echo $the_image;?>);">
                    <div class="container">
                        <div class="detail-banner-left">
                            <div class="detail-banner-info">
                                <div class="detail-label"><?php echo $query['eventcategory_name'];?></div>
                                <div class="detail-verified"><?php 
                                  $timespan=$query['event_end_date']-$query['event_start_date'];
                                  $show_date='';
                                  if($timespan<=86400)
                                    $show_date=date('d M Y H:i:s', $query['event_start_date']).' - '.date('H:i:s',$query['event_end_date']);
                                  else
                                    $show_date=date('d M Y H:i:s', $query['event_start_date']).' - '.date('d M Y H:i:s', $query['event_end_date']);
                                ?><?php echo $show_date; ?>
                                </div>
                            </div><!-- /.detail-banner-info -->

                            <h2 class="detail-title">
                                <?php echo $query['event_title'];?>
                            </h2>

                            <div class="detail-banner-address">
                                <i class="fa fa-map-o"></i> <?php echo $query['event_venue']; ?>, <?php echo ucwords(strtolower($query['city_name'])); ?> - <?php echo ucwords(strtolower($query['province_name']));?>
                            </div><!-- /.detail-banner-address -->

                            <div class="detail-banner-rating">
                                <?php if ($query['budget_event'] == 0): ?>
                                      Free Entry !!
                                  <?php else: ?>
                                      <?php echo 'Rp. ' . number_format($query['budget_event'], 0, ',', '.'); ?>
                                      
                                          <?php if ($rticketing->eventticket_earlybird_seat > 0) : ?>
                                              - Early Bird <?php echo 'Rp. ' . number_format($rticketing->eventticket_earlybird_price, 0, ',', '.'); ?> 
                                          <?php endif; ?>
                                      
                                  <?php endif; ?>
                            </div><!-- /.detail-banner-rating -->
                            <?php if ($this->session->userdata('user_id') != $query['user_id']): ?>
                                <?php if($rticketing->eventticket_seat > 0): ?>
                                    <?php if($registered == true): ?>
                                        <div class="detail-banner-btn bookmark">
                                            <i class="fa fa-ticket">Terdaftar</i>
                                        </div>
                                    <?php else: ?>
                                        <div class="detail-banner-btn">
                                            <?php 
                                                $register_url='';
                                                if ($this->session->userdata('user_id'))
                                                    $register_url=site_url('event/event/register_event/' . $this->uri->segment(3) . '/' . $this->uri->segment(4));
                                                else
                                                    $register_url=site_url('login?redirect_to=event/event/register_event/' . $this->uri->segment(3) . '/' . $this->uri->segment(4));

                                            ?>
                                            <a href="<?php echo $register_url;?>"><i class="fa fa-ticket"></i> <span data-toggle="Bookmarked">Daftar di Event Ini</span></a>
                                        </div><!-- /.detail-claim -->
                                    <?php endif;?>
                                <?php endif;?>
                            <?php endif;?>

                            <div class="detail-banner-btn heart">
                                <i class="fa fa-question"></i> <span data-toggle="I Love It">Tanya HRPlasa</span>
                            </div><!-- /.detail-claim -->

                        </div><!-- /.detail-banner-left -->
                    </div><!-- /.container -->
                </div><!-- /.detail-banner -->

            </div>

<div class="container">
    <div class="row detail-content">
    <div class="col-sm-7">
        <?php if($query['event_picture']<>''): ?>
            <div class="background-white p5">
                <img src="<?php echo upload_path;?>uploads/event_images/<?php echo $query['event_picture'];?>" class="img-thumbnail" style="width:100%;">
            </div>
            <hr />
        <?php endif;?>
        

        <?php if ($query['event_geo'] <> ''): ?>
            <div class="background-white p20">

                <!-- Nav tabs -->
                <ul id="listing-detail-location" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#simple-map-panel" aria-controls="simple-map-panel" role="tab" data-toggle="tab">
                            <i class="fa fa-map"></i>Map
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#street-view-panel" aria-controls="street-view-panel" role="tab" data-toggle="tab">
                            <i class="fa fa-street-view"></i>Street View
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->

                <?php $latlng = explode(',', $query['event_geo']); ?>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="simple-map-panel">
                        <div class="detail-map">
                            <div class="map-position">
                                <div id="listing-detail-map"
                                     data-transparent-marker-image="<?php echo base_url();?>assets/img/transparent-marker-image.png"
                                     data-styles='[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"labels.text.fill","stylers":[{"color":"#b43b3b"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"lightness":"8"},{"color":"#bcbec0"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#5b5b5b"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#7cb3c9"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#abb9c0"}]},{"featureType":"water","elementType":"labels.text","stylers":[{"color":"#fff1f1"},{"visibility":"off"}]}]'
                                     data-zoom="15"
                                     data-latitude="<?php echo $latlng[0];?>"
                                     data-longitude="<?php echo $latlng[1];?>"
                                     data-icon="fa fa-map-marker">
                                </div><!-- /#map-property -->
                            </div><!-- /.map-property -->
                        </div><!-- /.detail-map -->
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="street-view-panel">
                        <div id="listing-detail-street-view"
                                data-latitude="<?php echo $latlng[0];?>"
                                data-longitude="<?php echo trim($latlng[1]);?>"
                                data-heading="225"
                                data-pitch="0"
                                data-zoom="1">
                        </div>
                    </div>
                </div>
            </div>
        <hr />
        <?php endif;?>
         
        <?php $event_description = explode('<!---split--->', $query['event_description']); ?>
		<div class="background-white p20">
    		<?php if($event_description[0]<>' '): ?>
                <?php echo $event_description[0]; ?>
            <?php endif;?>
            <?php if($event_description[1]<>' '): ?>
                <?php echo $event_description[1]; ?>
            <?php endif;?>
            <?php if($event_description[1]<>' '): ?>
              <?php echo $event_description[1]; ?>
            <?php endif;?>
            <?php if($event_description[2]<>' '): ?>
              <?php echo $event_description[2]; ?>
            <?php endif;?>
	    </div>
	
	    <?php if ($qeventspeakers->num_rows() > 0): ?>
        	<h2 id="reviews">Speaker</h2>
        		<div class="review">
	        		<?php foreach ($qeventspeakers->result() as $rspeaker): ?>
						<div class="review">
					        <div class="review-image">
					            <img src="<?php echo upload_path; ?>uploads/event_images/<?php echo $rspeaker->eventspeaker_image; ?>" alt="<?php echo $rspeaker->eventspeaker_name; ?>">
					        </div><!-- /.review-image -->

					        <div class="review-inner">
					            <div class="review-title">
					                <h2><?php echo $rspeaker->eventspeaker_name; ?></h2>
					                
					                <span class="report">
					                    <span class="separator">&#8226;</span><i class="fa fa-flag" title="Report" data-toggle="tooltip" data-placement="top"></i>
					                </span>

					                
					            </div><!-- /.review-title -->

					            <div class="review-content-wrapper">
					                <div class="review-content">
					                    <div class="review-pros">
					                        <p><?php echo $rspeaker->eventspeaker_description; ?></p>
					                    </div><!-- /.pros -->
					                    
					                </div><!-- /.review-content -->

					               
					            </div><!-- /.review-content-wrapper -->

					        </div><!-- /.review-inner -->
					    </div><!-- /.review -->
	        		<?php endforeach;?>
	        	</div>
        <?php endif;?>
        <?php if($event_description[3]<>' '): ?>
            <div class="widget">
                <h2 class="widgettitle">Sertifikat</h2>
                <div class="p20 background-white">
                    <?php echo $event_description[3];?>
                </div>
            </div><!-- /.widget -->
        <?php endif;?>

    </div><!-- /.col-sm-7 -->

    <div class="col-sm-5">

        <div class="background-white p20">
            <div class="detail-overview-hearts">
                <i class="fa fa-eye"></i> <strong><?php echo $query['event_totalview'];?> </strong>total dilihat
            </div>
            <div class="detail-overview-rating">
                <?php if ($query['event_start_date'] >= time()): ?>
                    <?php if($ticketing > 0): ?>
                        <?php $rticketing = $qeventtickets->row(); ?>
                        <?php if($rticketing->eventticket_seat > 0): ?>
                            <i class="fa fa-star"></i> Masih <strong><?php echo $rticketing->eventticket_seat;?></strong> Tiket Tersedia
                        <?php else: ?>
                            Silahkan menghubungi info@hrplasa.id untuk informasi pendaftaran
                        <?php endif;?>
                    <?php else: ?>
                        Silahkan menghubungi info@hrplasa.id untuk informasi pendaftaran
                    <?php endif;?>
                <?php endif;?>
            </div>

            <div class="detail-actions row">
                
                <?php if ($this->session->userdata('user_id') != $query['user_id']): ?>
                    <?php if($rticketing->eventticket_seat > 0): ?>
                        <div class="col-sm-4">
                            <?php if($registered == true): ?>
                                <div class="btn btn-primary btn-book" disabled><i class="fa fa-ticket"></i> Terdaftar</div>
                            <?php else: ?>
                                <a class="btn btn-primary btn-book" href="<?php echo $register_url;?>"><i class="fa fa-ticket"></i> Daftar</a>
                            <?php endif;?>
                         </div><!-- /.col-sm-4 -->
                    <?php endif;?>
                <?php endif;?>
               
                <div class="col-sm-4">
                    <div class="btn btn-secondary btn-share"><i class="fa fa-share-square-o"></i> Share
                        <div class="share-wrapper">
                            <ul class="share">
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(uri_string()); ?>"><i class="fa fa-facebook"></i> Facebook</a></li>
                                <li><a href="http://twitter.com/home?status=<?php echo base_url(uri_string()); ?>"><i class="fa fa-twitter"></i> Twitter</a></li>
                                <li><a href="https://plus.google.com/share?url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-google-plus"></i> Google+</a></li>
                                
                                <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-linkedin"></i> Linkedin</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-sm-4 -->
                <div class="col-sm-4">
                    <div class="btn btn-secondary btn-claim"><i class="fa fa-hand-peace-o"></i> Laporkan</div>
                </div><!-- /.col-sm-4 -->
            </div><!-- /.detail-actions -->
        </div>

        <h2>Tentang <span class="text-secondary"><?php
              if ($query['usertype_id'] == 1)
                  echo $query['userprofile_firstname'] . ' ' . $query['userprofile_lastname'];
              else
                  echo $query['userprofile_companyname'];
              ?></span></h2>
        <div class="background-white p20">
            <div class="detail-vcard">
                <div class="detail-logo">
                    <img src="<?php echo upload_path; ?>uploads/user_images/<?php echo $this->global_model->get_user_photo($query['userprofile_photo']); ?>">
                </div><!-- /.detail-logo -->

                <div class="detail-contact">
                    <div class="detail-contact-email">
                        <i class="fa fa-envelope-o"></i> <a href="mailto:#"><?php echo  $query['user_email'];?></a>
                    </div>
                    <div class="detail-contact-phone">
                        <i class="fa fa-mobile-phone"></i> <a href="tel:#"><?php echo $query['userprofile_phone'];?></a>
                    </div>
                    <div class="detail-contact-website">
                        <i class="fa fa-globe"></i> <a href="<?php echo site_url('vendor/view/' . $query['event_owner'].'/'.strtolower(url_title($query['username']))); ?>">Lihat Profil</a>
                    </div>
                    <div class="detail-contact-address">
                        <i class="fa fa-map-o"></i>
                        <?php echo $query['userprofile_address'];?><br>
                    </div>
                </div><!-- /.detail-contact -->
            </div><!-- /.detail-vcard -->

            <div class="detail-description">
            	<?php echo word_limiter(strip_tags($query['userprofile_aboutme']),20);?>
            </div>

            <div class="detail-follow">
                <h5>Follow Us:</h5>
                <div class="follow-wrapper">
                    <?php if($query['userprofile_fb']<>''): ?>
                        <a class="follow-btn facebook" href="<?php echo $query['userprofile_fb'];?>"><i class="fa fa-facebook"></i></a>
                    <?php endif;?>
                    <?php if($query['userprofile_twitter']<>''): ?>
                        <a class="follow-btn twitter" href="<?php echo $query['userprofile_twitter'];?>"><i class="fa fa-twitter"></i></a>
                    <?php endif;?>
                    <?php if($query['userprofile_linkedin']<>''): ?>
                        <a class="follow-btn google-plus" href="<?php echo $query['userprofile_linkedin'];?>"><i class="fa fa-linkedin"></i></a>
                    <?php endif;?>
                </div><!-- /.follow-wrapper -->
            </div><!-- /.detail-follow -->
        </div>

        
    
    

    


        <h2>Tanya HRPlasa tentang event ini</h2>
        <script type="text/javascript" src="http://assets.freshdesk.com/widget/freshwidget.js"></script>
<style type="text/css" media="screen, projection">
    @import url(http://assets.freshdesk.com/widget/freshwidget.css); 
</style> 
<iframe title="Feedback Form" class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://hrplasa.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&formTitle=Hubungi+HRPLasa&submitThanks=Terima+Kasih+Telah+Menghubungi+HRPlasa&screenshot=no&searchArea=no" scrolling="no" height="400px" width="100%" frameborder="0" >
</iframe>
        <?php /*
        <div class="detail-enquire-form background-white p20">
            <form method="post" action="?">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" class="form-control" name="" id="">
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label for="">Email <span class="required">*</span></label>
                    <input type="email" class="form-control" name="" id="" required>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label for="">Message <span class="required">*</span></label>
                    <textarea class="form-control" name="" id="" rows="5" required></textarea>
                </div><!-- /.form-group -->

                <p>Required fields are marked <span class="required">*</span></p>

                <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-paper-plane"></i>Send Message</button>
            </form>
        </div><!-- /.detail-enquire-form -->
        */ ?>
        <h2>Event Sejenis</h2>

        <div class="background-white p20 reasons">
            <?php if (!empty($related)): ?>
                <?php foreach ($related as $value): ?>
                    <div class="reason">
                        <div class="reason-icon">
                            <i class="fa fa-trophy"></i>
                        </div><!-- /.reason-icon -->
                        <div class="reason-content">
                            <h4><?php echo $value->event_title; ?></h4>
                            <p></p>
                        </div><!-- /.reason-content -->
                    </div><!-- /.reason -->
                <?php endforeach;?>
            <?php endif;?>
            
        </div>
        <div class="background-white p20 mt30">
            Punya Event Seperti Ini ?
              <?php if($this->session->userdata('user_id')): ?>
                <a href="<?php echo site_url('event/my_event/add_new');?>" class="btn btn-primary btn-block"><i class="fa fa-microphone"></i> Daftarkan Event Anda</a>
              <?php else: ?>
                <a href="<?php echo site_url('login?redirect_to=event/my_event/add_new');?>" class="btn btn-primary btn-block"><i class="fa fa-microphone"></i> Daftarkan Event Anda</a>
              <?php endif;?>
        </div>
        <?php /*
        <div class="detail-payments">
            <h3>Accepted Payments</h3>
            
            <ul>
                <li><a href="#"><i class="fa fa-paypal"></i></a></li>
    			<li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
                <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
    			<li><a href="#"><i class="fa fa-cc-stripe"></i></a></li>
                <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
    		</ul>
        </div> */?>
    </div><!-- /.col-sm-5 -->

    <?php /*
    <div class="col-sm-12">
        <h2>Kirimkan Tanggapan</h2>

        <form class="background-white p20 add-review" method="post" action="?">
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Name <span class="required">*</span></label>
                    <input type="text" class="form-control" id="" required>
                </div><!-- /.col-sm-6 -->

                <div class="form-group col-sm-6">
                    <label for="">Email <span class="required">*</span></label>
                    <input type="email" class="form-control" id="" required>
                </div><!-- /.col-sm-6 -->
            </div><!-- /.row -->

            <div class="row">
                <div class="form-group input-rating col-sm-3">

                    <div class="rating-title">Peneyelenggara</div>

                    <input type="radio" value="1" name="food" id="rating-food-1">
                    <label for="rating-food-1"></label>
                    <input type="radio" value="2" name="food" id="rating-food-2">
                    <label for="rating-food-2"></label>
                    <input type="radio" value="3" name="food" id="rating-food-3">
                    <label for="rating-food-3"></label>
                    <input type="radio" value="4" name="food" id="rating-food-4">
                    <label for="rating-food-4"></label>
                    <input type="radio" value="5" name="food" id="rating-food-5">
                    <label for="rating-food-5"></label>

                </div><!-- /.col-sm-3 -->
                <div class="form-group input-rating col-sm-3">

                    <div class="rating-title">Materi</div>

                    <input type="radio" value="1" name="staff" id="rating-staff-1">
                    <label for="rating-staff-1"></label>
                    <input type="radio" value="2" name="staff" id="rating-staff-2">
                    <label for="rating-staff-2"></label>
                    <input type="radio" value="3" name="staff" id="rating-staff-3">
                    <label for="rating-staff-3"></label>
                    <input type="radio" value="4" name="staff" id="rating-staff-4">
                    <label for="rating-staff-4"></label>
                    <input type="radio" value="5" name="staff" id="rating-staff-5">
                    <label for="rating-staff-5"></label>

                </div><!-- /.col-sm-3 -->
                <div class="form-group input-rating col-sm-3">

                    <div class="rating-title">Venue</div>

                    <input type="radio" value="1" name="value" id="rating-value-1">
                    <label for="rating-value-1"></label>
                    <input type="radio" value="2" name="value" id="rating-value-2">
                    <label for="rating-value-2"></label>
                    <input type="radio" value="3" name="value" id="rating-value-3">
                    <label for="rating-value-3"></label>
                    <input type="radio" value="4" name="value" id="rating-value-4">
                    <label for="rating-value-4"></label>
                    <input type="radio" value="5" name="value" id="rating-value-5">
                    <label for="rating-value-5"></label>

                </div><!-- /.col-sm-3 -->
                <div class="form-group input-rating col-sm-3">

                    <div class="rating-title">Trainer</div>

                    <input type="radio" value="1" name="atmosphere" id="rating-atmosphere-1">
                    <label for="rating-atmosphere-1"></label>
                    <input type="radio" value="2" name="atmosphere" id="rating-atmosphere-2">
                    <label for="rating-atmosphere-2"></label>
                    <input type="radio" value="3" name="atmosphere" id="rating-atmosphere-3">
                    <label for="rating-atmosphere-3"></label>
                    <input type="radio" value="4" name="atmosphere" id="rating-atmosphere-4">
                    <label for="rating-atmosphere-4"></label>
                    <input type="radio" value="5" name="atmosphere" id="rating-atmosphere-5">
                    <label for="rating-atmosphere-5"></label>

                </div><!-- /.col-sm-3 -->
            </div><!-- /.row -->

            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Yang Baik</label>
                    <textarea class="form-control" rows="5" id=""></textarea>
                </div><!-- /.col-sm-6 -->
                <div class="form-group col-sm-6">
                    <label for="">Yang Perlu Diperbaiki</label>
                    <textarea class="form-control" rows="5" id=""></textarea>
                </div><!-- /.col-sm-6 -->

                <div class="col-sm-8">
                    <p>Required fields are marked <span class="required">*</span></p>
                </div><!-- /.col-sm-8 -->
                <div class="col-sm-4">
                    <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-star"></i>Submit Review</button>
                </div><!-- /.col-sm-4 -->
            </div><!-- /.row -->
        </form>
    </div><!-- /.col-* -->
    */ ?>
</div><!-- /.row -->

</div><!-- /.container -->

            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

<?php $this->load->view('html_footer');?>
<script type="text/javascript">
    $(document).ready(function(){
        function PopupCenter(url, title, w, h) {
            // Fixes dual-screen position                         Most browsers      Firefox
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            // Puts focus on the newWindow
            if (window.focus) {
                newWindow.focus();
            }


        }
        $('.share a').click(function(e){
            e.preventDefault();
            the_link=$(this).attr('href');
            PopupCenter(the_link, "", 500,400);
        })
    })
</script>