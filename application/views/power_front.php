<div class="block background-black-light fullwidth">
        <div class="row">
            <div class="col-sm-4">
                <div class="box">
                    <div class="box-icon">
                        <i class="fa fa-life-ring"></i>
                    </div><!-- /.box-icon -->

                    <div class="box-content">
                        <h2>E-mail Support</h2>
                        <p>Kami selalu siap menjawab semua pertanyaan Anda. Silahkan hubungi kami di support@hrplasa.id.</p>

                        <a href="#">Read More <i class="fa fa-chevron-right"></i></a>
                    </div><!-- /.box-content -->
                </div>
            </div><!-- /.col-sm-4 -->

            <div class="col-sm-4">
                <div class="box">
                    <div class="box-icon">
                        <i class="fa fa-flask"></i>
                    </div><!-- /.box-icon -->

                    <div class="box-content">
                        <h2>Always Improving</h2>
                        <p>Tim kami selalu berupaya untuk memberikan layanan terbaik di HRplasa.id.</p>

                        <a href="#">Read More <i class="fa fa-chevron-right"></i></a>
                    </div><!-- /.box-content -->
                </div>
            </div><!-- /.col-sm-4 -->

            <div class="col-sm-4">
                <div class="box">
                    <div class="box-icon">
                        <i class="fa fa-thumbs-o-up"></i>
                    </div><!-- /.box-icon -->

                    <div class="box-content">
                        <h2>Best Quality</h2>
                        <p>Kami memastikan, semua listing dan informasi yang kami tampilkan, telah melewati seleksi dari Tim kami.</p>

                        <a href="#">Read More <i class="fa fa-chevron-right"></i></a>
                    </div><!-- /.box-content -->
                </div>
            </div><!-- /.col-sm-4 -->
        </div><!-- /.row -->

    </div><!-- /.block -->