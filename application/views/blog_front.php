<div class="events">
    <?php foreach($queryblog->result() as $rowsblog): ?>
    <div class="event">
        <div class="event-date">
            <span class="day"><?php echo date('d',$rowsblog->post_addedat);?></span>
            <span class="month"><?php echo date('M',$rowsblog->post_addedat);?></span>
        </div><!-- /.event-date -->

        <div class="event-content">
            <h2><a href="<?php echo site_url('blog/read/'.$rowsblog->post_id.'/'.url_title($rowsblog->post_title)); ?>"><?php echo $rowsblog->post_title;?></a></h2>
            <p><?php echo word_limiter(strip_tags($rowsblog->post_content),20); ?></p>
            <div class="event-time"><?php echo $rowsblog->userprofile_firstname.' '.$rowsblog->userprofile_lastname;?></div><!-- /.event-time -->
        </div><!-- /.event-content -->
    </div><!-- /.event -->
    <?php endforeach;?>
    
</div><!-- /.events -->