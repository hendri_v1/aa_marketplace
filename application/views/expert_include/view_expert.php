<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-lg-3">
						<div class="sidebar">
							<div class="widget">
								<div class="user-photo">
									<img src="<?php echo upload_path; ?>uploads/user_images/<?php echo $this->global_model->get_user_photo($row->userprofile_photo); ?>">
								</div>
							</div>
							<div class="detail-content">
								<div class="detail-vcard">
									
									<div class="detail-contact">
										<div class="detail-contact-email">
					                        <i class="fa fa-envelope-o"></i> <a href="mailto:#"><?php echo $row->user_email;?></a>
					                    </div>
					                    <div class="detail-contact-phone">
					                        <i class="fa fa-mobile-phone"></i> <a href="tel:#"><?php echo $row->userprofile_phone;?></a>
					                    </div>
					                    
					                    <div class="detail-contact-address">
					                        <i class="fa fa-map-o"></i>
					                        <?php echo $row->userprofile_address;?> <?php echo $row->city_name;?>, <?php echo $row->province_name;?><br>
					                    </div>
									</div>
								</div>
								<div class="detail-follow">
									<h5>Follow Me: </h5>
									<div class="follow-wrapper">
										<?php if($row->userprofile_fb<>''): ?>
					                        <a href="<?php echo $row->userprofile_fb ?>" class="follow-btn facebook"><i class="fa fa-facebook"></i></a>
					                    <?php endif;?>
					                    <?php if($row->userprofile_twitter<>''): ?>
					                        <a href="<?php echo $row->userprofile_twitter ?>" class="follow-btn twitter"><i class="fa fa-twitter"></i></a>
					                    <?php endif;?>
					                    <?php if($row->userprofile_linkedin<>''): ?>
					                        <a href="<?php echo $row->userprofile_linkedin ?>" class="follow-btn linkedin"><i class="fa fa-linkedin"></i></a>
					                    <?php endif;?>
									</div>
								</div>
								<hr />
								<div class="detail-actions">
									<div class="btn btn-secondary btn-share"><i class="fa fa-share-square-o"></i> Share
				                        <div class="share-wrapper">
				                            <ul class="share">
				                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(uri_string()); ?>"><i class="fa fa-facebook"></i> Facebook</a></li>
				                                <li><a href="http://twitter.com/home?status=<?php echo base_url(uri_string()); ?>"><i class="fa fa-twitter"></i> Twitter</a></li>
				                                <li><a href="https://plus.google.com/share?url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-google-plus"></i> Google+</a></li>
				                                
				                                <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-linkedin"></i> Linkedin</a></li>
				                                
				                            </ul>
				                        </div>
				                    </div>
				                </div>
							</div>
							
						</div>
					</div>
					<div class="col-sm-8 col-lg-9">
						<div class="content">
							<div class="page-title">
								<h1><?php echo $row->userprofile_firstname;?> <?php echo $row->userprofile_lastname;?></h1>
								<?php $qcateg=$this->global_model->get_category_by_id($row->user_id);
				                        foreach ($qcateg->result() as $rowscateg): ?>
				                            <?php echo $rowscateg->category_name;?> 
				                        <?php endforeach;?> | <?php echo $this->global_model->get_membership($row->user_status,$row->is_premium);?> | Member Sejak <?php echo mdate('%M %Y',$row->user_register_date);?>
							</div>
							
							<div class="background-white p20 mb30">
								<h3 class="page-title">Tentang</h3>
								<?php echo $row->userprofile_aboutme;?>
							</div>
							<div class="post-detail">
								<div class="post-meta-tags p0 m0 mb30">
									<ul>
										<?php $qkeyword=$this->global_model->get_keywords(1,$row->user_id);
						                foreach($qkeyword->result() as $rowskw): ?>
						                	<li class="tag"><a href="#"><?php echo $rowskw->keyword_text;?></a></li>

						                <?php endforeach;?>
						            </ul> 
								</div>
							</div>

							<div class="background-white p20 mb30">
								<h3 class="page-title">Studi Kasus</h3>
								<?php $totalpf=$case->num_rows();?>
		                        <?php if($totalpf==0): ?>
		                            Belum ada Case Study ditambahkan
		                        <?php else: ?>
		                            <ul>
		                            <?php foreach ($case->result_array() as $listCase): ?>
		                                <li>
		                                    <span class="small"><?php echo mdate('%F %Y',$listCase['userextra_when']);?></span><br />
		                                    <span class="text-warning"><?php echo $listCase['userextra_title']; ?></span><br />
		                                    <?php echo strip_tags($listCase['userextra_description']); ?>
		                                    <?php if ($listCase['userextra_attachment']): ?><br />
		                                        <a href="<?php echo site_url('profile/my_profile/download').'/'.$listCase['userextra_attachment'].'/'.url_title($listCase['userextra_title']).'_attachment' ?>">Attachment</a>
		                                    <?php endif ?>
		                                
		                                </li>
		                            <?php endforeach ?>
		                            </ul>
		                        <?php endif;?>
		                    </div>

		                    <div class="background-white p20 mb30">
		                    	<h3 class="page-title">Sertifikasi</h3>
		                    	<?php if($totalpf==0): ?>
			                        Belum ada sertifikasi yang ditambahkan
			                    <?php else: ?>
			                        <ul>
			                        <?php foreach ($case->result_array() as $listCase): ?>
			                            <li>
			                                <span class="small"><?php echo mdate('%F %Y',$listCase['userextra_when']);?></span><br />
			                                <span class="text-warning"><?php echo $listCase['userextra_title']; ?></span><br />
			                                <?php echo strip_tags($listCase['userextra_description']); ?>
			                                <br />
			                                <?php if ($listCase['userextra_attachment']): ?><br />
			                                    <a href="<?php echo site_url('profile/my_profile/download').'/'.$listCase['userextra_attachment'].'/'.url_title($listCase['userextra_title']).'_attachment' ?>">Attachment</a>
			                                <?php endif ?>
			                            </li>
			                            
			                            
			                        <?php endforeach ?>
			                        </ul>
			                    <?php endif;?>
			                </div>
						</div>

					</div>
				</div>

			</div>
			
		</div>
	</div>
<?php $this->load->view('html_footer');?>
<script type="text/javascript">
    $(document).ready(function(){
        function PopupCenter(url, title, w, h) {
            // Fixes dual-screen position                         Most browsers      Firefox
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            // Puts focus on the newWindow
            if (window.focus) {
                newWindow.focus();
            }


        }
        $('.share a').click(function(e){
            e.preventDefault();
            the_link=$(this).attr('href');
            PopupCenter(the_link, "", 500,400);
        })
    })
</script>