<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner" style="background:url('<?php echo base_url();?>assets/img/login.jpeg');background-size:cover;background-position:center center;">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						
						<div class="background-white p20">
							<form class="form-signin" method="post" action="<?php echo site_url('program/get_login/?redirect_to='.$this->input->get('redirect_to'));?>" role="form" style="padding:10px;">
	                            <div class="form-group">
			                        <label for="email">Username / Email</label>
			                        <input type="text" class="form-control" name="email" placeholder="Username or Email address" required="" autofocus="">
								</div>
								<div class="form-group">
									<label for="password">Password</label>
			                        <input type="password" class="form-control" name="password" placeholder="Password" required="">
			                    </div>
		                        <div class="form-group">
		                      		<div class="checkbox">
			                        	<input type="checkbox" name="remember" id="remember"><label for="remember">Tetap Login</label>
			                        </div>
			                    </div>
		                        <a href="<?php echo site_url('program/reset') ?>">Lupa Password</a><br />
		                        <hr />
		                    	<button class="btn btn-block btn-primary" type="submit">Masuk</button>
		                    	<hr />
								Belum memiliki akun ? 
								<?php if($this->input->get('redirect_to')): ?>
									<a href="<?php echo site_url('register?redirect_to='.$this->input->get('redirect_to'));?>">Daftar Sekarang</a>
								<?php else: ?>
									<a href="<?php echo site_url('register');?>">Daftar Sekarang</a>
								<?php endif;?>
		                    </form>

		                </div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('html_footer');?>