<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner" style="background:url('<?php echo base_url();?>assets/img/login.jpeg');background-size:cover;background-position:center center;">
			<div class="container">
				<form class="form-horizontal" method="POST" action="<?php echo site_url('program/save_register/?redirect_to='.$this->input->get('redirect_to'));?>" role="form">
		            <div class="row">
		            	<?php if($this->session->flashdata('error_return')): ?>
		                	<div class="col-sm-12">
		                    	<div class="boxes">
		                        	<?php echo $this->session->flashdata('error_return');?>
		                        </div>
		                    </div>
		                <?php endif;?>
		                <div class="col-sm-6">
		                	<div class="row">
		                        <div class="col-sm-12">
		                            
		                            <div class="background-white p20 mb30">
		                            	<h4 class="page-title">Informasi Login</h4>
		                               <input type="text" id="username" name="username" class="form-control with_alert" placeholder="Username" data-checkwhat="username" required="" autofocus="">
		                                <span class="messagefor hidden" data-messagefor="username"></span>
		                               <br />
		                               <input type="password" name="password" class="form-control" placeholder="Password" required=""><br />
		                               <input type="password" name="rpassword" class="form-control" placeholder="Repeat Password" required="required"><br />
		                               <input type="email" name="email" id="email" class="form-control with_alert" placeholder="Email" data-checkwhat="email" required="required">
		                               <span class="messagefor hidden" data-messagefor="email"></span><br />
		                                   
		                            </div>
		                        </div>
		                        <div class="col-sm-12">
		                            
		                            <div class="background-white p20 mb30">
		                            	<h4 class="page-title">Informasi Anggota</h4>
		                                 <select name="registered" class="form-control" id="registered" place-holder="Register As" required="required">
		                                     <?php foreach ($utype as $key => $value): ?>
		                                        <option value="<?php echo $key;?>">Register sebagai <?php echo ucfirst($value) ?></option>
		                                     <?php endforeach ?>
		                                 </select>
		                                 
		                                 <br />
		                                 <select name="userprofile_function" id="userprofile_function" class="form-control">
		                                    <option value="98">Pilih Kategori Utama</option>
		                                    <?php foreach($this->global_model->get_category(1)->result() as $rowscateg): ?>
		                                        <option value="<?php echo $rowscateg->category_id;?>"><?php echo $rowscateg->category_name;?></option>
		                                    <?php endforeach;?>
		                                 </select>
		                                 
		                                 <select name="userprofile_function2" id="userprofile_function2" class="form-control hidden">
		                                    <option value="99">Pilih Kategori</option>
		                                    <?php foreach($this->global_model->get_category(2)->result() as $rowscateg): ?>
		                                        <option value="<?php echo $rowscateg->category_id;?>"><?php echo $rowscateg->category_name;?></option>
		                                    <?php endforeach;?>
		                                 </select>

		                                 <select name="userprofile_function3" id="userprofile_function3" class="form-control hidden">
		                                    <option value="100">Non HR</option>
		                                 </select>
		                                 <br class="brhide hidden" />
		                                 <input type="text" name="cname" id="cname" class="form-control hidden" placeholder="Nama Perusahaan" /><br />
		                                 <input type="text" name="fname" class="form-control" placeholder="Nama Depan" required="required" autofocus=""><br />
		                                 <input type="text" name="lname" class="form-control" placeholder="Nama Belakang" required="required" autofocus=""><br />
		                                 
		                                 
		                                 <input type="text" name="position" id="position" class="form-control hidden" placeholder="Jabatan" />
		                                 <br class="brhide hidden" />
		                                 <select name="province_id" id="provinceAjax" class="form-control">
		                                     <option value="0">Pilih Provinsi</option>
		                                     <?php foreach ($province as $listProvince): ?>
		                                         
		                                         <option value="<?php echo $listProvince->province_id ?>"><?php echo $listProvince->province_name ?></option>
		                                     <?php endforeach ?>
		                                 </select>
		                                 <br>
		                                 <select name="city_id" id="cityAjax" class="form-control">
		                                     <option value="0">Mohon pilih provinsi</option>
		                                 </select>
		                             </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="col-sm-6">
		                	
		                    <div class="background-white p20">
		                    	<h4 class="page-title">Siap Mendaftar</h4>
		                    	<div class="form-group">
		                    		<div class="checkbox">
				                        <input type="checkbox" id="mustAgree" name="checkbox" class="checkbox-inline" />
				                        <label for="mustAgree">I may wish to become a full member of HRplasa, to gain access to the resources and/or market my services and/or take part in discussions and/or have access to other services offered by HRplasa on behalf of it members.
				                        I agree to HRplasa's <a href="<?php echo site_url('syarat-dan-ketentuan');?>" target="_blank">Terms and Conditions</a> confirm that all my details, including products & services that I offer, are correct and will be kept up to date</label>
									</div>
			                    </div>
		                        <hr />
		                        <button class="btn btn-primary btn-md" id="submitregister" type="submit">Submit Details</button>
		                    </div>
		                </div>
		            
		            </div>
		            
		               
		                
		            
		        </form>
			</div>
		</div>
	</div>
<?php $this->load->view('html_footer');?>
<script type="text/javascript">
    $(document).ready(function(e) {
        $('#submitregister').attr('disabled', 'disabled');
        $( "#mustAgree" ).change(function() {
            if($(this).is(":checked")){
                $('#submitregister').removeAttr('disabled');
            } else {
                $('#submitregister').attr('disabled', 'disabled');
            }
        });

        $('#registered').change(function(){
            tselect=$(this).val();
            if(tselect==1)
            {
                $('#cname').addClass('hidden');
                $('#position').addClass('hidden');
                $('#userprofile_function2').addClass('hidden');
                $('#userprofile_function').removeClass('hidden');
                $('#userprofile_function3').addClass('hidden');
                $('.brhide').addClass('hidden');
            }
            else if(tselect==2)
            {
                $('#cname').removeClass('hidden');
                $('#position').removeClass('hidden');
                $('#userprofile_function2').removeClass('hidden');
                $('#userprofile_function').addClass('hidden');
                $('#userprofile_function3').addClass('hidden');
                $('.brhide').removeClass('hidden')
            }
            else
            {
                $('#cname').addClass('hidden');
                $('#position').addClass('hidden');
                $('#userprofile_function2').addClass('hidden');
                $('#userprofile_function3').removeClass('hidden');
                $('#userprofile_function').addClass('hidden');
                $('.brhide').addClass('hidden');
            }
        });
        $('#provinceAjax').change(function(event) {
            var prov = $("#provinceAjax").val();

             $.post('<?php echo site_url('program/getProvince');?>',
             {
                province:prov
             },
             function(data)
             {
                $('#cityAjax').html(data);
             }
             );
        });
        $('.with_alert').blur(function() {

            thedata=$(this).val();
            checkwhat=$(this).data('checkwhat');
            $('.messagefor').each(function(){
                if($(this).data('messagefor')==checkwhat)
                 selected_message=$(this)
            });
            $.post('<?php echo site_url('program/check_exist');?>',
            {
                tocheck:$(this).val(),
                check_what:checkwhat
            },
            function(data)
            {
                 $(selected_message).removeClass('hidden');
                if(data.error==true)
                {
                   
                    $(selected_message).css('color','red');
                }
                else
                {
                    
                    $(selected_message).css('color','green');

                }
                $(selected_message).html(data.message);
            },
            'json'
            );
        });
    });
</script>