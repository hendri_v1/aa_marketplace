<?php $this->load->view('html_head');?>
	<div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    


                    <div class="col-sm-8 col-lg-9">
                        <div class="content">
                            <div class="page-title">
							    <h1>FAQ</h1>
							</div><!-- /.page-title -->

							<div class="faq">
							    
						        <div class="faq-item">
						            <div class="faq-item-question">
						                <h2>Apa itu HRPlasa?</h2>
						            </div><!-- /.faq-item-question -->

						            <div class="faq-item-answer">
						                <p>
						                    HRPlasa adalah sebuah market place yang mempertemukan HR Expert, Vendor, Lembaga Sertifikasi, Training Provider, Consulting dalam sebuah platform online dengan skema keanggotaan (membership) berjenjang (freemium, premium, partner untuk saling bertukar informasi, memenuhikebutuhan pengembangan Sumber Daya Manusia (SDM).
						                </p>
						            </div><!-- /.faq-item-answer -->

						            <div class="faq-item-meta">
						                Was this answer helpful?
						                <span class="rate">
						                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
						                </span><!-- /.rate -->
						            </div><!-- /.faq-item-meta -->
						        </div><!-- /.faq-item -->
						    
						        <div class="faq-item">
						            <div class="faq-item-question">
						                <h2>Kenapa Anda harus menggunakan HRPlasa?</h2>
						            </div><!-- /.faq-item-question -->

						            <div class="faq-item-answer">
						                <p>
						                    Dengan bergabung dalam keanggotaan HRPlasa, seluruh anggota akan dapat menggunakan fitur-fitur yang tersedia yang membantu dalam mempermudah pekerjaan sehari-hari dari mencari dan mengevaluasi vendor, mengirimkan lowongan pekerjaan, mencari venue untuk tempat meeting/training/conference hingga membuat sebuah project yang bersifat tender yang dapat diikuti oleh semua/vendor terpilih
						                </p>
						            </div><!-- /.faq-item-answer -->

						            <div class="faq-item-meta">
						                Was this answer helpful?
						                <span class="rate">
						                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
						                </span><!-- /.rate -->
						            </div><!-- /.faq-item-meta -->
						        </div><!-- /.faq-item -->
						    
						        <div class="faq-item">
						            <div class="faq-item-question">
						                <h2>Bagaimana sistem keanggotaan HRPlasa?</h2>
						            </div><!-- /.faq-item-question -->

						            <div class="faq-item-answer">
						                <p>
						                    HRpalsa memiliki 2 (dua) macam keanggotaan, keanggotaan sebagai INDIVIDU yang terbagi menjadi 2(dua)  jenis yaitu Freemium, Premium dan yang kedua adalah keanggotaan sebagai CORPORATE.  Lebih lengkapnya ada di <a href="<?php echo site_url('membership');?>">halaman keanggotaan</a>.
						                </p>
						            </div><!-- /.faq-item-answer -->

						            <div class="faq-item-meta">
						                Was this answer helpful?
						                <span class="rate">
						                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
						                </span><!-- /.rate -->
						            </div><!-- /.faq-item-meta -->
						        </div><!-- /.faq-item -->
						    
						        <div class="faq-item">
						            <div class="faq-item-question">
						                <h2>Saya memiliki kebutuhan pengembangan SDM (e.g: training/asesment/coaching, consulting), apa yang harus dilakukan?</h2>
						            </div><!-- /.faq-item-question -->

						            <div class="faq-item-answer">
						                <p>
						                    Setelah bergabung dalam keanggotaan HRPlasa maka anda dapat membuat sbuah proejct tender dengan klik "Post a Project". Kemudian lengkapi form yang sudah disediakan. Pilih opsi-opsi yang diberikan pada langkah selanjutnya. . HRPlasa akan mengenakan biaya sebesar 2% dari transaksi kepada anda
						                </p>
						            </div><!-- /.faq-item-answer -->

						            <div class="faq-item-meta">
						                Was this answer helpful?
						                <span class="rate">
						                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
						                </span><!-- /.rate -->
						            </div><!-- /.faq-item-meta -->
						        </div><!-- /.faq-item -->
						    
						        <div class="faq-item">
						            <div class="faq-item-question">
						                <h2>Saya akan mengadakan event (training/seminar/conference/gathering, dsb) apakah HRPlasa bisa membantu menginformasikan kepada calon pelanggan?</h2>
						            </div><!-- /.faq-item-question -->

						            <div class="faq-item-answer">
						                <p>
						                    Anda dapat memilih project dengan  opsi "Featured Project" dan dikenakan Rp. 250.000 per project
						                </p>
						            </div><!-- /.faq-item-answer -->

						            <div class="faq-item-meta">
						                Was this answer helpful?
						                <span class="rate">
						                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
						                </span><!-- /.rate -->
						            </div><!-- /.faq-item-meta -->
						        </div><!-- /.faq-item -->

						        <div class="faq-item">
						            <div class="faq-item-question">
						                <h2>Saya akan mengadakan event (training/seminar/conference/gathering, dsb) apakah HRPlasa bisa membantu menginformasikan kepada calon pelanggan?</h2>
						            </div><!-- /.faq-item-question -->

						            <div class="faq-item-answer">
						                <p>
						                    Anda dapat memilih project dengan  opsi "Featured Project" dan dikenakan Rp. 250.000 per project
						                </p>
						            </div><!-- /.faq-item-answer -->

						            <div class="faq-item-meta">
						                Was this answer helpful?
						                <span class="rate">
						                    <a href="#">Yes</a><span class="separator">/</span><a href="#">No</a>
						                </span><!-- /.rate -->
						            </div><!-- /.faq-item-meta -->
						        </div><!-- /.faq-item -->
							    
							</div><!-- /.faq -->



                        </div><!-- /.content -->
                    </div><!-- /.col-* -->

                    <div class="col-sm-4 col-lg-3">
                        <div class="sidebar">
                            
                            <div class="widget">
							    <h2 class="widgettitle">Link Terkait</h2>

							    <ul class="menu">
							        <li><a href="<?php echo site_url('tentang-kami');?>">Tentang Kami</a></li>
							        <li><a href="#">Cara Kerja</a></li>
							        <li><a href="#">Membership</a></li>
							        
							    </ul><!-- /.menu -->
							</div><!-- /.wifget -->


                           

                        </div><!-- /.sidebar -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
<?php $this->load->view('html_footer');?>