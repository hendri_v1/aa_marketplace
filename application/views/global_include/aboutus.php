<?php $this->load->view('html_head');?>
	<div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="content">
                    
				    <div class="document-title">
				        <h1>Tentang Kami</h1>

				        <ul class="breadcrumb">
				            <li><a href="<?php echo base_url();?>">HRPlasa</a></li>
				            <li><a href="#">Tentang Kami</a></li>
				        </ul>
				    </div><!-- /.document-title -->



                    <p>HRPlasa.id menyoroti berita, artikel, dan praktik terbaik dalam industri Pengembangan Sumber Daya Manusia (Human Resources Development). Fokus kami adalah membantu para professional, individu dan organisasi mendapatkan informasi terkini, wawasan dan referensi yang diperlukan untuk lebih efektif dalam pengelolaan pengembangan sumber daya manusia.</p>

<div class="row">
    <div class="col-sm-8">
        <div class="row">
            <div class="col-sm-4">
                <h3>Alamat</h3>

                <p>
	                Graha Positif, <br />Jl.pasar Jumat No.44a LT3<br />
                    Pondok Pinang Jakarta Selatan 12310<br />
	                
	                <br />
	            </p>
            </div><!-- /.col-sm-4 -->

            <div class="col-sm-4">
                <h3>Kontak</h3>

                <p>
                    Phone +62-21-759-05509<br />
	                email info@hrplasa.id
                </p>
            </div><!-- /.col-sm-4 -->

            <div class="col-sm-4">
                <h3>Sosial Media</h3>

                <ul class="social-links nav nav-pills">
                    <li><a href="https://twitter.com/hrplasa"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="http://www.facebook.com/hrplasa"><i class="fa fa-facebook"></i></a></li>
                   
                    <li><a href="http://id.linkedin.com/pub/hr-plasa-id/b0/136/321/en"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCczhk7qUxggG5P5Qao2Y6bA"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div><!-- /.col-sm-4 -->
        </div><!-- /.row -->

        <div class="background-white p30 mt30">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7931.619913522312!2d106.77350056047824!3d-6.288692620572651!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1cd3f7cc3cd%3A0xf7e4c30e9742ccee!2sJalan+Pasar+Jumat+Lebak+Bulus+No.44%2C+Cilandak%2C+Kota+Jakarta+Selatan%2C+DKI+Jakarta+12440!5e0!3m2!1sen!2sid!4v1414602126475" width="100%" height="250" frameborder="0" style="border:0"></iframe>
        </div>
    </div>

    <div class="col-sm-4">
        <h3>We’d love to hear from you</h3>

        <script type="text/javascript" src="http://assets.freshdesk.com/widget/freshwidget.js"></script>
<style type="text/css" media="screen, projection">
    @import url(http://assets.freshdesk.com/widget/freshwidget.css); 
</style> 
<iframe title="Feedback Form" class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://hrplasa.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&formTitle=Hubungi+HRPLasa&submitThanks=Terima+Kasih+Telah+Menghubungi+HRPlasa&screenshot=no&searchArea=no" scrolling="no" height="600px" width="100%" frameborder="0" >
</iframe>
    </div>
</div><!-- /.row -->

                </div><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

<?php $this->load->view('html_footer');?>