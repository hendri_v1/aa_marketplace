<div class="block background-white fullwidth">
        <div class="page-header">
            <h1>Testimonials</h1>
            <p>Apa kata mereka tentang HRPlasa.id ?</p>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-sm-6">
                <div class="testimonial">
                    <div class="testimonial-image">
                        <img src="<?php echo base_url();?>uploads/testimoni/th.jpg" alt="">
                    </div><!-- /.testimonial-image -->

                    <div class="testimonial-inner">
                        <div class="testimonial-title">
                            <h2>Toto Handoyo</h2>

                            <div class="testimonial-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.testimonial-rating -->
                        </div><!-- /.testimonial-title -->

                         "Sangat Informatif dan layak menjadi referensi pengembangan SDM di Indonesia.<br /> Sukses Selalu HRPlasa.id"

                        <div class="testimonial-sign">- HR Business Partner - Japfa Comfeed</div><!-- /.testimonial-sign -->
                    </div><!-- /.testimonial-inner -->
                </div><!-- /.testimonial -->

                <div class="testimonial">
                    <div class="testimonial-image">
                        <img src="<?php echo base_url();?>uploads/testimoni/rs.jpg" alt="">
                    </div><!-- /.testimonial-image -->

                    <div class="testimonial-inner">
                        <div class="testimonial-title">
                            <h2>Reza Soedomo</h2>

                            <div class="testimonial-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.testimonial-rating -->
                        </div><!-- /.testimonial-title -->

                       "Saya Baru Pertama kali menggunakan jasa HR plasa Jauh diatas harapan Saya ternyata HRplasa Sangat efektif dalam menyebarkan informasi guna mendatangkan peserta untuk menghadiri event yang kami selanggarakan.<br />
                                Terbukti , proses dan teknologi sederhana yang digunakan dapat secara tepat menjangkau target peserta yang dituju. Terima Kasih Atas Bantuan yang diberikan dengan penuh Semangat dan Tulus."

                        <div class="testimonial-sign">- Managing Partner of Blessing White.</div><!-- /.testimonial-sign -->
                    </div><!-- /.testimonial-inner -->
                </div><!-- /.testimonial -->

                <div class="testimonial last">
                    <div class="testimonial-image">
                        <img src="<?php echo base_url();?>uploads/testimoni/wd.jpg" alt="">
                    </div><!-- /.testimonial-image -->

                    <div class="testimonial-inner">
                        <div class="testimonial-title">
                            <h2>Widijatmoko</h2>

                            <div class="testimonial-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.testimonial-rating -->
                        </div><!-- /.testimonial-title -->

                       Saya melihat HRPlasa.id sangat membantu bagi saya sebagai sales dan leadership trainer (fasilitator) di dalam menjembatani komunikasi dengan perusahaan-perusahaan yang membutuhkan pelatihan dan pengembangan
                        <div class="testimonial-sign">- Sales &amp; leadership Trainer</div><!-- /.testimonial-sign -->
                    </div><!-- /.testimonial-inner -->
                </div><!-- /.testimonial -->
            </div><!-- /.col-* -->

            <div class="col-sm-6">
                <div class="testimonial">
                    <div class="testimonial-image">
                        <img src="http://hrplasa.id/uploads/event_images/1434940349.jpg" alt="">
                    </div><!-- /.testimonial-image -->

                    <div class="testimonial-inner">
                        <div class="testimonial-title">
                            <h2>City Training</h2>

                            <div class="testimonial-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.testimonial-rating -->
                        </div><!-- /.testimonial-title -->

                        City Training baru bergabung kerja sama dengan HRPlasa sekitar 2 bulan yang lalu.  Tidak ada kesulitan dalam memposting jadwal, langkah-langkahnya sangat mudah dan tidak membingungkan. HRPlasa juga sangat informatif dan membantu sekali dalam hal mempromosikan event-event pelatihan  Dan yang paling penting, pembayaran ke vendor juga tepat waktu. Sangat senang sekali dapat bekerjasama dengan HRPlasa.

                        <div class="testimonial-sign">- HRPlasa Member</div><!-- /.testimonial-sign -->
                    </div><!-- /.testimonial-inner -->
                </div><!-- /.testimonial -->

                <div class="testimonial last">
                    <div class="testimonial-image">
                        <img src="http://hrplasa.id/uploads/user_images/1426669586.png" alt="">
                    </div><!-- /.testimonial-image -->

                    <div class="testimonial-inner">
                        <div class="testimonial-title">
                            <h2>Neo Talenta</h2>

                            <div class="testimonial-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.testimonial-rating -->
                        </div><!-- /.testimonial-title -->

                        HRplasa mempermudah kami para Training Provider dalam menyampaikan informasi training yang akan kami laksanakan. Dengan pelayanan yang profesional yang semakin ditingkatkan saya yakin HRplasa akan menjadi referensi perusahaan-perusahaan dalam mencari informasi pelatihan. HRplasa semoga semakin sukses.

                        <div class="testimonial-sign">- HRPlasa Member</div><!-- /.testimonial-sign -->
                    </div><!-- /.testimonial-inner -->
                </div><!-- /.testimonial -->
            </div><!-- /.col-* -->
        </div>

    </div>