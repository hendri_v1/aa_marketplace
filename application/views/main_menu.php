<header class="header">
    <div class="header-wrapper">
        <div class="container">
            <div class="header-inner">
                <div class="header-logo">
                    <a href="<?php echo base_url();?>">
                        <img src="<?php echo base_url();?>assets/img/logo.png" alt="Logo">
                        
                    </a>
                </div><!-- /.header-logo -->
                
                <div class="header-content">
                    
                    <div class="header-bottom">
                        <div class="header-action">
                            <?php if($this->session->userdata('logged')): ?>
                                <a href="<?php echo site_url('program/dashboard');?>" class="btn btn-danger"><i class="fa fa-user"></i> My HRPlasa</a>
                                <a href="<?php echo site_url('program/logout');?>" class="btn btn-primary"><i class="fa fa-key"></i> Logout</a>
                            <?php else: ?>
                                <?php if($this->uri->segment(1)<>"login"): ?>
                                    <a href="<?php echo site_url('login');?>" class="btn btn-primary"><i class="fa fa-key"></i> Login</a>
                                    <a href="<?php echo site_url('register');?>" class="btn btn-danger"><i class="fa fa-plus"></i> Register</a>
                                <?php endif;?>
                            <?php endif;?>
                        </div><!-- /.header-action -->

                        <ul class="header-nav-primary nav nav-pills collapse navbar-collapse">
                            <?php if ($this->session->userdata('logged')): ?>
                                <li id="homenav"><a href="<?php echo base_url();?>">Home</a></li>
                                <li id="browseallnav">
                                    <a href="#">Direktori <i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu">

                                        <li><a href="<?php echo site_url('events'); ?>">Event</a></li>
                                        <li><a href="<?php echo site_url('venue_directory'); ?>">Venue</a></li>
                                        <li><a href="<?php echo site_url('jobs'); ?>">Lowongan Kerja</a></li>
                                        <li><a href="<?php echo site_url('vendor-directory'); ?>">Vendor</a></li>
                                        <li><a href="<?php echo site_url('individual-directory'); ?>">Expert</a></li>
                                        
                                        
                                        <?php /* <li><a href="<?php echo site_url('project/latest');?>">Project</a></li> */ ?>
                                        
                                    </ul>
                                </li>
                                <li id="blognav"><a href="<?php echo site_url('blog'); ?>">Blog</a></li>
                                <li id="prnewswirenav"><a href="<?php echo site_url('prnewswire'); ?>">PR Newswire</a></li>
                            <?php else: ?>
                                <li id="homenav"><a href="<?php echo base_url();?>">Home</a></li>
                                <li id="browseallnav">
                                    <a href="#">Direktori <i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu">

                                        <li><a href="<?php echo site_url('events'); ?>">Event</a></li>
                                        <li><a href="<?php echo site_url('venue_directory'); ?>">Venue</a></li>
                                        <li><a href="<?php echo site_url('jobs'); ?>">Lowongan Kerja</a></li>
                                        <li><a href="<?php echo site_url('vendor-directory'); ?>">Vendor</a></li>
                                        <li><a href="<?php echo site_url('individual-directory'); ?>">Expert</a></li>
                                        
                                        
                                        <?php /* <li><a href="<?php echo site_url('project/latest');?>">Project</a></li> */ ?>
                                        
                                    </ul>
                                </li>
                                <li id="aboutusnav">
                                    <a href="#">Mengapa Kami? <i class="fa fa-chevron-down"></i></a>
                            
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo site_url('tentang-kami'); ?>">Tentang Kami</a></li>
                                        <li><a href="<?php echo base_url('cara-kerja'); ?>">Cara Kerja</a></li>
                                        <li><a href="<?php echo site_url('membership'); ?>">Keanggotaan</a></li>
                                        <li><a href="<?php echo site_url('faq'); ?>">FAQ</a></li>
                                    </ul>
                                        
                                </li>
                                

                                <li id="blognav"><a href="<?php echo site_url('blog'); ?>">Blog</a></li>
                                <li id="prnewswirenav"><a href="<?php echo site_url('prnewswire'); ?>">PR Newswire</a></li>
                                

                            <?php endif;?>
                            <?php /*
                            <li class="active">
                                <a href="#">Home <i class="fa fa-chevron-down"></i></a>

                                <ul class="sub-menu">
                                    <li><a href="index-video.html">Video v1</a></li>
                                    <li><a href="index-video-transparent-header.html">Video v2</a></li>
                                    <li><a href="index-google-map.html">Google Map V1</a></li>
                                    <li><a href="index-google-map-transparent-header.html">Google Map v2</a></li>
                                    <li><a href="index-image.html">Image v1</a></li>
                                    <li><a href="index.html">Image v2</a></li>
                                    <li><a href="index-bootstrap-slider.html">Boxed Slider</a></li>
                                </ul>
                            </li>

                            <li >
                                <a href="#">Listing <i class="fa fa-chevron-down"></i></a>

                                <ul class="sub-menu">
                                    <li><a href="listing-detail.html">Detail</a></li>
                                    <li><a href="listing-map.html">Row + Map</a></li>
                                    <li><a href="listing-grid.html">Grid</a></li>
                                    <li><a href="listing-grid-sidebar.html">Grid Sidebar</a></li>
                                    <li><a href="listing-row.html">Row</a></li>
                                    <li><a href="listing-row-sidebar.html">Row Sidebar</a></li>
                                </ul>
                            </li>

                            <li class="has-mega-menu ">
                                <a href="#">Pages <i class="fa fa-chevron-down"></i></a>

                                <ul class="mega-menu">
                                    <li>
                                        <a href="#">General</a>

                                        <ul>
                                            <li><a href="faq.html">FAQ</a></li>
                                            <li><a href="invoice.html">Invoice</a></li>
                                            <li><a href="pricing.html">Pricing</a></li>
                                            <li><a href="grid.html">Grid System</a></li>
                                            <li><a href="testimonials.html">Testimonials</a></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="#">User Account</a>

                                        <ul>
                                            <li><a href="user-profile-edit.html">Profile Edit</a></li>
                                            <li><a href="login.html">Login Form</a></li>
                                            <li><a href="register.html">Register Form</a></li>
                                            <li><a href="change-password.html">Change Password Form</a></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="#">Miscellaneous</a>

                                        <ul>
                                            <li><a href="sticky-footer.html">Sticky Footer</a></li>
                                            <li><a href="terms-conditions.html">Terms &amp; Conditions</a></li>
                                            <li><a href="error-403.html">403 - Forbidden</a></li>
                                            <li><a href="error-404.html">404 - Not Found</a></li>
                                            <li><a href="error-500.html">500 - Internal Error</a></li>
                                        </ul>
                                    </li>

                                    <li class="hidden-xs">
                                        <div class="special">
                                            <a href="register.html">Sign Up Now</a>
                                        </div><!-- /.special-->
                                    </li>
                                </ul>
                            </li>

                            <li >
                                <a href="#">Blog <i class="fa fa-chevron-down"></i></a>

                                <ul class="sub-menu">
                                    <li><a href="blog-standard-right-sidebar.html">Standard Right Sidebar</a></li>
                                    <li><a href="blog-standard-left-sidebar.html">Standard Left Sidebar</a></li>
                                    <li><a href="blog-boxed.html">Boxed Style</a></li>
                                    <li><a href="blog-condensed.html">Condensed Style</a></li>
                                    <li><a href="blog-detail.html">Detail Fullwidth</a></li>
                                    <li><a href="blog-detail-right-sidebar.html">Detail Right Sidebar</a></li>
                                    <li><a href="blog-detail-left-sidebar.html">Detail Left Sidebar</a></li>
                                </ul>
                            </li>

                            <li >
                                <a href="#">Admin <i class="fa fa-chevron-down"></i></a>

                                <ul class="sub-menu">
                                    <li><a href="admin-dashboard.html">Dashboard</a></li>
                                    <li><a href="admin-grid.html">Grid System</a></li>
                                    <li><a href="admin-tables.html">Tables</a></li>
                                    <li><a href="admin-forms.html">Forms</a></li>
                                    <li><a href="admin-notifications.html">Notifications</a></li>
                                </ul>
                            </li>

                            <li >
                                <a href="#">Contact <i class="fa fa-chevron-down"></i></a>
                                <ul class="sub-menu">
                                    <li><a href="contact-1.html">Contact v1</a></li>
                                    <li><a href="contact-2.html">Contact v2</a></li>
                                    <li><a href="contact-3.html">Contact v3</a></li>
                                </ul>
                            </li>
                            */ ?>
                        </ul>
                        
                        <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav-primary">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    </div><!-- /.header-bottom -->
                </div><!-- /.header-content -->
            </div><!-- /.header-inner -->
        </div><!-- /.container -->
    </div><!-- /.header-wrapper -->
</header><!-- /.header -->