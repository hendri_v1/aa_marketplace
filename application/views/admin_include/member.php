<?php $this->load->view('member_include/html_head');?>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#pendingtab" class="tab-ajax" role="tab" data-toggle="tab">Upgrade Request</a></li>
                <li><a href="#aktiftab" role="tab" class="tab-ajax" data-toggle="tab">Aktif</a></li>
                <li><a href="#premiumtab" role="tab" class="tab-ajax" data-toggle="tab">Premium</a></li>
                <li><a href="#searchtab" role="tab" class="tab-ajax" data-toggle="tab">Search Member</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="pendingtab">


                </div>
                <div class="tab-pane fade" id="aktiftab">

                </div>
                <div class="tab-pane fade" id="premiumtab">

                </div>
                <div class="tab-pane fade" id="searchtab">

                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript">
    $(function () {
        $('#pendingtab').html(loader);
        $('#pendingtab').load('<?php echo site_url('themaster/member/pending_user'); ?>');

        $('.tab-ajax').click(function () {
            target = $(this).attr('href');
            ur_target = '';
            if (target == "#pendingtab")
                url_target = '<?php echo site_url('themaster/member/pending_user'); ?>';
            else if (target == "#aktiftab")
                url_target = '<?php echo site_url('themaster/member/aktif_user'); ?>';
            else if (target == "#premiumtab")
                url_target = '<?php echo site_url('themaster/member/premium_user'); ?>';
            else if (target == "#searchtab")
                url_target = '<?php echo site_url('themaster/member/search_user'); ?>';
            $(target).html(loader);
            $(target).load(url_target);
        });

        $('.tab-content').on('click', '.pager>li>a', function (e) {
            e.preventDefault();
            parent1 = $(this).parent();
            parent2 = $(parent1).parent();
            parent3 = $(parent2).parent();
            parent4 = $(parent3).parent();
            target = $(this).attr('href');
            $(parent4).append(loader);
            $(parent4).load(target);

        });
    });
</script>