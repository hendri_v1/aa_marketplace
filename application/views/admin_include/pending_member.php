<div class="background-white p20">
	<table class="table">
		<thead>
			<tr>
				<th>ID</th><th>Name</th><th>User Type</th><th>Company Name</th><th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($queryuser->result() as $rowsuser): ?>
				<tr>
					<td><?php echo $rowsuser->user_id;?></td>
					<td><?php echo $rowsuser->userprofile_firstname;?> <?php echo $rowsuser->userprofile_lastname;?></td>
					<td><?php echo $this->global_model->get_usertype($rowsuser->usertype_id);?></td>
					<td><?php echo $rowsuser->userprofile_companyname;?></td>
					<td>
						<a href="<?php echo site_url('themaster/member/view_user/'.$rowsuser->user_id);?>" class="btn btn-info btn-sm">Detail</a>
						<?php if ($rowsuser->is_patnership == 0): ?>
							<a href="<?php echo site_url('themaster/member/upgrade_patnership/'.$rowsuser->user_id);?>" class="btn btn-warning btn-sm">Upgrade Patnership</a>
						<?php endif ?>
					</td>
				</tr>
			<?php endforeach;?>
		</tbody>
	</table>
	<div class="pull-right">
	        <?php echo $this->pagination->create_links(); ?>
	</div>
</div>