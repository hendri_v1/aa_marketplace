<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<ul class="nav nav-tabs" role="tablist">
              <li class="active"><a href="#overviewtab" role="tab" data-toggle="tab">Overview</a></li>
              <li><a href="#outlinetab" role="tab" data-toggle="tab">Outline</a></li>
              <li><a href="#outcometab" role="tab" data-toggle="tab">Outcome</a></li>
              <li><a href="#certificatetab" role="tab" data-toggle="tab">Sertifikat</a></li>
            </ul>
            <div class="tab-content">
            	<?php
            		$event_description=explode('<!---split--->', $query['event_description'])
            	?>
				<div class="tab-pane fade in active" id="overviewtab">
					<div class="background-white p20">
						<span><i class="glyphicon glyphicon-time"></i> <?php echo date('d F Y H:i:s',$query['event_start_date']);?> - <?php echo date('d F Y H:i:s',$query['event_end_date']);?></span><br />
						<?php if($query['event_picture']<>''): ?>
							<img src="<?php echo base_url('uploads/event_images/'.$query['event_picture'])?>" alt="<?php echo $query['event_title'] ?>">
						<?php endif;?>
						<?php echo $event_description[0];?>
					</div>

				</div>
				<div class="tab-pane fade" id="outlinetab">
					<div class="background-white p20">
						<?php echo $event_description[1];?>
					</div>
				</div>
				<div class="tab-pane fade" id="outcometab">
					<div class="background-white p20">
						<?php echo $event_description[2];?>
					</div>
				</div>
				<div class="tab-pane fade" id="certificatetab">
					<div class="background-white p20">
						<?php echo $event_description[3];?>
					</div>
				</div>
            </div>
		</div>
		<div class="col-sm-10">
			
				<hr />
				<table class="table">
					<thead>
						<tr><th>Billing Invoice No</th><th>Date</th><th>Due Date</th><th>Total</th><th>Status</th><th></th></tr>
					</thead>
					<tbody>
						<?php foreach($querybilling->result() as $rowsbilling): ?>
							<tr>
								<td><?php echo $rowsbilling->billing_id;?></td>
								<td><?php echo mdate('%d-%m-%Y',$rowsbilling->billing_date);?></td>
								<td><?php echo mdate('%d-%m-%Y',$rowsbilling->billing_due);?></td>
								<td>
									<?php 
									$querydetbilling=$this->userproject_model->get_detail_billing($rowsbilling->billing_id);
									$total_billing=0;
									foreach($querydetbilling->result() as $rowsdetbilling)
									{
										$total_billing=$total_billing+$rowsdetbilling->billingdetail_total;
									}
									echo number_format($total_billing,0,',','.');
									?>
								</td>
								<td><?php echo $this->global_model->get_billing_status($rowsbilling->billing_status);?></td>
								<td>
									<a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="detail-billing" data-billing_id="<?php echo $rowsbilling->billing_id;?>">Detail</a>
									| <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="payment-billing" data-billing_id="<?php echo $rowsbilling->billing_id;?>">Payment</a>
									| <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="payment-history" data-billing_id="<?php echo $rowsbilling->billing_id;?>">Payment History</a>
                                    | <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="send-invoice" data-billing_id="<?php echo $rowsbilling->billing_id;?>">Kirim Invoice</a>
								</td>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			
		</div>
		<div class="col-sm-2">
			<hr />
				<?php if($query['is_pending']==0): ?>
					<a href="<?php echo site_url('themaster/event/publish_event/'.$query['event_id']);?>" class="btn btn-info btn-sm btn-block">Publish</a>
				<?php else: ?>
					<a href="<?php echo site_url('themaster/event/unpublish_event/'.$query['event_id']);?>" class="btn btn-info btn-sm btn-block">Unpublish</a>
				<?php endif;?>
			
		</div>
	</div>
	<div class="modal fade" id="myModal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title">Edit Profile</h4>
	      </div>
	      <div class="modal-body">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.detail-billing').click(function(){
			billing_id=$(this).data('billing_id');
			$('.modal-title').html('Billing Detail No-'+billing_id);
			$('.modal-body').html(loader);
			$('.modal-body').load('<?php echo site_url('themaster/project/get_billing_detail');?>/'+billing_id);
		});

		$('.payment-billing').click(function(){
			billing_id=$(this).data('billing_id');
			event_id='<?php echo $query['event_id'];?>';
			$('.modal-title').html('Payment Billing No-'+billing_id);
			$('.modal-body').html(loader);
			$('.modal-body').load('<?php echo site_url('themaster/event/payment');?>/'+billing_id+'/'+event_id);
		});

		$('.payment-history').click(function(){
			billing_id=$(this).data('billing_id');
			$('.modal-title').html('Payment History for Billing No-'+billing_id);
			$('.modal-body').html(loader);
			$('.modal-body').load('<?php echo site_url('themaster/project/payment_history');?>/'+billing_id);
		});
		
		$('.send-invoice').click(function(){
			billing_id=$(this).data('billing_id');
			$('.modal-title').html('Send Invoice No-'+billing_id);
			$('.modal-body').html('Mengirimkan email ...');
			$('.modal-body').load('<?php echo site_url('themaster/billing/send_invoice');?>/'+billing_id+'/no');
		});
	});
</script>