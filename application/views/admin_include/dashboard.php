<?php $this->load->view('member_include/html_head');?>

	<div class="row">
		<div class="col-sm-12">
			<div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h3>Statistik</h3>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="statusbox">
                                <h2>Event</h2>
                                <div class="statusbox-content">
                                    <strong><?php echo $this->event_model->get_event_paginate(0,0,FALSE,TRUE)->num_rows();?> Pending</strong>
                                    <hr />
                                    <strong><?php echo $this->event_model->get_event_paginate(0,0,TRUE)->num_rows();?></strong> Aktif</span><hr />
                                    <span><strong><?php echo $this->event_model->get_event_paginate(0,0)->num_rows();?></strong> Total</span>
                                </div><!-- /.statusbox-content -->

                                <div class="statusbox-actions">
                                    <a href="<?php echo site_url('themaster/event'); ?>"><i class="fa fa-bar-chart"></i></a>
                                    
                                </div><!-- /.statusbox-actions -->
                            </div><!-- /.statusbox -->
                        </div>

                        <div class="col-sm-3">
                            <div class="statusbox">
                                <h2>Member</h2>
                                <div class="statusbox-content">
                                    <strong><?php echo $this->user_role_model->get_user_paginate(0,0)->num_rows();?> Total</strong>
                                    <hr />
                                    <span><strong><?php echo $this->user_role_model->get_user_paginate(0,0,false,false,false,'',2)->num_rows();?></strong> Vendor</span>
                                    <hr />
                                    <span><strong><?php echo $this->user_role_model->get_user_paginate(0,0,false,false,false,'',1)->num_rows();?></strong> Expert</span>
                                </div><!-- /.statusbox-content -->

                                <div class="statusbox-actions">
                                    
                                    <a href="<?php echo site_url('themaster/member'); ?>"><i class="fa fa-bar-chart"></i></a>
                                    
                                </div><!-- /.statusbox-actions -->
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="statusbox">
                                <h2>Venue</h2>
                                <div class="statusbox-content">
                                    
                                </div><!-- /.statusbox-content -->

                                <div class="statusbox-actions">
                                    <a href="<?php echo site_url('blog/my_blog/add_new');?>"><i class="fa fa-pencil"></i></a>
                                    <a href="<?php echo site_url('blog/my_blog');?>"><i class="fa fa-bar-chart"></i></a>
                                    
                                </div><!-- /.statusbox-actions -->
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.col-* -->
            </div>
		</div>
	</div>

<?php $this->load->view('member_include/html_footer');?>