<?php $this->load->view('member_include/html_head');?>
    <div class="row">
        <div class="col-sm-12">
			<ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#pendingtab" class="tab-ajax" role="tab" data-toggle="tab">Pending</a></li>
                <li><a href="#aktiftab" role="tab" class="tab-ajax" data-toggle="tab">Aktif</a></li>
            </ul>
            
            <div class="tab-content">
                <div class="tab-pane fade in active" id="pendingtab">
                    

                </div>
                <div class="tab-pane fade" id="aktiftab">

                </div>
            </div>    
		</div>
    </div>
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#pendingtab').load('<?php echo site_url('themaster/venue/pending_venue');?>');
        $('.tab-ajax').click(function(){
            target=$(this).attr('href');
            ur_target='';
            if(target=="#pendingtab")
                url_target='<?php echo site_url('themaster/venue/pending_venue');?>';
            else if(target=="#aktiftab")
                url_target='<?php echo site_url('themaster/venue/active_venue');?>';
            $(target).html(loader);
            $(target).load(url_target);
        })
    });
</script>