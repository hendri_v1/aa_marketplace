<?php $this->load->view('member_include/html_head');?>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#detilprofiltab" role="tab" data-toggle="tab">Detail Profil</a></li>
                <li><a href="#tentangsayatab" role="tab" data-toggle="tab">Informasi Umum</a></li>
                
                <li><a href="#akuntab" role="tab" data-toggle="tab">Akun</a></li>
            </ul>
            <div class="tab-content">
                <div id="detilprofiltab" class="tab-pane fade in active">
                    <div class="background-white p20">
                        <a href="javascript:void(0);" data-edittype="contact-details" class="btn btn-sm edit-detail pull-right" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                        <h4>Detail Profil</h4>
                        <table class="table">
                            <?php if($this->session->userdata('user_meta')->usertype_id==2): ?>
                                <tr><td><strong><i class="glyphicon glyphicon-home"></i></strong></td><td><?php echo $row->userprofile_companyname;?></td></tr>

                                
                            <?php endif;?>
                            <tr><td><strong><i class="glyphicon glyphicon-user"></i></strong></td><td><?php echo $row->userprofile_firstname;?> <?php echo $row->userprofile_lastname;?> | <?php echo $row->userprofile_position;?></td></tr>
                            
                            <tr>
                            <td><strong><i class="glyphicon glyphicon-ok-sign"></i></strong></td>
                            <td><?php foreach($this->global_model->get_category_by_id($this->session->userdata('user_id'))->result() as $rowscateg): ?>
                                    <?php echo $rowscateg->category_name;?>

                                <?php endforeach;?>
                            </td>
                            </tr>
                            
                            <tr><td><strong><i class="glyphicon glyphicon-envelope"></i></strong></td><td><?php echo $row->user_email;?></td></tr>
                            <tr><td><strong><i class="glyphicon glyphicon-map-marker"></i></strong></td><td><?php echo $row->userprofile_address;?> <?php echo $row->city_name;?>, <?php echo $row->province_name;?></td></tr>
                            <tr><td><strong><i class="glyphicon glyphicon-earphone"></i></strong></td><td><?php echo $row->userprofile_phone;?></td></tr>
                            <tr><td colspan="2">
                                <?php if($row->userprofile_fb<>''): ?>
                                    <img src="<?php echo base_url();?>assets/css/images/fb.png" alt="Facebook Icon" />
                                <?php endif;?>
                                <?php if($row->userprofile_twitter<>''): ?>
                                    <img src="<?php echo base_url();?>assets/css/images/twitter.png" alt="Twitter Icon" />
                                <?php endif;?>
                                <?php if($row->userprofile_linkedin<>''): ?>
                                    <img src="<?php echo base_url();?>assets/css/images/linkedin.png" alt="Linkedin Icon" />
                                <?php endif;?>
                               
                            </td></tr>
                        </table>
                    </div>
                </div>
                <div id="tentangsayatab" class="tab-pane fade">
                    <div class="background-white p20">
                        <a href="#" data-edittype="summary" class="btn btn-sm edit-detail pull-right" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                        <h4 class="title-sub"><i class="glyphicon glyphicon-tasks"></i> Ringkasan</h4>
                        <?php echo $row->userprofile_aboutme_brief;?>
                        <br />
                        
                        <hr />
                        <a href="#" data-edittype="about-me" class="btn btn-sm edit-detail pull-right" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                        <h4 class="title-sub"><i class="glyphicon glyphicon-list-alt"></i> Deskripsi</h4>
                        
                        <?php echo $row->userprofile_aboutme;?>
                        <br />
                        <hr />
                    </div>
                </div>
                
                
                <div id="akuntab" class="tab-pane fade">
                    <div class="background-white p20">
                        <table class="table">
                            <tr><td><strong>Tipe Akun</strong></td><td>: <?php echo $this->global_model->get_usertype($row->usertype_id);?></td></tr>
                            <tr><td><strong>Membership</strong></td><td>: <?php echo $this->global_model->get_membership($row->user_status,$row->is_premium);?></td></tr>
                            <tr><td><strong>Tanggal Register</strong></td><td>: <?php echo mdate('%F %Y',$row->user_register_date);?></td></tr>
                        </table>
                        <?php if($row->is_premium == 0): ?>
                            <a href="<?php echo site_url('themaster/member/upgrade_premium_now/'.$row->user_id);?>" id="premiumUpgrde" class="btn btn-info">Upgrade Now</a>
                            
                        <?php endif ?>
                        <a href="<?php echo site_url('themaster/member/act_like_this/'.$row->user_id);?>" class="btn btn-danger">Login sebagai user ini</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
			
			<hr />
			<table class="table">
				<thead>
					<tr><th>Billing ID</th><th>Date</th><th>Due Date</th><th>Total</th><th>Status</th><th></th></tr>
				</thead>
				<tbody>
					<?php foreach($querybilling->result() as $rowsbilling): ?>
						<tr>
							<td><?php echo $rowsbilling->billing_id;?></td>
							<td><?php echo mdate('%d-%m-%Y',$rowsbilling->billing_date);?></td>
							<td><?php echo mdate('%d-%m-%Y',$rowsbilling->billing_due);?></td>
							<td>
								<?php 
								$querydetbilling=$this->userproject_model->get_detail_billing($rowsbilling->billing_id);
								$total_billing=0;
								foreach($querydetbilling->result() as $rowsdetbilling)
								{
									$total_billing=$total_billing+$rowsdetbilling->billingdetail_total;
								}
								echo number_format($total_billing,0,',','.');
								?>
							</td>
							<td><?php echo $this->global_model->get_billing_status($rowsbilling->billing_status);?></td>
							<td>
								<a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="detail-billing" data-billing_id="<?php echo $rowsbilling->billing_id;?>">Detail</a>
								| <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="payment-billing" data-billing_id="<?php echo $rowsbilling->billing_id;?>">Lunas</a>
                                | <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="send-invoice" data-billing_id="<?php echo $rowsbilling->billing_id;?>">Kirim Invoice</a>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
			
		</div>
    </div>
    <div class="modal fade" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Edit Profile</h4>
          </div>
          <div class="modal-body">
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.detail-billing').click(function(){
            billing_id=$(this).data('billing_id');
            $('.modal-title').html('Billing Detail No-'+billing_id);
            $('.modal-body').html(loader);
            $('.modal-body').load('<?php echo site_url('themaster/project/get_billing_detail');?>/'+billing_id);
        });

        $('.payment-billing').click(function(){
            billing_id=$(this).data('billing_id');
            user_id='<?php echo $row->user_id;?>';
            $('.modal-title').html('Payment Billing No-'+billing_id);
            $('.modal-body').html(loader);
            $('.modal-body').load('<?php echo site_url('themaster/member/payment');?>/'+billing_id+'/'+user_id);
        });
        
        $('.send-invoice').click(function(){
            billing_id=$(this).data('billing_id');
            $('.modal-title').html('Send Invoice No-'+billing_id);
            $('.modal-body').html('Mengirimkan email ...');
            $('.modal-body').load('<?php echo site_url('themaster/Billing/send_invoice');?>/'+billing_id+'/no');
        });
    });
</script>