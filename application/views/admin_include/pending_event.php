<div class="background-white p20">
    <?php $this->load->view('admin_include/event_sort'); ?>
    <table class="table table-striped">
        <thead>
            <tr><th>#</th><th>Title</th><th>Time</th><th>Submitter</th><th></th></tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() as $rows): ?>
                <tr>
                    <td><?php echo $rows->event_id; ?></td>
                    <td><?php echo $rows->event_title; ?></td>
                    <td><?php echo mdate('%d-%m-%Y %H:%i:%s', $rows->event_start_date); ?> <?php echo mdate('%d-%m-%Y', $rows->event_end_date); ?></td>
                    <td><?php
                        if ($rows->usertype_id == 1)
                            echo $rows->userprofile_firstname . ' ' . $rows->userprofile_lastname;
                        else
                            echo $rows->userprofile_companyname;
                        ?>
                    </td>
                    <td><a href="<?php echo site_url('themaster/event/view_event/' . $rows->event_id); ?>" class="btn btn-sm btn-info" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <?php if ($rows->is_featured <> 2): ?>
                            <a href="<?php echo site_url('themaster/event/make_featured/' . $rows->event_id); ?>" class="btn btn-sm btn-warning" title="Featurekan Sekarang"><i class="glyphicon glyphicon-edit"></i></a></td>
                            <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>