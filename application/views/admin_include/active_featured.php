<div class="background-white p20">
    <?php $this->load->view('admin_include/event_sort'); ?>
    <table class="table table-striped">
        <thead>
            <tr><th>#</th><th>Judul</th><th>Status Event</th><th>Batas Featured</th><th></th></tr>
        </thead>
        <tbody>
            <?php foreach ($query->result() as $rows): ?>
                <tr>
                    <td><?php echo $rows->event_id; ?></td>
                    <td><?php echo $rows->event_title; ?></td>
                    <td>
                        <?php
                        if ($rows->is_pending == 0)
                            echo '<span class="label label-warning">Belum Diterbitkan<span>';
                        else
                            echo '<span class="label label-success">Diterbitkan</span>';
                        ?>
                    </td>
                    <td>
                        <?php if ($rows->event_featured_valid == 0): ?>
                            Tanpa Batas Waktu
                        <?php else: ?>
                            <?php if ($rows->event_featured_valid < time()): ?>
                                Sudah Lewat
                            <?php else: ?>
                                <?php echo mdate('%d %M %Y', $rows->event_featured_valid); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="<?php echo site_url('themaster/event/view_event/' . $rows->event_id); ?>" class="btn btn-sm btn-info" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <?php if ($rows->event_featured_valid < time()): ?>
                            <a href="<?php echo site_url('themaster/event/make_featured/'.$rows->event_id);?>" class="btn btn-sm btn-warning" title="Perpanjang Sekarang"><i class="glyphicon glyphicon-edit"></i></a>
                        <?php else: ?>
                            <a href="<?php echo site_url('themaster/event/stop_featured/' . $rows->event_id); ?>" class="btn btn-warning btn-sm" title="Hentikan Featured"><i class="glyphicon glyphicon-remove"></i></a>
                        <?php endif;?>
                    </td>

                </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</div>