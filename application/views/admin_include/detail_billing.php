<table class="table">
	<thead>
		<tr><th>Keterangan</th><th><div align="right">Total</div></th></tr>
	</thead>
	<tbody>
		<?php foreach($querydetbilling->result() as $rowsdet): ?>
			<tr>
				<td><?php echo $rowsdet->billingdetail_info;?></td>
				<td><div align="right"><?php echo number_format($rowsdet->billingdetail_total);?></div></td>
			</tr>
		<?php endforeach;?>
	</tbody>
</table>