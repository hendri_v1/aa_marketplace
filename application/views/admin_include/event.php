<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
	        <ul class="nav nav-tabs" role="tablist">
	            <li class="active"><a href="#pendingtab" class="tab-ajax" role="tab" data-toggle="tab">Dalam Moderasi</a></li>
	            <li><a href="#aktiftab" role="tab" class="tab-ajax" data-toggle="tab">Aktif</a></li>
	            <li><a href="#pendingfeatured" role="tab" class="tab-ajax" data-toggle="tab">Pengajuan Featured</a></li>
	            <li><a href="#aktiffeatured" role="tab" class="tab-ajax" data-toggle="tab">Featured Aktif</a></li>
	        </ul>
	        <div class="tab-content">
	            <div class="tab-pane fade in active" id="pendingtab"></div>
	            <div class="tab-pane fade" id="aktiftab"></div>
	            <div class="tab-pane fade" id="pendingfeatured"></div>
	            <div class="tab-pane fade" id="aktiffeatured"></div>
	        </div>    
	    </div>
	</div>

<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pendingtab').load('<?php echo site_url('themaster/event/pending_event'); ?>');
        $('.tab-ajax').click(function () {
            target = $(this).attr('href');
            ur_target = '';
            if (target == "#pendingtab") {
                url_target = '<?php echo site_url('themaster/event/pending_event'); ?>';
            } else if (target == "#pendingfeatured") {
                url_target = '<?php echo site_url('themaster/event/pending_featured'); ?>';
            } else if (target == "#aktiftab") {
                url_target = '<?php echo site_url('themaster/event/active_event'); ?>';
            } else if (target == "#aktiffeatured") {
                url_target = '<?php echo site_url('themaster/event/active_featured'); ?>';
            }
            $(target).html(loader);
            $(target).load(url_target);
        });

        $('.tab-content').on('click', '.pagination>li>a', function (e) {
            e.preventDefault();
            parent1 = $(this).parent();
            parent2 = $(parent1).parent();
            parent3 = $(parent2).parent();
            parent4 = $(parent3).parent();
            target = $(this).attr('href');
            $(parent4).append(loader);
            $(parent4).load(target);

        });
    });
</script>