<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<a href="javascript:void(0);" id="preview_campaign" data-toggle="modal" data-target="#myModal" class="btn btn-info">Preview</a>
			<hr />
			<form action="<?php echo site_url('themaster/campaign/send_campaign');?>" method="post" id="form_campaign">
			<div class="background-white p20">
				<div class="row">
					<div class="col-sm-6">
						<?php
							$recipents=array(1=>'Semua Member',2=>'Masukkan Manual',3=>'Semua member dan Manual');
							echo bt_select('recipents','Pilih Penerima',$recipents);
						?>
						<?php echo bt_text('additional_recipents','Email Manual','pisahkan email dengan ; (titik koma)');?>
						<?php echo bt_text('campaign_title','Judul Campaign');?>
					</div>
					<div class="col-sm-6">
						<?php
							$featured_event=array();
							foreach($all_active_event->result() as $rowsae)
							{
								$featured_event[$rowsae->event_id]=$rowsae->event_title;
							}
							echo bt_select('event_id','Pilih Event Untuk Dimasukkan',$featured_event);
						?>
						<?php
							$featured_article=array();
							foreach($all_blog->result() as $rowsb)
							{
								$featured_article[$rowsb->post_id]=$rowsb->post_title;
							}									
							echo bt_select('post_id','Pilih Postingan untuk dimasukkan',$featured_article);
						?>
					</div>
					<div class="col-sm-12">
						<?php echo bt_textarea('campaign_word','Campaign Promo Script');?>
					</div>
					<div class="col-sm-12">
						<input type="submit" value="Kirim Sekarang" class="btn btn-primary">
					</div>
				</div>

			</div>
			</form>
		</div>
	</div>
	<div class="modal fade" id="myModal">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title">Preview Campaign</h4>
	      </div>
	      <div class="modal-body">
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var config = {
            toolbar:
            [
                ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Undo', 'Redo', '-', 'SelectAll'],
                ['UIColor']
            ]
        };
        $('#campaign_word').ckeditor(config);
        $('#preview_campaign').click(function(){
        	$('.modal-body').html(loader);
        	var datastring = $("#form_campaign").serialize();
        	
        	$.post('<?php echo site_url('themaster/campaign/preview_campaign');?>',
        		datastring,
	        	function(data)
	        	{
	        		$('.modal-body').html(data);
	        	}
	        );
        })
	})
</script>