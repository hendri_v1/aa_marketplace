
<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>User Type</th>
            <th>Expired At</th>
            <th>Company Name</th><th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($queryuser->result() as $rowsuser): ?>
            <tr>
                <td><?php echo $rowsuser->user_id; ?></td>
                <td><?php echo $rowsuser->userprofile_firstname; ?> <?php echo $rowsuser->userprofile_lastname; ?></td>
                <td><?php
                    if ($rowsuser->usertype_id == 99)
                        echo "Administrator";
                    else
                        echo $this->global_model->get_usertype($rowsuser->usertype_id);
                    ?></td>
                <td><?php echo (!empty($waktu->premium_due)) ? date('d M Y', strtotime(premium_due)) : '-'; ?></td>
                <td><?php echo $rowsuser->userprofile_companyname; ?></td>
                <td>
                    <a href="<?php echo site_url('themaster/member/view_user/' . $rowsuser->user_id); ?>" class="btn btn-info btn-sm">Detail</a>
                    <?php if ($rowsuser->is_patnership == 0): ?>
                        <a href="<?php echo site_url('themaster/member/upgrade_patnership/' . $rowsuser->user_id); ?>" class="btn btn-warning btn-sm">Upgrade Membership</a>
                    <?php endif ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>