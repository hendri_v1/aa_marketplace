<div class="background-white p20">
	<table class="table table-striped">
		<thead>
			<tr><th>#</th><th>Judul</th><th>Status Event</th><th>Submitter</th><th></th></tr>
		</thead>
		<tbody>
			<?php foreach($query->result() as $rows): ?>
				<tr>
					<td><?php echo $rows->event_id;?></td>
					<td><?php echo $rows->event_title;?></td>
					<td>
						<?php if($rows->is_pending==0)
							echo '<span class="label label-warning">Belum Diterbitkan<span>';
						else
							echo '<span class="label label-success">Diterbitkan</span>';
						?>
					</td>
					<td><?php if($rows->usertype_id==1)
							echo $rows->userprofile_firstname.' '.$rows->userprofile_lastname;
						else
							echo $rows->userprofile_companyname;
						?>
					</td>
					<td>
						<a href="<?php echo site_url('themaster/event/view_event/'.$rows->event_id);?>" class="btn btn-sm btn-info" title="Lihat Detail"><i class="glyphicon glyphicon-eye-open"></i></a>
						<a href="<?php echo site_url('themaster/event/make_featured/'.$rows->event_id);?>" class="btn btn-sm btn-warning" title="Featurekan Sekarang"><i class="glyphicon glyphicon-edit"></i></a>
					</td>

				</tr>
			<?php endforeach;?>
		</tbody>
	</table>
</div>