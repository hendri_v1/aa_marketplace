<?php ?>
<div class="row">
    <div class="col-md-2 col-xs-12 col-md-offset-10">
        <form action="">
            <?php
            $options = array(
                '' => 'Sort by :',
                'start_date_asc' => 'Start Date (ASC)',
                'start_date_desc' => 'Start Date (DESC)',
                'title_asc' => 'Title (ASC)',
                'title_desc' => 'Title (DESC)',
                'event_organizer' => 'Event Organizer'
            );
            $selected = array($this->input->get('order_by'));
            echo form_dropdown('eventorder', $options, $selected, " class='form-control' id='eventorder' onchange='choose_order(this.value)'");
            ?>
        </form>
    </div>
</div>
<?php ?>
<script>
    function choose_order(val) {
        var selector = $('.tab-pane.active.in').attr('id');
        var method = '';
        switch (selector) {
            case 'pendingtab':
                method = 'pending_event';
                break;
            case 'pendingfeatured':
                method = 'pending_featured';
                break;
            case 'aktiftab':
                method = 'active_event';
                break;
            case 'aktiffeatured':
                method = 'active_featured';
                break;
            default :
                break;
        }
        if (method != '') {
            $('#' + selector).html(loader);
            $('#' + selector).load('<?php echo site_url('themaster/event/'); ?>/' + method + '?order_by=' + val);
        }
    }
</script>