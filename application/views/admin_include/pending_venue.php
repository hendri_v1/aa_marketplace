<div class="background-white p20">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>#ID</th>
				<th>Nama Venue</th>
				<th>User</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($queryvenue->result() as $rowsvenue): ?>
				<tr>
					<td>
						<?php echo $rowsvenue->venue_main_id;?>
					</td>
					<td>
						<?php echo $rowsvenue->venue_main_name;?>
					</td>
					<td>
						<?php echo $rowsvenue->userprofile_companyname;?>
					</td>
					<td><a href="<?php echo site_url('themaster/venue/activate/'.$rowsvenue->venue_main_id);?>" class="btn btn-primary">Aktifkan</a></td>
				</tr>
				<?php endforeach;?>
		</tbody>
	</table>
</div>