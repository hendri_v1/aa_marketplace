<div class="block background-white background-transparent-image fullwidth">
    <div class="page-header">
        <h1>Featured Vendors</h1>
        
    </div><!-- /.page-header -->

    <div class="cards-simple-wrapper">
        <div class="row">
            <?php foreach($queryvendor->result() as $rowsvendor): ?>
            <div class="col-sm-6 col-md-4">
                
                <div class="card-simple" data-background-image="<?php echo upload_path;?>uploads/user_images/<?php echo $this->global_model->get_user_photo($rowsvendor->userprofile_photo);?>">
                    <div class="card-simple-background">
                        <div class="card-simple-content">
                            <h2><a href="<?php echo site_url('vendor/view/'.$rowsvendor->user_id.'/'.url_title(strtolower($rowsvendor->userprofile_companyname)));?>"><?php echo $rowsvendor->userprofile_companyname;?></a></h2>
                            <div class="card-simple-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div><!-- /.card-rating -->

                            <div class="card-simple-actions">
                                
                                <a href="<?php echo site_url('vendor/view/'.$rowsvendor->user_id.'/'.url_title(strtolower($rowsvendor->userprofile_companyname)));?>" class="fa fa-search"></a>
                                
                            </div><!-- /.card-simple-actions -->
                        </div><!-- /.card-simple-content -->

                        <div class="card-simple-label">
                            <?php echo $rowsvendor->city_name;?>
                        </div>
                        <div class="card-simple-price">
                            <?php $qcateg=$this->global_model->get_category_by_id($rowsvendor->user_id);
                            foreach ($qcateg->result() as $rowscateg): ?>
                                <?php echo $rowscateg->category_name;?>
                            <?php endforeach;?>
                        </div>
                        
                    </div><!-- /.card-simple-background -->
                </div><!-- /.card-simple -->
               
            </div><!-- /.col-* -->
             <?php endforeach;?>
            

            
        </div><!-- /.row -->
    </div><!-- /.cards-simple-wrapper -->
</div>