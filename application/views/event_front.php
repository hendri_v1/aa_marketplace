<div class="posts">
    <?php $i=0; foreach($queryfeaturedevents->result() as $rowsevents): $i++; ?>
        <?php if($i==1): ?>
            <div class="card" data-background-image="<?php echo upload_path;?>uploads/event_images/<?php echo $rowsevents->event_picture;?>">
                <div class="card-background">
                    <div class="card-content">
                        <h2><a href="<?php echo site_url('event/view/' . $rowsevents->event_id . '/' . url_title(strtolower(word_limiter($rowsevents->event_title,10)))); ?>"><?php echo $rowsevents->userprofile_companyname;?></a></h2>
                        <div class="card-meta">
                           <?php $sameday=$rowsevents->event_end_date-$rowsevents->event_start_date;?>
                            <?php if($sameday>86400): ?>
                                <i class="fa fa-clock-o"></i> <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_start_date);?> - <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_end_date);?> 
                            <?php else: ?>
                                <i class="fa fa-clock-o"></i> <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_start_date);?> - <?php echo mdate('%H:%i:%s',$rowsevents->event_end_date);?> 
                            <?php endif;?>
                        </div><!-- /.card-meta -->
                        <div class="card-rating">
                            <?php echo $rowsevents->event_title; ?>
                        </div><!-- /.card-rating -->

                        <div class="card-actions">
                            
                            <a href="<?php echo site_url('event/view/' . $rowsevents->event_id . '/' . url_title(strtolower(word_limiter($rowsevents->event_title,10)))); ?>" class="fa fa-search"></a>
                            
                        </div><!-- /.card-simple-actions -->
                    </div><!-- /.card-simple-content -->

                    <div class="card-label">
                        <?php echo $rowsevents->city_name;?>
                    </div>
                    <div class="card-price">
                       <?php echo $rowsevents->eventcategory_name;?>
                    </div>
                    
                </div><!-- /.card-simple-background -->
            </div><!-- /.card-simple -->
        <?php else: ?>
            <div class="post">
                <div class="post-image">
                    <?php $mini_image='';
                        
                            $mini_image=upload_path.'uploads/user_images/'.$this->global_model->get_user_photo($rowsevents->userprofile_photo);
                    ?>
                    <img src="<?php echo $mini_image; ?>" alt="">
                    <a class="read-more" href="<?php echo site_url('event/view/' . $rowsevents->event_id . '/' . url_title(strtolower(word_limiter($rowsevents->event_title,10)))); ?>">View</a>
                </div><!-- /.post-image -->

                <div class="post-content">
                    <div class="post-label"><?php echo $rowsevents->eventcategory_name;?></div><!-- /.post-label -->
                    <div class="post-date">
                        <?php $sameday=$rowsevents->event_end_date-$rowsevents->event_start_date;?>
                        <?php if($sameday>86400): ?>
                            <i class="fa fa-clock-o"></i> <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_start_date);?> - <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_end_date);?> 
                        <?php else: ?>
                            <i class="fa fa-clock-o"></i> <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_start_date);?> - <?php echo mdate('%H:%i:%s',$rowsevents->event_end_date);?> 
                        <?php endif;?>
                    </div><!-- /.post-date -->
                    <h2><?php echo $rowsevents->event_title; ?></h2>
                    <p><?php echo word_limiter(strip_tags($rowsevents->event_description),20);?></p>
                </div><!-- /.post-content -->
            </div><!-- /.post -->
        <?php endif;?>
    <?php endforeach;?>
</div>