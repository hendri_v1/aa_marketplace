<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner">
			<div class="content">
				<div class="mt-80 mb80">
					<div class="detail-banner" style="background-image:url(<?php echo base_url();?>assets/img/bg_transparent.jpeg">
						<div class="container">
							<div class="detail-banner-left">
								<div class="detail-banner-info">
									<div class="detail-label">
										<?php $qcateg=$this->global_model->get_category_by_id($row->user_id);
				                        foreach ($qcateg->result() as $rowscateg): ?>
				                            <?php echo $rowscateg->category_name;?> 
				                        <?php endforeach;?>
									</div>
									<div class="detail-verified">
										<?php echo $this->global_model->get_membership($row->user_status,$row->is_premium);?>
									</div>
									<h2 class="detail-title"><?php echo $row->userprofile_companyname;?></h2>
									<div class="detail-banner-address">
										<i class="fa fa-map-o"></i> <?php echo $row->userprofile_address;?> <?php echo $row->city_name;?>, <?php echo $row->province_name;?>
									</div>
								</div>		
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row detail-content">
					<div class="col-sm-7">
						<div class="background-white p20">
							<?php echo $row->userprofile_aboutme;?>
						</div>
						<div class="post-detail">
							<div class="post-meta-tags p0 m0">
								<ul>
									<?php $qkeyword=$this->global_model->get_keywords(1,$row->user_id);
					                foreach($qkeyword->result() as $rowskw): ?>
					                	<li class="tag"><a href="#"><?php echo $rowskw->keyword_text;?></a></li>

					                <?php endforeach;?>
					            </ul> 
							</div>
						</div>
						<?php if($my_event->num_rows()>0): ?>
							<h2>Event Aktif</h2>
							<div class="background-white p20 reasons">
								<?php foreach($my_event->result() as $rowsevents): ?>
									<div class="reason">
						                <div class="reason-icon">
						                    <?php echo mdate('%d %M',$rowsevents->event_start_date);?> 
						                </div><!-- /.reason-icon -->
						                <div class="reason-content">
						                    <p><a href="<?php echo site_url('event/view/' . $rowsevents->event_id . '/' . url_title($rowsevents->event_title)); ?>"><?php echo $rowsevents->event_title;?></a></p>
						                    
						                </div><!-- /.reason-content -->
						            </div>
						        <?php endforeach;?>
								
							</div>
						<?php endif;?>
						
					</div>
					<div class="col-sm-5">
						<div class="background-white p20">
							<div class="detail-vcard">
								<div class="detail-logo">
									<img src="<?php echo upload_path; ?>uploads/user_images/<?php echo $this->global_model->get_user_photo($row->userprofile_photo); ?>" />
								</div>
								<div class="detail-contact">
									<div class="detail-contact-email">
				                        <i class="fa fa-envelope-o"></i> <a href="mailto:#"><?php echo $row->user_email;?></a>
				                    </div>
				                    <div class="detail-contact-phone">
				                        <i class="fa fa-mobile-phone"></i> <a href="tel:#"><?php echo $row->userprofile_phone;?></a>
				                    </div>
				                    
				                    <div class="detail-contact-address">
				                        <i class="fa fa-map-o"></i>
				                        <?php echo $row->userprofile_address;?><br>
				                    </div>
								</div>
							</div>
							
							<div class="detail-follow">
								<h5>Follow Us: </h5>
								<div class="follow-wrapper">
									<?php if($row->userprofile_fb<>''): ?>
				                        <a href="<?php echo $row->userprofile_fb ?>" class="follow-btn facebook"><i class="fa fa-facebook"></i></a>
				                    <?php endif;?>
				                    <?php if($row->userprofile_twitter<>''): ?>
				                        <a href="<?php echo $row->userprofile_twitter ?>" class="follow-btn twitter"><i class="fa fa-twitter"></i></a>
				                    <?php endif;?>
				                    <?php if($row->userprofile_linkedin<>''): ?>
				                        <a href="<?php echo $row->userprofile_linkedin ?>" class="follow-btn linkedin"><i class="fa fa-linkedin"></i></a>
				                    <?php endif;?>
								</div>
							</div>
							<div class="detail-actions">
								<hr />
								<div class="btn btn-secondary btn-share"><i class="fa fa-share-square-o"></i> Share
			                        <div class="share-wrapper">
			                            <ul class="share">
			                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(uri_string()); ?>"><i class="fa fa-facebook"></i> Facebook</a></li>
			                                <li><a href="http://twitter.com/home?status=<?php echo base_url(uri_string()); ?>"><i class="fa fa-twitter"></i> Twitter</a></li>
			                                <li><a href="https://plus.google.com/share?url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-google-plus"></i> Google+</a></li>
			                                
			                                <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-linkedin"></i> Linkedin</a></li>
			                                
			                            </ul>
			                        </div>
			                    </div>
			                </div>

						</div>
						<h2>Studi Kasus</h2>
						<div class="background-white p20">
							<?php $totalpf=$case->num_rows();?>
	                        <?php if($totalpf==0): ?>
	                            Belum ada Case Study ditambahkan
	                        <?php else: ?>
	                            <ul>
	                            <?php foreach ($case->result_array() as $listCase): ?>
	                                <li>
	                                    <span class="small"><?php echo mdate('%F %Y',$listCase['userextra_when']);?></span><br />
	                                    <span class="text-warning"><?php echo $listCase['userextra_title']; ?></span><br />
	                                    <?php echo strip_tags($listCase['userextra_description']); ?>
	                                    <?php if ($listCase['userextra_attachment']): ?><br />
	                                        <a href="<?php echo site_url('profile/my_profile/download').'/'.$listCase['userextra_attachment'].'/'.url_title($listCase['userextra_title']).'_attachment' ?>">Attachment</a>
	                                    <?php endif ?>
	                                
	                                
	                            <?php endforeach ?>
	                            </li>
	                        <?php endif;?>
						</div>
						<h2>Sertifikasi</h2>
						<div class="background-white p20">
							<?php $totalpf=$certificate->num_rows();?>
                    <?php if($totalpf==0): ?>
                        Belum ada sertifikasi yang ditambahkan
                    <?php else: ?>
                        <ul>
                        <?php foreach ($case->result_array() as $listCase): ?>
                            <li>
                                <span class="small"><?php echo mdate('%F %Y',$listCase['userextra_when']);?></span><br />
                                <span class="text-warning"><?php echo $listCase['userextra_title']; ?></span><br />
                                <?php echo strip_tags($listCase['userextra_description']); ?>
                                <br />
                                <?php if ($listCase['userextra_attachment']): ?><br />
                                    <a href="<?php echo site_url('profile/my_profile/download').'/'.$listCase['userextra_attachment'].'/'.url_title($listCase['userextra_title']).'_attachment' ?>">Attachment</a>
                                <?php endif ?>
                            </li>
                            
                            
                        <?php endforeach ?>
                        </ul>
                    <?php endif;?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('html_footer');?>