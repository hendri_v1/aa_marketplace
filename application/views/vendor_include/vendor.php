<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner">
			<div class="container">
				<div class="content">
					<div class="document-title" style="background-image:url('<?php echo base_url();?>assets/img/vendor.jpg');background-size:cover;">
						<h1>Direktori Vendor</h1>

						<ul class="breadcrumb">
							<li><a href="<?php echo base_url();?>">HRplasa</a></li>
							<li><a href="#">Direktori Vendor</a></li>
						</ul>
					</div>
					<form class="filter" method="get">
					    <div class="row">
					        <div class="col-sm-12 col-md-4">
					            <div class="form-group">
					                <?php echo bt_group_text('vendor_keyword','Keyword',$this->input->get('vendor_keyword'),'fa fa-key');?>
					            </div><!-- /.form-group -->
					        </div><!-- /.col-* -->

					        <div class="col-sm-12 col-md-4">
					            <div class="form-group">
					                <?php
				    					$array_province=array();
				    					$array_province[0]="Semua Provinsi";
				    					foreach($province as $rowsprovince)
				    					{
				    						$array_province[$rowsprovince->province_id]=$rowsprovince->province_name;
				    					}
				    					echo bt_group_select('province_id',$array_province,$this->input->get('province_id'),'fa fa-bullseye');

				    				?>
		    					</div><!-- /.form-group -->
					        </div><!-- /.col-* -->

					        <div class="col-sm-12 col-md-4">
					            <div class="form-group">
					                <?php 
										$array_category=array();
										$array_category[0]="Semua Kategori";
										foreach($querycvendor->result() as $rowscuser)
										{
											$array_category[$rowscuser->category_id]=$rowscuser->category_name;
										}
										echo bt_group_select('category_id',$array_category,'','fa fa-tags');
									?>
					            </div><!-- /.form-group -->
					        </div><!-- /.col-* -->
					    </div><!-- /.row -->

					    <hr>

					    <div class="row">
					        <div class="col-sm-8">
					            <div class="filter-actions">
					                <a href="#"><i class="fa fa-close"></i> Reset Filter</a>
					            
					            </div><!-- /.filter-actions -->
					        </div><!-- /.col-* -->

					        <div class="col-sm-4">
					            <button type="submit" class="btn btn-primary">Filter</button>
					        </div><!-- /.col-* -->
					    </div><!-- /.row -->
					</form>
					<?php foreach($querylist->result() as $rowsusers): ?>
						<div class="col-sm-4">
							<div class="card" data-background-image="<?php echo upload_path;?>uploads/user_images/<?php echo $this->global_model->get_user_photo($rowsusers->userprofile_photo);?>">
		                        <div class="card-label">
		                            <a href="<?php echo site_url('vendor/view/'.$rowsusers->user_id.'/'.url_title(strtolower($rowsusers->userprofile_companyname)));?>">
		                            	<?php $qcateg=$this->global_model->get_category_by_id($rowsusers->user_id);
	                                    foreach ($qcateg->result() as $rowscateg): ?>
	                                        <?php echo $rowscateg->category_name;?>
	                                    <?php endforeach;?>
		                            </a>
		                        </div><!-- /.card-label -->

		                        <div class="card-content">
		                            <h2><a href="<?php echo site_url('vendor/view/'.$rowsusers->user_id.'/'.url_title(strtolower($rowsusers->userprofile_companyname)));?>">
		                            	<?php if($rowsusers->usertype_id==1): ?>
                                            <?php echo $rowsusers->userprofile_firstname;?> <?php echo $rowsusers->userprofile_lastname;?>
                                        <?php else: ?>
                                            <?php echo $rowsusers->userprofile_companyname;?>
                                        <?php endif;?>
		                            </a></h2>

		                            <div class="card-meta">
		                                <i class="fa fa-map-o"></i> <?php echo $rowsusers->userprofile_address;?> <?php echo $rowsusers->city_name;?>, <?php echo $rowsusers->province_name;?>
		                            </div><!-- /.card-meta -->
									<?php /*
		                            <div class="card-rating">
		                                <i class="fa fa-star"></i>
		                                <i class="fa fa-star"></i>
		                                <i class="fa fa-star"></i>
		                                <i class="fa fa-star"></i>
		                                <i class="fa fa-star"></i>
		                            </div><!-- /.card-rating -->
									*/ ?>

		                            <div class="card-actions">
		                                
		                                <a href="<?php echo site_url('vendor/view/'.$rowsusers->user_id.'/'.url_title(strtolower($rowsusers->userprofile_companyname)));?>" class="fa fa-search"></a>
		                                
		                            </div>
		                        </div><!-- /.card-content -->
		                    </div>
						</div>
					<?php endforeach;?>
					<div class="col-sm-12">
						<?php echo $this->pagination->create_links(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('html_footer');?>