<?php $this->load->view('member_include/html_head');?>
	<div class="col-sm-12">
        <div class="bacgkround-white p20">
            <form action="<?php echo site_url('blog/my_blog/update_post/')?>" method="POST">
                <?php echo bt_text('title','Judul','Masukkan Judul',$row->post_title);?>
                <?php echo bt_select('post_category_id','Kategori',$cate,$row->post_type_id); ?>
                <textarea name="content" id="contentPost" placeholder="Content"><?php echo $row->post_content;?></textarea>
                <br />
                <input type="hidden" name="post_id" value="<?php echo $row->post_id;?>">
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(e) {
        $('#contentPost').ckeditor();
    });
</script>