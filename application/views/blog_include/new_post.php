<?php $this->load->view('member_include/html_head');?>
	<div class="col-sm-12">
        <div class="bacgkround-white p20">
            <form action="<?php echo site_url('blog/my_blog/save_post/')?>" method="POST">
                <?php echo bt_text('title','Title','Masukkan Judul');?>
                <?php echo bt_select('post_category_id','Select your post category',$cate,''); ?>
                <textarea name="content" id="contentPost" placeholder="Content"></textarea><br />
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(e) {
        $('#contentPost').ckeditor();
    });
</script>