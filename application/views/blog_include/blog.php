<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-lg-9">
                        <div class="content">
                            <div class="page-title">
							    <h1>Tulisan di HRPlasa</h1>
							</div><!-- /.page-title -->

							<div class="posts">
								<?php $i=0; foreach($query->result() as $rows): $i++; ?>
									<?php
										$user_link='';
									if($rows->usertype_id==1)
										$user_link=site_url('expert/view/'.$rows->user_id.'/'.url_title($rows->userprofile_firstname));
									elseif($rows->usertype_id==2)
										$user_link=site_url('vendor/view/'.$rows->user_id.'/'.url_title($rows->userprofile_companyname));
									?>
									<div class="post">
	                                    <div class="post-image">
	                                        <img src="<?php echo upload_path?>uploads/user_images/<?php echo $this->global_model->get_user_photo($rows->userprofile_photo);?>" alt="<?php echo ucfirst(strtolower($rows->post_title));?>">
	                                        <a class="read-more" href="<?php echo site_url('blog/read/'.$rows->post_id.'/'.url_title($rows->post_title)); ?>">View</a>
	                                    </div><!-- /.post-image -->

	                                    <div class="post-content">
	                                        <h2><a href="<?php echo site_url('blog/read/'.$rows->post_id.'/'.url_title($rows->post_title)) ?>"><?php echo ucfirst(strtolower($rows->post_title));?></a></h2>
	                                        <p><?php echo word_limiter(strip_tags($rows->post_content),90); ?></p>
	                                    </div><!-- /.post-content -->

	                                    <div class="post-meta clearfix">
	                                        <div class="post-meta-author">Oleh <a href="<?php echo $user_link;?>"></i> <?php echo $rows->userprofile_firstname.' '.$rows->userprofile_lastname;?></a></div><!-- /.post-meta-author -->
	                                        <div class="post-meta-date"><?php echo date('d F Y',$rows->post_addedat);?></div><!-- /.post-meta-date -->
	                                        <div class="post-meta-categories"><i class="fa fa-tags"></i> <a href="#"><?php echo $rows->category_name;?></a></div><!-- /.post-meta-categories -->
	                         
	                                        <div class="post-meta-more"><a href="<?php echo site_url('blog/read/'.$rows->post_id.'/'.url_title($rows->post_title)) ?>">Read More <i class="fa fa-chevron-right"></i></a></div><!-- /.post-meta-more -->
	                                    </div><!-- /.post-meta -->
	                                </div><!-- /.post -->
								<?php endforeach;?>
								<?php echo $this->pagination->create_links(); ?>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-lg-3">
						<div class="sidebar">

							<div class="widget">
								<h2 class="widgettitle">PRNewsWire</h2>
								<script type="text/javascript" src="http://widget.prnewswire.com/widget/widget.js?dir=36,37&l=en&preset=2&expandheight=1&width=auto&height=auto&showlogos=1&industries=OFP,OUT,WRK,EDU,HED,HSC,HMS&regions=Southeast%20Asia,Brunei,Laos,Indonesia,Malaysia,Myanmar,Philippines,Singapore,Thailand,East%20Timor,Cambodia,Vietnam&subjects=SBS,TDS,ANW,RCY,CSR,POL,ECO,EGV,FDA,FOR,LBR,LAW,LEG,NPT,PLW,CPN,PSF,TRD,EXE,STP&andorquestion=OR&widgettitle=Berita dari PR Newswire&numresults=5&numwords=10&hlineorbrief=both&bold=1&bkgcolor=FFFFFF&rowcolor1=FFFFFF&rowcolor2=FFFFFF&bordercolor=CFCFCF&link=26291D&visitedcolor=FFFFFF&textcolor=363636&datecolor=888888&fontface=Arial&fontsize=10&bordersize=1&borderstyle=dotted&showRelease=1&getwidget=0&"></script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('html_footer');?>