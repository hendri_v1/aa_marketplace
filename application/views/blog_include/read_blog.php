<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-9">
						<div class="content">
							<div class="page-title">
								<h1><?php echo $row->post_title;?></h1>
							</div>
							<div class="posts post-detail">
								<?php if($featured_image<>''): ?>
							    	<img src="<?php echo $featured_image;?>" alt="<?php echo $row->post_title;?>">
								<?php endif;?>
								<?php
									$user_link='';
									if($row->usertype_id==1)
										$user_link=site_url('expert/view/'.$row->user_id.'/'.url_title($row->userprofile_firstname));
									elseif($row->usertype_id==2)
										$user_link=site_url('vendor/view/'.$row->user_id.'/'.url_title($row->userprofile_companyname));
								?>
							    <div class="post-meta clearfix">
							        <div class="post-meta-author">Oleh <a href="<?php echo $user_link;?>"><?php echo $row->userprofile_firstname.' '.$row->userprofile_lastname;?></a></div><!-- /.post-meta-author -->
							        <div class="post-meta-date"><?php echo date('d F Y',$row->post_addedat);?></div><!-- /.post-meta-date -->
							        <div class="post-meta-categories"><i class="fa fa-tags"></i> <a href="blog-detail.html"><?php echo $row->category_name;?></a></div><!-- /.post-meta-categories -->
							        
							    </div><!-- /.post-meta -->

							    <div class="post-content">
							        <?php echo $row->post_content;?>
							    </div><!-- /.post-content -->

							    

							    <div id="disqus_thread"></div>
				                <script>
				                    /**
				                     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
				                     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
				                     */
				                    /*
				                    var disqus_config = function () {
				                        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
				                        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
				                    };
				                    */
				                    (function() {  // DON'T EDIT BELOW THIS LINE
				                        var d = document, s = d.createElement('script');
				                        
				                        s.src = '//hrplasa.disqus.com/embed.js';
				                        
				                        s.setAttribute('data-timestamp', +new Date());
				                        (d.head || d.body).appendChild(s);
				                    })();
				                </script>
				                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

							    

							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-3">
						<div class="background-white p20">
							<img src="<?php echo upload_path?>uploads/user_images/<?php echo $this->global_model->get_user_photo($row->userprofile_photo);?>" class="img-thumbnail">
							<hr />
							<h4><a href="<?php echo $user_link;?>"><?php echo $row->userprofile_firstname.' '.$row->userprofile_lastname;?></a></h4>
						</div>
						<div class="detail-content text-right">
							<div class="btn btn-secondary btn-share"><i class="fa fa-share-square-o"></i> Share
		                        <div class="share-wrapper">
		                            <ul class="share">
		                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(uri_string()); ?>"><i class="fa fa-facebook"></i> Facebook</a></li>
		                                <li><a href="http://twitter.com/home?status=<?php echo base_url(uri_string()); ?>"><i class="fa fa-twitter"></i> Twitter</a></li>
		                                <li><a href="https://plus.google.com/share?url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-google-plus"></i> Google+</a></li>
		                                
		                                <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-linkedin"></i> Linkedin</a></li>
		                                
		                            </ul>
		                        </div>
		                    </div>
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('html_footer');?>
<script id="dsq-count-scr" src="//hrplasa.disqus.com/count.js" async></script>