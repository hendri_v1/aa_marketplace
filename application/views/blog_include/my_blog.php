<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
        	<a href="<?php echo site_url('blog/my_blog/add_new') ?>" class="btn btn-primary"><i class="glyphicon glyphicon-bullhorn"></i> Post Baru</a>
        	<hr />
			<div class="background-white p20 mb30">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Post Title</th>
							<th>Post Category</th>
							<th>Created</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; foreach($query->result() as $rows): ?>
							<tr>
								<td><?php echo $rows->post_id;?></td>
								<td><a href="<?php echo site_url('blog/read/'.$rows->post_id.'/'.url_title($rows->post_title)) ?>"><?php echo $rows->post_title;?></a></td>
								<td><?php echo $rows->category_name;?></td>
								<td><?php echo date('d F Y',$rows->post_addedat);?></td>
								<td><a href="<?php echo site_url('blog/my_blog/delete_post/'.$rows->post_id);?>" onclick="return confirm('Apakah Anda Yakin? Tindakan ini tidak dapat dibatalkan')" class="btn btn-primary btn-sm">Hapus</a> <a href="<?php echo site_url('blog/my_blog/edit_post/'.$rows->post_id);?>" class="btn btn-danger btn-sm">Edit</a></td>
							</tr>
						<?php $i++; endforeach;?>
					</tbody>
				</table>
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>

<?php $this->load->view('member_include/html_footer');?>