<?php $this->load->view('member_include/html_head');?>
	
	<div class="row">
		<div class="col-sm-3 col-lg-2">
			<div class="sidebar">
				<div class="widget">

					<div class="user-photo">
						
						<?php if ($row->userprofile_photo != ''): 
	                        $profile_images=base_url('uploads/user_images').'/'.$row->userprofile_photo;
	                    else:
	                        $profile_images=base_url().'uploads/user_images/profile-pic.jpg';
	                    endif ?>
				        <a href="#">
				            <img src="<?php echo $profile_images;?>" alt="User Photo">
				            <span class="user-photo-action">Click here to reupload</span>
				        </a>
				    </div>
				    <?php echo form_open_multipart('ajax/image',array('id' => 'myform'));?>
	                    <input type="file" name="img_profile" id="profile_img">
	                    <div id="error_alert"></div>
	                <?php echo form_close(); ?>
				</div>
			</div>
		</div>
		<form id="form-update" method="post" action="<?php echo site_url('profile/my_profile/update_profile');?>">
		<div class="col-sm-6 col-lg-7">
			<div class="content">
				<div class="page-title">
					<h1>Update Profile</h1>
				</div>
				<div class="background-white p20 mb30">
				    <h3 class="page-title">
				        Contact Information

				        <a href="#" class="btn btn-primary pull-right btn-save">Save</a>
				    </h3>

				    <div class="row">
				        <div class="form-group col-sm-6">
				            <label>Nama Depan</label>
				            <input type="text" class="form-control" value="<?php echo $row->userprofile_firstname;?>" name="userprofile_firstname">
				        </div><!-- /.form-group -->

				        <div class="form-group col-sm-6">
				            <label>Nama Belakang</label>
				            <input type="text" class="form-control" value="<?php echo $row->userprofile_lastname;?>" name="userprofile_lastname">
				        </div><!-- /.form-group -->

				        <div class="form-group col-sm-6">
				            <label>Email</label>
				            <input type="text" class="form-control" value="<?php echo $row->user_email;?>" name="user_email" disabled="disabled">
				        </div><!-- /.form-group -->

				        <div class="form-group col-sm-6">
				            <label>Telepon</label>
				            <input type="text" class="form-control" value="<?php echo $row->userprofile_phone;?>" name="userprofile_phone">
				        </div><!-- /.form-group -->

				        <?php if($row->usertype_id==2): ?>
							<div class="form-group col-sm-6">
								<label>Nama Perusahaan</label>
								<input type="text" class="form-control" value="<?php echo $row->userprofile_companyname;?>" name="userprofile_companyname">
							</div>
							<div class="form-group col-sm-6">
								<label>Jabatan</label>
								<input type="text" class="form-control" value="<?php echo $row->userprofile_position;?>" name="userprofile_position">
							</div>
							<div class="form-group col-sm-6">
								<label>Kategori</label>
								<?php echo form_dropdown('userprofile_function',$this->global_model->list_userfunction2(),$row->userprofile_function,'id="userprofile_function" class="form-control"');?>
							</div>
						<?php else: ?>
							<div class="form-group col-sm-6">
								<label>Kategori</label>
								<?php echo form_dropdown('userprofile_function',$this->global_model->list_userfunction(),$row->userprofile_function,'id="userprofile_function" class="form-control"');?>
							</div>

				        <?php endif;?>
				       	<div class="form-group col-sm-3">
							<label>Provinsi</label>

							<?php 
								$array_province = array();
			                      $array_province[0] = "Semua Provinsi";
			                      foreach ($province as $rowsprovince) {
			                          $array_province[$rowsprovince->province_id] = $rowsprovince->province_name;
			                      }
								echo form_dropdown('province',$array_province,$row->province_id,'class="form-control"');
							?>
				       	</div>
				       	<div class="form-group col-sm-3">
							<label>Kota</label>
							<?php 
								$array_city = array();
		                        $array_city[0] = "Semua Kota";
		                        echo form_dropdown('city_id',$array_city,$row->city_id,'class="form-control"');
							?>
				       	</div>
				       	<div class="form-group col-sm-12">
							<label>Alamat</label>
							<textarea name="userprofile_address" class="form-control" style="height:80px;"><?php echo $row->userprofile_address;?></textarea>
				       	</div>
				    </div><!-- /.row -->
				</div>

				<div class="background-white p20 mb30">
					<h3 class="page-title">
						About
						<a href="#" class="btn btn-primary btn-x pull-right btn-save">Save</a>
					</h3>
					<div class="form-group">
						<label>About</label>
						<textarea name="userprofile_aboutme" id="userprofile_aboutme" class="form-control">
							<?php echo $row->userprofile_aboutme;?>
						</textarea>
					</div>
				</div>

				<div class="background-white p20 mb30">
					<?php if($row->usertype_id==1 or $row->usertype_id==3): ?>
                        <h3 class="page_title">Pengalaman</h3>
                        <?php $totalpf=$case->num_rows();?>
                        <?php if($totalpf==0): ?>
                            Belum ada pengalaman ditambahkan
                        <?php else: ?>
                            <ul>
                            <?php foreach ($case->result_array() as $listCase): ?>
                                <li>
                                    <a href="javascript.void(0);" data-title="Edit Case Studies for <?php echo $listCase['userextra_title'] ?>" data-edittype="casestudy" data-editid="<?php echo $listCase['userextra_id'] ?>" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-info edit-multi pull-right"><i class="fa fa-pencil"></i> Edit</a>
                                    <span class="small"><?php echo mdate('%F %Y',$listCase['userextra_when']);?></span><br />
                                    <span class="text-warning"><?php echo $listCase['userextra_title']; ?></span><br />
                                    <?php echo strip_tags($listCase['userextra_description']); ?>
                                    <?php if ($listCase['userextra_attachment']): ?><br />
                                        <a href="<?php echo site_url('profile/my_profile/download').'/'.$listCase['userextra_attachment'].'/'.url_title($listCase['userextra_title']).'_attachment' ?>">Attachment</a>
                                    <?php endif ?>
                                </li>
                                
                            <?php endforeach ?>
                            </ul>
                        <?php endif;?>
                        <hr />
                        <a href="javascript:void(0);" data-title="Manage Experience" data-edittype="experience" class="btn btn-sm btn-warning manage-multi" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Edit</a>
                    <?php else: ?>
                        <h3 class="page-title">Studi Kasus</h3>
                        
                        <?php $totalpf=$case->num_rows();?>
                        <?php if($totalpf==0): ?>
                            Belum ada Case Study ditambahkan
                        <?php else: ?>
                            <ul>
                            <?php foreach ($case->result_array() as $listCase): ?>
                                <li>
                                    <a href="javascript.void(0);" data-title="Edit Case Studies for <?php echo $listCase['userextra_title'] ?>" data-edittype="casestudy" data-editid="<?php echo $listCase['userextra_id'] ?>" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-info edit-multi pull-right"><i class="fa fa-pencil"></i> Edit</a>
                                    <span class="small"><?php echo mdate('%F %Y',$listCase['userextra_when']);?></span><br />
                                    <span class="text-warning"><?php echo $listCase['userextra_title']; ?></span><br />
                                    <?php echo strip_tags($listCase['userextra_description']); ?>
                                    <?php if ($listCase['userextra_attachment']): ?><br />
                                        <a href="<?php echo site_url('profile/my_profile/download').'/'.$listCase['userextra_attachment'].'/'.url_title($listCase['userextra_title']).'_attachment' ?>">Attachment</a>
                                    <?php endif ?>
                                    
                                </li>
                            <?php endforeach ?>
                            </ul>
                        <?php endif;?>
                        <hr />
                        <?php if ($row->is_premium ==0): ?>
                            Premium Member dapat menambahkan atau mengubah informasi Studi Kasus
                            <a href="javascript.void(0);" id="premiumUpgrde" data-title="Upgrade Premium Membership" data-toggle="modal" data-target="#myModal"  class="btn btn-info"><i class="fa fa-fire"></i> Upgrade Sekarang</a>
                        <?php else: ?>
                            <a href="javascript:void(0);" data-title="Manage Case Studies" data-edittype="casestudy" class="btn btn-sm btn-warning manage-multi" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Manage</a>
                        <?php endif;?>
                    <?php endif;?>
				</div>

				<div class="background-white p20 mb30">
					<h3 class="page-title">Sertifikasi</h3>
                    <?php $totalpf=$certificate->num_rows();?>
                    <?php if($totalpf==0): ?>
                        Belum ada sertifikasi yang ditambahkan
                    <?php else: ?>
                        <ul>
                        <?php foreach ($case->result_array() as $listCase): ?>
                            <li>
                                <a href="javascript.void(0);" data-title="Edit Case Studies for <?php echo $listCase['userextra_title'] ?>" data-edittype="casestudy" data-editid="<?php echo $listCase['userextra_id'] ?>" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-info edit-multi pull-right"><i class="fa fa-pencil"></i> Edit</a>
                                <span class="small"><?php echo mdate('%F %Y',$listCase['userextra_when']);?></span><br />
                                <span class="text-warning"><?php echo $listCase['userextra_title']; ?></span><br />
                                <?php echo strip_tags($listCase['userextra_description']); ?>
                                <br />
                                <?php if ($listCase['userextra_attachment']): ?><br />
                                    <a href="<?php echo site_url('profile/my_profile/download').'/'.$listCase['userextra_attachment'].'/'.url_title($listCase['userextra_title']).'_attachment' ?>">Attachment</a>
                                <?php endif ?>
                            </li>
                            
                            
                        <?php endforeach ?>
                        </ul>
                    <?php endif;?>
                    <hr />
                    <?php if ($row->is_premium == 0): ?>
                        Premium Member dapat menambahkan atau mengubah informasi Sertifikasi
                        <a href="javascript.void(0);" id="premiumUpgrde" data-title="Upgrade Premium Membership" data-toggle="modal" data-target="#myModal"  class="btn btn-info btn-sm"><i class="fa fa-fire"></i> Upgrade Sekarang</a>
                    <?php else: ?>
                        <a href="javascript:void(0);" data-title="Manage Sertifikasi" data-edittype="certification" class="btn btn-sm btn-warning manage-multi" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> Manage</a>
                    <?php endif;?>
				</div>
				
				
			</div>
		</div>
		<div class="col-sm-3 col-lg-3">
			<div class="background-white p20 mb30">
			    <h3 class="page-title">
			        Social Connections

			        <a href="#" class="btn btn-primary btn-xs pull-right btn-save">Save</a>
			    </h3>

			    <div class="form-horizontal">
			        <div class="form-group">
			            <label class="col-sm-2 control-label"><i class="fa fa-facebook"></i></label>

			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="userprofile_fb" value="<?php echo $row->userprofile_fb;?>" placeholder="Masukkan URL Lengkap">
			            </div><!-- /.col-* -->
			        </div><!-- /.form-group -->

			        <div class="form-group">
			            <label class="col-sm-2 control-label"><i class="fa fa-twitter"></i></label>

			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="userprofile_twitter" value="<?php echo $row->userprofile_twitter;?>" placeholder="Masukkan URL Lengkap">
			            </div><!-- /.col-* -->
			        </div><!-- /.form-group -->

			        <div class="form-group">
			            <label class="col-sm-2 control-label"><i class="fa fa-linkedin"></i></label>

			            <div class="col-sm-9">
			                <input type="text" class="form-control" name="userprofile_linkedin" value="<?php echo $row->userprofile_linkedin;?>" placeholder="Masukkan URL Lengkap">
			            </div><!-- /.col-* -->
			        </div><!-- /.form-group -->

			   
			    
			    </div><!-- /.form-inline -->
			</div>

		</div>
		</form>

	</div>

<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(e) {
        $('#userprofile_aboutme').ckeditor();
        $('#profile_img').hide();
        $('.user-photo').click(function() {
	        $('#profile_img').click();
	    });
	    $('#profile_img').change(function() {
	        var formData = new FormData($('#myform')[0]);
	        $.ajax({
	            url:"<?php echo site_url('profile/my_profile/get_image/') ?>",
	            type: 'POST',
	            xhr: function() {
	                var myXhr = $.ajaxSettings.xhr();
	                return myXhr;
	            },
	            success: function (data) {
	                if (data == 'true') {
	                    window.location.reload();
	                } else {
	                    //$('#error_upload').html(data);
	                    $('#error_alert').hide();
	                    $('#error_alert').html('<div class="alert text-center alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + data + '</div>');
	                    $('#error_alert').fadeIn('slow',function(){
	                        $(this).delay(5000).fadeOut('slow');
	                    });
	                }
	            },
	            data: formData,
	            cache: false,
	            contentType: false,
	            processData: false
	        });

	        return false;
	     

	    });

		$('.btn-save').click(function(){
			$('#form-update').submit();
		})
    });
</script>