<footer class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h2>Tentang HRPlasa.id</h2>

                        <p>HRPlasa.id menyoroti berita, artikel, dan praktik terbaik dalam industri Pengembangan Sumber Daya Manusia (Human Resources Development). Fokus kami adalah membantu para professional, individu dan organisasi mendapatkan informasi terkini, wawasan dan referensi yang diperlukan untuk lebih efektif dalam pengelolaan pengembangan sumber daya manusia.</p>
                    </div><!-- /.col-* -->

                    <div class="col-sm-4">
                        <h2>Contact</h2>
                        <p>
                            Graha Positif, Jl.pasar Jumat No.44a LT3<br />
                            Pondok Pinang Jakarta Selatan 12310<br />
                            +62-21-759-05509 <a href="#">info@hrplasa.id</a>
                            
                        </p>
                       
                    </div><!-- /.col-* -->

                    <div class="col-sm-4">
                        <h2>Stay Connected</h2>

                        <ul class="social-links nav nav-pills">
                            <li><a href="https://twitter.com/hrplasa"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="http://www.facebook.com/hrplasa"><i class="fa fa-facebook"></i></a></li>
                           
                            <li><a href="http://id.linkedin.com/pub/hr-plasa-id/b0/136/321/en"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCczhk7qUxggG5P5Qao2Y6bA"><i class="fa fa-youtube"></i></a></li>
                            
                        </ul><!-- /.header-nav-social -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.footer-top -->

        <div class="footer-bottom">
            <div class="container">
                <div class="footer-bottom-left">
                    &copy; 2014 - <?php echo date('Y');?> All rights reserved. Powered by <a href="http://www.semartek.com">seMartek</a>.
                </div><!-- /.footer-bottom-left -->

                <div class="footer-bottom-right">
                    <ul class="nav nav-pills">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li><a href="<?php echo site_url('membership');?>">Membership</a></li>
                        <li><a href="terms-conditions.html">Terms &amp; Conditions</a></li>
                        <li><a href="<?php echo site_url('tentang-kami');?>">Contact</a></li>
                    </ul><!-- /.nav -->
                </div><!-- /.footer-bottom-right -->
            </div><!-- /.container -->
        </div>
    </footer><!-- /.footer -->
    <!--Start of Zopim Live Chat Script-->

<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3dulVKnHCuvdLC2EiFEdU6HsuouejSuM";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->