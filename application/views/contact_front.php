<div class="block background-secondary fullwidth mt80 mb-80 pt60 pb60">
    <div class="row">
        <div class="col-sm-12">
            <div class="contact-info-wrapper">
                <h2>Anda masih memiliki pertanyaan?</h2>

                <div class="contact-info">
                    <span class="contact-info-item">
                        <i class="fa fa-at"></i>
                        <span>support@hrplasa.id</span>
                    </span><!-- /.contact-info-item -->

                    <span class="contact-info-item">
                        <i class="fa fa-phone"></i>
                        <span>+62-21-759-05509</span>
                    </span><!-- /.contact-info-item -->
                </div><!-- /.contact-info-->
            </div><!-- /.contact-info-wrapper -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->

</div>