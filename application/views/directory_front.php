<div class="block fullwidth">
    <div class="page-header">
        <h1>Jelajahi Direktori Kami</h1>
        
    </div><!-- /.page-header -->

    <div class="cards-wrapper">
        <div class="row">
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card" data-background-image="assets/img/event.jpeg">
                            

                            <div class="card-content">
                                <h2><a href="<?php echo site_url('events');?>">Event</a></h2>

                                <div class="card-meta">
                                   <?php echo $total_active_events;?> Event Aktif dari <?php echo $total_all_events;?> Event yang terdaftar
                                </div><!-- /.card-meta -->

                                <div class="card-rating">
                                    
                                   
                                </div><!-- /.card-rating -->

                                <div class="card-actions">
                                    
                                    <a href="<?php echo site_url('events');?>" class="fa fa-search"></a>
                                    
                                </div>

                            </div><!-- /.card-content -->
                        </div><!-- /.card -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-sm-6">
                        <div class="card" data-background-image="assets/img/trainer.jpeg">
                            

                            <div class="card-content">
                                <h2><a href="listing-detail.html">HR Expert</a></h2>

                                <div class="card-meta">
                                    <?php echo $this->global_model->get_all_user(FALSE,TRUE);?> Expert Terdaftar
                                </div><!-- /.card-meta -->

                                <div class="card-rating">
                                   
                                    
                                </div><!-- /.card-rating -->

                                <div class="card-actions">
                                   
                                    <a href="listing-detail.html" class="fa fa-search"></a>
                                    
                                </div>
                            </div><!-- /.card-content -->
                        </div><!-- /.card -->
                    </div>

                    <div class="col-sm-6">
                        <div class="card" data-background-image="assets/img/vendor.jpg">
                            

                            <div class="card-content">
                                <h2><a href="<?php echo site_url('vendor-directory');?>">Vendor</a></h2>

                                <div class="card-meta">
                                    <?php echo $this->global_model->get_all_user(TRUE);?> Vendor Terdaftar
                                </div><!-- /.card-meta -->

                                <div class="card-rating">
                                   
                                    
                                </div><!-- /.card-rating -->

                                <div class="card-actions">
                                    
                                    <a href="<?php echo site_url('vendor-directory');?>" class="fa fa-search"></a>
                                   
                                </div>
                            </div><!-- /.card-content -->
                        </div><!-- /.card -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.col-* -->

            <div class="col-sm-4">
                <div class="card" data-background-image="assets/img/venue_directory.jpg">
                    

                    <div class="card-content">
                        <h2><a href="<?php echo site_url('venue_directory');?>">Venue</a></h2>

                        <div class="card-meta">
                            Venue Direktori Terlengkap
                        </div><!-- /.card-meta -->

                        

                        <div class="card-actions">
                            
                            <a href="<?php echo site_url('venue_directory');?>" class="fa fa-search"></a>
                           
                        </div>
                    </div><!-- /.card-content -->
                </div><!-- /.card -->
                <div class="card" data-background-image="assets/img/job.jpeg">
                    

                    <div class="card-content">
                        <h2><a href="<?php echo site_url('jobs');?>">Lowongan</a></h2>

                        <div class="card-meta">
                            <?php echo $total_job;?> Lowongan
                        </div><!-- /.card-meta -->

                        

                        <div class="card-actions">
                            
                            <a href="<?php echo site_url('jobs');?>" class="fa fa-search"></a>
                            
                        </div>
                    </div><!-- /.card-content -->
                </div><!-- /.card -->
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.cards-wrapper -->

</div>