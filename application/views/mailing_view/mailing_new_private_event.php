<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>
	</head>
	<body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
    	<div style="border-bottom:1px solid #CCC;padding:20px;">
            <img src="http://www.hrplasa.id/assets/css/images/logo.fw.png"> <br />
            Pasar Online SDM Pertama di Indonesia
            </div>
    	<div style="padding:20px;">
        	
            <p>Hi <?php echo $event_meta->userprofile_firstname;?>,</p>
            <p>Terima kasih telah mendaftarkan Event Private (Internal) anda di HRplasa.id</p>
            <p>Event Anda terdaftar dengan judul <strong><?php echo $event_meta->event_title?></strong>, event ini telah kami tambahakan dalam daftar Event Anda :</p>
            <p>Anda dapat melihat event Anda pada link berikut : <br />
            <a href="<?php echo site_url('event/view/'.$event_meta->event_id.'/preview');?>"><?php echo site_url('event/view/'.$event_meta->event_id.'/preview');?></a>
            
            
            
            <p>Jika anda mempunyai pertanyaan dan masalah, bisa menghubungi email kami di support@hrplasa.id</p>
            
            
            
        </div>
        <div style="text-align:center;background-color:#3BAFDA;color:#FFF;float:left;width:100%;padding-top:10px;">
            
                HRplasa.id<br />
                Graha Positif, Jl.pasar Jumat No.44a Pondok <br />
                Pinang Jakarta Selatan 12310<br />
                T.75905509<br />
                email: info@hrplasa.id <br />
            
           
                <br />
                <a href="http://www.facebook.com/hrplasa" target="_blank"><img src="http://www.hrplasa.id/assets/css/images/fb.png"></a>
                <a href="https://twitter.com/hrplasa" target="_blank"><img src="http://www.hrplasa.id/assets/css/images/twitter.png"></a>
                <a href="https://www.youtube.com/channel/UCczhk7qUxggG5P5Qao2Y6bA" target="_blank"><img src="http://www.hrplasa.id/assets/css/images/youtube.png"></a>
                <a href="http://id.linkedin.com/pub/hr-plasa-id/b0/136/321/en"><img src="http://www.hrplasa.id/assets/css/images/linkedin.png"></a>
            
        </div>
	</body>
</html>