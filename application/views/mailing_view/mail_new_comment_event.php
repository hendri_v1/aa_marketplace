<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>New Comments</title>
    </head>
    <body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
        <?php $this->load->view('mailing_view/mail_header'); ?>
        <div style="padding:20px;">
            <p>Hi <?php echo $user['userprofile_firstname'] . ' ' . $user['userprofile_lastname']; ?>,</p>
            <p>Ada komentar baru pada event Anda</p>
            <em>
                <?php echo $comment['comment_text']; ?>
            </em>
            <br/>
            <a href="<?php echo site_url('event/view/' . $event->event_id . '/' . url_title($event->event_title)); ?>">Klik untuk melihat.</a>
            <p>Jika anda mempunyai pertanyaan dan masalah, bisa menghubungi email kami di support@hrplasa.id</p>
        </div>
        <?php $this->load->view('mailing_view/mail_footer'); ?>
    </body>
</html>