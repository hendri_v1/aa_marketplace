<!DOCTYPE html>
<html lang="">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Title Page</title>
</head>

<body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
	<?php $this->load->view('mailing_view/mail_header');?>
		<div style="padding:20px;">

			<p>Hi
				<?php echo $venue_meta->userprofile_firstname;?>,</p>
			<p>Terima kasih telah mendaftarkan Venue anda di HRplasa.id</p>
			<p>Venue Anda terdaftar dengan nama <strong><?php echo $venue_meta->venue_main_name?></strong>, venue ini telah kami terima dan akan kami terbitkan setelah lolos verifikasi :</p>
			<p>Anda dapat melihat venue Anda pada link berikut :
				<br />
				<a href="<?php echo site_url('venue/view/'.$venue_meta->venue_main_id.'/'.url_title($venue_meta->venue_main_name));?>"><?php echo site_url('venue/view/'.$venue_meta->venue_main_id.'/'.url_title($venue_meta->venue_main_name));?></a>

				<p>Pastikan Anda Menambahkan Room pada venue ini dengan mengklik tautan berikut ini : </p>
				<a href="<?php echo site_url('venue/my_venue/view_rooms/'.$venue_meta->venue_main_id);?>"><?php echo site_url('venue/my_venue/view_rooms/'.$venue_meta->venue_main_id);?></a>

				<p>Jika anda mempunyai pertanyaan dan masalah, bisa menghubungi email kami di support@hrplasa.id</p>



		</div>
		<?php $this->load->view('mailing_view/mail_footer');?>
</body>

</html>