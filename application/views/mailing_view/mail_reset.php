<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Reset Password</title>
	</head>
	
    <body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
    	<?php $this->load->view('mailing_view/mail_header');?>
    	<div style="padding:20px;">
            <p>Hi <?php echo $name;?>,</p>
            <p>
            	Silahkan klik Link dibawah ini untuk mereset password anda 
                <br> 
                <a href="<?php echo site_url('program/reset/'.$kode) ?>">Reset Password</a>
            </p>
            <p>Anda menerima email ini karena Anda melakukan request reset password pada situ HRplasa.id, apabila Anda tidak merasa melakukan hal tersebut, mohon abaikan email ini.</p>
            <p>Jika anda mempunyai pertanyaan dan masalah, bisa menghubungi email kami di support@hrplasa.id</p>
        </div>
        <?php $this->load->view('mailing_view/mail_footer');?>
	</body>
</html>