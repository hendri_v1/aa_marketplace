<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Title Page</title>
    </head>
    <body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
        <?php $this->load->view('mailing_view/mail_header');?>
        <div style="padding:20px;">
            
            <p>Dear <?php echo $name;?>,</p>

            <p>Ada pendaftaran online untuk Event Anda, dengan detail pendaftaran sebagai berikut : </p>
            <table>
                <tr><td>Nama Pendaftar</td><td>: <?php echo $registrar_name;?></td></tr>
                <tr><td>Event</td><td>: <strong><?php echo $row->event_title;?></strong></td></tr>
            </table>
            <p>Silahkan login ke HRPlasa.id dan masuk ke Kelola Event untuk melihat informasi lengkap tentang registrasi online terhadap Event Anda</p>
            
            <p>HRplasa.id menyoroti berita, artikel, dan praktik terbaik dalam industri Pengembangan Sumber Daya Manusia (Human Resources Development). Fokus kami adalah membantu para professional, individu dan organisasi mendapatkan informasi terkini, wawasan dan referensi yang diperlukan untuk lebih efektif dalam pengelolaan pengembangan sumber daya manusia.</p>
            <p>Jika anda mempunyai pertanyaan dan masalah, bisa menghubungi email kami di support@hrplasa.id</p>

        </div>
        <?php $this->load->view('mailing_view/mail_footer');?>
    </body>
</html>