<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Title Page</title>
    </head>
    <body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
        <?php $this->load->view('mailing_view/mail_header');?>
        <div style="padding:20px;">
            
            <p>Dear <?php echo $name;?>,</p>

            <p>Anda telah melakukan pendaftaran pada event <strong><?php echo $event_title;?></strong>. Pendaftaran ini telah kami terima dan akan kami proses lebih lanjut. Apabila ada biaya Pendaftaran untuk Event ini, kami akan mengirimkan email Invoice sesaat lagi.</p>
            <p>Terima Kasih telah mempercayakan HRPlasa.id sebagai media pendaftaran Anda. Anda dapat melihat detail Event yang Anda daftar pada tautan berikut ini : <br />
                <?php echo $event_url;?>
            </p>
            
            <p>HRplasa.id menyoroti berita, artikel, dan praktik terbaik dalam industri Pengembangan Sumber Daya Manusia (Human Resources Development). Fokus kami adalah membantu para professional, individu dan organisasi mendapatkan informasi terkini, wawasan dan referensi yang diperlukan untuk lebih efektif dalam pengelolaan pengembangan sumber daya manusia.</p>
            <p>Jika anda mempunyai pertanyaan dan masalah, bisa menghubungi email kami di support@hrplasa.id</p>
            
            
            
        </div>
        <?php $this->load->view('mailing_view/mail_footer');?>
    </body>
</html>