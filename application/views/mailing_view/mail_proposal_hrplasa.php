<!DOCTYPE html>
<html lang="">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Title Page</title>
</head>

<body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
	<?php $this->load->view('mailing_view/mail_header');?>
		<div style="padding:20px;">

			<p>Hi HRPlasa Admin,</p>
			<p>Ada proposal Request untuk Room <?php echo $room->venue_name;?> di <?php echo $venue_meta->venue_main_name;?></p>
			<p>Berikut detail dari Proposal yang diinginkan</p>
			<h4>Tanggal Yang Dipilih</h4>
			<ul>
				<?php foreach($qbooking->result() as $rbooking): ?>
					<li><?php echo mdate('%d %M %Y',$rbooking->venue_booking_start_date);?></li>
				<?php endforeach;?>
			</ul>
			<h4>Deskripsi</h4>
			<?php echo $set_array['proposal_description'];?>
			<h4>Request Tambahan</h4>
			<?php echo $set_array['proposal_additional'];?>
			<h4>Total Peserta</h4>
			<?php echo $set_array['proposal_audience'];?>
			<hr />
			<p>
				Contact Yang Melakukan Request : <br />
				Nama : <?php echo $user_meta->userprofile_firstname;?> <?php echo $user_meta->userprofile_lastname;?><br />
				Email : <?php echo $user_meta->user_email;?><br />
				Telepon : <?php echo $user_meta->userprofile_phone;?>
			</p>



		</div>
		<?php $this->load->view('mailing_view/mail_footer');?>
</body>

</html>