<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge" /><!--<![endif]-->
    <meta name="viewport" content="width=device-width" />
    <title> </title>
    <style type="text/css">
.wrapper a:hover {
  text-decoration: none !important;
}
.btn a:hover,
.footer__links a:hover {
  opacity: 0.8;
}
.wrapper .footer__share-button:hover {
  color: #ffffff !important;
  opacity: 0.8;
}
a[x-apple-data-detectors] {
  color: inherit !important;
  text-decoration: none !important;
  font-size: inherit !important;
  font-family: inherit !important;
  font-weight: inherit !important;
  line-height: inherit !important;
}
.column {
  font-size: 14px;
  line-height: 21px;
  padding: 0;
  text-align: left;
  vertical-align: top;
}
.mso .font-avenir,
.mso .font-cabin,
.mso .font-open-sans,
.mso .font-ubuntu {
  font-family: sans-serif !important;
}
.mso .font-bitter,
.mso .font-merriweather,
.mso .font-pt-serif {
  font-family: Georgia, serif !important;
}
.mso .font-lato,
.mso .font-roboto {
  font-family: Tahoma, sans-serif !important;
}
.mso .font-pt-sans {
  font-family: "Trebuchet MS", sans-serif !important;
}
.mso .footer p {
  margin: 0;
}
@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
  .fblike {
    background-image: url(https://i7.createsend1.com/static/eb/master/13-the-blueprint-3/images/fblike@2x.png) !important;
  }
  .tweet {
    background-image: url(https://i8.createsend1.com/static/eb/master/13-the-blueprint-3/images/tweet@2x.png) !important;
  }
  .linkedinshare {
    background-image: url(https://i9.createsend1.com/static/eb/master/13-the-blueprint-3/images/lishare@2x.png) !important;
  }
  .forwardtoafriend {
    background-image: url(https://i10.createsend1.com/static/eb/master/13-the-blueprint-3/images/forward@2x.png) !important;
  }
}
@media only screen and (max-width: 620px) {
  .wrapper h2,
  .wrapper .size-18,
  .wrapper .size-20 {
    font-size: 17px !important;
    line-height: 26px !important;
  }
  .wrapper .size-22 {
    font-size: 18px !important;
    line-height: 26px !important;
  }
  .wrapper .size-24 {
    font-size: 20px !important;
    line-height: 28px !important;
  }
  .wrapper h1,
  .wrapper .size-26 {
    font-size: 22px !important;
    line-height: 31px !important;
  }
  .wrapper .size-28 {
    font-size: 24px !important;
    line-height: 32px !important;
  }
  .wrapper .size-30 {
    font-size: 26px !important;
    line-height: 34px !important;
  }
  .wrapper .size-32 {
    font-size: 28px !important;
    line-height: 36px !important;
  }
  .wrapper .size-34,
  .wrapper .size-36 {
    font-size: 30px !important;
    line-height: 38px !important;
  }
  .wrapper .size-40 {
    font-size: 32px !important;
    line-height: 40px !important;
  }
  .wrapper .size-44 {
    font-size: 34px !important;
    line-height: 43px !important;
  }
  .wrapper .size-48 {
    font-size: 36px !important;
    line-height: 43px !important;
  }
  .wrapper .size-56 {
    font-size: 40px !important;
    line-height: 47px !important;
  }
  .wrapper .size-64 {
    font-size: 44px !important;
    line-height: 50px !important;
  }
  .divider {
    Margin-left: auto !important;
    Margin-right: auto !important;
  }
  .btn a {
    display: block !important;
    font-size: 14px !important;
    line-height: 24px !important;
    padding: 12px 10px !important;
    width: auto !important;
  }
  .btn--shadow a {
    padding: 12px 10px 13px 10px !important;
  }
  .image img {
    height: auto;
    width: 100%;
  }
  .layout,
  .column,
  .preheader__webversion,
  .header td,
  .footer,
  .footer__left,
  .footer__right,
  .footer__inner {
    width: 320px !important;
  }
  .preheader__snippet,
  .layout__edges {
    display: none !important;
  }
  .preheader__webversion {
    text-align: center !important;
  }
  .header__logo {
    Margin-left: 20px;
    Margin-right: 20px;
  }
  .layout--full-width {
    width: 100% !important;
  }
  .layout--full-width tbody,
  .layout--full-width tr {
    display: table;
    Margin-left: auto;
    Margin-right: auto;
    width: 320px;
  }
  .column,
  .layout__gutter,
  .footer__left,
  .footer__right {
    display: block;
    Float: left;
  }
  .footer__inner {
    text-align: center;
  }
  .footer__links {
    Float: none;
    Margin-left: auto;
    Margin-right: auto;
  }
  .footer__right p,
  .footer__share-button {
    display: inline-block;
  }
  .layout__gutter {
    font-size: 20px;
    line-height: 20px;
  }
  .layout--no-gutter.layout--has-border:not(.layout--full-width),
  .layout--has-gutter.layout--has-border .column__background {
    width: 322px !important;
  }
  .layout--has-gutter.layout--has-border {
    left: -1px;
    position: relative;
  }
}
@media only screen and (max-width: 320px) {
  .border {
    display: none;
  }
  .layout--no-gutter.layout--has-border:not(.layout--full-width),
  .layout--has-gutter.layout--has-border .column__background {
    width: 320px !important;
  }
  .layout--has-gutter.layout--has-border {
    left: 0 !important;
  }
}
</style>
    
  <!--[if !mso]><!--><style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Roboto:400,700,400italic,700italic);
</style><link href="https://fonts.googleapis.com/css?family=Roboto:400,700,400italic,700italic" rel="stylesheet" type="text/css" /><!--<![endif]--><style type="text/css">
body,.wrapper{background-color:#ecf3f5}.wrapper h1{color:#557187}.wrapper h2{color:#557187}.wrapper h3{color:#557187}.wrapper a{color:#557187}.wrapper a:hover{color:#2e3d48 !important}.layout--no-gutter .column,.column__background td{color:#757575}.layout--no-gutter .column,.column__background td{font-family:Roboto,Tahoma,sans-serif}.mso .layout--no-gutter .column,.mso .column__background td{font-family:Tahoma,sans-serif !important}.border{background-color:#a9c9d2}.layout--no-gutter.layout--has-border:not(.layout--full-width),.layout--has-gutter.layout--has-border .column__background,.layout--full-width.layout--has-border{border-top:1px solid #a9c9d2;border-bottom:1px solid #a9c9d2}.wrapper blockquote{border-left:4px solid #a9c9d2}.divider{background-color:#a9c9d2}.wrapper .btn a{color:#fff}.wrapper .btn a{font-family:Roboto,Tahoma,sans-serif}.mso .wrapper .btn 
a{font-family:Tahoma,sans-serif !important}.wrapper .btn a:hover{color:#fff !important}.btn--flat a,.btn--shadow a,.btn--depth a{background-color:#557187}.btn--ghost a{border:1px solid #557187}.preheader--inline,.footer__left{color:#b9b9b9}.preheader--inline,.footer__left{font-family:Roboto,Tahoma,sans-serif}.mso .preheader--inline,.mso .footer__left{font-family:Tahoma,sans-serif !important}.wrapper .preheader--inline a,.wrapper .footer__left a{color:#b9b9b9}.wrapper .preheader--inline a:hover,.wrapper .footer__left a:hover{color:#b9b9b9 !important}.header__logo{color:#c3ced9}.header__logo{font-family:Roboto,Tahoma,sans-serif}.mso .header__logo{font-family:Tahoma,sans-serif !important}.wrapper .header__logo a{color:#c3ced9}.wrapper .header__logo a:hover{color:#859bb1 !important}.footer__share-button{background-color:#767a7b}.footer__share-button{font-family:Roboto,Tahoma,sans-serif}.mso 
.footer__share-button{font-family:Tahoma,sans-serif !important}.layout__separator--inline{font-size:20px;line-height:20px;mso-line-height-rule:exactly}
</style><meta name="robots" content="noindex,nofollow" />
<meta property="og:title" content="My First Campaign" />
</head>
<!--[if mso]>
  <body class="mso">
<![endif]-->
<!--[if !mso]><!-->
  <body class="full-padding" style="margin: 0;-webkit-text-size-adjust: 100%;background-color: #ecf3f5;">
<!--<![endif]-->
    <div class="wrapper" style="background-color: #ecf3f5;">
      <table style="border-collapse: collapse;table-layout: fixed;color: #b9b9b9;font-family: Roboto,Tahoma,sans-serif;" align="center">
        <tbody><tr>
          <td class="preheader__snippet" style="padding: 10px 0 5px 0;vertical-align: top;" width="300">
            
          </td>
          <td class="preheader__webversion" style="text-align: right;padding: 10px 0 5px 0;vertical-align: top;" width="300">
            
          </td>
        </tr>
      </tbody></table>
      <table class="header" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;" align="center">
        <tbody><tr>
          <td style="padding: 0;" width="600">
            <div class="header__logo emb-logo-margin-box" style="font-size: 26px;line-height: 32px;Margin-top: 6px;Margin-bottom: 20px;color: #c3ced9;font-family: Roboto,Tahoma,sans-serif;">
              <div class="logo-center" style="font-size:0px !important;line-height:0 !important;" align="center" id="emb-email-header"><a style="text-decoration: none;transition: opacity 0.1s ease-in;color: #c3ced9;" href="http://hrplasa.id"><img style="height: auto;width: 100%;border: 0;max-width: 164px;" src="http://hrplasa.id/assets/img/logo.png" alt="" width="164" height="40" /></a></div>
            </div>
          </td>
        </tr>
      </tbody></table>
      <table class="layout layout--no-gutter layout--full-width" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;width: 100%;background-color: #ecf3f5;" align="center">
        <tbody><tr>
          <td class="layout__edges">&nbsp;</td>
          <td class="column" style="font-size: 14px;line-height: 21px;padding: 0;text-align: left;vertical-align: top;color: #757575;font-family: Roboto,Tahoma,sans-serif;" width="300">
    
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 24px;">
      <h2 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;font-size: 20px;line-height: 28px;color: #557187;"><?php echo $campaign_title;?></h2><p style="Margin-top: 16px;Margin-bottom: 20px;"><?php echo $campaign_word;?></p>
    </div>
    
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;">
      <div style="line-height:5px;font-size:1px">&nbsp;</div>
    </div>
    
          </td>
          <td class="column" style="font-size: 14px;line-height: 21px;padding: 0;text-align: left;vertical-align: top;color: #757575;font-family: Roboto,Tahoma,sans-serif;" width="300">
    
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 24px;">
      <div style="line-height:6px;font-size:1px">&nbsp;</div>
    </div>
    
        <div class="image" style="font-size: 12px;font-style: normal;font-weight: 400;" align="center">
          <!---<img style="display: block;border: 0;max-width: 182px;" src="images/WIP-homepageV082.png" alt="" width="182" height="92" />-->
        </div>
      
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;Margin-bottom: 24px;">
            <?php /*
      <div class="btn btn--flat" style="text-align:center;">
        <![if !mso]><a style="border-radius: 4px;display: inline-block;font-weight: bold;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #557187;font-family: Roboto, Tahoma, sans-serif;font-size: 12px;line-height: 22px;padding: 10px 28px;" href="http://test.com" data-width="98">View the roadmap</a><![endif]>
      <!--[if mso]><p style="line-height:0;margin:0;">&nbsp;</p><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="http://test.com" style="width:154px" arcsize="10%" fillcolor="#557187" stroke="f"><v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px"><center style="font-size:12px;line-height:22px;color:#FFFFFF;font-family:Tahoma,sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px">View the roadmap</center></v:textbox></v:roundrect><![endif]--></div>
    </div>
    */?>
          </td>
          <td class="layout__edges">&nbsp;</td>
        </tr>
      </tbody></table>
  
      <table class="layout layout--no-gutter layout--full-width" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;width: 100%;background-color: #ffffff;" align="center">
        <tbody><tr>
          <td class="layout__edges">&nbsp;</td>
          <td class="column" style="font-size: 14px;line-height: 21px;padding: 0;text-align: left;vertical-align: top;color: #757575;font-family: Roboto,Tahoma,sans-serif;" width="300">
    
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 24px;">
      <div style="line-height:29px;font-size:1px">&nbsp;</div>
    </div>
    
            <div style="Margin-left: 20px;Margin-right: 20px;">
      <div style="line-height:5px;font-size:1px">&nbsp;</div>
    </div>
    <?php
      $texthtml = $row_blog->post_content;
      ob_start();
      ob_end_clean();
      $html= $texthtml;

      $doc = new DOMDocument();
      @$doc->loadHTML($html);
      $img = $doc->getElementsByTagName('img');
      $data['featured_image']='';
      if ($img->item(0) == null)
      {
        $featured_image=base_url().'uploads/user_images/'.$this->global_model->get_user_photo($row_blog->userprofile_photo);
      }
      else
        $featured_image=$img->item(0)->getAttribute('src');

    ?>
    
        <div class="image" style="font-size: 12px;font-style: normal;font-weight: 400;" align="center">
          <img style="display: block;border: 0;max-width: 221px;" src="<?php echo $featured_image;?>" alt="" width="221" height="93" />
        </div>
      
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;Margin-bottom: 24px;">
      <div class="btn btn--flat" style="text-align:center;">
        <![if !mso]><a style="border-radius: 4px;display: inline-block;font-weight: bold;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #557187;font-family: Roboto, Tahoma, sans-serif;font-size: 12px;line-height: 22px;padding: 10px 28px;" href="<?php echo site_url('blog/read/'.$row_blog->post_id.'/'.url_title($row_blog->post_title)) ?>" data-width="98">Lihat Tulisan</a><![endif]>
      <!--[if mso]><p style="line-height:0;margin:0;">&nbsp;</p><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="http://test.com" style="width:154px" arcsize="10%" fillcolor="#557187" stroke="f"><v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px"><center style="font-size:12px;line-height:22px;color:#FFFFFF;font-family:Tahoma,sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px">View your profiles</center></v:textbox></v:roundrect><![endif]--></div>
    </div>
    
          </td>
          <td class="column" style="font-size: 14px;line-height: 21px;padding: 0;text-align: left;vertical-align: top;color: #757575;font-family: Roboto,Tahoma,sans-serif;" width="300">
    
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 24px;">
      <div style="line-height:20px;font-size:1px">&nbsp;</div>
    </div>
    
            <div style="Margin-left: 20px;Margin-right: 20px;">
      <h2 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;font-size: 20px;line-height: 28px;color: #557187;"><?php echo $row_blog->post_title;?></h2><p style="Margin-top: 16px;Margin-bottom: 20px;"><?php echo word_limiter(strip_tags($row_blog->post_content),50); ?></p>
    </div>
    
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;">
      <div style="line-height:10px;font-size:1px">&nbsp;</div>
    </div>
    
          </td>
          <td class="layout__edges">&nbsp;</td>
        </tr>
      </tbody></table>
  
      <table class="layout layout--no-gutter layout--full-width" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;width: 100%;background-color: #ecf3f5;" align="center">
        <tbody><tr>
          <td class="layout__edges">&nbsp;</td>
          <td class="column" style="font-size: 14px;line-height: 21px;padding: 0;text-align: left;vertical-align: top;color: #757575;font-family: Roboto,Tahoma,sans-serif;" width="300">
    
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 24px;">
      <div style="line-height:22px;font-size:1px">&nbsp;</div>
    </div>
    
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;">
      <h2 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;font-size: 20px;line-height: 28px;color: #557187;"><?php echo $row_event->event_title;?></h2><p style="Margin-top: 16px;Margin-bottom: 0;">oleh : <?php
                                        if($row_event->usertype_id==1)
                                            echo $row_event->userprofile_firstname.' '.$row_event->userprofile_lastname;
                                        else
                                            echo $row_event->userprofile_companyname;
                                    ?> 
                                <br />
                                <?php $event_description=explode('<!---split--->', $row_event->event_description); ?>
                                <?php echo word_limiter(strip_tags($event_description[0]),50); ?>.&nbsp;</p>
    </div>
    
          </td>
          <td class="column" style="font-size: 14px;line-height: 21px;padding: 0;text-align: left;vertical-align: top;color: #757575;font-family: Roboto,Tahoma,sans-serif;" width="300">
    
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 24px;">
      <div style="line-height:29px;font-size:1px">&nbsp;</div>
    </div>
    
        <div class="image" style="font-size: 12px;font-style: normal;font-weight: 400;" align="center">
        <?php 
                    $theimage='';
                    if($row_event->event_picture=='')
                        $theimage=base_url().'uploads/user_images/'.$this->global_model->get_user_photo($row_event->userprofile_photo);
                    else
                        $theimage=base_url().'uploads/event_images/'.$row_event->event_picture;
                ?>
          <img style="display: block;border: 0;max-width: 176px;" src="<?php echo $theimage;?>" alt="" width="176" height="92" />
        </div>
      
            <div style="Margin-left: 20px;Margin-right: 20px;Margin-top: 20px;Margin-bottom: 24px;">
      <div class="btn btn--flat" style="text-align:center;">
        <![if !mso]><a style="border-radius: 4px;display: inline-block;font-weight: bold;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #557187;font-family: Roboto, Tahoma, sans-serif;font-size: 12px;line-height: 22px;padding: 10px 28px;" href="<?php echo site_url('event/view/'.$row_event->event_id.'/'.url_title($row_event->event_title));?>" data-width="96">Lihat Event</a><![endif]>
      <!--[if mso]><p style="line-height:0;margin:0;">&nbsp;</p><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" href="http://test.com" style="width:152px" arcsize="10%" fillcolor="#557187" stroke="f"><v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px"><center style="font-size:12px;line-height:22px;color:#FFFFFF;font-family:Tahoma,sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px">Download the app</center></v:textbox></v:roundrect><![endif]--></div>
    </div>
    
          </td>
          <td class="layout__edges">&nbsp;</td>
        </tr>
      </tbody></table>
  
      <div style="font-size: 70px;line-height: 70px;mso-line-height-rule: exactly;">&nbsp;</div>
    
      <table class="footer" style="border-collapse: collapse;table-layout: fixed;Margin-right: auto;Margin-left: auto;border-spacing: 0;" width="600" align="center">
        <tbody><tr>
          <td style="padding: 0 0 40px 0;">
            <table class="footer__right" style="border-collapse: collapse;table-layout: auto;border-spacing: 0;" align="right">
              <tbody><tr>
                <td class="footer__inner" style="padding: 0;">
                  <p style="Margin-top: 0;Margin-bottom: 5px;mso-line-height-rule: exactly;line-height: 26px;"><fblike class="footer__share-button fblike" style="background-image: url(https://i1.createsend1.com/static/eb/master/13-the-blueprint-3/images/fblike.png);background-color: #767a7b;font-family: Roboto,Tahoma,sans-serif;background-repeat: no-repeat;background-size: 200px 56px;border-radius: 2px;color: #ffffff;display: block;font-size: 11px;font-weight: bold;line-height: 11px;padding: 8px 11px 7px 28px;text-align: left;text-decoration: none;" cs-button data-vml-width="80" left-align-text="true">Like</fblike></p>
                  <p style="Margin-top: 0;Margin-bottom: 5px;mso-line-height-rule: exactly;line-height: 26px;"><tweet class="footer__share-button tweet" style="background-image: url(https://i3.createsend1.com/static/eb/master/13-the-blueprint-3/images/tweet.png);background-color: #767a7b;font-family: Roboto,Tahoma,sans-serif;background-repeat: no-repeat;background-size: 200px 56px;border-radius: 2px;color: #ffffff;display: block;font-size: 11px;font-weight: bold;line-height: 11px;padding: 8px 11px 7px 28px;text-align: left;text-decoration: none;" cs-button data-vml-width="80" left-align-text="true">Tweet</tweet></p>
                  <p style="Margin-top: 0;Margin-bottom: 5px;mso-line-height-rule: exactly;line-height: 26px;"><linkedinshare class="footer__share-button linkedinshare" style="background-image: url(https://i2.createsend1.com/static/eb/master/13-the-blueprint-3/images/lishare.png);background-color: #767a7b;font-family: Roboto,Tahoma,sans-serif;background-repeat: no-repeat;background-size: 200px 56px;border-radius: 2px;color: #ffffff;display: block;font-size: 11px;font-weight: bold;line-height: 11px;padding: 8px 11px 7px 28px;text-align: left;text-decoration: none;" cs-button data-vml-width="80" left-align-text="true">Share</linkedinshare></p>
                  <p style="Margin-top: 0;Margin-bottom: 5px;mso-line-height-rule: exactly;line-height: 26px;"><forwardtoafriend class="footer__share-button forwardtoafriend" style="background-image: url(https://i4.createsend1.com/static/eb/master/13-the-blueprint-3/images/forward.png);background-color: #767a7b;font-family: Roboto,Tahoma,sans-serif;background-repeat: no-repeat;background-size: 200px 56px;border-radius: 2px;color: #ffffff;display: block;font-size: 11px;font-weight: bold;line-height: 11px;padding: 8px 11px 7px 28px;text-align: left;text-decoration: none;" cs-button data-vml-width="80" left-align-text="true">Forward</forwardtoafriend></p>
                </td>
              </tr>
            </tbody></table>
            <table class="footer__left" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;color: #b9b9b9;font-family: Roboto,Tahoma,sans-serif;" width="400">
              <tbody><tr>
                <td class="footer__inner" style="padding: 0;font-size: 12px;line-height: 19px;">
                  <table class="footer__links emb-web-links" style="border-collapse: collapse;table-layout: fixed;"><tbody><tr>
                    
<td class="emb-web-links" style="padding: 0;" width="26"><a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;" href="https://www.facebook.com/hrplasa/"><img style="border: 0;" src="https://i10.createsend1.com/static/eb/master/13-the-blueprint-3/images/facebook.png" width="26" height="26" /></a></td><td class="emb-web-links" style="padding: 0 0 0 3px;" width="26"><a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;" href="https://twitter.com/HRPlasa"><img style="border: 0;" src="https://i2.createsend1.com/static/eb/master/13-the-blueprint-3/images/twitter.png" width="26" height="26" /></a></td><td class="emb-web-links" style="padding: 0 0 0 3px;" width="26"><a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;" href="https://www.youtube.com/channel/UCu_G-w5r20XWROXlTe7FoVw"><img style="border: 0;" src="https://i3.createsend1.com/static/eb/master/13-the-blueprint-3/images/youtube.png" width="26" height="26" /></a></td><td class="emb-web-links" style="padding: 0 0 0 3px;" width="26"><a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;" href="https://id.linkedin.com/in/hrplasa"><img style="border: 0;" src="https://i4.createsend1.com/static/eb/master/13-the-blueprint-3/images/linkedin.png" width="26" height="26" /></a></td><td class="emb-web-links" style="padding: 0 0 0 3px;" width="26"><a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;" href="http://hrplasa.id"><img style="border: 0;" src="https://i5.createsend1.com/static/eb/master/13-the-blueprint-3/images/website.png" width="26" height="26" /></a></td></tr></tbody></table>
                  <div style="Margin-top: 26px;">
                    <div>hrplasa.id<br />
Graha Positif, Jl.pasar Jumat No.44a Pondok Pinang Jakarta Selatan 12310</div>
                  </div>
                  <div class="footer__permission" style="Margin-top: 18px;">
                    
                  </div>
                  <div>
                    <unsubscribe>Unsubscribe</unsubscribe>
                  </div>
                </td>
              </tr>
            </tbody></table>
          </td>
        </tr>
      </tbody></table>
      <badge>
        
      </badge>
    </div>
  

</body></html>