<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Update Info HRPlasa</title>
	</head>
	<body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
    	<?php $this->load->view('mailing_view/mail_header');?>
    	<div style="padding:20px;">
            <p>Hi *|FNAME|*,</p>
           	<p>Minggu ini, akan ada <a href="<?php echo site_url('events');?>"><?php echo $total_event_next_week;?> Event</a> yang di selenggarakan</p>
            <ul>
                <?php foreach($query->result() as $rows): ?>
                    <li><a href="<?php echo site_url('event/view/'.$rows->event_id.'/'.url_title($rows->event_title));?>"><?php echo $rows->event_title;?> oleh 
                        <?php if($rows->usertype_id==1)
                                echo $rows->userprofile_firstname.' '.$rows->userprofile_lastname;
                            else
                                echo $rows->userprofile_companyname;?></a> | <?php echo ucfirst(strtolower($rows->city_name));?>, <?php echo mdate('%d %M %Y',$rows->event_start_date);?></li>

                <?php endforeach;?>
            </ul>
            <p>Anda Menerima Email ini karena Anda terdaftar sebagai member di HRPlasa.id. Jika anda mempunyai pertanyaan, bisa menghubungi email kami di support@hrplasa.id</p>
        </div>
        
        <?php $this->load->view('mailing_view/mail_footer');?>
	</body>
</html>