<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>
	</head>
	<body>
		<p>Hi <?php echo $arraybid['name'];?>,</p>
		<p>Berikut Adalah tagihan Project <?php echo $arraybid['project_name'];?>.</p>
		<h2>Invoice Info</h2>
		<table>
			<thead>
				<tr><th>Invoice No</th><th>Date</th><th>Due Date</th><th>Total</th></tr>
			</thead>
			<tbody>
				<?php foreach($arraybid['querybilling']->result() as $rowsbilling): ?>
					<tr>
						<td><?php echo $rowsbilling->billing_id;?></td>
						<td><?php echo mdate('%d-%m-%Y',$rowsbilling->billing_date);?></td>
						<td><?php echo mdate('%d-%m-%Y',$rowsbilling->billing_due);?></td>
						<td>
							<?php 
							$querydetbilling=$this->userproject_model->get_detail_billing($rowsbilling->billing_id);
							$total_billing=0;
							foreach($querydetbilling->result() as $rowsdetbilling)
							{
								$total_billing=$total_billing+$rowsdetbilling->billingdetail_total;
							}
							echo number_format($total_billing,0,',','.');
							?>
						</td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<h2>Invoice Detail</h2>

		<table>
		<thead>
			<tr><th>Keterangan</th><th><div align="right">Total</div></th></tr>
		</thead>
		<tbody>
			<?php foreach($arraybid['querydetbilling']->result() as $rowsdet): ?>
				<tr>
					<td><?php echo $rowsdet->billingdetail_info;?></td>
					<td><div align="right"><?php echo number_format($rowsdet->billingdetail_total);?></div></td>
				</tr>
			<?php endforeach;?>
		</tbody>
	</table>
		<p>Salam, HRplasa.id</p>
	</body>
</html>