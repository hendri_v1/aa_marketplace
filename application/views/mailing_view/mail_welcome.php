<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>
	</head>
	<body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
    	<?php $this->load->view('mailing_view/mail_header');?>
    	<div style="padding:20px;">
        	
            <br />
            <p>Terima kasih telah mendaftarkan diri Anda di hrplasa.id</p>
            <p>Nama Anda terdaftar sebagai <?php echo $user_meta->userprofile_firstname;?> <?php echo $user_meta->userprofile_lastname;?>, proses registrasi Anda telah kami terima dan akun Anda telah kami aktifkan dengan detail sebagai berikut :</p>
            <table>
                <tr><td>Username</td><td>: <?php echo $user_meta->username;?></td></tr>
                <tr><td>Email</td><td>: <?php echo $user_meta->user_email;?></td></tr>
            </table>
            
            <p>HRplasa.id menyoroti berita, artikel, dan praktik terbaik dalam industri Pengembangan Sumber Daya Manusia (Human Resources Development). Fokus kami adalah membantu para professional, individu dan organisasi mendapatkan informasi terkini, wawasan dan referensi yang diperlukan untuk lebih efektif dalam pengelolaan pengembangan sumber daya manusia.</p>
            <p>Jika anda mempunyai pertanyaan dan masalah, bisa menghubungi email kami di support@hrplasa.id</p>
            
            
            
        </div>
        <?php $this->load->view('mailing_view/mail_footer');?>
	</body>
</html>