<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Billing Email</title>
	</head>
	<body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
    	<?php $this->load->view('mailing_view/mail_header');?>
    	<div style="padding:20px;">
            <p>Hi <?php echo $the_user; ?>,</p>
           	<table style="width:80%;">
         		<tr>
                	<td valign="top"><div style="vertical-align:top;">Invoice No : HRP-<?php echo $bill_meta->billing_id;?><br /><?php echo $the_title;?></div></td>
                    <td><div style="text-align:right;">
                    	Diterbitkan Oleh : <br />
                    	PT. Wijawiyata Prasadha<br />
                        Graha Positif, Jl.pasar Jumat No.44a <br />
                        Pondok Pinang Jakarta Selatan 12310
                        NPWP : 315591198411000
                        </div>
                    </td>
                </tr>
                <tr><td colspan="2"><hr /></td></tr> 
                <tr>
                	<td valign="top">
                    	Billed To :<br />
                        <?php echo $the_user; ?>
                        
                    </td>
                    <td valign="top">
                    	<div align="right">
                        	Tanggal Diterbitkan : <?php echo mdate('%d %M %Y',$bill_meta->billing_date);?><br />
                            Jatuh Tempo : <?php echo mdate('%d %M %Y',$bill_meta->billing_due);?>
                        </div>
                    </td>
                </tr>
         	</table>
            
            <table style="width:80%;">
            	<tr><td colspan="3"><hr /></td></tr>
            	<tr>
	            	<td>No</td><td>Description</td><td><div align="right">Total</div></td>
                </tr>
                <?php
					$totalbill=0;
					$waktu=time();
					$uniquecode=substr($waktu,-3);
					$qbdetail=$this->billing_model->get_detail_billing_by_id($bill_meta->billing_id);
					$i=0; foreach($qbdetail->result() as $rsdetail): $i++;
					$totalbill=$totalbill+$rsdetail->billingdetail_total;	
				 ?>
                <tr>
                	<td><?php echo $i;?></td>
                    <td><?php echo $rsdetail->billingdetail_info;?></td>
                    <td><div align="right"><?php echo number_format($rsdetail->billingdetail_total,0,',','.');?></div></td>
                </tr>
                <?php endforeach;?>
                <tr>
                	<td><?php echo $i+1;?></td>
                    <td>Kode Unik *</td>
                    <td><div align="right"><?php echo $uniquecode;?></div></td> 
                </tr>
                <?php $totalbill=$totalbill+$uniquecode;?>
                <tr><td colspan="3"><hr /></td></tr>
                <tr><td colspan="2"><div align="right">Total (include PPN 10%)</div></td><td><div align="right"><strong><?php echo number_format($totalbill,0,',','.');?></strong></div></td></tr>
                
            </table>
        	<p>Pembayaran dilakukan ke : 
            <br />
            PT.Wijawiyata Prasadha<br />
            BNI KCP Pasar Mayestik<br />
            Jakarta Selatan<br />
            Nomor Rekening 2727271110
            </p>   
            <p>Mohon me-reply email ini untuk konfirmasi pembayaran Anda</p>
            <p style="font-size:small">* Kode Unik kami gunakan untuk mengidentifikasi pembayaran Anda</p>
        </div>
        
        <?php $this->load->view('mailing_view/mail_footer');?>
	</body>
</html>