<!DOCTYPE html>
<html lang="">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Title Page</title>
</head>

<body style="color:#333;line-height:1.6;background-color:#F2F2F2;margin:0;padding:0;">
	<?php $this->load->view('mailing_view/mail_header');?>
		<div style="padding:20px;">

			<p>Hi
				<?php echo $venue_meta->userprofile_firstname;?>,</p>
			<p>Ada proposal Request untuk Room <?php echo $room->venue_name;?> di <?php echo $venue_meta->venue_main_name;?></p>
			<p>Tim HRPlasa akan mengelola Proposal Request ini dan akan menghubungi Anda dalam waktu maksimal 24 jam setelah proposal request ini diterima.</p>
				<p>Jika anda mempunyai pertanyaan dan masalah, bisa menghubungi email kami di support@hrplasa.id</p>



		</div>
		<?php $this->load->view('mailing_view/mail_footer');?>
</body>

</html>