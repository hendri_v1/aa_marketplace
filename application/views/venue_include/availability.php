<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<a href="<?php echo site_url('my-venue');?>" class="btn btn-sm btn-primary">Kembali Ke Daftar Venue</a>
			<hr />
	    	<?php foreach($query->result() as $rows): ?>
	    		<div class="col-sm-4">
		    		<div class="boxes">
		    			<h4><?php echo $rows->venue_name;?></h4>
		    			<span class="small">Klik pada tanggal untuk membuat tidak available, klik kembali untuk membuat tanggal tersebut available</span>	
		    			<hr />
		    			<div class="row">
		    				<div class="col-sm-12">
		    					<div class="calendar_view" data-venue_id="<?php echo $rows->venue_id;?>">
		    						Memuat Kalender ...
		    					</div>
		    				</div>
		    			</div>
		    		</div>
	    		</div>
	    	<?php endforeach;?>
	    </div>
	</div>
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.calendar_view').each(function(){
			venue_id=$(this).data('venue_id');
			$(this).load('<?php echo site_url('venue/my_venue/venue_calendar');?>/'+venue_id);
		});

		$(document).on("click", '.prev_cal, .next_cal', function(event) { 
		    the_url=$(this).data('urlto');
		    the_parent=$(this).closest(".calendar_view");
		    $(the_parent).fadeTo("fast" , 0.8,function(){
			    $(the_parent).load(the_url,function(){
			    	$(this).fadeTo("fast",1);
			    });
			});
		});

		$(document).on('click', '.make_book',function(event){
			the_url=$(this).data('urlto');
			the_parent=$(this).closest(".calendar_view");
			the_date=$(this).data('thedate');
			$.post(the_url,
			{
				thedate:the_date
			},function(data){
				$(the_parent).load(data)
			});
		})

		$(document).on('click', '.remove_book',function(event){
			the_url=$(this).data('urlto');
			the_parent=$(this).closest(".calendar_view");
			to_remove=$(this).data('toremove');
			$.post(the_url,
			{
				venue_booking_id:to_remove
			},function(data){
				$(the_parent).load(data)
			});
		})
	})
</script>