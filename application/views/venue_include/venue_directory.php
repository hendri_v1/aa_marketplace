<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner">
			<div class="container">
				<div class="row">
					<div class="document-title">
				        <h1>Direktori Venue</h1>

				        <ul class="breadcrumb">
				            <li><a href="#">HRPlasa</a></li>
				            <li><a href="#">Venue</a></li>
				        </ul>
				    </div><!-- /.document-title -->
				    <form class="filter" method="get" action="">
					    <div class="row">
					        <div class="col-sm-12 col-md-4">
					            <div class="form-group">
					                <input type="text" placeholder="Keyword" class="form-control">
					            </div><!-- /.form-group -->
					        </div><!-- /.col-* -->

					        <div class="col-sm-12 col-md-4">
					            <?php
			                      $array_province = array();
			                      $array_province[0] = "Semua Provinsi";
			                      foreach ($province as $rowsprovince) {
			                          $array_province[$rowsprovince->province_id] = $rowsprovince->province_name;
			                      }
			                      echo bt_group_select('province_id', $array_province, $this->input->get('province_id'), 'fa fa-bullseye');
			                      ?>
					        </div><!-- /.col-* -->

					        <div class="col-sm-12 col-md-4">
					            <div class="form-group">
					                <select class="form-control" title="Select Category" name="venue_types">
					                    <?php foreach($qvenuetypes->result() as $rvenuetypes): ?>
											<option value="<?php echo $rvenuetypes->venue_type_id;?>"><?php echo $rvenuetypes->venue_type_name;?></option>
					                    <?php endforeach;?>
					                </select>
					            </div><!-- /.form-group -->
					        </div><!-- /.col-* -->
					    </div><!-- /.row -->

					    <hr>

					    <div class="row">
					        <div class="col-sm-8">
					            <div class="filter-actions">
					                <a href="#"><i class="fa fa-close"></i> Reset Filter</a>
					                <a href="#"><i class="fa fa-save"></i> Save Search</a>
					            </div><!-- /.filter-actions -->
					        </div><!-- /.col-* -->

					        <div class="col-sm-4">
					            <button type="submit" class="btn btn-primary">Redefine Search Result</button>
					        </div><!-- /.col-* -->
					    </div><!-- /.row -->
					</form>
					<h2 class="page-title">
					    <?php echo $query->num_rows();?> Venue Di List

					    <form method="get" action="?" class="filter-sort">
					        <div class="form-group">
					            <select class="form-control" name="sorting_by">
					                <option name="price">Harga</option>
					                <option name="rating">Rating</option>
					                <option name="title">Judul</option>
					            </select>
					        </div><!-- /.form-group -->

					        <div class="form-group">
					            <select class="form-control" name="sorting_by_sort">
					                <option name="ASC">Asc</option>
					                <option name="DESC">Desc</option>
					            </select>
					        </div><!-- /.form-group -->
					    </form>

					</h2><!-- /.page-title -->
					<div class="cards-wrapper">
					    <div class="row">
					    	<div class="col-sm-12">
					    		<div class="row">
						    		<?php foreach($query->result() as $rows): ?>
										<div class="col-sm-4">
											<?php $split_filename=explode('.', $rows->venue_main_image);?>
						                    <div class="card" data-background-image="<?php echo upload_path.'uploads/project_images/'.$split_filename[0].'_thumb.'.$split_filename[1];?>">
						                       
						                        <div class="card-content">
						                            <h2><a href="listing-detail.html"><?php echo $rows->venue_main_name;?></a></h2>

						                            <div class="card-meta">
						                                <i class="fa fa-map-o"></i> <?php echo $rows->venue_main_address;?>, <?php echo $rows->city_name;?>
						                            </div><!-- /.card-meta -->

						                            <div class="card-rating">
						                               Start Rp. <?php echo number_format($rows->min_price,0,',','.');?> / Pax
						                            </div><!-- /.card-rating -->

						                            <div class="card-actions">
						                                <a href="#" class="fa fa-bookmark-o"></a>
						                                <a href="<?php echo site_url('venue/view/'.$rows->venue_main_id.'/'.url_title(strtolower($rows->venue_main_name)));?>" class="fa fa-search"></a>
						                                <a href="#" class="fa fa-heart-o"></a>
						                            </div>
						                        </div><!-- /.card-content -->
						                    </div><!-- /.card -->
						                </div>
						            <?php endforeach;?>
						        </div>
					    	</div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('html_footer');?>