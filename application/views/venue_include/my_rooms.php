<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<a href="<?php echo site_url('venue/my_venue/add_new_room/'.$row->venue_main_id);?>" class="btn btn-warning btn-sm" title="Tambah Room"><i class="fa fa-plus"></i> Tambah</a>
			<hr />
			<table class="table table-striped table-hover">
				<thead>
					<tr class="info">
						<th>#ID</th><th>Nama Room</th><th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($query->result() as $rows): ?>
						<tr>
							<td><?php echo $rows->venue_id;?></td>
							<td><?php echo $rows->venue_name;?></td>
							<td>
								<a href="<?php echo site_url('venue/my_venue/edit_room/'.$rows->venue_id);?>" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
								<a href="#" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Hapus</a>
							</td>
						</tr>

					<?php endforeach;?>
				</tbody>
			</table>
		</div>
	</div>
<?php $this->load->view('member_include/html_footer');?>