<?php $this->load->view('html_head');?>
	<div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="mt-80 mb80">
				    <div class="detail-banner" style="background-image: url(<?php echo upload_path;?>uploads/project_images/<?php echo $row->venue_main_image;?>);">
					    <div class="container">
					        <div class="detail-banner-left">
					            <div class="detail-banner-info">
					                <div class="detail-label">Restaurant</div>
					                <div class="detail-verified">Verified</div>
					            </div><!-- /.detail-banner-info -->

					            <h2 class="detail-title">
					                <?php echo $row->venue_main_name;?>
					            </h2>

					            <div class="detail-banner-address">
					                <i class="fa fa-map-o"></i> <?php echo $row->venue_main_address;?>, <?php echo $row->city_name;?> <?php echo $row->province_name;?>
					            </div><!-- /.detail-banner-address -->

					            <div class="detail-banner-rating">
					                <i class="fa fa-star"></i>
					                <i class="fa fa-star"></i>
					                <i class="fa fa-star"></i>
					                <i class="fa fa-star"></i>
					                <i class="fa fa-star-half-o"></i>
					            </div><!-- /.detail-banner-rating -->

					            <div class="detail-banner-btn bookmark">
					                <i class="fa fa-bookmark-o"></i> <span data-toggle="Bookmarked">Bookmark</span>
					            </div><!-- /.detail-claim -->

					            <div class="detail-banner-btn heart">
					                <i class="fa fa-heart-o"></i> <span data-toggle="I Love It">Give Heart</span>
					            </div><!-- /.detail-claim -->

					        </div><!-- /.detail-banner-left -->
					    </div><!-- /.container -->
					</div><!-- /.detail-banner -->

				</div>

<div class="container">
    <div class="row detail-content">
    <div class="col-sm-7">
        <div class="detail-gallery">
            <div class="detail-gallery-preview">
                <a href="<?php echo upload_path;?>uploads/project_images/<?php echo $row->venue_main_image;?>">
                    <img src="<?php echo upload_path;?>uploads/project_images/<?php echo $row->venue_main_image;?>">
                </a>
            </div>

            <ul class="detail-gallery-index">
            	<?php foreach($qgallery->result() as $rows): ?>
	                <li class="detail-gallery-list-item active">
	                    <a data-target="<?php echo upload_path;?>uploads/project_images/<?php echo $rows->venue_gallery_file;?>">
	                        <img src="<?php echo upload_path;?>uploads/project_images/<?php echo $rows->venue_gallery_file;?>" alt="...">
	                    </a>
	                </li>
	            <?php endforeach;?>
                
            </ul>
        </div><!-- /.detail-gallery -->

        <h2>We Are Here</h2>
        <div class="background-white p20">

            <!-- Nav tabs -->
            <ul id="listing-detail-location" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#simple-map-panel" aria-controls="simple-map-panel" role="tab" data-toggle="tab">
                        <i class="fa fa-map"></i>Map
                    </a>
                </li>
                <li role="presentation">
                    <a href="#street-view-panel" aria-controls="street-view-panel" role="tab" data-toggle="tab">
                        <i class="fa fa-street-view"></i>Street View
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="simple-map-panel">
                    <div class="detail-map">
                        <div class="map-position">
                        <?php $latlng = explode(',', $row->venue_main_geo); ?>
                            <div id="listing-detail-map"
                                 data-transparent-marker-image="assets/img/transparent-marker-image.png"
                                 data-styles='[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"labels.text.fill","stylers":[{"color":"#b43b3b"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"lightness":"8"},{"color":"#bcbec0"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#5b5b5b"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#7cb3c9"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#abb9c0"}]},{"featureType":"water","elementType":"labels.text","stylers":[{"color":"#fff1f1"},{"visibility":"off"}]}]'
                                 data-zoom="15"
                                 data-latitude="<?php echo $latlng[0];?>"
                                 data-longitude="<?php echo $latlng[1];?>"
                                 data-icon="fa fa-map-marker">
                            </div><!-- /#map-property -->
                        </div><!-- /.map-property -->
                    </div><!-- /.detail-map -->
                </div>
                <div role="tabpanel" class="tab-pane fade" id="street-view-panel">
                    <div id="listing-detail-street-view"
                            data-latitude="<?php echo $latlng[0];?>"
                            data-longitude="<?php echo $latlng[1];?>"
                            data-heading="225"
                            data-pitch="0"
                            data-zoom="1">
                    </div>
                </div>
            </div>
        </div>
        

        <h2 id="reviews">Rooms</h2>
        <?php foreach($qroom->result() as $rows):?>
			<div class="background-white p20 mb30">
				<h4>Ruangan <?php echo $rows->venue_name;?></h4>
				<span>Mulai Dari Rp. <?php echo number_format($rows->venue_price,0,',','.');?></span> / orang
				<?php if($rows->venue_price_type==0) echo "Half Day"; else echo "Full Day";?>
				<br />
				<i class="glyphicon glyphicon-stats"></i>
				<?php echo $rows->venue_capacity;?> orang
				<?php echo $rows->venue_description;?>
				<?php $venue_facility_list=array();?>
				<hr />
				<h4>Fasilitas :</h4>
				<ul class="tabling">
					<?php 
						$qf1=$this->venue_model->get_facility_connector($rows->venue_id,1);
						foreach($qf1->result() as $rf1): ?>
						<li>
							<i class="fa fa-check-circle-o"></i> <?php echo $rf1->venue_facility_name;?>
							<?php $venue_facility_list[]=$rf1->venue_facility_name;?>
						</li>
					<?php endforeach;?>
				</ul>
				<h4>Fasilitas Tambahan :</h4>
				<ul class="tabling">
					<?php
						$qf1=$this->venue_model->get_facility_connector($rows->venue_id,2);
						foreach($qf1->result() as $rf1): ?>
						<li>
							<i class="fa fa-check-circle-o"></i> <?php echo $rf1->venue_facility_name;?>
							<?php $venue_facility_list[]=$rf1->venue_facility_name;?>
						</li>
					<?php endforeach;?>
				</ul>
				<hr />
				<h4>Room Configuration :</h4>
				<div class="row text-center room_config_list">
					<div class="col-sm-2">
						<div class="r_c u_shape <?php if($rows->venue_u_shape>0) echo 'active'; else echo 'inactive';?>" title="U Shape"></div>
						<?php if($rows->venue_u_shape>0) echo $rows->venue_u_shape;?>
					</div>
					<div class="col-sm-2">
						<div class="r_c boardroom <?php if($rows->venue_boardroom>0) echo 'active'; else echo 'inactive';?>" title="BoardRoom"></div>
						<?php if($rows->venue_boardroom>0) echo $rows->venue_boardroom;?>
					</div>
					<div class="col-sm-2">
						<div class="r_c classroom <?php if($rows->venue_classroom>0) echo 'active'; else echo 'inactive';?>" title="ClassRoom"></div>
						<?php if($rows->venue_classroom>0) echo $rows->venue_classroom;?>
					</div>
					<div class="col-sm-2">
						<div class="r_c reception <?php if($rows->venue_reception>0) echo 'active'; else echo 'inactive';?>" title="Reception"></div>
						<?php if($rows->venue_reception>0) echo $rows->venue_reception;?>
					</div>
					<div class="col-sm-2">
						<div class="r_c banquet <?php if($rows->venue_banquet>0) echo 'active'; else echo 'inactive';?>" title="Banquet"></div>
						<?php if($rows->venue_banquet>0) echo $rows->venue_banquet;?>
					</div>
					<div class="col-sm-2">
						<div class="r_c theatre <?php if($rows->venue_theatre>0) echo 'active'; else echo 'inactive';?>" title="Theatre"></div>
						<?php if($rows->venue_theatre>0) echo $rows->venue_theatre;?>
					</div>
				</div>
				<hr />
				<h4>Availability</h4>
				<div class="boxes calendar_date" data-venue_id="<?php echo $rows->venue_id;?>">

				</div>
				<hr />
				<?php 
            		$url_booking='';
            		if($this->session->userdata('logged'))
            			$url_booking=site_url('venue/venue/booking/'.$rows->venue_id);
            		else
            			$url_booking=site_url('login?redirect_to='.site_url('venue/venue/booking/'.$rows->venue_id));
            	?>
				<?php /*<a href="<?php echo $url_booking;?>" class="btn btn-warning btn-block"><i class="fa fa-book"></i> Buat Booking</a> */ ?>
				<a href="<?php echo $url_booking;?>" class="btn btn-danger btn-block"><i class="fa fa-file-text-o"></i> Request Proposal</a>

			</div>
        <?php endforeach;?>

    </div><!-- /.col-sm-7 -->

    <div class="col-sm-5">

        <div class="background-white p20">
            <div class="detail-overview-hearts">
                <i class="fa fa-heart"></i> <strong>0 </strong>people love it
            </div>
            <div class="detail-overview-rating">
                <i class="fa fa-star"></i> <strong>0 / 5 </strong>dari <a href="#reviews">0 reviews</a>
            </div>
           
            <?php foreach($qroom->result() as $rows):?>
				<h4><?php echo $rows->venue_name;?> mulai dari Rp. <?php echo number_format($rows->venue_price,0,',','.');?></h4>
            <?php endforeach;?>

            <div class="detail-actions row">
                
                <div class="col-sm-4">
                    <div class="btn btn-secondary btn-share"><i class="fa fa-share-square-o"></i> Share
                        <div class="share-wrapper">
                            <ul class="share">
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(uri_string()); ?>"><i class="fa fa-facebook"></i> Facebook</a></li>
                                <li><a href="http://twitter.com/home?status=<?php echo base_url(uri_string()); ?>"><i class="fa fa-twitter"></i> Twitter</a></li>
                                <li><a href="https://plus.google.com/share?url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-google-plus"></i> Google+</a></li>
                                
                                <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo base_url(uri_string()); ?>"><i class="fa fa-linkedin"></i> Linkedin</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-sm-4 -->
                
            </div><!-- /.detail-actions -->
        </div>

        <h2>Tentang <span class="text-secondary"><?php echo $row->venue_main_name;?></span></h2>
        <div class="background-white p20">
            <div class="detail-vcard">
                <div class="detail-logo">
                    <img src="<?php echo upload_path; ?>uploads/user_images/<?php echo $this->global_model->get_user_photo($row->userprofile_photo); ?>">
                </div><!-- /.detail-logo -->

                <div class="detail-contact">
                    <div class="detail-contact-email">
                        <i class="fa fa-envelope-o"></i> <a href="mailto:#"><?php echo $row->user_email;?></a>
                    </div>
                    <div class="detail-contact-phone">
                        <i class="fa fa-mobile-phone"></i> <a href="tel:#"><?php echo $row->venue_main_contact_phone;?></a>
                    </div>
                    <div class="detail-contact-website">
                        <i class="fa fa-globe"></i> <a href="#">www.superlist.com</a>
                    </div>
                    <div class="detail-contact-address">
                        <i class="fa fa-map-o"></i>
                        <?php echo $row->venue_main_address;?><br>
                        <?php echo $row->city_name;?>, <?php echo $row->province_name;?>
                    </div>
                </div><!-- /.detail-contact -->
            </div><!-- /.detail-vcard -->

            <div class="detail-description">
                <?php echo $row->venue_main_description;?>
            </div>

            <div class="detail-follow">
                <h5>Follow Us:</h5>
                <div class="follow-wrapper">
                    <a class="follow-btn facebook" href="#"><i class="fa fa-facebook"></i></a>
                    <a class="follow-btn youtube" href="#"><i class="fa fa-youtube"></i></a>
                    <a class="follow-btn twitter" href="#"><i class="fa fa-twitter"></i></a>
                    <a class="follow-btn tripadvisor" href="#"><i class="fa fa-tripadvisor"></i></a>
                    <a class="follow-btn google-plus" href="#"><i class="fa fa-google-plus"></i></a>
                </div><!-- /.follow-wrapper -->
            </div><!-- /.detail-follow -->
        </div>

        <div class="widget">
    <h2 class="widgettitle">Working Hours</h2>

    <div class="p20 background-white">
        <div class="working-hours">
    		<div class="day clearfix">
    			<span class="name">Mon</span><span class="hours">07:00 AM - 07:00 PM</span>
    		</div><!-- /.day -->

    		<div class="day clearfix">
    			<span class="name">Tue</span><span class="hours">07:00 AM - 07:00 PM</span>
    		</div><!-- /.day -->

    		<div class="day clearfix">
    			<span class="name">Wed</span><span class="hours">07:00 AM - 07:00 PM</span>
    		</div><!-- /.day -->

    		<div class="day clearfix">
    			<span class="name">Thu</span><span class="hours">07:00 AM - 07:00 PM</span>
    		</div><!-- /.day -->

    		<div class="day clearfix">
    			<span class="name">Fri</span><span class="hours">07:00 AM - 07:00 PM</span>
    		</div><!-- /.day -->

    		<div class="day clearfix">
    			<span class="name">Sat</span><span class="hours">07:00 AM - 02:00 PM</span>
    		</div><!-- /.day -->

    		<div class="day clearfix">
    			<span class="name">Sun</span><span class="hours">Closed</span>
    		</div><!-- /.day -->
    	</div>
    </div>
</div><!-- /.widget -->


        <h2>Amenities</h2>

        <div class="background-white p20">
            <ul class="detail-amenities">
                <li class="yes">WiFi</li>
                <li class="yes">Parking</li>
                <li class="no">Vine</li>
                <li class="yes">Terrace</li>
                <li class="no">Bar</li>
                <li class="yes">Take Away Coffee</li>
                <li class="no">Catering</li>
                <li class="yes">Raw Food</li>
                <li class="no">Delivery</li>
                <li class="yes">No-smoking room</li>
                <li class="no">Reservations</li>
            </ul>
        </div><!-- /.detail-amenities -->

        <h2>Tanya HRPlasa tentang venue ini</h2>
        <script type="text/javascript" src="http://assets.freshdesk.com/widget/freshwidget.js"></script>
		<style type="text/css" media="screen, projection">
		    @import url(http://assets.freshdesk.com/widget/freshwidget.css); 
		</style> 
		<iframe title="Feedback Form" class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://hrplasa.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&formTitle=Hubungi+HRPLasa&submitThanks=Terima+Kasih+Telah+Menghubungi+HRPlasa&screenshot=no&searchArea=no" scrolling="no" height="400px" width="100%" frameborder="0" >
		</iframe>
		<?php /*
        <h2>Enquire Form</h2>

        <div class="detail-enquire-form background-white p20">
            <form method="post" action="?">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" class="form-control" name="" id="">
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label for="">Email <span class="required">*</span></label>
                    <input type="email" class="form-control" name="" id="" required>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label for="">Message <span class="required">*</span></label>
                    <textarea class="form-control" name="" id="" rows="5" required></textarea>
                </div><!-- /.form-group -->

                <p>Required fields are marked <span class="required">*</span></p>

                <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-paper-plane"></i>Send Message</button>
            </form>
        </div><!-- /.detail-enquire-form -->
		*/ ?>
        <h2>3 Reasons Why Choose Us</h2>

        <div class="background-white p20 reasons">
            <div class="reason">
                <div class="reason-icon">
                    <i class="fa fa-trophy"></i>
                </div><!-- /.reason-icon -->
                <div class="reason-content">
                    <h4>Coffee House of the Year 2015</h4>
                    <p>Fusce at venenatis lorem. Quisque volutpat aliquam leo, a pellentesque orci varius sit amet.</p>
                </div><!-- /.reason-content -->
            </div><!-- /.reason -->
            <div class="reason">
                <div class="reason-icon">
                    <i class="fa fa-coffee"></i>
                </div><!-- /.reason-icon -->
                <div class="reason-content">
                    <h4>High Quality Coffee Beans</h4>
                    <p>Fusce at venenatis lorem. Quisque volutpat aliquam leo, a pellentesque orci varius sit amet.</p>
                </div><!-- /.reason-content -->
            </div><!-- /.reason -->
            <div class="reason">
                <div class="reason-icon">
                    <i class="fa fa-cutlery"></i>
                </div><!-- /.reason-icon -->
                <div class="reason-content">
                    <h4>Snacks & Deserts</h4>
                    <p>Fusce at venenatis lorem. Quisque volutpat aliquam leo, a pellentesque orci varius sit amet.</p>
                </div><!-- /.reason-content -->
            </div><!-- /.reason -->
        </div>

        <div class="detail-payments">
            <h3>Accepted Payments</h3>
            
            <ul>
                <li><a href="#"><i class="fa fa-paypal"></i></a></li>
    			<li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
                <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
    			<li><a href="#"><i class="fa fa-cc-stripe"></i></a></li>
                <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
    		</ul>
        </div>
    </div><!-- /.col-sm-5 -->
	<?php /*
    <div class="col-sm-12">
        <h2>Submit a Review</h2>

        <form class="background-white p20 add-review" method="post" action="?">
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Name <span class="required">*</span></label>
                    <input type="text" class="form-control" id="" required>
                </div><!-- /.col-sm-6 -->

                <div class="form-group col-sm-6">
                    <label for="">Email <span class="required">*</span></label>
                    <input type="email" class="form-control" id="" required>
                </div><!-- /.col-sm-6 -->
            </div><!-- /.row -->

            <div class="row">
                <div class="form-group input-rating col-sm-3">

                    <div class="rating-title">Food</div>

                    <input type="radio" value="1" name="food" id="rating-food-1">
                    <label for="rating-food-1"></label>
                    <input type="radio" value="2" name="food" id="rating-food-2">
                    <label for="rating-food-2"></label>
                    <input type="radio" value="3" name="food" id="rating-food-3">
                    <label for="rating-food-3"></label>
                    <input type="radio" value="4" name="food" id="rating-food-4">
                    <label for="rating-food-4"></label>
                    <input type="radio" value="5" name="food" id="rating-food-5">
                    <label for="rating-food-5"></label>

                </div><!-- /.col-sm-3 -->
                <div class="form-group input-rating col-sm-3">

                    <div class="rating-title">Staff</div>

                    <input type="radio" value="1" name="staff" id="rating-staff-1">
                    <label for="rating-staff-1"></label>
                    <input type="radio" value="2" name="staff" id="rating-staff-2">
                    <label for="rating-staff-2"></label>
                    <input type="radio" value="3" name="staff" id="rating-staff-3">
                    <label for="rating-staff-3"></label>
                    <input type="radio" value="4" name="staff" id="rating-staff-4">
                    <label for="rating-staff-4"></label>
                    <input type="radio" value="5" name="staff" id="rating-staff-5">
                    <label for="rating-staff-5"></label>

                </div><!-- /.col-sm-3 -->
                <div class="form-group input-rating col-sm-3">

                    <div class="rating-title">Value</div>

                    <input type="radio" value="1" name="value" id="rating-value-1">
                    <label for="rating-value-1"></label>
                    <input type="radio" value="2" name="value" id="rating-value-2">
                    <label for="rating-value-2"></label>
                    <input type="radio" value="3" name="value" id="rating-value-3">
                    <label for="rating-value-3"></label>
                    <input type="radio" value="4" name="value" id="rating-value-4">
                    <label for="rating-value-4"></label>
                    <input type="radio" value="5" name="value" id="rating-value-5">
                    <label for="rating-value-5"></label>

                </div><!-- /.col-sm-3 -->
                <div class="form-group input-rating col-sm-3">

                    <div class="rating-title">Atmosphere</div>

                    <input type="radio" value="1" name="atmosphere" id="rating-atmosphere-1">
                    <label for="rating-atmosphere-1"></label>
                    <input type="radio" value="2" name="atmosphere" id="rating-atmosphere-2">
                    <label for="rating-atmosphere-2"></label>
                    <input type="radio" value="3" name="atmosphere" id="rating-atmosphere-3">
                    <label for="rating-atmosphere-3"></label>
                    <input type="radio" value="4" name="atmosphere" id="rating-atmosphere-4">
                    <label for="rating-atmosphere-4"></label>
                    <input type="radio" value="5" name="atmosphere" id="rating-atmosphere-5">
                    <label for="rating-atmosphere-5"></label>

                </div><!-- /.col-sm-3 -->
            </div><!-- /.row -->

            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="">Pros</label>
                    <textarea class="form-control" rows="5" id=""></textarea>
                </div><!-- /.col-sm-6 -->
                <div class="form-group col-sm-6">
                    <label for="">Cons</label>
                    <textarea class="form-control" rows="5" id=""></textarea>
                </div><!-- /.col-sm-6 -->

                <div class="col-sm-8">
                    <p>Required fields are marked <span class="required">*</span></p>
                </div><!-- /.col-sm-8 -->
                <div class="col-sm-4">
                    <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-star"></i>Submit Review</button>
                </div><!-- /.col-sm-4 -->
            </div><!-- /.row -->
        </form>
    </div><!-- /.col-* --> */?>
</div><!-- /.row -->

</div><!-- /.container -->

            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

<?php $this->load->view('html_footer');?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.calendar_date').each(function(){
    		venue_id=$(this).data('venue_id');
    		$(this).load('<?php echo site_url('venue/venue/venue_calendar');?>/'+venue_id);
    	})

    	$(document).on("click", '.prev_cal, .next_cal', function(event) { 
		    the_url=$(this).data('urlto');
		    the_parent=$(this).closest(".calendar_date");
		    $(the_parent).load(the_url);
		});

		function PopupCenter(url, title, w, h) {
            // Fixes dual-screen position                         Most browsers      Firefox
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            // Puts focus on the newWindow
            if (window.focus) {
                newWindow.focus();
            }


        }
        $('.share a').click(function(e){
            e.preventDefault();
            the_link=$(this).attr('href');
            PopupCenter(the_link, "", 500,400);
        })
	})
</script>