<?php $this->load->view('html_head');?>
	<div class="main">
		<div class="main-inner">
			<div class="content">
				<div class="container">
					<div class="page-title">
						<h3><span>Booking Room Pada Venue <?php echo $row->venue_main_name;?></span></h3>
						Request Proposal di <?php echo $row->venue_name;?>
					</div>
					<?php echo form_open('venue/venue/save_booking');?>
               			<div class="col-sm-12">
               				<div class="row">
               					<div class="col-sm-8">
		               				Silahkan Pilih Tanggal Yang Berwarna Hijau
		               				<hr />
		               				<div id="booking_calendar">

		               				</div>	
		               				<input type="hidden" name="venue_id" value="<?php echo $row->venue_id;?>">
		               			</div>
		               			<div id="booked_date" class="col-sm-4">

	               				</div>

               				</div>
                            
               				
               			</div>
               			<div class="col-sm-4">
               				Detail Kontak Berikut Yang Akan Kami Gunakan Untuk Menghubungi Anda Kembali :  
               				<hr />
               				<?php echo bt_text('userprofile_firstname','Nama Depan','',$this->session->userdata('user_meta')->userprofile_firstname);?>
               				<?php echo bt_text('userprofile_lastname','Nama Belakang','',$this->session->userdata('user_meta')->userprofile_lastname);?>
               				<?php echo bt_text('userprofile_phone','Telepon','',$this->session->userdata('user_meta')->userprofile_phone);?>
               				<?php echo bt_text('user_email','Email','',$this->session->userdata('user_meta')->user_email);?>
               				<span class="small">* Ganti data diatas apabila Anda ingin mengupdate data Anda</span>
               			</div>
               			<div class="col-sm-8">
               				
	               			<div class="row">
	               				<div class="col-sm-6">
			               			<?php echo bt_text('proposal_audience','Jumlah Peserta');?>
			               		</div>
			               		<div class="col-sm-12">
	    	           				<?php echo bt_textarea('proposal_description','Detail Event Yang Ingin Diselenggarakan');?>
	    	           			</div>
	    	           			<div class="col-sm-12">
	    	           				<?php echo bt_textarea('proposal_additional','Fasilitas / Package yang di Inginkan');?>
	    	           			</div>
	    	           		</div>	
	    	           		<input type="submit" class="btn btn-primary btn-block btn-lg" value="Simpan Request">
               			</div>
           			<?php form_close();?>
           		</div>
           	</div>
        </div>
    </div>

<?php $this->load->view('html_footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var config = {
            toolbar:
            [
                ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Undo', 'Redo', '-', 'SelectAll'],
                ['UIColor']
            ]
        };
    	$('#booking_calendar').load('<?php echo site_url('venue/venue/venue_calendar_booking/'.$row->venue_id);?>');
    	$(document).on("click", '.prev_cal, .next_cal', function(event) { 
		    the_url=$(this).data('urlto');
		    the_parent=$(this).closest("#booking_calendar");
		    $(the_parent).load(the_url);
		});

    	$(document).on('click', '.make_book',function(event){
			the_url=$(this).data('urlto');
			the_parent=$(this).closest("#booking_calendar");
			the_date=$(this).data('thedate');
			$.post(the_url,
			{
				thedate:the_date
			},function(data){
				$(the_parent).load(data);
				$('#booked_date').load('<?php echo site_url('venue/venue/my_booking_this_venue/'.$row->venue_id);?>');
			});
		})

		$(document).on('click', '.remove_book',function(event){
			the_url=$(this).data('urlto');
			the_parent=$(this).closest("#booking_calendar");
			to_remove=$(this).data('toremove');
			$.post(the_url,
			{
				venue_booking_id:to_remove
			},function(data){
				$(the_parent).load(data);
				$('#booked_date').load('<?php echo site_url('venue/venue/my_booking_this_venue/'.$row->venue_id);?>');
			});
		});

		$(document).on('click', '.remove_book_close',function(event){
			the_url=$(this).data('urlto');
			the_parent=$("#booking_calendar");
			to_remove=$(this).data('toremove');
			$.post(the_url,
			{
				venue_booking_id:to_remove
			},function(data){
				$(the_parent).load(data);
				$('#booked_date').load('<?php echo site_url('venue/venue/my_booking_this_venue/'.$row->venue_id);?>');
			});
		})



		$('#booked_date').load('<?php echo site_url('venue/venue/my_booking_this_venue/'.$row->venue_id);?>');

    	$('#proposal_description').ckeditor(config);
    	$('#proposal_additional').ckeditor(config);
	})
</script>