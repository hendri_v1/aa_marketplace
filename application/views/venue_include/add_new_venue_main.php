<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<form action="<?php echo site_url('venue/my_venue/save_main_venue');?>" method="post" id="post_venue" enctype="multipart/form-data">
			<ul class="nav nav-tabs" role="tablist">
				<li class="active"><a href="#venue_location" role="tab" data-toggle="tab"><b class="lead text-warning">1</b> Lokasi</a></li>
				<li><a href="#venue_desc_tab" role="tab" data-toggle="tab"><b class="lead text-warning">2</b> Deskripsi Utama</a></li>
				<li><a href="#venue_detail_tab" role="tab" data-toggle="tab"><b class="lead text-warning">3</b> Gambar Utama</a></li>
				<li><a href="#venue_contact_tab" role="tab" data-toggle="tab"><b class="lead text-warning">4</b> Kontak</a></li>
				<li><a href="#postnow" role="tab" data-toggle="tab"><b class="lead text-warning">5</b> Posting</a></li>
			</ul>
			<div class="tab-content">
				<div id="venue_location" class="tab-pane fade in active">
					
					<div class="background-white p20">
						<div class="row">
							<div class="col-sm-6">
								<?php
                            $array_province=array();
                            $array_province[0]="Pilih Provinsi";
                            foreach($province as $rowsprovince)
                            {
                                $array_province[$rowsprovince->province_id]=$rowsprovince->province_name;
                            }
                            echo bt_select('province_id','Provinsi',$array_province,'');

                        ?>
									<?php
                            $array_city=array();
                            echo bt_select('city_id','Kota',$array_city,'');
                        ?>
										<?php echo bt_textarea('venue_main_address','Alamat Lengkap','','100px');?>

											<?php echo bt_text('venue_main_geo','Alamat Geo Location','Akan terisi otomatis saat mengunci peta');?>
												Jika Penanda yang dihasilkan secara otomatis tidak tepat, Geser penanda untuk mengatur lokasi yang tepat
							</div>
							<div class="col-sm-6">
								<label>Geser Penanda Untuk Mengatur Lokasi Yang Tepat</label>
								<div id="map_canvas" style="height:300px;">

								</div>
								<span id="the_address"></span>
							</div>
						</div>
					</div>
					
				</div>
				<div id="venue_desc_tab" class="tab-pane fade">
					<div class="background-white p20">
						<div class="row">
							<div class="col-sm-12">
								<?php echo bt_text('venue_main_name','Nama Venue');?>
									<?php echo bt_textarea('venue_main_description','Deskripsi');?>
							</div>
						</div>
					</div>
				</div>
				<div id="venue_detail_tab" class="tab-pane fade">
					<div class="background-white p20">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label>Gambar Utama</label>
									<input type="file" name="venue_main_image" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="venue_contact_tab" class="tab-pane fade">
					<div class="background-whtie p20">
						<div class="row">
							<div class="col-sm-6">
								<?php echo bt_text('venue_main_contact_person','Contact Person');?>
									<?php echo bt_text('venue_main_contact_phone','Nomor Telepon');?>
										<?php echo bt_text('venue_main_contact_mobile_phone','Nomor Seluler');?>
							</div>
						</div>

					</div>
				</div>
				<div id="postnow" class="tab-pane fade">
					<div class="background-white p20">
						<div class="row">
							<div class="col-sm-12">
								<p>Pastikan semua data telah Anda masukkan</p>

								<input type="submit" class="btn btn-primary" value="Publish Venue">
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>

<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		map = new GMaps({
			div: '#map_canvas',
			lat: -12.043333,
			lng: -77.028333,
			zoom: 9
		});
		GMaps.geocode({
			address: 'Jakarta',
			callback: function (results, status) {
				if (status == 'OK') {
					var latlng = results[0].geometry.location;
					map.setCenter(latlng.lat(), latlng.lng());

				}
			}
		});
		$('#venue_main_description').ckeditor();
		$('#province_id').change(function (event) {
			var prov = $("#province_id").val();

			$.post('<?php echo site_url('program/getProvinceforMap');?>', {
					province: prov
				},
				function (data) {
					long_address = '';
					$('#city_id').html(data.option);
					GMaps.geocode({
						address: data.province_name,
						callback: function (results, status) {
							if (status == 'OK') {
								var latlng = results[0].geometry.location;
								map.setCenter(latlng.lat(), latlng.lng());
								map.setZoom(9);
							}
						}
					});
				}, 'json'
			);
		});
		$('#city_id').change(function () {
			var city = $(this).val();
			$.post('<?php echo site_url('program/getCityName');?>', {
					city_id: city
				},
				function (data) {
					long_address = data;
					$('#the_address').html(long_address);
				});

		});
		$('#venue_main_address').blur(function (event) {
			new_address = $(this).val() + ', ' + long_address;
			long_address = new_address;
			GMaps.geocode({
				address: long_address,
				callback: function (results, status) {
					if (status == 'OK') {
						var latlng = results[0].geometry.location;

						map.setCenter(latlng.lat(), latlng.lng());
						map.setZoom(15);
						map.addMarker({
							lat: latlng.lat(),
							lng: latlng.lng(),
							draggable: true,
							dragend: function (e) {
								$('#venue_main_geo').val(this.getPosition());
							}
						});
						$('#venue_main_geo').val(latlng.lat() + ',' + latlng.lng());
					}
				}
			});
			$('#the_address').html(long_address);
		});
	})
</script>