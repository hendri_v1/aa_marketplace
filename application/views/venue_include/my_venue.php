<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<a href="<?php echo site_url('venue/my_venue/add_new_main');?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Venue</a>
        	<hr />
			<table class="table table-striped table-hover">
				<thead>
					<tr class="default"><th>#ID</th><th>Nama Venue</th><th>Alamat</th><th>Status</th><th>Room</th><th></th></tr>
				</thead>
				<tbody>
					<?php foreach($query->result() as $rows): ?>

						<tr>
							<td>
								<?php echo $rows->venue_main_id;?>

							</td>
							<td><?php echo $rows->venue_main_name;?>
								<?php if($rows->total_room==0): ?>
									<br /><span class="text-warning small">Belum Ada Room Yang Ditambahkan</span>
								<?php endif;?>
							</td>
							<td><?php echo $rows->venue_main_address;?> <?php echo $rows->city_name;?> <?php echo $rows->province_name;?></td>
							<td><?php if($rows->venue_main_status==0)
										echo '<span class="label label-warning">Dalam Moderasi</span>';
									  elseif($rows->venue_main_status==1)
									  	echo '<span class="label label-success">Diterbitkan</span>';
									  elseif($rows->venue_main_status==2)
									  	echo '<span class="label label-danger">Ditolak</span>';?></td>
							<td>
								
									<a href="<?php echo site_url('venue/my_venue/view_rooms/'.$rows->venue_main_id);?>" class="btn btn-sm btn-info"><i class="fa fa-building-o"></i> <?php echo $rows->total_room;?> Room</a> 
									<a href="<?php echo site_url('venue/my_venue/availability/'.$rows->venue_main_id);?>" class="btn btn-info btn-sm" title="atur Availability"><i class="fa fa-calendar"></i> Ketersediaan</a>
								
							</td>
							<td>
								<a href="<?php echo site_url('venue/view/'.$rows->venue_main_id.'/'.url_title($rows->venue_main_name));?>" class="btn btn-sm btn-danger" title="Lihat Venue" target="_blank"><i class="fa fa-eye"></i></a>
								<a href="<?php echo site_url('venue/my_venue/edit/'.$rows->venue_main_id);?>" class="btn btn-sm btn-warning"><i class="fa fa-pencil-square-o"></i></a>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</div>
	</div>

<?php $this->load->view('member_include/html_footer');?>