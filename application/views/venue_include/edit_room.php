<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<form action="<?php echo site_url('venue/my_venue/update_room');?>" method="post" id="post_venue">
				<ul class="nav nav-tabs" role="tablist">
                  
                  <li class="active"><a href="#venue_desc_tab" role="tab" data-toggle="tab"><b class="lead text-warning">1</b> Deskripsi</a></li>
                  <li><a href="#venue_detail_tab" role="tab" data-toggle="tab"><b class="lead text-warning">2</b> Detail Room</a></li>
                  
                  <li><a href="#venue_facilities_tab" role="tab" data-toggle="tab"><b class="lead text-warning">3</b> Fasilitas</a></li>
                  <li><a href="#venue_gallery_tab" role="tab" data-toggle="tab"><b class="lead text-warning">4</b> Gallery</a></li>
                  <li><a href="#room_config_tab" role="tab" data-toggle="tab"><b class="lead text-warning">5</b> Room Configuration</a></li>
                  <li><a href="#postnow" role="tab" data-toggle="tab"><b class="lead text-warning">6</b> Posting</a></li>
                </ul>
                <div class="tab-content">
                    
                	<div id="venue_desc_tab" class="tab-pane fade in active">
        						<div class="background-white p20">
        							<div class="row">
        								<div class="col-sm-12">
        									<?php echo bt_text('venue_name','Nama Room','',$row->venue_name);?>
        									<?php echo bt_textarea('venue_description','Deskripsi',$row->venue_description);?>
        								</div>
        							</div>
        						</div>
                	</div>
                	<div id="venue_detail_tab" class="tab-pane fade">
						<div class="background-white p20">
							<div class="row">
								
								<div class="col-sm-6">
									<?php echo bt_text('venue_capacity','Kapasitas Maksimal','',$row->venue_capacity);?>
									<?php echo bt_text('venue_price','Harga mulai dari','',$row->venue_price);?>
                                    <?php
										$venue_price_type=array(0=>'Half Day',1=>'Full Day');
										echo bt_select('venue_price_type','Harga Untuk ',$venue_price_type,$row->venue_price_type);
									?>
									<?php
										$venue_type_id=array();
										foreach($qvenue_types->result() as $rvenue_types)
										{
											$venue_type_id[$rvenue_types->venue_type_id]=$rvenue_types->venue_type_name;
										}
										echo bt_select('venue_type_id','Tipe Venue',$venue_type_id,$row->venue_type_id);
									?>
                                    
								</div>
                                <div class="col-sm-6">
                                    <?php
                                        $venue_space=array(0=>'Indoor',1=>'Outdoor',2=>'Indoor / Outdoor');
                                        echo bt_select('venue_space','Space',$venue_space,$row->venue_space);
                                    ?>
                                </div>
							</div>
						</div>
               		</div>
                    
               		<div id="venue_gallery_tab" class="tab-pane fade">
						<div class="row">
							<div class="col-sm-12">
								<div class="background-white p20">
									<div class="row">
										<div class="col-sm-6">
										
											<span id="error_alert"></span><br />
											<p>Untuk hasil maksimal, pastikan gambar yang ditambahkan kedalam Gallery dalam mode landscape.</p>
                                            <a href="#" id="add_venue_gallery" class="btn btn-danger">Tambah Gallery</a>
										  
										</div>
                                        <div id="gallery_load_place" class="col-sm-6"></div>
										<div class="col-sm-12">
                                            <div id="gallery_list_exist">

                                            </div>
											<div id="gallery_list">
												
											</div>
										</div>
									</div>
								</div>
                            </div>
						</div>
               		</div>
                    <div id="room_config_tab" class="tab-pane fade">
                        <div class="background-white p20 room_config_list">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4>Ukuran</h4>
                                    <hr />
                                    <?php echo bt_text('venue_width','Lebar Ruangan','',$row->venue_width);?>
                                    <?php echo bt_text('venue_length','Panjang Ruangan','',$row->venue_length);?>
                                    <?php echo bt_text('venue_height','Tinggi Ruangan','',$row->venue_height);?>
                                </div>
                                <div class="col-sm-6">
                                    <h4>Kapasitas Maksimal</h4>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="r_c u_shape active" title="U Shape"></div>
                                            <input type="text" name="venue_u_shape" value="<?php echo $row->venue_u_shape;?>" class="text_config">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="r_c boardroom active" title="BoardRoom"></div>
                                            <input type="text" name="venue_boardroom" value="<?php echo $row->venue_boardroom;?>" class="text_config">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="r_c classroom active" title="ClassRoom"></div>
                                            <input type="text" name="venue_classroom" value="<?php echo $row->venue_classroom;?>" class="text_config">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="r_c reception active" title="Reception"></div>
                                            <input type="text" name="venue_reception" value="<?php echo $row->venue_reception;?>" class="text_config">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="r_c banquet active" title="Banquet"></div>
                                            <input type="text" name="venue_banquet" value="<?php echo $row->venue_banquet;?>" class="text_config">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="r_c theatre active" title="Theatre"></div>
                                            <input type="text" name="venue_theatre" value="<?php echo $row->venue_theatre;?>" class="text_config">
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="r_c auditorium active" title="Auditorium"></div>
                                            <input type="text" name="venue_auditorium" value="<?php echo $row->venue_auditorium;?>" class="text_config">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="r_c circle active" title="Circle"></div>
                                            <input type="text" name="venue_circle" value="<?php echo $row->venue_circle;?>" class="text_config">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <hr />
                                            * Kosongkan data untuk tipe konfigurasi yang tidak didukung
                                        </div>
                                    </div>
                                </div>  
                                
                            </div>
                        </div>
                    </div>
                    <div id="venue_facilities_tab" class="tab-pane fade">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="background-white p20">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4>Fasilitas Standard</h4>
                                            <hr />
                                            <?php echo bt_text('standard_facility','Masukkan Fasilitas Standard','Pisahkan Antar Fasilitas dengan ; (titik koma)');?>
                                            atau pilih dari daftar berikut :<br />
                                            <?php $q1=$this->venue_model->get_facilities(1);
                                                foreach($q1->result() as $r1): ?>
                                                <?php if($this->venue_model->get_checked($r1->venue_facility_id,$row->venue_id)==1): ?>
																	<input type="checkbox" name="venue_facilities[]" value="<?php echo $r1->venue_facility_id;?>" checked="checked" /> <?php echo $r1->venue_facility_name;?><br />
                                                <?php else: ?>
	                                                <input type="checkbox" name="venue_facilities[]" value="<?php echo $r1->venue_facility_id;?>" /> <?php echo $r1->venue_facility_name;?><br />
	                                             <?php endif;?>
                                            <?php endforeach;?>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4>Fasilitas Tambahan</h4>
                                            <hr />
                                            <?php echo bt_text('additional_facility','Masukkan Fasilitas Tambahan','Pisahkan Antar Fasilitas dengan ; (titik koma)');?>
                                            atau pilih dari daftar berikut :<br />
                                            <?php $q2=$this->venue_model->get_facilities(2);
                                                foreach($q2->result() as $r2): ?>
                                                <?php if($this->venue_model->get_checked($r2->venue_facility_id,$row->venue_id)==1):?>
                                                	<input type="checkbox" name="venue_facilities1[]" value="<?php echo $r2->venue_facility_id;?>" checked="checked" /> <?php echo $r2->venue_facility_name;?><br />
                                                <?php else: ?>
                                                	<input type="checkbox" name="venue_facilities1[]" value="<?php echo $r2->venue_facility_id;?>" /> <?php echo $r2->venue_facility_name;?><br />
                                            		<?php endif;?>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               		<div id="postnow" class="tab-pane fade">
        						<div class="row">
        							<div class="col-sm-12">
        								<div class="boxes">
                                            <input type="hidden" name="venue_main_id" value="<?php echo $row->venue_main_id;?>">
                                            <input type="hidden" name="venue_id" value="<?php echo $row->venue_id;?>">
        									<p>Pastikan semua data telah Anda masukkan</p>
                                            <input type="hidden" name="venue_gallery_hash2" value="<?php echo $venue_gallery_hash;?>">
        									<input type="submit" class="btn btn-primary" value="Update Room">
        								</div>
        							</div>
        						</div>
               		</div>
                    
               	</div>
			</form>
		</div>
	</div>
		<?php echo form_open_multipart('ajax/image',array('id' => 'myform'));?>
    	<input type="file" name="venue_image" id="venue_image">
    	<input type="hidden" name="venue_gallery_hash" value="<?php echo $venue_gallery_hash;?>">
	<?php echo form_close(); ?>

    <?php echo form_open_multipart('ajax/image',array('id' => 'roomconfig'));?>
        <input type="file" name="room_config" id="room_config">
        <input type="hidden" name="venue_gallery_hash" value="<?php echo $venue_gallery_hash;?>">
    <?php echo form_close();?>
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
  
	$(document).ready(function() {
    

		$('#venue_description').ckeditor();
		

        $('#venue_image').hide();
        $('#room_config').hide();

        $('#add_venue_gallery').click(function(){
        	$('#venue_image').click();
        });

        $('#add_room_config').click(function(){
            $('#room_config').click();
        });

        $('#gallery_list_exist').load('<?php echo site_url('venue/my_venue/view_gallery_by_venue_id/'.$row->venue_id);?>');

        $('#venue_image').change(function() {
            $('#gallery_load_place').html(loader);
            var formData = new FormData($('#myform')[0]);
            $.ajax({
                url:"<?php echo site_url('venue/my_venue/add_gallery') ?>",
                type: 'POST',
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                success: function (data) {
                    if (data == 'true') {
                        $('#gallery_list').load('<?php echo site_url('venue/my_venue/view_gallery_by_hash/'.$venue_gallery_hash);?>');
                    } else {
                        //$('#error_upload').html(data);
                        $('#error_alert').hide();
                        $('#error_alert').html('<div class="alert text-center alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + data + '</div>');
                        $('#error_alert').fadeIn('slow',function(){
                            $(this).delay(5000).fadeOut('slow');
                        });
                    }
                    $('#gallery_load_place').html('');
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
         

        });
        
        $('#room_config').change(function(){
            $('#gallery_room_config').html(loader);
            var formData = new FormData($('#roomconfig')[0]);
            $.ajax({
                url:"<?php echo site_url('venue/my_venue/add_room_config');?>",
                type: 'POST',
                xhr: function(){
                    var myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                success: function (data) {
                    if(data == 'true'){
                        $('#room_config_list').load('<?php echo site_url('venue/my_venue/view_gallery_by_hash/'.$venue_gallery_hash.'/1');?>');
                    } else {
                        $('#error_alert').hide();
                        $('#error_alert').html('<div class="alert text-center alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + data + '</div>');
                        $('#error_alert').fadeIn('slow',function(){
                            $(this).delay(5000).fadeOut('slow');
                        })
                    }
                    $('#gallery_room_config').html('');
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false

            });
        })
    
	});
</script>