<div class="block background-primary fullwidth mt80 pt60">
    <div class="row">
        <div class="col-sm-12">
            <div class="contact-info-wrapper">
                
                <h2 style="color:#FFF;">Statistik</h2>
                <div class="contact-info">
                    <span class="contact-info-item">
                        <i class="fa fa-users"></i>
                        <span><?php echo $this->global_model->get_all_user();?> Member</span>
                    </span><!-- /.contact-info-item -->

                    <span class="contact-info-item">
                        <i class="fa fa-ticket"></i>
                        <span><?php echo $total_all_events;?> Event Terdaftar</span>
                    </span><!-- /.contact-info-item -->

                    <span class="contact-info-item">
                        <i class="fa fa-user"></i>
                        <span><?php echo $this->global_model->get_all_user(FALSE,TRUE);?> Expert</span>
                    </span>

                    <span class="contact-info-item">
                        <i class="fa fa-user-plus"></i>
                        <span><?php echo $this->global_model->get_all_user(TRUE);?> Vendor</span>
                    </span>
                </div><!-- /.contact-info-->
            </div><!-- /.contact-info-wrapper -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->

</div>