
    <?php /*
    <div class="page-header">
        <h1>Featured Events</h1>
                    
    </div><!-- /.page-header -->*/?>
    <div class="mt-80">
        <div class="hero-slider">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

            <div class="carousel-inner" role="listbox">
                <?php $i=0; foreach($queryfeaturedpremium->result() as $rowsevents): $i++; ?>
                <div class="item <?php if($i==1): ?>active<?php endif;?>">
                    <div class="item-bg" style="background-image: url('<?php echo upload_path;?>uploads/event_images/<?php echo $rowsevents->event_picture;?>')"></div>
                    

                    <div class="carousel-caption">
                        <div class="hero-slider-content">
                            <h1><?php echo $rowsevents->userprofile_companyname;?></h1>

                            <div class="hero-slider-rating">
                                <?php echo $rowsevents->event_title; ?>
                            </div><!-- /.card-simple-rating -->
                        </div><!-- /.hero-slider-content -->

                        <div class="hero-slider-actions">
                            <?php $sameday=$rowsevents->event_end_date-$rowsevents->event_start_date;?>
                            <?php if($sameday>86400): ?>
                                <i class="fa fa-clock-o"></i> <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_start_date);?> - <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_end_date);?> 
                            <?php else: ?>
                                <i class="fa fa-clock-o"></i> <?php echo mdate('%d %M %Y %H:%i:%s',$rowsevents->event_start_date);?> - <?php echo mdate('%H:%i:%s',$rowsevents->event_end_date);?> 
                            <?php endif;?>
                            <br />
                            <i class="fa fa-map-marker"></i> <?php echo $rowsevents->city_name;?>
                           
                            <a href="<?php echo site_url('event/view/' . $rowsevents->event_id . '/' . url_title(strtolower(word_limiter($rowsevents->event_title,10)))); ?>">Lihat Lebih Detail</a>
                            
                        </div><!-- /.hero-slider-actions -->
                    </div><!-- /.carousel-caption -->
                </div><!-- /.item -->
                <?php endforeach;?>
                
            </div><!-- /.carousel-inner -->

              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <i class="fa fa-long-arrow-left"></i>
              </a>

              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <i class="fa fa-long-arrow-right"></i>
              </a>
            </div>
        </div><!-- /.hero-slider -->

    </div>
