<header class="header header-minimal">
    <div class="header-wrapper">
        <div class="container-fluid">
            <div class="header-inner">
                <div class="header-logo">
                    <a href="<?php echo base_url();?>">
                        <img src="<?php echo base_url();?>assets/img/logo.png" alt="Logo">
                    </a>
                </div><!-- /.header-logo -->

                <div class="header-content">
                    <div class="header-bottom">
                        <ul class="header-nav-primary nav nav-pills collapse navbar-collapse">
                            
                            <?php if($this->session->userdata('user_meta')->usertype_id==99): ?>
                                <li id="dashboard"><a href="<?php echo site_url('program');?>">Dashboard</a></li>
                                <li>
                                    <a href="#">Management <i class="fa fa-chevron-down"></i></a>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo site_url('themaster/project'); ?>">Project</a></li>
                                        <li id="em_admin"><a href="<?php echo site_url('themaster/event'); ?>">Event</a></li>
                                        <li id="mem_admin"><a href="<?php echo site_url('themaster/member'); ?>">Member</a></li>
                                        <li id="vn_admin"><a href="<?php echo site_url('themaster/venue'); ?>">Venue</a></li>
                                        <li><a href="<?php echo site_url('themaster/documents'); ?>">Artikel</a></li>
                                        <li><a href="<?php echo site_url('themaster/category'); ?>">Category</a></li>
                                    </ul>
                                </li>
                                <li id="marketingnav"><a href="<?php echo site_url('themaster/campaign'); ?>">Campaign Manager</a></li>
                            <?php else: ?>
                                <li id="dashboard"><a href="<?php echo site_url('program/dashboard');?>">Dashboard</a></li>
                                <li id="pms"><a href="<?php echo site_url('profile/my_profile');?>">Profile Management</a></li>
                                <li id="ems"><a href="<?php echo site_url('event/my_event');?>">Event Management</a></li>
                                <?php if ($this->session->userdata('user_meta')->userprofile_function == 13): ?>
                                    <li id="vms"><a href="<?php echo site_url('my-venue'); ?>">Venue Management</a></li>
                                <?php endif;?>
                                <li id="bms"><a href="<?php echo site_url('blog/my_blog'); ?>">Blog Management</a></li>
                                <li id="jms"><a href="<?php echo site_url('job/my_job');?>">Job Management</a></li>
                                <li id="proms"><a href="#">Project Management</a></li>
                            <?php endif;?>

                        </ul>

                        <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav-primary">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>


                        <div class="header-nav-user">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <div class="user-image">
                                        <img src="<?php echo upload_path; ?>uploads/user_images/<?php echo $this->global_model->get_user_photo($this->session->userdata('user_meta')->userprofile_photo); ?>">
                                        <div class="notification"></div><!-- /.notification-->
                                    </div><!-- /.user-image -->

                                    <span class="header-nav-user-name"><?php echo $this->session->userdata('user_meta')->userprofile_firstname;?> <?php echo $this->session->userdata('user_meta')->userprofile_lastname;?></span> <i class="fa fa-chevron-down"></i>
                                </button>

                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="<?php echo site_url('profile/my_profile');?>">Edit Profile</a></li>
                                    <li><a href="<?php echo site_url('profile/notification');?>">Pemberitahuan</a></li>
                                    <li><a href="<?php echo site_url('program/logout');?>">Logout</a></li>
                                </ul>
                            </div><!-- /.dropdown -->
                        </div><!-- /.header-nav-user -->

                    </div><!-- /.header-bottom -->
                </div><!-- /.header-content -->
            </div><!-- /.header-inner -->
        </div><!-- /.container -->
    </div><!-- /.header-wrapper -->

    <div class="header-statusbar">
        <div class="header-statusbar-inner">
            <div class="header-statusbar-left">
                <h1><?php echo $title;?></h1>

                
            </div><!-- /.header-statusbar-left -->

            
        </div><!-- /.header-statusbar-inner -->
    </div><!-- /.header-statusbar -->
</header><!-- /.header -->