</div><!-- /.container-fluid -->
            </div><!-- /.content-admin-main-inner -->
        </div><!-- /.content-admin-main -->

        <div class="content-admin-footer">
            <div class="container-fluid">
                <div class="content-admin-footer-inner">
                    &copy; <a href="<?php echo base_url();?>">HRPlasa.id</a> 2014 - <?php echo date('Y');?> All rights reserved. Powered by <a href="http://www.semartek.com" target="_blank">seMartek</a>.
                </div><!-- /.content-admin-footer-inner -->
            </div><!-- /.container-fluid -->
        </div><!-- /.content-admin-footer  -->
    </div><!-- /.content-admin-wrapper -->
</div><!-- /.content-admin -->
</div><!-- /.wrapper-admin -->
        </div><!-- /.outer-admin -->
    </div><!-- /.main -->
</div><!-- /.page-wrapper -->

<script src="<?php echo base_url();?>assets/js/jquery.js" type="text/javascript"></script>


<script src="<?php echo base_url();?>assets/libraries/bootstrap-sass/javascripts/bootstrap/collapse.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/libraries/bootstrap-sass/javascripts/bootstrap/carousel.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/libraries/bootstrap-sass/javascripts/bootstrap/transition.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/libraries/bootstrap-sass/javascripts/bootstrap/dropdown.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/libraries/bootstrap-sass/javascripts/bootstrap/tooltip.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/libraries/bootstrap-sass/javascripts/bootstrap/tab.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/libraries/bootstrap-sass/javascripts/bootstrap/alert.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/libraries/bootstrap-sass/javascripts/bootstrap/modal.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/libraries/colorbox/jquery.colorbox-min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/libraries/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/libraries/flot/jquery.flot.spline.js" type="text/javascript"></script>

<?php /*<script src="<?php echo base_url();?>assets/libraries/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>*/?>



<?php if (isset($map)) : ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBa2zn5pIU_-LWmzKVX_nPI6Nms_y7NyCo&amp;sensor=true&amp;language=id"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/libraries/gmaps.js"></script>
<?php else: ?>

    <script src="<?php echo base_url();?>assets/js/map.js" type="text/javascript"></script>
    <script src="http://maps.googleapis.com/maps/api/js?libraries=weather,geometry,visualization,places,drawing&amp;sensor=false" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/libraries/jquery-google-map/infobox.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/libraries/jquery-google-map/markerclusterer.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/libraries/jquery-google-map/jquery-google-map.js"></script>
<?php endif;?>

<script type="text/javascript" src="<?php echo base_url();?>assets/libraries/owl.carousel/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/libraries/bootstrap-fileinput/fileinput.min.js"></script>

<script src="<?php echo base_url();?>assets/js/superlist.js" type="text/javascript"></script>
<script type="text/javascript">
var loader = '<div align="center" style="position:absolute;top:0;"><img src="<?php echo base_url(); ?>assets/img/loader.gif" alt="Loader" /></div>';
$(document).ready(function(){
	<?php if(isset($nav_side)): ?>
		
		$("#<?php echo $nav_side;?>").addClass('active');
	<?php endif;?>
    <?php if(isset($child_nav)): ?>
        $('#<?php echo $child_nav;?>').addClass('active');
    <?php endif;?>
})
</script>

</body>
</html>