<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<div class="row">

				<?php foreach($query->result() as $rows): ?>
					<div class="col-sm-12">
						<div class="background-white p20 mb30">
							<div class="image-notif" style="float:left;width:50px;margin-right:30px;">
								<img src="<?php echo base_url()?>uploads/user_images/<?php echo $this->global_model->get_user_photo($rows->userprofile_photo);?>" class="img-thumbnail" width="100%" />
							</div>
							<div style="">
								<span class="text-danger"><?php echo mdate('%d-%m-%Y %H:%i:%s',$rows->notification_date);?></span><br />
								<?php if($rows->usertype_id<>2): ?>
									<?php echo $rows->userprofile_firstname;?> <?php echo $rows->userprofile_lastname;?>
								<?php else: ?>
									<?php echo $rows->userprofile_companyname;?>
								<?php endif;?>
								<?php echo $this->global_model->get_notification_message($rows->notification_type);?>
								<strong><?php if($rows->notification_type==1)
									  {
										$row=$this->event_model->get_event_by_id($rows->notification_target)->row();
										echo $row->event_title;
									}
								?></strong>
								<br />

							</div>
						</div>
					</div>

					
				<?php endforeach;?>
			</div>
		</div>
	</div>

<?php $this->load->view('member_include/html_footer');?>