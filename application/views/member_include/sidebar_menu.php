<?php if($nav_side=="dashboard"): ?>
    <div class="sidebar-admin">
        <ul>
            <li class="active"><a href="#"><i class="fa fa-tachometer"></i></a></li>
            
        </ul>
    </div>
<?php elseif($nav_side=="ems"): ?>
    <div class="sidebar-admin">
        <ul>
            <li><a href="<?php echo site_url('program/dashboard');?>"><i class="fa fa-tachometer"></i></a></li>
            <li title="Event Management System" id="ems_side"><a href="<?php echo site_url('event/my_event');?>"><i class="fa fa-bar-chart"></i></a></li>
            <li title="Buat Event Publik" id="ems_new"><a href="<?php echo site_url('event/my_event/add_new') ?>"><i class="fa fa-pencil"></i></a></li>
            <li title="Buat Event Private" id="ems_new_private"><a href="<?php echo site_url('event/my_event/add_private') ?>"><i class="fa fa-key"></i></a></li>
            <li><a href="#"><i class="fa fa-users"></i></a></li>
            
        </ul>
    </div>
<?php elseif($nav_side=="pms"): ?>
    <div class="sidebar-admin">
        <ul>
            <li><a href="<?php echo site_url('program/dashboard');?>"><i class="fa fa-tachometer"></i></a></li>
            <li id="pms_side"><a href="<?php echo site_url('profile/my_profile');?>"><i class="fa fa-user"></i></a></li>
            <li id="pms_view"><a title="Siapa yang melihat saya?" href="<?php echo site_url('profile/viewer');?>"><i class="fa fa-eye"></i></a></li>
        </ul>
    </div>
<?php elseif($nav_side=="bms"): ?>
    <div class="sidebar-admin">
        <ul>
            <li><a href="<?php echo site_url('program/dashboard');?>"><i class="fa fa-tachometer"></i></a></li>
            <li title="Blog Management System" id="bms_side"><a href="<?php echo site_url('blog/my_blog');?>"><i class="fa fa-bar-chart"></i></a></li>
            <li title="Buat Post Baru" id="bms_new"><a href="<?php echo site_url('blog/my_blog/add_new');?>"><i class="fa fa-pencil"></i></a></li>
        </ul>
    </div>
<?php elseif($nav_side=="billing"): ?>
    <div class="sidebar-admin">
        <ul>
            <li><a href="<?php echo site_url('program/dashboard');?>"><i class="fa fa-tachometer"></i></a></li>
        </ul>
    </div>
<?php elseif($nav_side=="vms"): ?>
    <div class="sidebar-admin">
        <ul>
            <li><a href="<?php echo site_url('program/dashboard');?>"><i class="fa fa-tachometer"></i></a></li>
            <li id="vms"><a href="<?php echo site_url('my-venue');?>"><i class="fa fa-bar-chart"></i></a></li>
            <li id="vms_new"><a href="<?php echo site_url('venue/my_venue/add_new_main');?>"><i class="fa fa-pencil"></i></a></li>
        </ul>
    </div>
<?php endif;?>
<!-- /.sidebar-admin-->
<?php /*
<div class="sidebar-secondary-admin">
    <ul>
        <li id="dashboard">
            <a href="<?php echo site_url('program/dashboard');?>">
                <span class="icon"><i class="fa fa-tachometer"></i></span>
                <span class="title">Dashboard</span>
                <span class="subtitle">Statistik Akun Anda</span>
            </a>
        </li>

        <li id="ems">
            <a href="<?php echo site_url('event/my_event');?>">
                <span class="icon"><i class="fa fa-i-cursor"></i></span>
                <span class="title">EMS</span>
                <span class="subtitle">Event Management</span>
            </a>
        </li>

        <li >
            <a href="admin-tables.html">
                <span class="icon"><i class="fa fa-table"></i></span>
                <span class="title">PMS</span>
                <span class="subtitle">Profile Management</span>
            </a>
        </li>

        <li >
            <a href="admin-grid.html">
                <span class="icon"><i class="fa fa-th"></i></span>
                <span class="title">Grid <span class="notification">2</span></span>
                <span class="subtitle">Bootstrap Grid</span>
            </a>
        </li>

        <li >
            <a href="admin-notifications.html">
                <span class="icon"><i class="fa fa-bell"></i></span>
                <span class="title">Notifications</span>
                <span class="subtitle">Alert messages</span>
            </a>
        </li>
    </ul>
</div><!-- /.sidebar-secondary-admin -->*/ ?>