<?php $this->load->view('member_include/html_head');?>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>INV no</th><th>Tanggal</th><th colspan="2">Billing Untuk</th><th><div align="right">Total</div></th><th>Tenggat Waktu</th><th>Status</th><th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($query->result() as $rows): ?>
						<tr>
							<td><?php echo $rows->billing_id;?></td>
							<td><?php echo mdate('%d %M %Y',$rows->billing_date);?></td>
							
								<?php if($rows->billing_type==1): ?>
									<td>Project</td><td></td>
								<?php elseif($rows->billing_type==2): ?>
									<td>Event</td>
									<td>
										<?php $row_event=$this->event_model->get_event_by_id($rows->billing_for);?>
										<?php if($row_event->num_rows()>0): 
											$row_event=$row_event->row();
										?>
											<a href="<?php echo site_url('themaster/event/view_event/'.$row_event->event_id);?>"><?php echo $row_event->event_title;?></a>
										<?php endif;?>
									</td>
								<?php elseif($rows->billing_type==3): ?>
									<td>Upgrade Membership a.n </td>
									<td>
										<?php $row_member=$this->user_role_model->get_user_by_id($rows->billing_for);?>
										<a href="<?php echo site_url('themaster/member/view_user/'.$row_member->user_id);?>"><?php echo $row_member->userprofile_firstname;?> <?php echo $row_member->userprofile_lastname;?></a>
									</td>
								<?php endif;?> 
									
							
							<td><div align="right"><?php echo number_format($rows->billing_total,0,',','.');?></div></td>
							<td><?php echo mdate('%d %M %Y',$rows->billing_due);?></td>
							<td><?php if($rows->billing_status==0) echo "Belum Dibayar"; else echo "Lunas";?></td>
							<td><a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="detail-billing" data-billing_id="<?php echo $rows->billing_id;?>">Detail</a></td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</div>
	</div>

<?php $this->load->view('member_include/html_footer');?>