<?php $this->load->view('member_include/html_head');?>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="message">
                                        <h1>Selamat Datang di Dashboard</h1>

                                        <p>Ini adalah halaman My HRPlasa, dimana Anda dapat mengelola aktifitas Anda di HRPlasa, apabila Anda memiliki pertanyaan terkait penggunaan My HRPlasa silahkan menghubungi support@hrplasa.id</p>
                                    </div><!-- /.message -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-sm-12 col-lg-6">
                                    <h3>Statistik</h3>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="statusbox">
                                                <h2>Event</h2>
                                                <div class="statusbox-content">
                                                    <strong><?php echo $this->event_model->get_event(0,0,TRUE)->num_rows();?> Event Aktif</strong>
                                                    <span>dari <?php echo $my_event->num_rows();?> Event Yang Didaftarkan</span>
                                                </div><!-- /.statusbox-content -->

                                                <div class="statusbox-actions">
                                                    <a href="<?php echo site_url('event/my_event/add_new');?>"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo site_url('event/my_event');?>"><i class="fa fa-bar-chart"></i></a>
                                                    
                                                </div><!-- /.statusbox-actions -->
                                            </div><!-- /.statusbox -->
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="statusbox">
                                                <h2>Tulisan</h2>
                                                <div class="statusbox-content">
                                                    <strong><?php echo $my_post->num_rows();?> Tulisan</strong>
                                                    <span>Diterbitkan di Blog</span>
                                                </div><!-- /.statusbox-content -->

                                                <div class="statusbox-actions">
                                                    <a href="<?php echo site_url('blog/my_blog/add_new');?>"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo site_url('blog/my_blog');?>"><i class="fa fa-bar-chart"></i></a>
                                                    
                                                </div><!-- /.statusbox-actions -->
                                            </div>
                                        </div>
                                    </div><!-- /.row -->
                                </div><!-- /.col-* -->

                                <div class="col-sm-12 col-lg-6">
                                    <h3>.</h3>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="statusbox">
                                                <h2>Billing / Tagihan</h2>
                                                <div class="statusbox-content">
                                                <?
                                                    $total_billing=0;
                                                    $billing_date=0;
                                                    $i=0;
                                                    foreach($qbilling->result() as $rbilling)
                                                    {
                                                        $i++;
                                                        if($i==1)

                                                            $billing_date=$rbilling->billing_date;
                                                        if($rbilling->billing_status==0)
                                                        $total_billing=$total_billing+$rbilling->billing_total;
                                                    }
                                                ?>
                                                    <strong>Rp. <?php echo number_format($total_billing,0,',','.');?></strong>
                                                    <span>Updated <?php echo mdate('%d/%m/%Y',$billing_date);?></span>
                                                </div><!-- /.statusbox-content -->

                                                <div class="statusbox-actions">
                                                    
                                                    <a href="<?php echo site_url('profile/billing');?>"><i class="fa fa-bar-chart"></i></a>
                                                
                                                </div><!-- /.statusbox-actions -->
                                            </div><!-- /.statusbox -->
                                        </div><!-- /.col-* -->

                                        <div class="col-sm-6">
                                            <div class="statusbox">
                                                <h2>Pemberitahuan</h2>
                                                <div class="statusbox-content">
                                                    <strong><?php echo $this->notifications_model->get_my_notification(true, false, $this->session->userdata('user_id'))->num_rows(); ?> Aktifitas Baru</strong>
                                                    <span>Updated <?php echo date('d/M/Y H:i:s');?></span>
                                                </div><!-- /.statusbox-content -->

                                                <div class="statusbox-actions">
                                                    <a href="<?php echo site_url('profile/notification');?>"><i class="fa fa-eye"></i></a>
                                                    
                                                </div><!-- /.statusbox-actions -->
                                            </div><!-- /.statusbox -->
                                        </div><!-- /.col-* -->
                                    </div><!-- /.row -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Event &amp; View History</h3>
                                    <div class="p30 background-white mb50">
                                        <div id="superlist-chart"></div>
                                    </div>
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-sm-12 col-lg-5">
                                    <h3>Event Terbaru <a href="<?php echo site_url('events');?>">Lihat Semua Event <i class="fa fa-chevron-right"></i></a></h3>

                                    <div class="users">
                                        <table class="table">
                                            <tbody>
                                                <?php foreach($latest_event->result() as $rowslevent): ?>
                                                    <tr>
                                                        <td><a class="user" href="#"><img src="<?php echo upload_path.'uploads/user_images/'.$this->global_model->get_user_photo($rowslevent->userprofile_photo);?>" alt=""></a></td>
                                                        <td class="hidden-xs visible-sm visible-md visible-lg">
                                                            <h2><a href="<?php echo site_url('event/view/' . $rowslevent->event_id . '/' . url_title(strtolower(word_limiter($rowslevent->event_title,10)))); ?>"><?php echo $rowslevent->event_title;?></a></h2>
                                                            <h3>Start: <?php echo mdate('%d/%m/%Y %H:%i:%s',$rowslevent->event_start_date);?></h3>
                                                        </td>
                                                        <td class="right">
                                                            <a href="<?php echo site_url('event/view/' . $rowslevent->event_id . '/' . url_title(strtolower(word_limiter($rowslevent->event_title,10)))); ?>" class="btn btn-xs btn-primary">Lihat</a>
                                                            
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                                
                                            </tbody>
                                        </table>
                                    </div><!-- /.users -->

                                </div><!-- /.col-* -->

                                <div class="col-sm-12 col-lg-7">
                                    <h3>Event Populer Anda <a href="<?php echo site_url('event/my_event');?>">Lihat Semua Event <i class="fa fa-chevron-right"></i></a></h3>

                                    <div class="background-white p30 mb50">
                                        <div class="cards-system">
                                            <?php foreach($my_event_popular->result() as $rowsepopular): ?>
                                                <?php $mini_image='';
                                                    if($rowsepopular->event_picture=='')
                                                        $mini_image=upload_path.'uploads/user_images/'.$this->global_model->get_user_photo($this->session->userdata('user_meta')->userprofile_photo);
                                                    else
                                                        $mini_image=upload_path.'uploads/event_images/'.$rowsepopular->event_picture;
                                                ?>
                                                <div class="card-system">
                                                    <div class="card-system-inner">
                                                        <div class="card-system-image" data-background-image="<?php echo $mini_image;?>">
                                                            <a href="listing-detail.html"></a>
                                                        </div><!-- /.card-system-image -->

                                                        <div class="card-system-content">
                                                            <h2><a href="listing-detail.html"><?php echo $rowsepopular->event_title;?></a></h2>
                                                            <h3>Total View <?php echo $rowsepopular->event_totalview;?></h3>
                                                            <a href="<?php echo site_url('event/my_event/edit_event/' . $rowsepopular->event_id); ?>" class="btn btn-primary btn-xs">Edit</a>
                                                            <a href="<?php echo site_url('event/view/' . $rowsepopular->event_id . '/' . url_title($rowsepopular->event_title)) ?>" class="btn btn-secondary btn-xs" target="_blank">Lihat</a>
                                                        </div>
                                                    </div>
                                                </div><!-- /.card-system -->
                                            <?php endforeach;?>
    
        
                                            
                                    
                                        </div><!-- /.cards-system -->
                                    </div>

                                </div><!-- /.col-* -->
                            </div><!-- /.row -->
                        </div><!-- /.col-* -->

                    </div>
                
<?php $this->load->view('member_include/html_footer');?>
<script type="text/javascript">
$(document).ready(function(){
    /**
     * Chart
     */
     if ($('#superlist-chart').length !== 0) {
        var counter = 0;
        var increase = Math.PI * 2 / 100;

        var fun1 = [];
        for ( i = 0; i <= 1; i += 0.015 ) {
          var x = i;
          var y = Math.sin( counter );
          fun1.push([x, y]);
          counter += increase;
        }

        var counter = 0;
        var increase = Math.PI * 2 / 100;

        var fun2 = [];
        for ( i = 0; i <= 1; i += 0.015 ) {
          var x = i;
          var y = Math.cos( counter );
          fun2.push([x, y]);
          counter += increase;
        }

        var plot = $.plot($('#superlist-chart'),[
            {
                color: '#ceb65f',
                data: fun1
            },
            {
                color: '#009f8b',
                data: fun2
            }
        ],
        {
            series: {
                splines: {
                    show: true,
                    tension: 0.24,
                    lineWidth: 3,
                    fill: .40
                },
                lines: false,
                shadowSize: 0
            },
            points: { show: true },
            legend: false,
            grid: {
                borderColor: '#f1f1f1',
                borderWidth: 0
            },
            xaxis: {
                color: '#f1f1f1'
            },
            yaxis: {
                color: '#f1f1f1',
                min: -1,
                max: 1
            }
        });
    }
})
</script>
