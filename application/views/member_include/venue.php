<?php $this->load->view('member_include/html_head');?>
    <div class="row">
        <div class="col-sm-12">
			<ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#pendingtab" class="tab-ajax" role="tab" data-toggle="tab">Pending</a></li>
                <li><a href="#aktiftab" role="tab" class="tab-ajax" data-toggle="tab">Aktif</a></li>
            </ul>
            <hr class="space" />
            <div class="tab-content">
                <div class="tab-pane fade in active" id="pendingtab">
                    

                </div>
                <div class="tab-pane fade" id="aktiftab">

                </div>
            </div>    
		</div>
    </div>
<?php $this->load->view('member_include/html_footer');?>