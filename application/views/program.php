<?php $this->load->view('html_head');?>
    
    
    <div class="main">
        <div class="main-inner">
            <div class="content">
                
                    <?php //$this->load->view('hero');?>

               
                <?php $this->load->view('featured_events');?>
                <div class="container">
                    <?php $this->load->view('directory_front.php');?>
                    <?php $this->load->view('statistic_front');?>
                    <div class="block background-white fullwidth pt0 pb0">
                        <?php $this->load->view('partners');?>

                    </div>
                    

                    


                    <div class="block background-white fullwidth">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="page-header">
                                    <h1>Event Utama</h1>
                                    
                                </div><!-- /.page-header -->
                                <?php $this->load->view('event_front.php');?>

                            </div><!-- /.col-* -->

                            <div class="col-sm-6">
                                <div class="page-header">
                                    <h1>Tulisan Terbaru</h1>
                                    
                                </div><!-- /.page-header -->
                                <?php $this->load->view('blog_front.php');?>

                            </div><!-- /.col-* -->
                        </div><!-- /.row -->
                    </div><!-- /.block -->
                    <?php $this->load->view('vendors_front');?>
                    <?php $this->load->view('power_front.php');?>

                    
                    
                    <?php $this->load->view('testimonial.php');?>
                    
                    <?php $this->load->view('pricing.php');?>

                    <?php // $this->load->view('category_front');?>

                    <?php $this->load->view('contact_front');?>
    

    
                </div><!-- /.container -->

            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

<?php $this->load->view('html_footer');?>