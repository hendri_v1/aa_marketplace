<div class="page-header">
        <h1>Membership</h1>
        <p>Untuk informasi lebih lanjut mengenai Membership di HRPlasa, silahkan menghubungi <a href="#">support@hrplasa.id</a>
        
    </div><!-- /.page-header -->

    <div class="pricings">
        <div class="row">
            <div class="col-sm-4">
                <div class="pricing">
                    <div class="pricing-title">Free</div><!-- /.pricing-title -->
                    <div class="pricing-subtitle">Personal / Vendor</div><!-- /.pricing-subtitle -->
                    <div class="pricing-price"><span class="pricing-currency"></span>Free! <span class="pricing-period"></span></div><!-- /.pricing-price -->
                    <a href="<?php echo site_url('register');?>" class="btn-primary">Daftar Sekarang!</a>
                    <hr>
                    <ul class="pricing-list">
                        <li><span>Jumlah Kategori Terdaftar</span><strong>3</strong></li>
                        <li><span>Publikasi</span><strong>Event, Venue, Post, Job</strong></li>
                        <li><span>Management System</span><strong>EMS, VMS, PMS, JMS</strong></li>
                        <li><span>Advanced Management System</span><strong>---</strong></li>
                        <li><span>Featured</span><strong>250k / Event</strong></li>
                        <li><span>Featured Partner</span><strong>--</strong></li>
                        <li><span>Featured Banner</span><strong>--</strong></li>
                        <li><span>Pre-Event</span><strong>--</strong></li>
                        <li><span>Spesial</span><strong>Diskon 10% untuk Event oleh HRPlasa</strong></li>
                    </ul><!-- /.pricing-list -->
                    <hr>
                   
                </div><!-- /.pricing -->
            </div><!-- /.col-* -->

            <div class="col-sm-4">
                <div class="pricing">
                    <div class="pricing-title">Premium</div><!-- /.pricing-title -->
                    <div class="pricing-subtitle">Personal / Vendor</div><!-- /.pricing-subtitle -->
                    <div class="pricing-price"><span class="pricing-currency">Rp.</span>3.000k <span class="pricing-period">/ tahun</span></div><!-- /.pricing-price -->
                    <a href="<?php echo site_url('register');?>" class="btn-primary">Daftar Sekarang!</a>
                    <hr>
                    <ul class="pricing-list">
                        <li><span>Jumlah Kategori Terdaftar</span><strong>10</strong></li>
                        <li><span>Publikasi</span><strong>Event, Venue, Post, Job</strong></li>
                        <li><span>Management System</span><strong>EMS, VMS, PMS, JMS</strong></li>
                        <li><span>Advanced Management System</span><strong>EMS (Ticketing, Post Event Action)</strong></li>
                        <li><span>Featured</span><strong>Free 14hr / Listing</strong></li>
                        <li><span>Featured Partner</span><strong>--</strong></li>
                        <li><span>Featured Banner</span><strong>--</strong></li>
                        <li><span>Pre-Event</span><strong>Pengelolaan Project/Tender Online</strong></li>
                        <li><span>Spesial</span><strong>Diskon 10% untuk Event oleh HRPlasa</strong></li>
                    </ul><!-- /.pricing-list -->
                    <hr>
                    
                </div><!-- /.pricing -->
            </div><!-- /.col-* -->

            <div class="col-sm-4">
                <div class="pricing">
                    <div class="pricing-title">Premium</div><!-- /.pricing-title -->
                    <div class="pricing-subtitle">Vendor</div><!-- /.pricing-subtitle -->
                    <div class="pricing-price"><span class="pricing-currency">Rp.</span>10.000k <span class="pricing-period">/ tahun</span></div><!-- /.pricing-price -->
                    <a href="<?php echo site_url('register');?>" class="btn-primary">Daftar Sekarang!</a>
                    <hr>
                    <ul class="pricing-list">
                        <li><span>Jumlah Kategori Terdaftar</span><strong>Unlimited</strong></li>
                        <li><span>Publikasi</span><strong>Event, Venue, Post, Job</strong></li>
                        <li><span>Management System</span><strong>EMS, VMS, PMS, JMS</strong></li>
                        <li><span>Advanced Management System</span><strong>EMS (Ticketing, Post Event Action)</strong></li>
                        <li><span>Featured</span><strong>Free 30hr / Listing</strong></li>
                        <li><span>Featured Partner</span><strong>Display di Halaman Utama HRPlasa</strong></li>
                        <li><span>Featured Banner</span><strong>Posisi Teratas Halaman Vendor</strong></li>
                        <li><span>Pre-Event</span><strong>Pengelolaan Project/Tender Online</strong></li>
                        <li><span>Spesial</span><strong>Diskon 10% untuk Event oleh HRPlasa</strong></li>
                    </ul><!-- /.pricing-list -->
                    <hr>
                    
                </div><!-- /.pricing -->
            </div><!-- /.col-* -->
        </div><!-- /.row -->
    </div><!-- /.pricings -->