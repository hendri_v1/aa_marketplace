<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications_model extends CI_Model {

	function add_notification($type,$from,$to,$target)
	{
		$this->db->set('notification_type',$type);
		$this->db->set('notification_from',$from);
		$this->db->set('notification_to',$to);
		$this->db->set('notification_date',time());
		$this->db->set('notification_target',$target);
		$this->db->insert('notifications');
	}

	function get_my_notification($is_unread=TRUE,$limit=FALSE,$the_user,$start_from=0)
	{
		$this->db->join('user','user.user_id=notifications.notification_from');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		if($is_unread==TRUE)
			$this->db->where('notification_status',0);
		$this->db->where('notifications.notification_to',$the_user);
		if($limit==TRUE)
			$this->db->limit(10,$start_from);
		$this->db->order_by('notification_id','DESC');
		$query=$this->db->get('notifications');
		return $query;
	}

	function clear_notifications($user_id)
	{
		$this->db->set('notification_status',1);
		$this->db->where('notification_to',$user_id);
		$this->db->update('notifications');
	}

}	