<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

    var $table = 'category';

    public function __construct() {
        parent::__construct();
    }

    public function find($id) {
        return $this->db->where('category_id', $id)->get($this->table);
    }

    public function getAll($limit = '', $offset = '', $category_for = '') {
        if (!empty($limit)) {
            $this->db->limit($limit, $offset);
        }
        if (!empty($category_for)) {
            $this->db->where('category_for', $category_for);
        }
        return $this->db->get($this->table);
    }

    public function save($data = array(), $id = '') {
        return (!empty($id)) ? $this->db->where('category_id', $id)->limit(1)->update($this->table, $data) : FALSE;
    }

    public function remove($category_id = '') {
        return (!empty($category_id)) ? $this->db->where('category_id', $category_id)->limit(1)->delete($this->table) : FALSE;
    }

    public function add($data = array()) {
        return $this->db->insert($this->table, $data) ? $this->db->insert_id() : FALSE;
    }

}
