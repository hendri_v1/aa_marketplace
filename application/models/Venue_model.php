<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venue_model extends CI_Model {

	 function get_venue_types()
	 {
		$this->db->order_by('venue_type_name');
	 	$query=$this->db->get('venue_types');
	 	return $query;
	 }

	 function get_gallery_by_hash($the_hash,$gallery_type=0)
	 {
	 	$this->db->where('venue_gallery_hash',$the_hash);
	 	if($gallery_type==0)
	 		$this->db->where('venue_gallery_description','');
	 	else
	 		$this->db->where('venue_gallery_description','room_config');
	 	$query=$this->db->get('venue_galleries');
	 	return $query;

	 }

	 function get_gallery_by_venue($venue_id,$gallery_type=0)
	 {
	 	$this->db->where('venue_id',$venue_id);
	 	if($gallery_type==0)
	 		$this->db->where('venue_gallery_description','');
	 	else
	 		$this->db->where('venue_gallery_description','room_config');
	 	$query=$this->db->get('venue_galleries');
	 	return $query;

	 }

	 function add_venue_gallery($set_array=array())
	 {
	 	$this->db->set($set_array);
	 	$this->db->insert('venue_galleries');
	 }

	 function save_venue_main($set_array=array())
	 {
	 	$this->db->set($set_array);
	 	$this->db->insert('venue_main');
		return $this->db->insert_id();
	 }

	 function save_venue($set_array=array())
	 {
	 	$this->db->set($set_array);
	 	$this->db->insert('venues');
	 	return $this->db->insert_id();
	 }

	 function update_venue($set_array=array(),$venue_id)
	 {
	 	$this->db->set($set_array);
	 	$this->db->where('venue_id',$venue_id);
	 	$this->db->update('venues');
	 }

	 function update_gallery_hash($hash,$id)
	 {
	 	$this->db->set('venue_id',$id);
	 	$this->db->where('venue_gallery_hash',$hash);
	 	$this->db->update('venue_galleries');
	 }

	 function get_my_venue()
	 {
	 	$this->db->select('venue_main.*');
	 	$this->db->select('city.city_name');
	 	$this->db->select('province.province_name');
	 	$this->db->select('(SELECT COUNT(*) FROM venues where venues.venue_main_id=venue_main.venue_main_id) as total_room');
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->where('venue_main.venue_main_owner',$this->session->userdata('user_id'));
		$this->db->order_by('venue_main.venue_main_id','desc');
		$query=$this->db->get('venue_main');
		return $query;
	 }

	 function get_venue_main_by_id($venue_main_id)
	 {
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->join('user','user.user_id=venue_main.venue_main_owner');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		$this->db->where('venue_main.venue_main_id',$venue_main_id);
		$query=$this->db->get('venue_main');
		return $query->row();
	 }
	 
	 function get_venue($limit=0,$offset=0,$active_only=false,$pending_only=false)
	 {
		$this->db->join('user','user.user_id=venue_main.venue_main_owner');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		if($limit>0)
			$this->db->limit($limit,$offset);
		if($active_only==true)
			$this->db->where('venue_main.venue_main_status',1);
		if($pending_only==true)
			$this->db->where('venue_main.venue_main_status',0);
		$this->db->order_by('venue_main.venue_main_id','desc');
		$query=$this->db->get('venue_main');
		return $query;
	 }

	 function get_venue_front($filter_array)
	 {

	 	$this->db->select('venue_main.*');
	 	$this->db->select('(SELECT COUNT(*) FROM venues where venues.venue_main_id=venue_main.venue_main_id) as total_room');
	 	$this->db->select('(SELECT MIN(venue_price) FROM venues where venues.venue_main_id=venue_main.venue_main_id) as min_price');
	 	$this->db->select('city.*');
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->where('venue_main.venue_main_status',1);
		if(!empty($filter_array))
		{
			
			$this->db->where('province.province_id',$filter_array['province_id']);
		}
		$this->db->order_by('venue_main.venue_main_id','DESC');
		$query=$this->db->get('venue_main');
		return $query;
	 }

	 function get_venue_group_by_province()
	 {
	 	$this->db->select('province.province_id');
	 	$this->db->select('province.province_name');
	 	$this->db->select('COUNT(venue_main_id) as total_venues');
	 	$this->db->select('venue_main.venue_main_image');
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->where('venue_main.venue_main_status',1);
		$this->db->group_by('city.province_id');
		$this->db->order_by('total_venues','desc');

		$query=$this->db->get('venue_main');
		return $query;
	 }

	 function get_venue_group_by_city($province_id)
	 {
	 	$this->db->select('city.city_id');
	 	$this->db->select('city.city_name');
	 	$this->db->select('COUNT(venue_main_id) as total_venues');
	 	$this->db->select('venue_main.venue_main_image');
	 	$this->db->select('province.province_name');
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->where('venue_main.venue_main_status',1);
		$this->db->where('city.province_id',$province_id);
		$this->db->group_by('city.city_id');
		$this->db->order_by('total_venues','desc');

		$query=$this->db->get('venue_main');
		return $query;
	 }

	 function get_venue_by_province($province_id)
	 {
	 	$this->db->select('venue_main.*');
	 	$this->db->select('(SELECT COUNT(*) FROM venues where venues.venue_main_id=venue_main.venue_main_id) as total_room');
	 	$this->db->select('(SELECT MIN(venue_price) FROM venues where venues.venue_main_id=venue_main.venue_main_id) as min_price');
	 	$this->db->select('city.*');
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->where('venue_main.venue_main_status',1);
		$this->db->where('city.province_id',$province_id);
		$query=$this->db->get('venue_main');
		return $query;
	 }

	 function get_venue_by_city($city_id)
	 {
	 	$this->db->select('venue_main.*');
	 	$this->db->select('(SELECT COUNT(*) FROM venues where venues.venue_main_id=venue_main.venue_main_id) as total_room');
	 	$this->db->select('(SELECT MIN(venue_price) FROM venues where venues.venue_main_id=venue_main.venue_main_id) as min_price');
	 	$this->db->select('city.*');
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->where('venue_main.venue_main_status',1);
		$this->db->where('venue_main.city_id',$city_id);
		$query=$this->db->get('venue_main');
		return $query;
	 }

	 function get_venue_by_id($venue_id)
	 {
	 	$this->db->join('user','user.user_id=venue_main.venue_main_owner');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->where('venue_main.venue_main_id',$venue_id);
		$query=$this->db->get('venue_main');
		return $query->row();
	 }

	 function get_room_by_venue_id($venue_main_id)
	 {
	 	$this->db->join('venue_types','venue_types.venue_type_id=venues.venue_type_id');
	 	$this->db->where('venues.venue_main_id',$venue_main_id);
	 	$query=$this->db->get('venues');
	 	return $query;
	 }

	 function get_all_gallery($venue_main_id,$gallery_type=0)
	 {
	 	$this->db->join('venues','venues.venue_id=venue_galleries.venue_id');
	 	$this->db->join('venue_main','venue_main.venue_main_id=venues.venue_main_id');
	 	$this->db->where('venue_main.venue_main_id',$venue_main_id);
	 	if($gallery_type==0)
	 		$this->db->where('venue_galleries.venue_gallery_description','');
	 	else
	 		$this->db->where('venue_galleries.venue_gallery_description','room_config');
	 	$query=$this->db->get('venue_galleries');
	 	return $query;
	 }
	 
	 function activate_venue($venue_main_id)
	 {
		$this->db->set('venue_main_status',1);
		$this->db->where('venue_main_id',$venue_main_id);
		$this->db->update('venue_main'); 
	 }
	 
	 function get_facilities($not_all=0)
	 {
		if($not_all<>0)
			$this->db->where('venue_facility_type',$not_all);
		$this->db->order_by('venue_facility_name');
		$query=$this->db->get('venue_facilities');
		return $query; 
	 }
	 
	 function save_facility_connector($venue_facility_id,$venue_id)
	 {
		$this->db->set('venue_id',$venue_id);
		$this->db->set('venue_facility_id',$venue_facility_id);
		$this->db->insert('venue_facility_connector');
		 
	 }
	 
	 function get_facility_connector($venue_id,$venue_facility_type=0)
	 {
		$this->db->join('venue_facilities','venue_facilities.venue_facility_id=venue_facility_connector.venue_facility_id');
		if($venue_facility_type<>0)
			$this->db->where('venue_facilities.venue_facility_type',$venue_facility_type);
		$this->db->where('venue_facility_connector.venue_id',$venue_id);
		$query=$this->db->get('venue_facility_connector');
		return $query; 
	 }
	
	function get_checked($venue_facility_id,$venue_id)
	{
		$this->db->where('venue_id',$venue_id);
		$this->db->where('venue_facility_id',$venue_facility_id);
		$query=$this->db->get('venue_facility_connector');
		return $query->num_rows();
	}
	
	function clear_facilities($venue_id)
	{
		$this->db->where('venue_id',$venue_id);
		$this->db->delete('venue_facility_connector');
		
	}

	 function search_by_keyword($keyword='')
	 {
	 	$this->db->like('venue_name',$keyword);
	 	$this->db->or_like('venue_description',$keyword);
	 	$query=$this->db->get('venues');
	 	return $query;
	 }

	 function save_book_owner($set_array=array())
	 {
	 	$this->db->set($set_array);
	 	$this->db->insert('venue_booking');
	 	return $this->db->insert_id();
	 }

	 function save_venue_booking_detail($set_array=array())
	 {
	 	$this->db->set($set_array);
	 	$this->db->insert('venue_booking_detail');
	 }

	 function get_booking_pm($year,$month,$venue_id)
	 {
	 	$total_days=days_in_month($month,$year);
	 	$start_day=human_to_unix($year.'-'.$month.'-1 00:00:00');
	 	$end_day=human_to_unix($year.'-'.$month.'-'.$total_days.' 23:59:59');

	 	$this->db->where('venue_booking_start_date >=',$start_day);
	 	$this->db->where('venue_booking_start_date <=',$end_day);
	 	$this->db->where('venue_booking_status',1);
	 	$this->db->where('venue_id',$venue_id);
	 	$query=$this->db->get('venue_booking');
	 	return $query;
	 }

	 function get_my_booking($year,$month,$venue_id)
	 {
	 	$total_days=days_in_month($month,$year);
	 	$start_day=human_to_unix($year.'-'.$month.'-1 00:00:00');
	 	$end_day=human_to_unix($year.'-'.$month.'-'.$total_days.' 23:59:59');
	 	$this->db->join('venue_booking_detail','venue_booking_detail.venue_booking_id=venue_booking.venue_booking_id');
	 	$this->db->where('venue_booking.venue_booking_start_date >=',$start_day);
	 	$this->db->where('venue_booking.venue_booking_start_date <=',$end_day);
	 	
	 	//$this->db->where('venue_booking.venue_booking_status',1);
	 	$this->db->where('venue_booking.venue_id',$venue_id);
	 	$query=$this->db->get('venue_booking');
	 	return $query;
	 }

	 function get_my_unconfirmed_booking($venue_id)
	 {
	 	$this->db->join('venue_booking_detail','venue_booking_detail.venue_booking_id=venue_booking.venue_booking_id');
	 	$this->db->where('venue_booking_detail.user_id',$this->session->userdata('user_id'));
	 	$this->db->where('venue_booking.venue_booking_status',0);
	 	$this->db->where('venue_booking.venue_id',$venue_id);
	 	$this->db->order_by('venue_booking.venue_booking_start_date');
	 	$query=$this->db->get('venue_booking');
	 	return $query;
	 }

	 function remove_booking($venue_booking_id)
	 {
	 	$this->db->where('venue_booking_id',$venue_booking_id);
	 	$this->db->delete('venue_booking');

	 	$this->db->where('venue_booking_id',$venue_booking_id);
	 	$this->db->delete('venue_booking_detail');
	 }

	 function get_room_by_room_id($venue_id)
	 {
	 	$this->db->join('venue_main','venues.venue_main_id=venue_main.venue_main_id');
	 	$this->db->join('city','city.city_id=venue_main.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->where('venues.venue_id',$venue_id);
		$query=$this->db->get('venues');
		return $query;
	 }

	 function save_proposal($set_array=array())
	 {
	 	$this->db->set($set_array);
	 	$this->db->insert('proposal');
	 	return $this->db->insert_id();
	 }

	 function save_booking($venue_id,$user_id,$proposal_id)
	 {
	 	$this->db->join('venue_booking_detail','venue_booking_detail.venue_booking_id=venue_booking.venue_booking_id');
	 	$this->db->where('venue_booking.venue_id',$venue_id);
	 	$this->db->where('venue_booking_detail.user_id',$user_id);
	 	$this->db->where('venue_booking.venue_booking_status',0);
	 	$query=$this->db->get('venue_booking');

	 	foreach($query->result() as $rows)
	 	{
	 		$this->db->set('venue_booking_status',2);
	 		$this->db->set('proposal_id',$proposal_id);
	 		$this->db->where('venue_booking_id',$rows->venue_booking_id);
	 		$this->db->update('venue_booking');

	 		$this->db->set('venue_booking_detail_note',"Received!");
	 		$this->db->where('venue_booking_id',$rows->venue_booking_id);
	 		$this->db->update('venue_booking_detail');
	 	}

	 	return $query;

	 }


}