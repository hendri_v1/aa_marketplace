<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userproject_model extends CI_Model {
	
	function get_category()
	{
		$query=$this->db->get('projectcat');
		return $query;
	}
	function add_project($set_array=array())
	{
		$this->db->set($set_array);
		$this->db->insert('projects');
		
		return $this->db->insert_id();
	}
	
	function get_projects($isfeatured=false,$isurgent=false,$limit=0,$my_project=false,$active_only=false,$pending_only=false,$percategory=0)
	{

		$this->db->join('user','user.user_id=projects.user_id');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		//$this->db->join('projectcat','projects.projectcat_id=projectcat.projectcat_id');
		$this->db->join('city','city.city_id=projects.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		if($isfeatured==true)
			$this->db->where('projects.project_isfeatured',1);
		if($my_project==true)
			$this->db->where('projects.user_id',$this->session->userdata('user_id'));
		if($active_only==true)
		{
			$this->db->where('project_status',1);
			$this->db->where('project_end_date >=',time());
			$this->db->where('is_pending',1);
		}
		if($pending_only==true)
			$this->db->where('is_pending',0);
		if($percategory<>0)
			$this->db->where('projectcat_id',$percategory);
		if($limit>0)
			$this->db->limit($limit);
		$this->db->order_by('projects.project_id','DESC');
		$query=$this->db->get('projects');
		return $query;	
	}

	function get_projects_paginate($isfeatured=false,$isurgent=false,$my_project=false,$active_only=false,$limit=0,$ofset=0,$pending_only=false,$percategory=0)
	{

		$this->db->join('user','user.user_id=projects.user_id');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		//$this->db->join('projectcat','projects.projectcat_id=projectcat.projectcat_id');
		$this->db->join('city','city.city_id=projects.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		if($isfeatured==true)
			$this->db->where('projects.project_isfeatured',1);
		if($isurgent==true)
			$this->db->where('projects.project_isurgent',1);
		if($my_project==true)
			$this->db->where('projects.user_id',$this->session->userdata('user_id'));
		if($active_only==true)
		{
			$this->db->where('project_status',1);
			$this->db->where('project_end_date >=',time());
			$this->db->where('is_pending',1);
		}
		if($pending_only==true)
			$this->db->where('is_pending',0);
		if($percategory<>0)
			$this->db->where('projectcat_id',$percategory);
		if($limit>0)
			$this->db->limit($limit,$ofset);
		$this->db->order_by('projects.project_id','DESC');
		$query=$this->db->get('projects');
		return $query;	
	}

	function get_single_project($project_id)
	{
		$this->db->join('user','user.user_id=projects.user_id');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		$this->db->join('city','city.city_id=projects.city_id');
		$this->db->join('province','province.province_id=city.province_id');
		//$this->db->join('projectcat','projects.projectcat_id=projectcat.projectcat_id');
		$this->db->where('projects.project_id',$project_id);
		$query=$this->db->get('projects');
		return $query->row();
	}
	function get_list_project($user_id)
	{
		$this->db->where('user_id',$user_id);
		$query=$this->db->get('projects');
		return $query;
	}

	function add_bid($set_array=array())
	{
		$this->db->set($set_array);
		$this->db->insert('projectbid');
	}

	function get_bidder($project_id)
	{
		$this->db->where('projectbid.project_id',$project_id);
		$this->db->join('user','projectbid.projectbid_user_id=user.user_id');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		$query=$this->db->get('projectbid');
		return $query;
	}

	function count_shortlisted($project_id)
	{
		$this->db->where('projectbid.project_id',$project_id);
		$query=$this->db->get('projectbid');
		return $query->num_rows();
	}

	function get_single_bidder($projectbid_id)
	{
		$this->db->where('projectbid.projectbid_id',$projectbid_id);
		$this->db->join('user','projectbid.projectbid_user_id=user.user_id');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		$query=$this->db->get('projectbid');
		return $query->row();
	}

	function get_single_bidder_id($projectbid_id)
	{
		$this->db->where('projectbid.projectbid_id',$projectbid_id);
		$this->db->where('projectbid.projectbid_user_id',$this->session->userdata('user_id'));
		$query=$this->db->get('projectbid');
		return $query->row_array();
	}


	function update_winner($project_id,$projectbid_id)
	{
		$this->db->set('project_status',2);
		$this->db->set('project_winner',$projectbid_id);
		$this->db->where('project_id',$project_id);
		$this->db->update('projects');
	}

	function update_shortlisted($projectbid_id)
	{
		$this->db->set('is_shortlisted',1);
		$this->db->where('projectbid_id',$projectbid_id);
		$this->db->update('projectbid');
	}

	function have_bidded($project_id)
	{
		$this->db->where('projectbid_user_id',$this->session->userdata('user_id'));
		$this->db->where('project_id',$project_id);
		$query=$this->db->get('projectbid');
		return $query->num_rows();
	}

	function bidding_list($user_id)
	{
		$this->db->join('projects','projects.project_id=projectbid.project_id');
		$this->db->where('projectbid.projectbid_user_id',$user_id);
		$this->db->order_by('projectbid.projectbid_id','DESC');
		$query=$this->db->get('projectbid');
		return $query;
	}

	function bidding_list_paginate($user_id,$limit=0,$ofset=0)
	{
		$this->db->join('projects','projects.project_id=projectbid.project_id');
		$this->db->where('projectbid.projectbid_user_id',$user_id);
		$this->db->order_by('projectbid.projectbid_id','DESC');
		if($limit>0)
			$this->db->limit($limit,$ofset);
		$query=$this->db->get('projectbid');
		return $query;
	}

	function save_billing($set_array)
	{
		$this->db->set($set_array);
		$this->db->insert('billing');
		return $this->db->insert_id();
	}

	function save_billing_details($set_array)
	{
		$this->db->set($set_array);
		$this->db->insert('billingdetail');
		return $this->db->insert_id();
	}


	function get_single_billing($billing_type,$billing_for)
	{
		$this->db->where('billing_type',$billing_type);
		$this->db->where('billing_for',$billing_for);
		$query=$this->db->get('billing');
		return $query;
	}

	function get_detail_billing($billing_id)
	{
		$this->db->where('billing_id',$billing_id);
		$query=$this->db->get('billingdetail');
		return $query;
	}

	function publish($project_id)
	{
		$this->db->set('is_pending',1);
		$this->db->where('project_id',$project_id);
		$this->db->update('projects');
	}

	function reject($project_id)
	{
		$this->db->set('is_pending',0);
		$this->db->set('project_status',5);
		$this->db->where('project_id',$project_id);
		$this->db->update('projects');
	}

	function update_billing($billing_id)
	{
		$this->db->set('billing_status',1);
		$this->db->where('billing_id',$billing_id);
		$this->db->update('billing');
	}

	function save_payment($set_array=array())
	{
		$this->db->set($set_array);
		$this->db->insert('payment');
	}

	function get_payment_by_id($billing_id)
	{
		$this->db->where('billing_id',$billing_id);
		$query=$this->db->get('payment');
		return $query;
	}

	function save_comment($set_array)
	{
		$this->db->set($set_array);
		$this->db->insert('comments');
		return $this->db->insert_id();
	}

	function get_comments($where=array())
	{
		$this->db->join('user','user.user_id=comments.user_id');
		$this->db->join('userprofile','user.user_id=userprofile.user_id');
		$this->db->where($where);
		$this->db->order_by('comment_id','DESC');
		$query=$this->db->get('comments');
		return $query;
	}

	function save_rating($set_array)
	{
		$this->db->set($set_array);
		$this->db->insert('rating');
		return $this->db->insert_id();
	}
	
}