<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Event_model extends CI_Model {

    var $table_events = "events";
    var $table_eventcategory = "eventcategory";
    var $table_eventtickets = "eventtickets";

    function get_event($limit = 0, $ofset = 0, $active_only = FALSE, $pending_only = FALSE, $out_of_date = FALSE, $draft_only = FALSE, $order_by=0, $private_only = FALSE)
    {
        $this->db->join('eventcategory', 'eventcategory.eventcategory_id=events.eventcategory_id');
        $this->db->where('events.event_owner', $this->session->userdata('user_id'));
        $this->db->where('events.deleted_at',0);
        if ($draft_only == TRUE) {
            $this->db->where('events.is_draft', 1);
            $this->db->where('events.event_start_date >=', time());
        }
        if ($active_only == TRUE) {
            $this->db->where('events.is_draft', 0);
            $this->db->where('events.is_pending', 1);
            $this->db->where('events.event_start_date >=', time());
        }
        if ($pending_only == TRUE) {
            $this->db->where('events.is_draft', 0);
            $this->db->where('events.is_pending', 0);
            $this->db->where('events.event_start_date >= ', time());
        }

        if ($private_only == TRUE)
        {
            $this->db->where('events.is_private', 1);
        }

        if ($out_of_date == TRUE) {
            $this->db->where('events.event_start_date < ', time());
        }
        if ($order_by == 0)
        {
            $this->db->order_by('events.event_id', 'DESC');
        }
        elseif($order_by==1)
        {
            $this->db->order_by('events.event_title', 'ASC');
        }
        elseif($order_by==2)
        {
            $this->db->order_by('events.event_title', 'DESC');
        }
        elseif($order_by==3)
        {
            $this->db->order_by('events.event_totalview', 'DESC');
        }
        elseif($order_by==4)
        {
            $this->db->order_by('events.event_start_date','ASC');
        }
        elseif($order_by==5)
        {
            $this->db->order_by('events.event_start_date','DESC');
        }

        if ($limit > 0)
            $this->db->limit($limit, $ofset);
        
        $result = $this->db->get($this->table_events);
        return $result;
    }

    function get_event_by_user($limit = 0, $ofset = 0, $user_id, $active_only = TRUE)
    {
        $this->db->join('eventcategory', 'eventcategory.eventcategory_id=events.eventcategory_id');
        $this->db->where('events.deleted_at',0);
        $this->db->where('events.event_owner', $user_id);
        $this->db->where('events.is_pending', 1);
        $this->db->where('events.event_start_date >=', time());
        if ($limit > 0)
            $this->db->limit($limit, $ofset);
        if ($active_only == true) {
            $this->db->where('events.is_pending', 1);
            $this->db->where('events.event_start_date >=', time());
        }
        $this->db->order_by('events.event_id', 'DESC');
        $result = $this->db->get($this->table_events);
        return $result;
    }

    function get_event_paginate($limit = 0, $offset = 0, $active_only = FALSE, $pending_only = FALSE, $is_featured = FALSE, $province_id = 0, $city_id = 0, $eventcategory_id = 0, $event_keyword = '', $order_events_by = 0, $min_price = 0, $max_price = 0,$premium_only=false)
    {
        $this->db->join('user', 'user.user_id=events.event_owner');
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=events.city_id');
        $this->db->join('province', 'province.province_id=city.province_id');
        $this->db->join('eventcategory', 'eventcategory.eventcategory_id=events.eventcategory_id');
        if ($limit <> 0)
            $this->db->limit($limit, $offset);
        $this->db->where('events.deleted_at',0);
        if ($active_only == true) {
            $this->db->where('events.is_draft', 0);
            $this->db->where('events.is_pending', 1);
            $this->db->where('events.event_start_date >=', time());
        } elseif ($active_only == false) {
            if ($pending_only == true)
            {
                $this->db->where('events.is_private', 0);
                $this->db->where('events.is_pending', 0);
            }
            else
                $this->db->where('events.is_pending !=', 0);
        }
        if ($min_price > 0) {
            $this->db->where('events.budget_event >=', (int) $min_price);
        }
        if ($max_price > 0) {
            $this->db->where('events.budget_event <=', (int) $max_price);
        }
        if ($is_featured == true) {
            $this->db->where('events.is_featured', 2);
            $this->db->where('events.event_featured_valid >=', time());
        }
        if ($province_id > 0)
            $this->db->where('province.province_id', $province_id);
        if ($city_id > 0)
            $this->db->where('city.city_id', $city_id);
        if ($eventcategory_id > 0)
            $this->db->where('events.eventcategory_id', $eventcategory_id);
        if ($event_keyword <> '')
            $this->db->where("(events.event_title like '%$event_keyword%' or events.event_description like '%$event_keyword%')");
        if ($premium_only==true)
        {
            $this->db->where('user.is_premium',1);
        }
        $this->db->order_by('events.is_featured','DESC');
        if ($order_events_by == 0)
            $this->db->order_by('events.event_start_date', 'ASC');
        elseif ($order_events_by == 1)
            $this->db->order_by('events.event_start_date', 'DESC');
        elseif ($order_events_by == 2)
            $this->db->order_by('events.event_title', 'ASC');
        elseif ($order_events_by == 3)
            $this->db->order_by('events.event_title', 'DESC');
        elseif ($order_events_by == 4)
            $this->db->order_by('events.eventcategory_id', 'ASC');
        $query = $this->db->get('events');
        return $query;
    }

    function get_featured_list($limit = 0, $offset = 0, $pending_only = FALSE, $order_events_by = 0)
    {
        $this->db->join('user', 'user.user_id=events.event_owner');
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=events.city_id');
        $this->db->join('province', 'province.province_id=city.province_id');
        $this->db->join('eventcategory', 'eventcategory.eventcategory_id=events.eventcategory_id');
        if ($limit > 0)
            $this->db->limit($limit, $offset);
        $this->db->where('events.deleted_at',0);
        if ($pending_only == TRUE)
            $this->db->where('events.is_featured', 1);
        else
            $this->db->where('events.is_featured', 2);

        if ($order_events_by == 0)
            $this->db->order_by('events.event_start_date', 'ASC');
        elseif ($order_events_by == 1)
            $this->db->order_by('events.event_start_date', 'DESC');
        elseif ($order_events_by == 2)
            $this->db->order_by('events.event_title', 'ASC');
        elseif ($order_events_by == 3)
            $this->db->order_by('events.event_title', 'DESC');
        elseif ($order_events_by == 4)
            $this->db->order_by('events.eventcategory_id', 'ASC');

        $query = $this->db->get('events');
        return $query;
    }

    function get_popular_events($limit = 0, $offset = 0)
    {
        $this->db->join('user', 'user.user_id=events.event_owner');
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=events.city_id');
        $this->db->join('province', 'province.province_id=city.province_id');
        $this->db->join('eventcategory', 'eventcategory.eventcategory_id=events.eventcategory_id');
        $this->db->limit($limit, $offset);
        $this->db->where('events.is_pending', 1);
        $this->db->where('events.event_start_date >=', time());
        $this->db->where('events.deleted_at',0);
		  $this->db->where('events.added_at >=',time()-(30*86400));
        $this->db->order_by('events.event_totalview', 'DESC');
        $query = $this->db->get('events');
        return $query;
    }

    function get_max_price_ticket()
    {
        $this->db->select_max('budget_event');
        $this->db->limit(1);
        $results = $this->db->get($this->table_events);
        if ($results->num_rows() > 0) {
            $data = $results->row();
            return $data->budget_event;
        } else
            return 0;
    }

    function get_eventcategory()
    {
        $result = $this->db->get($this->table_eventcategory);
        return $result;
    }

    function get_event_by_category($category_id)
    {
        $this->db->where('eventcategory_id', $category_id);
        $this->db->where('deleted_at',0);
        $query = $this->db->get('events');
        return $query;
    }

    function save_category($category_name)
    {
        $this->db->set('eventcategory_name', $category_name);
        $this->db->insert($this->table_eventcategory);
    }

    function save_event($post)
    {
        $this->db->insert($this->table_events, $post);
        return $this->db->insert_id();
    }

    function add_keywords($keyword_text, $connect_to)
    {
        $this->db->where('keyword_text', $keyword_text);
        $querycheck = $this->db->get('keywords');
        $totalcheck = $querycheck->num_rows();
        if ($totalcheck == 0) {
            $this->db->set('keyword_text', $keyword_text);
            $this->db->insert('keywords');
            $the_id = $this->db->insert_id();
        } else {
            $rowcheck = $querycheck->row();
            $the_id = $rowcheck->keyword_id;
        }
        $this->db->set('keyword_id', $the_id);
        $this->db->set('event_id', $connect_to);
        $this->db->insert('eventconnector');
    }

    function add_speaker($set_array = array())
    {
        $this->db->set($set_array);
        $this->db->insert('eventspeakers');
    }

    function get_speaker_by_hash($eventspeaker_hash)
    {
        $this->db->where('eventspeaker_hash', $eventspeaker_hash);
        $query = $this->db->get('eventspeakers');
        return $query;
    }

    function get_speaker_by_event_id($event_id)
    {
        $this->db->where('event_id', $event_id);
        $query = $this->db->get('eventspeakers');
        return $query;
    }

    function get_my_speaker()
    {
        $this->db->join('events','events.event_id=eventspeakers.event_id');
        $this->db->where('events.event_owner',$this->session->userdata('user_id'));
        $this->db->group_by('eventspeakers.eventspeaker_name');
        $query=$this->db->get('eventspeakers');
        return $query;
    }

    function get_speaker_by_id($speaker_id)
    {
        $this->db->where('eventspeaker_id',$speaker_id);
        $query=$this->db->get('eventspeakers');
        return $query;
    }

    function get_event_by_id($id = 0)
    {
        $this->db->join('user', 'user.user_id=events.event_owner');
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=events.city_id');
        $this->db->join('province', 'city.province_id=province.province_id');
        $this->db->join('eventcategory', 'eventcategory.eventcategory_id=events.eventcategory_id');
        $this->db->where('event_id', $id);
        $result = $this->db->get($this->table_events);
        return $result;
    }

    function get_related_event($id)
    {
        $this->db->select('eventcategory_id, event_owner, keyword_text');
        $this->db->join('eventconnector as b', 'a.event_id = b.event_id');
        $this->db->join('keywords as c', 'c.keyword_id = b.keyword_id');
        $this->db->where('a.event_id', $id);
        $data = $this->db->get($this->table_events . ' as a');
        $result = array();
        if ($data->num_rows() > 0) {
            $row = $data->row();
            $this->db->join('user', 'user.user_id=events.event_owner');
            $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
            $this->db->join('city', 'city.city_id=events.city_id');
            $this->db->join('province', 'city.province_id=province.province_id');
            $this->db->join('eventconnector as b', $this->table_events . '.event_id = b.event_id');
            $this->db->join('keywords as c', 'c.keyword_id = b.keyword_id');
            $this->db->where('eventcategory_id', $row->eventcategory_id);
            $this->db->where('event_owner', $row->event_owner);
            $this->db->where($this->table_events . '.event_id != ', $id);
            $this->db->like('keyword_text', $row->keyword_text);
            $this->db->where('events.is_pending', 1);
            $this->db->where('events.event_start_date >=', time());
            $this->db->where('events.deleted_at',0);
            $this->db->limit(3);
            $result = $this->db->get($this->table_events);
        }


        return $result;
    }

    function get_registered_event($id = 0)
    {
        $this->db->where('event_id', $id);
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $result = $this->db->get('event_audience');
        return $result;
    }

    function get_audience($id = 0)
    {
        $this->db->join('user', 'user.user_id=event_audience.user_id');
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=userprofile.city_id');
        $this->db->join('province', 'city.province_id=province.province_id');
        $this->db->where('event_id', $id);
        $result = $this->db->get('event_audience');
        return $result;
    }

    function save_audience($set_array = array())
    {
        $this->db->set($set_array);
        $this->db->insert('event_audience');
        return $this->db->insert_id();
    }

    function check_registered($where_array=array())
    {
        $this->db->where($where_array);
        $query=$this->db->get('event_audience');
        return $query->num_rows();
    }

    public function del_key_event($connect_to = 0)
    {
        $this->db->where('event_id', $connect_to);
        $this->db->delete('eventconnector');
    }

    function update_event($event_id, $post)
    {
        $this->db->set($post);
        $this->db->where('event_id', $event_id);
        $this->db->update('events');
    }

    function publish_event($event_id)
    {
        $this->db->set('is_pending', 1);
        $this->db->where('event_id', $event_id);
        $this->db->update('events');
    }

    function unpublish_event($event_id)
    {
        $this->db->set('is_pending', 0);
        $this->db->where('event_id', $event_id);
        $this->db->update('events');
    }

    function search_by_keyword($keyword = '', $search_method = 1, $location = 0)
    {
        if ($search_method == 1) {
            $this->db->join('keywords', 'keywords.keyword_id=eventconnector.keyword_id');
            $this->db->join('events', 'eventconnector.event_id=events.event_id');
        }
        $this->db->join('user', 'user.user_id=events.event_owner');
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=events.city_id');
        $this->db->join('province', 'province.province_id=city.province_id');
        $this->db->join('eventcategory', 'eventcategory.eventcategory_id=events.eventcategory_id');
        $this->db->where('events.is_pending', 1);
        $this->db->where('events.event_start_date >=', time());
        if ($location > 0) {
            $this->db->where('province.province_id', $location);
        }
        if ($search_method == 1) {
            $this->db->like('keywords.keyword_text', $keyword);
        } elseif ($search_method == 2) {
            $this->db->like('events.event_title', $keyword);
        } elseif ($search_method == 3) {
            $this->db->where("(events.event_title like '%$keyword%' or events.event_description like '%$keyword%')");
        }
        $this->db->order_by('events.event_start_date', 'ASC');
        if ($search_method == 1) {
            $this->db->group_by('events.event_id');
            $query = $this->db->get('eventconnector');
        } else {
            $query = $this->db->get('events');
        }
        return $query;
    }

    function add_eventticket($set_array = array())
    {
        $this->db->set($set_array);
        $this->db->insert('eventtickets');
    }

    function get_eventticket($event_id)
    {
        $this->db->where('event_id', $event_id);
        $query = $this->db->get('eventtickets');
        return $query;
    }

    function edit_eventticket($eventticket_id,$set_array)
    {
        $this->db->set($set_array);
        $this->db->where('eventticket_id',$eventticket_id);
        $this->db->update('eventtickets');
    }

    function update_total_view($event_id)
    {
        $query = "update events set event_totalview=event_totalview+1 where event_id=" . $event_id;
        $this->db->query($query);
    }

    function min_total_ticket($eventticket_id)
    {
        $query = "update eventtickets set eventticket_seat=eventticket_seat-1 where eventticket_id=" . $eventticket_id;
        $this->db->query($query);
    }

    function min_early_bird($eventticket_id)
    {
        $query = "update eventtickets set eventticket_earlybird_seat=eventticket_earlybird_seat-1 where eventticket_id=" . $eventticket_id;
        $this->db->query($query);
    }

    function get_next_week()
    {
        $today = time();
        $sevenday = time() + (7 * 86400);
        $this->db->join('user', 'user.user_id=events.event_owner');
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=events.city_id');
        $this->db->join('province', 'province.province_id=city.province_id');
        $this->db->join('eventcategory', 'eventcategory.eventcategory_id=events.eventcategory_id');
        $this->db->where('events.is_pending', 1);
        $this->db->where('events.event_start_date >=', $today);
        $this->db->where('events.event_start_date <=', $sevenday);
        $query = $this->db->get('events');
        return $query;
    }

    function save_comment($set_array)
    {
        $this->db->set($set_array);
        $this->db->insert('comments');
        return $this->db->insert_id();
    }

    function get_comments($where = array())
    {
        $this->db->join('user', 'user.user_id=comments.user_id');
        $this->db->join('userprofile', 'user.user_id=userprofile.user_id');
        $this->db->where($where);
        $this->db->order_by('comment_id', 'DESC');
        $query = $this->db->get('comments');
        return $query;
    }

    function make_featured($event_id)
    {
        $this->db->set('is_featured', 2);
        $this->db->set('event_featured_valid', strtotime('+7 day', time()));
        $this->db->where('event_id', $event_id);
        $this->db->update('events');
    }

    function stop_featured($event_id)
    {
        $this->db->set('is_featured', 0);
        $this->db->where('event_id', $event_id);
        $this->db->update('events');
    }

    function delete_event($event_id)
    {
        $this->db->set('deleted_at',time());
        $this->db->where('event_id',$event_id);
        $this->db->update('events');
    }

}
