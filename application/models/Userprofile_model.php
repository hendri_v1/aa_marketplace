<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userprofile_model extends CI_Model {
	
	function update_profile($set_array=array())
	{
		$this->db->set($set_array);
		$this->db->where('user_id',$this->session->userdata('user_id'));
		$this->db->update('userprofile');
	}

	function update_profile2($set_array=array(),$user_id)
	{
		$this->db->set($set_array);
		$this->db->where('user_id',$user_id);
		$this->db->update('userprofile');	
	}
	
	function get_extra($userextra_type='',$userprofile_id='')
	{
		$this->db->where('userextra_type',$userextra_type);
		$this->db->where('userprofile_id',$userprofile_id);
		$query=$this->db->get('userextra');
		return $query;
	}
	
	function add_extra($set_array=array())
	{
		$this->db->set($set_array);
		$this->db->insert('userextra');
	}

	function get_extra_single($id='')
	{
		$this->db->where('userextra_id',$id);
		$query=$this->db->get('userextra');
		return $query;
	}

	function update_extra($set_array=array(),$id='')
	{
		$this->db->set($set_array);
		$this->db->where('userextra_id',$id);
		$this->db->update('userextra');
	}

	function get_rating($rating_type,$user_id)
	{
		$this->db->join('comments','comments.comment_id=rating.rating_comment');
		$this->db->where('rating.rating_type',$rating_type);
		$this->db->where('rating.rating_for',$user_id);
		$query=$this->db->get('rating');
		return $query;
	}

	function get_user_category($user_id)
	{
		$this->db->where('user_id',$user_id);
		$query=$this->db->get('category_connector');
		//echo $this->db->last_query();
		return $query->row();
	}

	function list_per_category($category_id,$category_connector_type)
	{
		$this->db->join('user','user.user_id=category_connector.user_id');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		$this->db->where('category_connector.category_connector_type',$category_connector_type);
		$this->db->where('category_connector.category_id',$category_id);
		$query=$this->db->get('category_connector');
		return $query;
	}
	


}