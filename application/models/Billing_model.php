<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Billing_model extends CI_Model {
	
	function get_billing_by_id($billing_id)
	{
			
		$this->db->where('billing.billing_id',$billing_id);
		$query=$this->db->get('billing');
		return $query;
	}
	
	function get_detail_billing_by_id($billing_id)
	{
		$this->db->where('billing_id',$billing_id);
		$query=$this->db->get('billingdetail');
		return $query;	
	}

	function save_billing($set_array)
	{
		$this->db->set($set_array);
		$this->db->insert('billing');
		return $this->db->insert_id();
	}

	function save_billing_details($set_array)
	{
		$this->db->set($set_array);
		$this->db->insert('billingdetail');
		return $this->db->insert_id();
	}

	function get_single_billing($billing_type,$billing_for)
	{
		$this->db->where('billing_type',$billing_type);
		$this->db->where('billing_for',$billing_for);
		$query=$this->db->get('billing');
		return $query;
	} 

	function get_all_billing()
	{
		$this->db->where('billing_status',0);
		$this->db->order_by('billing_id','DESC');
		$query=$this->db->get('billing');
		return $query;
	}

	function get_all_billing_grouped($paid=FALSE)
	{
		$this->db->select('billing.billing_id');
		$this->db->select('billing.billing_for');
		$this->db->select('billing.billing_type');
		$this->db->select('billing.billing_date');
		$this->db->select('billing.billing_due');
		$this->db->select('SUM(billingdetail.billingdetail_total) as billing_total');
		$this->db->join('billing','billing.billing_id=billingdetail.billing_id');
		if($paid==FALSE)
			$this->db->where('billing_status',0);
		else
			$this->db->where('billing_status',1);
		$this->db->order_by('billingdetail.billingdetail_id','DESC');
		$this->db->group_by('billing.billing_id');
		$query=$this->db->get('billingdetail');
		return $query;	
	}

	function get_pricing_by_type($pricing_for)
	{
		$this->db->where('pricing_for',$pricing_for);
		$query=$this->db->get('pricing');
		return $query;
	}

	function get_pricing_by_id($pricing_id)
	{
		$this->db->where('pricing_id',$pricing_id);
		$query=$this->db->get('pricing');
		return $query->row();
	}

	function get_my_billing_grouped()
	{
		$this->db->select('billing.billing_id');
		$this->db->select('billing.billing_type');
		$this->db->select('billing.billing_for');
		$this->db->select('billing.billing_date');
		$this->db->select('billing.billing_due');
		$this->db->select('billing.billing_status');
		$this->db->select('SUM(billingdetail.billingdetail_total) as billing_total');
		$this->db->join('billing','billing.billing_id=billingdetail.billing_id');
		$this->db->where('billing.billing_for',$this->session->userdata('user_id'));
		$this->db->where('billing.billing_type',3);
		$this->db->order_by('billingdetail.billingdetail_id','DESC');
		$this->db->group_by('billing.billing_id');
		$query=$this->db->get('billingdetail');
		return $query;	
	}

	
		
}