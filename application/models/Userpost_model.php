<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userpost_model extends CI_Model {

	protected $table_posts = 'posts';
	protected $table_events = 'events';
	protected $table_eventcategory = 'eventcategory';
	
	function get_mypost($limit=0,$ofset=0,$job_post=false)
	{
		$this->db->join('category','category.category_id = posts.post_type_id');
		
		$this->db->where('post_owner',$this->session->userdata('user_id'));
		if($job_post==true)
			$this->db->where('posts.post_type_id',999);
		if($limit>0)
			$this->db->limit($limit,$ofset);
		$result = $this->db->get($this->table_posts);
	 	return $result;
	}

	function get_list_post($userid)
	{
		$this->db->join('category','category.category_id = posts.post_type_id');
		$this->db->where('post_owner',$userid);
		$result = $this->db->get($this->table_posts);
	 	return $result;
	}

	function get_blog($limit=0,$ofset=0)
	{
		$this->db->join('category','category.category_id = posts.post_type_id');
		$this->db->join('user','user.user_id=posts.post_owner');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		if($limit>0)
			$this->db->limit($limit,$ofset);
		$this->db->order_by('post_id','desc');
		$result = $this->db->get($this->table_posts);
	 	return $result;
	}

	function get_job($limit=0,$offset=0)
	{
		//$this->db->join('category','category.category_id = posts.post_type_id');
		$this->db->join('user','user.user_id=posts.post_owner');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		if($limit>0)
			$this->db->limit($limit,$offset);
		$this->db->where('posts.post_type_id',999);
		$this->db->order_by('posts.post_id','DESC');
		$result = $this->db->get($this->table_posts);
	 	return $result;	
	}

	
	function get_post_by_id($id=0)
	{
		//$this->db->join('category','category.category_id = posts.post_type_id');
		$this->db->join('user','user.user_id=posts.post_owner');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		$this->db->where('posts.post_id',$id);
		$result = $this->db->get($this->table_posts);
	 	return $result;
	}

	function get_blog_by_id($id=0)
	{
		$this->db->join('category','category.category_id = posts.post_type_id');
		$this->db->join('user','user.user_id=posts.post_owner');
		$this->db->join('userprofile','userprofile.user_id=user.user_id');
		$this->db->where('posts.post_id',$id);
		$result = $this->db->get($this->table_posts);
	 	return $result;
	}

	function update_post($set_array,$post_id)
	{
		$this->db->set($set_array);
		$this->db->where('post_id',$post_id);
		$this->db->update('posts');
	}

	function get_documents()
	{
		$query=$this->db->get('documents');
		return $query;
	}

	function save_documents($set_array=array())
	{
		$this->db->set($set_array);
		$this->db->insert('documents');
	}
	
	
	function dell_img_event($id='')
	{
		$this->db->set('event_picture','');
		$this->db->where('event_id',$id);
		$this->db->update('events');
	}

}

/* End of file userpost_model.php */
/* Location: ./application/models/userpost_model.php */