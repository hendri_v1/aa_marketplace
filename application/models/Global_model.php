<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Global_model extends CI_Model {
	
	function get_membership($user_status,$is_premium)
	{
		$return='';
		if($user_status==2)
			$return="Partnership";
		elseif($user_status==1)
		{
			if($is_premium==0)
				$return="Free - Menunggu Premium";
			else
				$return="Premium";
		}
		else
			$return="Free";
		return $return;
	}
	
	function get_usertype($usertype_id)
	{
		$usertype=array(1=>'Individual Contributor',2=>'Corporate Vendor / Supplier',3=>'Professional - Non HR');
		return $usertype[$usertype_id];	
	}

	function get_billing_status($billing_status)
	{
		$billing_statuss=array(0=>'Unpaid',1=>'Paid',2=>'Pending Paid');
		return $billing_statuss[$billing_status];
	}
	
	function get_billing_type($billing_type)
	{
		$billing_type=array(2=>'Event Billing',3=>'User Billing',1=>'Project Billing');
		return $billing_type[$billing_type];	
	}
	
	function list_userfunction()
	{
		$userfunction=array(98=>'Personal',1=>'Associate Trainer',2=>'Asessor',3=>'HR Consultant',4=>'Coach',5=>'HR Expert Professional',6=>'Executives',7=>'Others',23=>'Recruiter',24=>'Writer',25=>'Translator');
		return $userfunction;	
	}

	function list_userfunction2()
	{
		$userfunction=array(99=>'General',8=>'HR Consulting',9=>'Training Provider',10=>'Assessment Center',11=>'Team Building',12=>'Event Organizer',13=>'Training and Meeting Venue Provider',14=>'Multimedia and Games',16=>'HR IT Solutions',17=>'Outsourcing Services',18=>'Catering',19=>'IT Rental Hardware',20=>'Head Hunter and Recruiter',21=>'Translator Service',22=>'Others');
		return $userfunction;
	}
	
	function get_userfunction($userfunction_id)
	{
		$userfunction=array(0=>'Personal',1=>'Associate Trainer',2=>'Asessor',3=>'HR Consultant',4=>'Coach',5=>'HR Expert Professional',6=>'Executives',7=>'Others');
		return $userfunction[$userfunction_id];
	}

	function get_userfunction2($userfunction_id)
	{
		$userfunction=array(0=>'General',8=>'HR Consulting',9=>'Training Provider',10=>'Assessment Center',11=>'Team Building',12=>'Event Organizer',13=>'Training and Meeting Venue Provider',14=>'Multimedia and Games',15=>'Others');
		return $userfunction[$userfunction_id];
	}
	
	function get_all_user($vendor=FALSE,$individual=FALSE)
	{
		if($vendor==TRUE)
		{
			$this->db->join('userprofile','userprofile.user_id=user.user_id');
			$this->db->where('userprofile.usertype_id',2);
		}
		if($individual==TRUE)
		{
			$this->db->join('userprofile','userprofile.user_id=user.user_id');
			$this->db->where('userprofile.usertype_id',1);
		}
		$query=$this->db->get('user');
		return $query->num_rows();	
	}
	
	function get_all_project()
	{
		$query=$this->db->get('projects');
		return $query->num_rows();	
	}
	
	function get_all_event($active_only=FALSE)
	{
		if($active_only==true)
		{
			$this->db->where('events.is_pending',1);
			$this->db->where('events.event_start_date >=', time());
		}
		$query=$this->db->get('events');
		return $query->num_rows();	
	}
	
	function get_all_budget()
	{
		$this->db->select_sum('project_budget');
		$query=$this->db->get('projects');
		return $query->row()->project_budget;	
	}
	
	function get_user_photo($userprofile_photo)
	{
		$user_photo='';
		if($userprofile_photo=='')
			$user_photo='profile-pic.jpg';
		else
			$user_photo=$userprofile_photo;
		return $user_photo;
		
	}

	function get_project_status($project_status)
	{
		if($project_status==1)
			echo "Open";
		elseif($project_status==2)
			echo "Closed";
	}

	function valid_email($str)
	{
	    return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
	}

	function get_province()
	{
		$query=$this->db->get('province');
		return $query->result();
	}

	function get_province_by_id($province_id)
	{
		$this->db->where('province_id',$province_id);
		$query=$this->db->get('province');
		return $query->row();
	}

	function get_city($province_id = 0)
	{
		$this->db->where('province_id',$province_id);
		$query=$this->db->get('city');
		return $query;
	}

	function get_city_by_id($city_id)
	{
		$this->db->join('province','province.province_id=city.province_id');
		$this->db->where('city.city_id',$city_id);
		$query=$this->db->get('city');
		return $query->row();
	}

	function add_keywords($keyword_text,$connector_type,$connect_to)
	{
		$this->db->where('keyword_text',$keyword_text);
		$querycheck=$this->db->get('keywords');
		$totalcheck=$querycheck->num_rows();
		if($totalcheck==0)
		{
			$this->db->set('keyword_text',$keyword_text);
			$this->db->insert('keywords');
			$the_id=$this->db->insert_id();
		}
		else
		{
			$rowcheck=$querycheck->row();
			$the_id=$rowcheck->keyword_id;
		}
		$this->db->set('keyword_connector_type',$connector_type);
		$this->db->set('keyword_id',$the_id);
		if($connector_type==1)
			$this->db->set('user_id',$connect_to);
		elseif($connector_type==2)
			$this->db->set('project_id',$connect_to);
		$this->db->insert('keyword_connector');

	}

	function get_keywords($connect_type,$connect_to)
	{
		$this->db->join('keywords','keywords.keyword_id=keyword_connector.keyword_id');
		if($connect_type==1)
			$this->db->where('keyword_connector.user_id',$connect_to);
		else
			$this->db->where('keyword_connector.project_id',$connect_to);
		$query=$this->db->get('keyword_connector');
		return $query;
	}

	function get_keywords_event($connect_to)
	{
		$this->db->join('keywords','keywords.keyword_id=eventconnector.keyword_id');
		$this->db->where('eventconnector.event_id',$connect_to);
		$query=$this->db->get('eventconnector');
		return $query;
	}


	function get_category($category_for=0)
	{
		if($category_for<>0)
			$this->db->where('category_for',$category_for);
		$this->db->order_by('category_name');
		$query=$this->db->get('category');
		return $query;
	}

	function get_category_by_id($user_id)
	{
		$this->db->join('category','category.category_id=category_connector.category_id');
		$this->db->where('category_connector.user_id',$user_id);
		$query=$this->db->get('category_connector');
		return $query;
	}

	function get_category_single($user_id)
	{
		$this->db->select('category_id');
		$this->db->where('user_id',$user_id);
		$query=$this->db->get('category_connector');
		return $query->row();
	}

	function get_category_by_pid($project_id)
	{
		$this->db->join('category','category.category_id=category_connector.category_id');
		$this->db->where('category_connector.project_id',$project_id);
		$query=$this->db->get('category_connector');
		return $query;
	}

	function add_category($category_id,$use_for,$connect_to)
	{
		if($use_for==1)
			$this->db->set('user_id',$connect_to);
		elseif($use_for==2)
			$this->db->set('project_id',$connect_to);
		$this->db->set('category_id',$category_id);
		$this->db->set('category_connector_type',$use_for);
		$this->db->insert('category_connector');
	}

	

	function get_notification_message($notification_type)
	{
		$notification_message=array(
			1=>'Mendaftar Pada Event',
			2=>'Event Anda Telah Diterbitkan',
			3=>'Menambahkan Komentar Pada Event Anda',
			4=>'Menerbitkan Invoice Untuk Anda',
			5=>'Konfirmasi Pembayaran Telah Diterima',
			6=>'Selamat Datang di HRPlasa'
		);
		return $notification_message[$notification_type];
	}

	function get_stream_message($stream_type)
	{
		$stream_message=array(
			1=>'Menambahkan Event',
			2=>'Join ke HRPlasa'
		);
	}

	function sendmailserver($to,$cc,$subject,$content)
	{
		$config = Array(
		
		'mailtype'  => 'html', 
		'charset'   => 'iso-8859-1'
		);
		$this->load->library('email',$config);
		$this->email->set_newline("\r\n");
		$this->email->from('info@hrplasa.id', 'HRplasa');
		$this->email->to($to); 
		$this->email->cc($cc);
		$this->email->subject($subject);
		$this->email->message($content);	
	
		$result = $this->email->send();
		return $result;
	}

	function sendmailMandrill2($to,$cc,$subject,$content)
	{
		require("./assets/sendgrid/sendgrid-php.php");
		$sendgrid = new SendGrid('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');
		$email = new SendGrid\Email();
		$email
		    ->addTo($to)
		    //->addTo('bar@foo.com') //One of the most notable changes is how `addTo()` behaves. We are now using our Web API parameters instead of the X-SMTPAPI header. What this means is that if you call `addTo()` multiple times for an email, **ONE** email will be sent with each email address visible to everyone.
		    ->setFrom('info@hrplasa.id')
		    ->setFromName('HRPlasa Indonesia')
		    ->addBcc('info@hrplasa.id')
		    ->setSubject($subject)
		    ->setText('Hello World!')
		    ->setHtml($content)
		;

		$sendgrid->send($email);
		/*bye bye mandril 
		require_once('./assets/mandrill/Mandrill.php');
	
		//$mandrill = new Mandrill('2PZQViIzdC-odngBL7_DTw');

		try {
		    $mandrill = new Mandrill('2PZQViIzdC-odngBL7_DTw');
		    $message = array(
		        'html' => $content,
		        'subject' => $subject,
		        'from_email' => 'info@hrplasa.id',
		        'from_name' => 'HRPlasa',
		        'to' => array(
		            array(
		                'email' => $to,
		                'name' => $to,
		                'type' => 'to'
		            )
		        ),
				'bcc_address' => 'info@hrplasa.id',
		        'headers' => array('Reply-To' => 'info@hrplasa.id')

		    );
		    $async = false;
		    $ip_pool = 'Main Pool';
		    $send_at = date('Y-m-d h:i:s');
		    $result = $mandrill->messages->send($message, $async, $ip_pool);
		    
		    
		} catch(Mandrill_Error $e) {
		    // Mandrill errors are thrown as exceptions
		    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		    throw $e;
		}

		return json_encode($result);

		*/
	}

	function sendmailMandrillBulk($to,$cc,$subject,$content)
	{
		require("./assets/sendgrid/sendgrid-php.php");
		$sendgrid = new SendGrid('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');
		$email = new SendGrid\Email();
		foreach($to as $key=>$value)
		{
			$email->addSmtpapiTo($value['email'], $value['name']);
		}
		$email
		    //->addTo('bar@foo.com') //One of the most notable changes is how `addTo()` behaves. We are now using our Web API parameters instead of the X-SMTPAPI header. What this means is that if you call `addTo()` multiple times for an email, **ONE** email will be sent with each email address visible to everyone.
		    ->setFrom('sales@hrplasa.id')
		    ->setFromName('HRPlasa Indonesia')
		    ->addBcc('info@hrplasa.id')
		    ->setSubject($subject)
		    ->setText('Hello World!')
		    ->setHtml($content)
		;

		$sendgrid->send($email);
		/*bye bye mandrill
		require_once('./assets/mandrill/Mandrill.php');
	
		//$mandrill = new Mandrill('2PZQViIzdC-odngBL7_DTw');

		try {
			//$mandrill = new Mandrill('2DR-6pypFE2KtU9UxkM13w'); //this is test key
		    $mandrill = new Mandrill('2PZQViIzdC-odngBL7_DTw');
		    $message = array(
		        'html' => $content,
		        'subject' => $subject,
		        'from_email' => 'info@hrplasa.id',
		        'from_name' => 'HRPlasa',
		        'to' => $to,
				//'bcc_address' => 'info@hrplasa.id',
		        'headers' => array('Reply-To' => 'info@hrplasa.id'),
		        'preserve_recipients'=>FALSE

		    );
		    $async = false;
		    $ip_pool = 'Main Pool';
		    $send_at = date('Y-m-d h:i:s');
		    $result = $mandrill->messages->send($message, $async, $ip_pool);
		    
		    
		} catch(Mandrill_Error $e) {
		    // Mandrill errors are thrown as exceptions
		    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		    throw $e;
		}

		return json_encode($result);
		*/
	}

	function sendmailMandrillBulk2($to,$cc,$subject,$content)
	{
		require("./assets/sendgrid/sendgrid-php.php");
		$sendgrid = new SendGrid('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');
		$email = new SendGrid\Email();
		foreach($to as $key=>$value)
		{
			$email->addSmtpapiTo($value['email'], $value['name']);
		}
		$email
		    //->addTo('bar@foo.com') //One of the most notable changes is how `addTo()` behaves. We are now using our Web API parameters instead of the X-SMTPAPI header. What this means is that if you call `addTo()` multiple times for an email, **ONE** email will be sent with each email address visible to everyone.
		    ->setFrom('sales@hrplasa.id')
		    ->setFromName('HRPlasa Indonesia')
		    ->addBcc('info@hrplasa.id')
		    ->setSubject($subject)
		    ->setText('Hello World!')
		    ->setHtml($content)
		;

		$sendgrid->send($email);
		/*bye bye mandrill 
		require_once('./assets/mandrill/Mandrill.php');
	
		//$mandrill = new Mandrill('2PZQViIzdC-odngBL7_DTw');

		try {
			//$mandrill = new Mandrill('2DR-6pypFE2KtU9UxkM13w'); //this is test key
		    $mandrill = new Mandrill('2PZQViIzdC-odngBL7_DTw');
		    $message = array(
		        'html' => $content,
		        'subject' => $subject,
		        'from_email' => 'sales@hrplasa.id',
		        'from_name' => 'HRPlasa',
		        'to' => $to,
				//'bcc_address' => 'info@hrplasa.id',
		        'headers' => array('Reply-To' => 'info@hrplasa.id'),
		        'preserve_recipients'=>FALSE

		    );
		    $async = false;
		    $ip_pool = 'Main Pool';
		    $send_at = date('Y-m-d h:i:s');
		    $result = $mandrill->messages->send($message, $async, $ip_pool);
		    
		    
		} catch(Mandrill_Error $e) {
		    // Mandrill errors are thrown as exceptions
		    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		    throw $e;
		}

		return json_encode($result);
		*/
	}

	function sendmailMandrill($to,$cc,$subject,$content)
	{
		$config = Array(
		'protocol' => 'smtp',
		'smtp_host' => 'smtp.mandrillapp.com',
		'smtp_port' => 587,
		'smtp_user' => 'info@hrplasa',
		'smtp_pass' => '2PZQViIzdC-odngBL7_DTw',
		'mailtype'  => 'html', 
		'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('info@hrplasa.id', 'HRplasa');
		$this->email->to($to); 
		$this->email->cc($cc);
		$this->email->subject($subject);
		$this->email->message($content);	
	
		$result = $this->email->send();
		return $result;
	}

	function randomizeMailCode($length)
		{
			$random= "";
			$data = "";
			$letters = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));

			foreach ($letters as $key => $value) {
				$data .= $value;
			}
			for($i = 0; $i < $length; $i++)
			{
				$random .= substr($data, (rand()%(strlen($data))), 1);
			}
			return $random;
		}
	function dell_keywords($type='',$id='')
	{
		$this->db->where('keyword_connector_type', $type);
		$this->db->where('user_id', $id);
		$this->db->delete('keyword_connector'); 
	}

}