<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_role_model extends CI_Model {

    protected $table_user = 'user';
    protected $table_userprofile = 'userprofile';
    protected $table_usertype = 'usertype';
    protected $table_posts = 'posts';
    protected $table_events = 'events';
    protected $table_posts_type = 'post_type';

    function get_user_type() {
        $result = $this->db->get($this->table_usertype);
        return $result;
    }

    function get_user_paginate($limit = 0, $offset = 0, $premium_only = false, $partner_only = false, $pending_only = false, $search = '', $usertype_id=0, $order_by_last_login=FALSE,$get_array=array()) {
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=userprofile.city_id');
        $this->db->join('province', 'province.province_id=city.province_id');
        if ($limit <> 0)
            $this->db->limit($limit, $offset);
        if ($premium_only == true) {
            $this->db->where('user_status', 1);
            $this->db->where('is_premium', 1);
        }
        if ($pending_only == true) {
            $this->db->where('user_status', 1);
            $this->db->where('is_premium', 0);
        }

        if ($usertype_id<>0)
        {
            $this->db->where('usertype_id',$usertype_id);
        }

        if ($order_by_last_login==TRUE)
        {
            $this->db->order_by('last_login','DESC');
        }

        if(!empty($get_array))
        {
            if($usertype_id==2)
                $this->db->like('userprofile.userprofile_companyname',$get_array['vendor_keyword']);
            else
                $this->db->like('userprofile.userprofile_firstname',$get_array['expert_keyword']);
            if($get_array['province_id']!=0)
                $this->db->where('province.province_id',$get_array['province_id']);
            if($get_array['category_id']!=0)
                $this->db->where('userprofile.userprofile_function',$get_array['category_id']);
        }

        if (!empty($search)) {
            $this->db->like('user_email', $search);
            $this->db->or_like('user_email', $search);
            $this->db->or_like('userprofile_firstname', $search);
            $this->db->or_like('userprofile_lastname', $search);
            $this->db->or_like('userprofile_aboutme', $search);
            $this->db->or_like('userprofile_aboutme_brief', $search);
            $this->db->or_like('userprofile_phone', $search);
            $this->db->or_like('userprofile_fb', $search);
            $this->db->or_like('userprofile_twitter', $search);
            $this->db->or_like('userprofile_linkedin', $search);
            $this->db->or_like('userprofile_companyname', $search);
            $this->db->or_like('userprofile_position', $search);
        }

        $query = $this->db->get($this->table_user);
        return $query;
    }



    function save_user($post) {
        $this->db->insert($this->table_user, $post);
        return $this->db->insert_id();
    }

    function save_userprofile($post) {
        $this->db->insert($this->table_userprofile, $post);
    }

    function check_mail($where = array()) {
        $this->db->where($where);
        return $this->db->get($this->table_user);
    }

    function update_last_login($user_id) {
        $this->db->set('last_login', time());
        $this->db->where('user_id', $user_id);
        $this->db->update('user');
    }

    function get_user_by_id($user_id, $for_login = false) {
        if ($for_login == true) {
            $this->db->select('user.user_id');
            $this->db->select('userprofile.usertype_id');
            $this->db->select('userprofile.userprofile_firstname');
            $this->db->select('userprofile.userprofile_lastname');
            $this->db->select('userprofile.userprofile_id');
            $this->db->select('userprofile.userprofile_photo');
            $this->db->select('userprofile.userprofile_function');
            $this->db->select('userprofile.userprofile_phone');
            $this->db->select('user.user_status');
            $this->db->select('user.user_email');
            $this->db->select('user.is_premium');
            $this->db->select('userprofile.userprofile_companyname');
            $this->db->select('userprofile.userprofile_position');
            //$this->db->select('category.category_name');
        }
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        //$this->db->join('category','category.category_id=userprofile.userprofile_function');
        $this->db->join('city', 'city.city_id=userprofile.city_id');
        $this->db->join('province', 'province.province_id=city.province_id');
        //remove the usertype join
        //$this->db->join('usertype','usertype.usertype_id=userprofile.usertype_id');
        $this->db->where('user.user_id', $user_id);
        $query = $this->db->get($this->table_user);

        return $query->row();
    }

    function get_user_by_email($email) {
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->where('user.user_email', $email);
        $query = $this->db->get($this->table_user);
        return $query->result_array();
    }

    function get_user_by_kode($kode) {
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->where('user.reset_password', $kode);
        $query = $this->db->get($this->table_user);
        return $query->result_array();
    }

    function get_user_by_event($id) {
        $this->db->select('user.*');
        $this->db->select('userprofile.*');
        $this->db->join('user', "user.user_id={$this->table_events}.event_owner");
        $this->db->join('userprofile', "userprofile.user_id=user.user_id");
        $this->db->where($this->table_events . '.event_id', $id);
        $query = $this->db->get($this->table_events);
        return $query->row_array();
    }

    function check_exist($where = array()) {
        $this->db->where($where);
        $query = $this->db->get($this->table_user);
        return $query->num_rows();
    }

    function get_keywords($search) {
        $this->db->select('keyword_text as name,keyword_id as id');
        $this->db->like('keyword_text', $search);
        return $this->db->get('keywords')->result_array();
    }

    function main_search_project($keyword = '', $location = 0, $budget = 0, $category = 0) {
        if ($keyword != '') {
            $this->db->where('keyword_text', $keyword);
        }
        if ($location != 0) {
            $this->db->where('city.province_id', $location);
        }
        if ($category != 0) {
            $this->db->where('category_connector.category_id', $category);
        }
        if ($budget != 0) {
            $this->db->where('projects.project_budget', $budget);
        }
        $this->db->where('projects.project_status', 1);
        $this->db->where('projects.project_end_date >=', time());
        $this->db->where('projects.is_pending', 1);
        $this->db->join('city', 'projects.city_id=city.city_id', 'LEFT');
        $this->db->join('province', 'province.province_id=city.province_id', 'LEFT');
        $this->db->join('keyword_connector', 'keyword_connector.project_id=projects.project_id', 'LEFT');
        $this->db->join('keywords', 'keywords.keyword_id=keyword_connector.keyword_id', 'LEFT');
        $this->db->join('userprofile', 'userprofile.user_id=projects.user_id', 'LEFT');
        $this->db->join('category_connector', 'projects.project_id=category_connector.project_id', 'LEFT');
        $this->db->join('category', 'category_connector.category_id=category.category_id', 'LEFT');
        $this->db->group_by('projects.project_id');
        return $this->db->get('projects');
    }

    function main_search_user($keyword = '', $location = 0, $category = 0) {
        if ($keyword == '' && $location == 0 && $category == 0) {
            $this->db->where('user.user_id', NULL);
        }
        if ($keyword != '') {
            $this->db->where('keyword_text', $keyword);
        }
        if ($category != 0) {
            $this->db->where('category_connector.category_id', $category);
        }
        if ($location != 0) {
            $this->db->where('city.province_id', $location);
        }
        $this->db->join('userprofile', 'user.user_id=userprofile.user_id', 'LEFT');
        $this->db->join('city', 'userprofile.city_id=city.city_id', 'LEFT');
        $this->db->join('province', 'province.province_id=city.province_id', 'LEFT');
        $this->db->join('keyword_connector', 'keyword_connector.user_id=user.user_id', 'LEFT');
        $this->db->join('keywords', 'keywords.keyword_id=keyword_connector.keyword_id', 'LEFT');
        $this->db->join('category_connector', 'user.user_id=category_connector.user_id', 'LEFT');
        $this->db->join('category', 'category_connector.category_id=category.category_id', 'LEFT');
        $this->db->group_by('user.user_id');
        return $this->db->get('user');
    }

    function main_search_event($keyword = '', $location = 0, $category = 0) {
        if ($keyword == '' && $location == 0 && $category == 0) {
            $this->db->where('events.event_id', NULL);
        }
        if ($keyword != '') {
            $this->db->where('keyword_text', $keyword);
        }
        // if ($category != 0) {
        // 	$this->db->where('events.eventcategory_id',$category);
        // }
        if ($location != 0) {
            $this->db->where('city.province_id', $location);
        }
        $this->db->where('events.is_pending', 1);
        $this->db->join('city', 'events.city_id=city.city_id', 'LEFT');
        $this->db->join('province', 'province.province_id=city.province_id', 'LEFT');
        $this->db->join('eventconnector', 'eventconnector.event_id=events.event_id', 'LEFT');
        $this->db->join('keywords', 'keywords.keyword_id=eventconnector.keyword_id', 'LEFT');
        // $this->db->join('eventcategory','events.eventcategory_id=eventcategory.eventcategory_id','LEFT');
        $this->db->join('user', 'user.user_id=events.event_owner', 'LEFT');
        $this->db->join('userprofile', 'user.user_id=userprofile.user_id', 'LEFT');
        $this->db->join('usertype', 'userprofile.usertype_id=userprofile.usertype_id', 'LEFT');
        $this->db->group_by('events.event_id');
        return $this->db->get('events');
    }

    function get_users($limit = 0) {
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=userprofile.city_id');
        $this->db->join('province', 'province.province_id=city.province_id');
        if ($limit <> 0)
            $this->db->limit($limit);
        $query = $this->db->get($this->table_user);
        return $query;
    }

    function get_user_complete($vendor = false) {
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->join('city', 'city.city_id=userprofile.city_id');
        $this->db->join('province', 'province.province_id=city.province_id');
        $this->db->limit(4);
        if ($vendor == true)
            $this->db->where('userprofile.usertype_id', 2);
        $query = $this->db->get('user');
        return $query;
    }

    function save_post($post) {
        $this->db->insert($this->table_posts, $post);
        return $this->db->insert_id();
    }

    function delete_post($post_id) {
        $this->db->where('post_id', $post_id);
        $this->db->delete('posts');
    }

    function get_all_post_type() {
        $query = $this->db->get($this->table_posts_type);
        return $query;
    }

    function get_event($limit = 0, $active_only = false) {
        if ($active_only == true)
            $this->db->where('events.is_pending', 1);
        if ($limit <> 0)
            $this->db->limit($limit);
        $this->db->join('user', 'user.user_id=events.event_owner');
        $this->db->join('userprofile', 'userprofile.user_id=user.user_id');
        $this->db->order_by('events.event_id', 'DESC');
        $query = $this->db->get('events');
        return $query;
    }

    function update_user($set_array = array(), $id = '') {
        $this->db->set($set_array);
        $this->db->where('user_id', $id);
        $this->db->update('user');
    }

    function search_by_keyword($keyword = '', $usertype_id = 0) {
        $this->db->join('keywords', 'keywords.keyword_id=keyword_connector.keyword_id');
        $this->db->join('user', 'user.user_id=keyword_connector.user_id');
        $this->db->join('userprofile', 'userprofile.user_id=keyword_connector.user_id');
        $this->db->like('keywords.keyword_text', $keyword);
        if ($usertype_id > 0) {
            $this->db->where('userprofile.usertype_id', $usertype_id);
        }
        $query = $this->db->get('keyword_connector');
        return $query;
    }

}

/* End of file user_role_model.php */
/* Location: ./application/models/user_role_model.php */