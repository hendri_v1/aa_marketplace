<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Job_model extends CI_Model {


	function get_my_job($limit=0, $offset=0, $where_array=array())
	{
		$this->db->join('job_categories','job_categories.job_category_id=jobs.job_category_id');
		$this->db->where($where_array);
		$this->db->where('job_poster',$this->session->userdata('user_meta')->user_id);
		if($limit>0)
			$this->db->limit($limit,$offset);
		$query=$this->db->get('jobs');
		return $query;
	}

	function add_new($set_array=array())
	{
		$this->db->set($set_array);
		$this->db->insert('jobs');
		return $this->db->insert_id();
	}

	function get_categories()
	{
		$this->db->order_by('job_category_name');
		$query = $this->db->get('job_categories');
		return $query;
	}

}